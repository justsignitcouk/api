<?php

namespace App\Transformers;

use MyCompose\Model\Document\Documents;
use PhalconRest\Transformers\ModelTransformer;

class DocumentsTransformer extends ModelTransformer
{
    protected $modelClass = Documents::class;

    protected function excludedProperties()
    {
        return [];
    }
}