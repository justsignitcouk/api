<?php

namespace App\Transformers;

use MyCompose\Model\Departments;
use PhalconRest\Transformers\ModelTransformer;

class DepartmentsTransformer extends ModelTransformer
{
    protected $modelClass = Departments::class;

    protected function excludedProperties()
    {
        return [];
    }
}