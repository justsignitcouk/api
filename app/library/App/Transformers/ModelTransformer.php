<?php

namespace MyCompose\Transformers;

use PhalconRest\Transformers\ModelTransformer as ModelTransformerCore;
use Phalcon\Db\Column;

class ModelTransformer extends ModelTransformerCore
{

    protected function getFieldValue($item, $propertyName, $fieldName)
    {
        $dataType = array_key_exists($propertyName,
            $this->getModelDataTypes()) ? $this->getModelDataTypes()[$propertyName] : null;

        $model = $this->getModel($item);

        $value = isset($model->$propertyName) ? $model->$propertyName : null;

        if ($value === null) {
            return null;
        }

        $typedValue = $value;

        switch ($dataType) {

            case self::TYPE_INTEGER: {

                $typedValue = $this->formatHelper->int($value);
                break;
            }

            case self::TYPE_FLOAT: {

                $typedValue = $this->formatHelper->float($value);
                break;
            }

            case self::TYPE_DOUBLE: {

                $typedValue = $this->formatHelper->double($value);
                break;
            }

            case self::TYPE_BOOLEAN: {

                $typedValue = $this->formatHelper->bool($value);
                break;
            }

            case self::TYPE_STRING: {

                $typedValue = (string)$value;
                break;
            }

            case self::TYPE_DATE: {

                $typedValue = $this->formatHelper->date($value);
                break;
            }

            case self::TYPE_JSON: {

                $typedValue = json_decode($value);
                break;
            }
        }

        return $typedValue;
    }

}