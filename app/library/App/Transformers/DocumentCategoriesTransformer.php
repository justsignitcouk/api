<?php

namespace App\Transformers;

use MyCompose\Model\Document\DocumentCategories;
use PhalconRest\Transformers\ModelTransformer;

class DocumentCategoriesTransformer extends ModelTransformer
{
    protected $modelClass = DocumentCategories::class;

    protected function excludedProperties()
    {
        return [];
    }
}