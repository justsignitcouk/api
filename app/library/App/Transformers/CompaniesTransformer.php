<?php

namespace App\Transformers;

use MyCompose\Model\Users;
use PhalconRest\Transformers\ModelTransformer;

class CompaniesTransformer extends ModelTransformer
{
    protected $modelClass = Users::class;

    protected function excludedProperties()
    {
        return [];
    }
}