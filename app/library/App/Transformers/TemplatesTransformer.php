<?php

namespace App\Transformers;

use MyCompose\Model\Templates;
use PhalconRest\Transformers\ModelTransformer;

class TemplatesTransformer extends ModelTransformer
{
    protected $modelClass = Templates::class;

    protected function excludedProperties()
    {
        return [];
    }
}