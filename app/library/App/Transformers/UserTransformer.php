<?php

namespace App\Transformers;

use MyCompose\Model\Users;
use MyCompose\Transformers\ModelTransformer;

class UserTransformer extends ModelTransformer
{
    protected $modelClass = Users::class;

    protected function excludedProperties()
    {
        return ['password'];
    }
}