<?php

namespace App\Transformers;

use MyCompose\Model\Actions;
use PhalconRest\Transformers\ModelTransformer;

class ActionsTransformer extends ModelTransformer
{
    protected $modelClass = Actions::class;

    protected function excludedProperties()
    {
        return [];
    }
}