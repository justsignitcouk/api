<?php

namespace App\Transformers;

use MyCompose\Model\Workflow;
use PhalconRest\Transformers\ModelTransformer;

class WorkFlowTransformer extends ModelTransformer
{
    protected $modelClass = Workflow::class;

    protected function excludedProperties()
    {
        return [];
    }
}