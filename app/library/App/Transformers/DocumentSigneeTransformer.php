<?php

namespace App\Transformers;

use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\Documentsignee;
use PhalconRest\Transformers\ModelTransformer;

class DocumentSigneeTransformer extends ModelTransformer
{
    protected $modelClass = DocumentSignee::class;

    protected function excludedProperties()
    {
        return [];
    }
}