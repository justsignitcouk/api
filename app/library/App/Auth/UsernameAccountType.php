<?php
namespace App\Auth;

use App\Constants\Services;
use Phalcon\Di;

class UsernameAccountType implements \PhalconApi\Auth\AccountType
{
    const NAME = "email";
    const MOBILE = "mobile";
    const ID = "id";
    public $AccountType = 'email' ;
    public function login($data)
    {
        /** @var \Phalcon\Security $security */
        $security = Di::getDefault()->get(Services::SECURITY);

        $username = $data[Manager::LOGIN_DATA_USERNAME];
        $password = $data[Manager::LOGIN_DATA_PASSWORD];

        /** @var \App\Model\User $user */
        $user = \MyCompose\Model\Users::findFirst([
            'conditions' => $this->AccountType .' = :email:',
            'bind' => ['email' => $username]
        ]);

        if (!$user) {
            return null;
        }

        if (!$security->checkHash($password, $user->password)) {
            return null;
        }

        return (string)$user->id;
    }

    public function login_social($data)
    {


        /** @var \Phalcon\Security $security */
        $security = Di::getDefault()->get(Services::SECURITY);

        //$username = $data[Manager::LOGIN_DATA_USERNAME];
        //$password = $data[Manager::LOGIN_DATA_PASSWORD];

        /** @var \App\Model\User $user */
        $user = \MyCompose\Model\Users::findFirst([
            'conditions' => ' id = :id:',
            'bind' => ['id' => $data]
        ]);

        if (!$user) {
            return null;
        }

        /*if (!$security->checkHash($password, $user->password)) {
            return null;
        }*/

        return (string)$user->id;
    }

    public function authenticate($identity)
    {
        return \MyCompose\Model\Users::count([
            'conditions' => 'id = :id:',
            'bind' => ['id' => (int)$identity]
        ]) > 0;
    }
}
