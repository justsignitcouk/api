<?php

namespace App\Auth;


use MyCompose\Model\Access\Permissions;
use PhalconRest\Exception;

class Access extends \App\Components\Auth\Manager
{

    public static function can($aParams){
        $di = \Phalcon\DI::getDefault();
        $userService = $di->get("userService");

        if (!$userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $userService->getIdentity();

        $sKey = $aParams['key'] ;
        $oPermissions = Permissions::findFirst("key='" . $sKey . "'") ;
        if(!$oPermissions){
            return false;
        }

        $nPermissionID = $oPermissions->id ;


        $checkRole = false ;
        $oPermissionRoles = PermissionRole::find("permission_id=" . $nPermissionID) ;
        if($oPermissionRoles->count() > 0) {
            $checkRole = true ;
        }

        if(!empty($nPlanID)){
            $checkPlanPermission = PlanPermission::findFirst("plan_id=" . $nPlanID . " and permission_id=" . $nPermissionID ) ;
            if($checkPlanPermission) {
                return true;
            }

            if( $checkRole ){
                foreach($oPermissionRoles as $oPermissionRole){
                    $nRoleID = $oPermissionRole->role_id ;
                    $oPlanRole = PlanRole::findFirst("plan_id=" . $nPlanID . " role_id=" . $nRoleID ) ;
                    if($oPlanRole){
                        return true;
                    }
                }

            }


            $getPlanFeatures = PlanFeature::find("plan_id='" . $nPlanID . "'" );
            if($getPlanFeatures->count()>0){
                foreach($getPlanFeatures as $oPlanFeature) {
                    $nFeatureID = $oPlanFeature->feature_id ;
                    $checkAccess = FeaturePermission::findFirst("permission_id=" . $nPermissionID . " and feature_id=" . $nFeatureID );
                    if($checkAccess){
                        return true;
                    }
                }
            }
        }

        return false;

    }

}