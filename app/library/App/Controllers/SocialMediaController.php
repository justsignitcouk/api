<?php

namespace App\Controllers;

use App\Constants\ErrorCodes;
use MyCompose\LaravelModels\Contacts\Names;
use MyCompose\Model\Contacts\Avatar;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\Social;
use MyCompose\Model\Contacts\UserContacts;
use MyCompose\Model\Devices;
use MyCompose\Model\Files;
use MyCompose\Model\Profiles\UserProfileRequirements;
use MyCompose\Model\SocialMedia\Facebook;
use MyCompose\Model\Users;
use PhalconRest\Mvc\Controllers\CollectionController;

use MyCompose\LaravelModels\SocialMedia\Google;
use MyCompose\LaravelModels\SocialMedia\LinkedIn;
use MyCompose\LaravelModels\User;
use MyCompose\LaravelModels\Account;
use  App\Components\Auth\Manager ;
use App\Components\File\File;
use PhalconApi\Exception;


class SocialMediaController extends CollectionController
{


    public function check_mail()
    {


        $email = $this->request->getPost('email');
        if(empty($email)){
            throw new Exception(ErrorCodes::DATA_FAILED, 'Email empty');
        }

        $oUser = Users::findFirst("email='" . $email . "'") ;
        if(!$oUser){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'Email not register');
        }

        return $this->createArrayResponse(['oUser' => $oUser, 'status'=>'success' ], 'data');

    }

    public function regist_by_facebook(){
        $aUserbData = $this->request->getPost('user_data') ;
        if(!empty($aUserbData['email'])){
            $sEmail = $aUserbData['email'] ;
            $oUser = Users::findFirst("email='" . $sEmail . "'") ;
            if($oUser){
                echo "this user is exist";
                exit();
            }
        }

        $dCreatedAt = date('Y-m-d H:i:s') ;
        $name = isset($aUserbData['name']) ? $aUserbData['name'] : '' ;
        $aName = $this->getArrayNameFromFullName($name) ;
        $sUserName = Users::getUsername($aName['first_name'],$aName['last_name']) ;
        $oUser = new Users() ;
        $oUser->email = isset($aUserbData['email']) ? $aUserbData['email'] : null ;
        $oUser->mobile = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;
        $oUser->facebook_id = $aUserbData['id'];
        $oUser->full_name = $name;
        $oUser->first_name = $aName['first_name'];
        $oUser->middle_name = $aName['middle_name'];
        $oUser->last_name = $aName['last_name'];
        $oUser->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;
        $oUser->activated = 1 ;
        $oUser->username = $sUserName ;
        $oUser->register_by = 'facebook';
        $oUser->created_at = $dCreatedAt;

        $nFileID= null;
        if(isset($aUserbData['avatar_original'])){
            $url = $aUserbData['avatar_original'];
            $nFileID = File::putFromUrl($url,'facebook_avatar',$sUserName);
            $oUser->avatar = $nFileID ;

        }

        if(!$oUser->save()){
            dd($oUser->getMessages());
        }

        $nUserID = $oUser->id ;

        if(!is_null($nFileID)) {
            $oUserAvatar = new UserAvatar();
            $oUserAvatar->user_id = $nUserID ;
            $oUserAvatar->file_id = $nFileID ;
            $oUserAvatar->created_at = date('Y-m-d H:i:s') ;
            $oUserAvatar->save() ;
        }

        $oFacebook = new Facebook() ;
        $oFacebook->name = $name;
        $oFacebook->facebook_id = $aUserbData['id'];
        $oFacebook->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;

        $oFacebook->avatar = $nFileID ;
        $oFacebook->nickname = isset($aUserbData['nickname']) ? $aUserbData['nickname'] : null ;
        $oFacebook->created_at = $dCreatedAt ;
        $oFacebook->user_id = $nUserID;
        $oFacebook->link = isset($aUserbData['profileUrl']) ? $aUserbData['profileUrl'] : null ;
        $oFacebook->phone = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;

        if(!$oFacebook->save()){
            dd($oFacebook->getMessages());
        }
        $oCheckVerify = UserProfileRequirements::findFirst("user_id=" . $nUserID . " and profile_requirements_id=2") ;
        if(!$oCheckVerify){
            $oUserProfileRequirements= new UserProfileRequirements();
            $oUserProfileRequirements->profile_requirements_id = 2 ;
            $oUserProfileRequirements->user_id = $nUserID ;
            $oUserProfileRequirements->created_at = $dCreatedAt ;
            $oUserProfileRequirements->status = 'complete' ;
            $oUserProfileRequirements->save();
        }

        $aResponse = [ 'status' => true ];

        return $this->createArrayResponse($aResponse, 'data');


    }




    public function log_in_by_facebook(){

        $aUserbData = $this->request->getPost('user_data') ;
        $dCreatedAt= date('Y-m-d H:i:s');
        $manager = new Manager ;

        $oUser = Users::findFirst("email='" . $aUserbData['email'] . "'") ;
        $nUserID = $oUser->id;
        $oFacebook = Facebook::findFirst("user_id=" . $oUser->id ) ;
        if(!$oFacebook){
            $oFacebook = new Facebook() ;
            $oFacebook->created_at = $dCreatedAt ;
        }
        $name = isset($aUserbData['name']) ? $aUserbData['name'] : '' ;
        $oFacebook->name = $name;
        $oFacebook->facebook_id = $aUserbData['id'];
        $oFacebook->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;
        $nFileID= null;
        if(isset($aUserbData['avatar_original'])){
            $url = $aUserbData['avatar_original'];
            $nFileID = File::putFromUrl($url,'facebook_avatar',$name);
        }
        $oFacebook->avatar = $nFileID ;
        $oFacebook->nickname = isset($aUserbData['nickname']) ? $aUserbData['nickname'] : null ;
        $oFacebook->updated_at = $dCreatedAt ;
        $oFacebook->user_id = $nUserID;
        $oFacebook->link = isset($aUserbData['profileUrl']) ? $aUserbData['profileUrl'] : null ;
        $oFacebook->phone = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;

        if(!$oFacebook->save()){
            dd($oFacebook->getMessages());
        }
        $oUserProfileRequirements= new UserProfileRequirements();
        $oUserProfileRequirements->profile_requirements_id = 2 ;
        $oUserProfileRequirements->user_id = $nUserID ;
        $oUserProfileRequirements->created_at = $dCreatedAt ;
        $oUserProfileRequirements->status = 'complete' ;
        $oUserProfileRequirements->save();
        $aResponse = [ 'status' => true ];

        // to do update facebook if exist





        //store new data
        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';

        $aParams = [
            'sIP' => $sIP,
            'sFCMToken' => $sFCMToken,
            'sBrowser' => $sBrowser,
            'sBrowserVersion' => $sBrowserVersion,
            'sPlatform' => $sPlatform,
            'sPlatformVersion' => $sPlatformVersion,
            'sDevice' => $sDevice,
            'sLanguages' => $sLanguages,

        ] ;
        /*Login the user*/
        $session = $manager->loginWithSocialMedia(\App\Auth\UsernameAccountType::ID,['user_id'=> $oUser->id ]);
        $nUserId = $session->getIdentity();
        $response = Manager::getLoginInfo($nUserId,$aParams,$session);
        //$response = []
        return $this->createArrayResponse($response, 'data');


    }

    /**
     * @return array
     */
    public function regist_by_google(){



        $aUserbData = $this->request->getPost('user_data') ;
        if(!empty($aUserbData['email'])){
            $sEmail = $aUserbData['email'] ;
            $oUser = Users::findFirst("email='" . $sEmail . "'") ;
            if($oUser){
                echo "this user is exist";
                exit();
            }
        }

        $dCreatedAt = date('Y-m-d H:i:s') ;
        $name = isset($aUserbData['name']) ? $aUserbData['name'] : '' ;
        $aName = $this->getArrayNameFromFullName($name) ;
        $sUserName = Users::getUsername($aName['first_name'],$aName['last_name']) ;
        $oUser = new Users() ;
        $oUser->email = isset($aUserbData['email']) ? $aUserbData['email'] : null ;
        $oUser->mobile = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;
        $oUser->google_id = $aUserbData['id'];
        $oUser->full_name = $name;
        $oUser->first_name = $aName['first_name'];
        $oUser->middle_name = $aName['middle_name'];
        $oUser->last_name = $aName['last_name'];
        $oUser->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;
        $oUser->activated = 1 ;
        $oUser->username = $sUserName ;
        $oUser->register_by = 'google';
        $oUser->created_at = $dCreatedAt;

        $nFileID= null;
        if(isset($aUserbData['avatar_original'])){
            $url = $aUserbData['avatar_original'];
            $nFileID = File::putFromUrl($url,'facebook_avatar',$sUserName);
            $oUser->avatar = $nFileID ;
        }

        if(!$oUser->save()){
            dd($oUser->getMessages());
        }

        $nUserID = $oUser->id ;

        if(!is_null($nFileID)) {
            $oUserAvatar = new UserAvatar();
            $oUserAvatar->user_id = $nUserID ;
            $oUserAvatar->file_id = $nFileID ;
            $oUserAvatar->created_at = date('Y-m-d H:i:s') ;
            $oUserAvatar->save() ;
        }

        $oGoogle = new Google() ;
        $oGoogle->name = $name;
        $oGoogle->google_id = $aUserbData['id'];
        $oGoogle->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;

        $oGoogle->avatar = $nFileID ;
        $oGoogle->nickname = isset($aUserbData['nickname']) ? $aUserbData['nickname'] : null ;
        $oGoogle->created_at = $dCreatedAt ;
        $oGoogle->user_id = $nUserID;
        $oGoogle->link = isset($aUserbData['profileUrl']) ? $aUserbData['profileUrl'] : null ;
        $oGoogle->phone = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;
        $oGoogle->language = isset($aUserbData['language']) ? $aUserbData['language'] : null ;

        if(!$oGoogle->save()){
            dd($oGoogle->getMessages());
        }

        $oCheckVerify = UserProfileRequirements::findFirst("user_id=" . $nUserID . " and profile_requirements_id=2") ;
        if(!$oCheckVerify){
            $oUserProfileRequirements= new UserProfileRequirements();
            $oUserProfileRequirements->profile_requirements_id = 2 ;
            $oUserProfileRequirements->user_id = $nUserID ;
            $oUserProfileRequirements->created_at = $dCreatedAt ;
            $oUserProfileRequirements->status = 'complete' ;
            $oUserProfileRequirements->save();
        }

        $aResponse = [ 'status' => true ];

        return $this->createArrayResponse($aResponse, 'data');



    }

    public function log_in_by_google(){


        $aUserbData = $this->request->getPost('user_data') ;
        $dCreatedAt = date('Y-m-d H:i:s') ;
        $name = isset($aUserbData['name']) ? $aUserbData['name'] : '' ;

        $manager = new Manager ;

        /*update user data*/
        $oUser = Users::findFirst("email='" . $aUserbData['email'] . "'") ;
        if(!$oUser){
            return false;
        }


        $nUserID = $oUser->id ;
        $oGoogle = \MyCompose\Model\SocialMedia\Google::findFirst("user_id=" . $nUserID );
        if(!$oGoogle){
            $oGoogle = new Google() ;
            $oGoogle->created_at = $dCreatedAt ;

        }

        $oGoogle->name = $name;
        $oGoogle->google_id = $aUserbData['id'];
        $oGoogle->gender = isset($aUserbData['gender']) ? $aUserbData['gender'] : null ;
        $nFileID= null;
        if(isset($aUserbData['avatar_original'])){
            $url = $aUserbData['avatar_original'];
            $nFileID = File::putFromUrl($url,'facebook_avatar',$name);
            $oUser->avatar = $nFileID ;
        }
        $oGoogle->avatar = $nFileID ;
        $oGoogle->nickname = isset($aUserbData['nickname']) ? $aUserbData['nickname'] : null ;
        $oGoogle->updated_at = $dCreatedAt ;
        $oGoogle->user_id = $nUserID;
        $oGoogle->link = isset($aUserbData['profileUrl']) ? $aUserbData['profileUrl'] : null ;
        $oGoogle->phone = isset($aUserbData['phone']) ? $aUserbData['phone'] : null ;
        $oGoogle->language = isset($aUserbData['language']) ? $aUserbData['language'] : null ;
        if(!is_null($nFileID)) {
            $oUserAvatar = new UserAvatar();
            $oUserAvatar->user_id = $nUserID ;
            $oUserAvatar->file_id = $nFileID ;
            $oUserAvatar->created_at = date('Y-m-d H:i:s') ;
            $oUserAvatar->save() ;
        }
        if(!$oGoogle->save()){
            dd($oGoogle->getMessages());
        }

        $oCheckVerify = UserProfileRequirements::findFirst("user_id=" . $nUserID . " and profile_requirements_id=2") ;
        if(!$oCheckVerify){
            $oUserProfileRequirements= new UserProfileRequirements();
            $oUserProfileRequirements->profile_requirements_id = 2 ;
            $oUserProfileRequirements->user_id = $nUserID ;
            $oUserProfileRequirements->created_at = $dCreatedAt ;
            $oUserProfileRequirements->status = 'complete' ;
            $oUserProfileRequirements->save();
        }




        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';

        $aParams = [
            'sIP' => $sIP,
            'sFCMToken' => $sFCMToken,
            'sBrowser' => $sBrowser,
            'sBrowserVersion' => $sBrowserVersion,
            'sPlatform' => $sPlatform,
            'sPlatformVersion' => $sPlatformVersion,
            'sDevice' => $sDevice,
            'sLanguages' => $sLanguages,

        ] ;

        /*Login the user*/
        $session = $manager->loginWithSocialMedia(\App\Auth\UsernameAccountType::NAME,['user_id'=>$oUser->id]);
        $nUserId = $session->getIdentity();
        $response = Manager::getLoginInfo($nUserId,$aParams,$session);

        return $this->createArrayResponse($response, 'data');


    }


    public function regist_by_linkedin(){

        $user = new User ;
        $socialMedia= new LinkedIn ;

        /*define variables*/
        $aUserbData = $this->request->getPost('user_data');
        $name = explode(" ", $aUserbData['name']);
        $password = $this->security->hash(rand());

        /*store new user*/
        !empty($aUserbData['email']) ? $user->email = $aUserbData['email'] : '' ;
        !empty($name[0])  ? $user->first_name = $name[0] : '' ;
        !empty($name[1])  ? $user->last_name  = $name[1] : '' ;
        $user->activated  = 1 ;
        $user->password   = $password ;
        //$user->profile_image = File::put(Phalcon\Http\Request\File(array($aUserbData['avatar'])));
        if(!$user->save()) {
            $messages = $user->getMessages();

            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];
            return $response;
        }

        /*create user default account*/
        $this->createDefaultAccount($aUserbData['name'] , $user->id) ;


        /*store user social media*/
        !empty($aUserbData['id'])  ? $socialMedia->account_id  = $aUserbData['id'] : '' ;
        !empty($aUserbData['name'])  ? $socialMedia->name      = $aUserbData['name'] : '' ;
        !empty($aUserbData['user']['headline']) ? $socialMedia->headline = $aUserbData['user']['headline'] : '' ;
        !empty($aUserbData['avatar']) ? $socialMedia->avatar     = $aUserbData['avatar'] : '' ;
        !empty($aUserbData['nickname'])  ? $socialMedia->nickname  = $aUserbData['nickname'] : '' ;
        !empty($aUserbData['user']['industry']) ? $socialMedia->industry = $aUserbData['user']['industry'] : '' ;
        !empty($aUserbData['user']['location']['name'])  ? $socialMedia->country  = $aUserbData['user']['location']['name'] : '' ;
        $socialMedia->created_at  = date('Y-m-d H:i:s') ;
        $socialMedia->user_id  = $user->id ;


        if(!$socialMedia->save()) {
            $messages = $socialMedia->getMessages();

            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];

            return $response;
        }


        $regist_response = array('status' => true);
        return $this->createArrayResponse($regist_response, 'data');

    }

    public function log_in_by_linkedin(){


        $aUserbData = $this->request->getPost('user_data') ;
        $name = explode(" ", $aUserbData['name']);

        $manager = new Manager ;

        /*update user data*/
        $user = User::where('email',$aUserbData['email'])->first();
        $check_linkedin = LinkedIn::select()->where('user_id',$user->id)->get()->count();


        if($check_linkedin > 0){


            LinkedIn::where('user_id',$user->id)->update([

                'account_id'=>!empty($aUserbData['id']) ? $aUserbData['id'] : '',
                'name'      =>!empty($aUserbData['name']) ? $aUserbData['name'] : '',
                'headline'  =>!empty($aUserbData['user']['headline']) ? $aUserbData['user']['headline'] : '',
                'industry'  =>!empty($aUserbData['user']['industry']) ? $aUserbData['user']['industry'] : '',
                'country'   =>!empty($aUserbData['user']['location']['name']) ? $aUserbData['user']['location']['name']: '',
                'nickname'  =>!empty($aUserbData['name']) ? $aUserbData['name'] : '',
                'avatar'    =>!empty($aUserbData['name']) ? $aUserbData['name'] : '',
                'updated_at'=> date('Y-m-d H:i:s'),

            ]);

        }else{


            $socialMedia= new LinkedIn ;
            !empty($aUserbData['id'])  ? $socialMedia->account_id  = $aUserbData['id'] : '' ;
            !empty($aUserbData['name'])  ? $socialMedia->name      = $aUserbData['name'] : '' ;
            !empty($aUserbData['user']['headline']) ? $socialMedia->headline = $aUserbData['user']['headline'] : '' ;
            !empty($aUserbData['avatar']) ? $socialMedia->avatar     = $aUserbData['avatar'] : '' ;
            !empty($aUserbData['nickname'])  ? $socialMedia->nickname  = $aUserbData['nickname'] : '' ;
            !empty($aUserbData['user']['industry']) ? $socialMedia->industry = $aUserbData['user']['industry'] : '' ;
            !empty($aUserbData['user']['location']['name'])  ? $socialMedia->country  = $aUserbData['user']['location']['name'] : '' ;
            $socialMedia->created_at  = date('Y-m-d H:i:s') ;
            $socialMedia->user_id  = $user->id ;


            if(!$socialMedia->save()) {
                $messages = $socialMedia->getMessages();

                $errorMsg = '';
                foreach ($messages as $message) {
                    $errorMsg .= "{$message} <br>";
                }
                $response = ['messages' => $errorMsg];

                return $response;
            }



        }

        User::where('id',$user->id)->update([

            'first_name'=>$name[0],
            'last_name'=> $name[1] ,

        ]);


        /*Login the user*/
        $session = $manager->loginWithSocialMedia(\App\Auth\UsernameAccountType::NAME,['user_id'=>$user->id]);

        $transformer = new \App\Transformers\UserTransformer;
        $transformer->setModelClass('MyCompose\Model\Users');
        $nUserId = $session->getIdentity();
        $oUser = \MyCompose\Model\Users::findFirst($session->getIdentity());

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED, 'User Not Found');
        }

        $nCurrentAccount = $oUser->current_account ;
        if(is_null($nCurrentAccount)){
            $oAccount = Accounts::findFirst('user_id='.$nUserId . ' and is_default=1') ;
            if(!$oAccount){
                $oAccount = Accounts::findFirst('user_id='. $nUserId ) ;
            }
            $nCurrentAccount = $oAccount->id ;
        }

        $oAccount = Account::where('id',$nCurrentAccount)->with('ProfileImage','Department.Branch.Company','User')->get()->first() ;

        $user = $this->createItemResponse($oUser, $transformer);
        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';

        $oDevices = Devices::findFirst("token ='" . $session->getToken() . "'");
        if (!$oDevices) {
            $oDevices = new Devices();
            $oDevices->user_id = $session->getIdentity();
            $oDevices->ip = $sIP;
            $oDevices->token = $session->getToken();
            $oDevices->expires = $session->getExpirationTime();
            $oDevices->fcm_token = $sFCMToken;
            $oDevices->browser = $sBrowser;
            $oDevices->browser_version = $sBrowserVersion;
            $oDevices->platform = $sPlatform;
            $oDevices->platform_version = $sPlatformVersion;
            $oDevices->device = $sDevice;
            $oDevices->languages = $sLanguages;
            $oDevices->created_at = date('Y-m-d H:i:s');
        }

        $oDevices->last_login = date('Y-m-d H:i:s');
        $oDevices->ip = $sIP;

        if (!$oDevices->save()) {
            return $oDevices->getMessages();
        }


        $response = [
            'token' => $session->getToken(),
            'expires' => $session->getExpirationTime(),
            'user' => $user,
            'default_account' => $oAccount
        ];


        return $this->createArrayResponse($response, 'data');



    }


    private function createDefaultAccount($acoountName , $userID){

        $oAccount = new Account ;
        $oAccount->account_name = $acoountName;
        $oAccount->verify = 1;
        $oAccount->active = 1;
        $oAccount->user_id = $userID;
        $oAccount->company_id = 1;
        $oAccount->is_default = 1;
        $oAccount->created_at = date('Y-m-d');

        if (!$oAccount->save()) {

            $messages = $oAccount->getMessages();

            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];

            return $response;

        }

    }


    private function getArrayNameFromFullName($name){
        $aName = explode(" ",$name) ;
        $first_name = '' ;
        $middle_name = '' ;
        $last_name = '' ;
        if(sizeof($aName)==3){
            $first_name = $aName[0] ;
            $middle_name = $aName[1] ;
            $last_name = $aName[2] ;
        }elseif(sizeof($aName)==2){
            $first_name = $aName[0] ;
            $middle_name = '' ;
            $last_name = $aName[1] ;
        }elseif(sizeof($aName)==1){
            $first_name = $aName[0] ;
            $middle_name = '' ;
            $last_name = '' ;
        }

        return [ 'first_name' => $first_name,'middle_name'=>$middle_name , 'last_name' => $last_name ] ;
    }


}