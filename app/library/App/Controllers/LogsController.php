<?php

namespace App\Controllers;


use MyCompose\Model\CompileLogs;
use PhalconRest\Mvc\Controllers\CollectionController;

class LogsController extends CollectionController
{

    public function all(){
        $aLogs = array('response_code' => 1, 'response_message' => "1");
        return $this->createArrayResponse($aLogs, 'data');
    }


    public function save()
    {

        $nUserId = $this->userService->getIdentity();
        $sData = $this->request->get("data") ;

        $sIP = $this->request->get("ip") ;
        $sConfigData = $this->request->get("config_data") ;
        $created_at = date('Y-m-d H:i:s') ;

        $oLogs = new CompileLogs() ;
        $oLogs->data = $sData ;
        $oLogs->ip = $sIP;
        $oLogs->config_data = $sConfigData;
        $oLogs->created_at = $created_at;
        $oLogs->save() ;
        $aLogs = array('response_code' => 1, 'response_message' => "1");
        return $this->createArrayResponse($aLogs, 'data');

    }
}
