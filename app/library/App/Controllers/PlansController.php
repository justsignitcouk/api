<?php

namespace App\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MyCompose\Model\Business\Companies;
use MyCompose\Model\Plans\Plan;

use MyCompose\Model\Users;
use MyCompose\Model\Workflow\Workflow;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class PlansController extends CrudResourceController
{


    public function all(){
        $aPlans = array('plans' => array() ,'response_code' => 1 ,'response_message' => "success" );

        $oPlans = Plan::find("activated=1");
        $aPlans = $oPlans->toArray();
        foreach($oPlans as $k => $oPlan) {

            if($oPlan->PlanFeature){
                $aPlans[$k]['plans_features'] = $oPlan->PlanFeature->toArray();
                $oPlanFeatures = $oPlan->PlanFeature;
                $aPlanFeatures = $oPlan->PlanFeature->toArray();
                foreach($oPlanFeatures as $k2 => $oPlanFeature){
                    $aPlans[$k]['plans_features'][$k2]['feature'] = $oPlanFeature->Feature;
                }
            }
        }

        $aResponse = array('plans' => $aPlans ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aPlans,'data');
    }

    public function getPlanById($nPlanID){

        if( empty($nPlanID) || !is_numeric($nPlanID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('plan_id' => $nPlanID));
        }

        $oPlan = Plan::findFirstById($nPlanID) ;
        $aPlan = $oPlan->toArray();
        if($oPlan->PlanFeature){
            $aPlan['plans_features'] = $oPlan->PlanFeature->toArray();
            $oPlanFeatures = $oPlan->PlanFeature;
            $aPlanFeatures = $oPlan->PlanFeature->toArray();
            foreach($oPlanFeatures as $k2 => $oPlanFeature){
                $aPlan['plans_features'][$k2]['feature'] = $oPlanFeature->Feature;
            }
        }

        $aResponse = array('plan' => $aPlan ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }

    public function getFeatturesByUserId($nUserID){
        $oUser = Users::findFirstById($nUserID);
        if(!$oUser){
            return false;
        }

        $oPlans = $oUser->Plans;

        $aPlans = [] ;

        $oCompanies = Companies::find("user_id=" . $nUserID) ;
        $nWorkFlows = 0 ;
        $nAccounts = 0 ;
        if($oCompanies->count()>0){
            foreach($oCompanies as $oCompany) {
                $oWorkflow = Workflow::find(" company_id=" . $oCompany->id );
                $nWorkFlows += $oWorkflow->count();


            }

        }

        $aUsage = [ 'number_of_companies' =>  $oCompanies->count(),'number_of_workflows' => $nWorkFlows ,'number_of_accounts' => '' ];
        if(!empty($oPlans)) {
          $aPlans = $oPlans->toArray();
          foreach($oPlans as $k=>$oPlan){
              if(sizeof($oPlan->PlanFeature)!=0){
                  $aPlans[$k]['PlanFeature'] = $oPlan->PlanFeature->toArray();
                  foreach($oPlan->PlanFeature as $k2 => $plan_feature){
                      $aPlans[$k]['PlanFeature'][$k2]['info'] = $plan_feature->Feature;
                  }
              }
          }

        }

        $aResponse = array('plans' => $aPlans ,'aUsage'=>$aUsage, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }

}
