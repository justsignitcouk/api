<?php


namespace App\Controllers;

use MyCompose\Model\Business\BranchType;
use MyCompose\Model\Countries;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class LookupController extends CrudResourceController
{


    public function get($lookup_table){
        if($lookup_table=='countries') {
            $oCountries = Countries::find() ;
            return $this->createArrayResponse($oCountries, 'data');

        }

        if($lookup_table=='branch_types') {
            $oBranchTypes = BranchType::find() ;
            return $this->createArrayResponse($oBranchTypes, 'data');
        }

        return false;
    }



}