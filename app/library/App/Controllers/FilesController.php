<?php

namespace App\Controllers;


use App\Components\File\File;
use MyCompose\Model\CompileLogs;
use MyCompose\Model\Files;
use PhalconRest\Mvc\Controllers\CollectionController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class FilesController extends CollectionController
{

    public function upload()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $sFile = $this->request->getPost("file_data");
        $sPathType = $this->request->get("path_type");

        if($this->request->hasFiles()){
            foreach ($this->request->getUploadedFiles() as $file) {
                $nFile = File::put($file,$sPathType);

            }

        }
        $oFile = Files::findFirstById($nFile);
        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');
    }

    public function uploadFromExternal()
    {


        $username = $this->request->getUsername();
        $password = $this->request->getPassword();

        $AccountType = \App\Auth\UsernameAccountType::ID;
        $session = $this->authManager->loginWithUsernamePassword($AccountType, $username,
            $password);

        $nUserId = $session->getIdentity();

        $sFile = $this->request->getPost("file_data");
        $sPathType = $this->request->get("path_type");
        $userfile = $this->request->get("file");
        $userfile = $this->request->getPost();
        $hasFiles   = $this->request->hasFiles();
        $files  =  $_FILES;


        $aFile = array('file_id' =>$userfile ,
            "hasFiles" => $hasFiles,
            "files" => $files,
            'response_code' => 1 ,'response_message' => "success" );

        return $this->createArrayResponse($aFile, 'data');
        $nFile = false;
        if($this->request->hasFiles()){
            foreach ($this->request->getUploadedFiles() as $file) {
                dd($file);
                $nFile = File::put($file,$sPathType);

            }

        }
        $oFile = Files::findFirstById($nFile);
        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');
    }
}
