<?php

namespace App\Controllers;

use App\Components\File\File;
use App\Components\Mail\Mail;
use App\Components\PDF\PDF;
use MyCompose\Model\AccountDeactivate;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Companies as Companies;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\Mobiles;
use MyCompose\Model\Document\DocumentCcAccount;
use MyCompose\Model\Document\DocumentCcInvitations;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentSenderAccount ;
use MyCompose\Model\Document\DocumentToInvitations;
use MyCompose\Model\Document\DocumentWorkflow;
use MyCompose\Model\Draft\DraftDocumentCcAccount;
use MyCompose\Model\Draft\DraftDocumentSignerAccount;
use MyCompose\Model\Draft\DraftDocumentToAccount;
use MyCompose\Model\Files;
use MyCompose\Model\UserLink;
use MyCompose\Model\Users;
use MyCompose\LaravelModels\Document\DocumentSenderAccount as DSenderA;
use MyCompose\Model\Document\DocumentSignData;
use MyCompose\Model\Document\DocumentSignerAccount;
use MyCompose\LaravelModels\Document\DocumentSignerAccount as DSignerA ;
use MyCompose\Model\Document\DocumentToAccount;
use MyCompose\Model\Document\DocumentUserRead;
use MyCompose\Model\Draft\DraftDocuments;
use MyCompose\Model\Draft\DraftDocumentTowhom;
use MyCompose\Model\Signs;
use MyCompose\LaravelModels\Account;
use MyCompose\LaravelModels\Workflow\Stage;
use MyCompose\LaravelModels\Workflow\State;
use MyCompose\LaravelModels\UserActivity;
use MyCompose\Model\Workflow\StateActionPermitsUser;
use MyCompose\Model\Workflow\StateActionPermitsRole;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
use App\Components\Notifications\Notifications;
use Illuminate\Database\Capsule\Manager as DB;

class AccountController extends CrudResourceController
{


    public function all()
    {

        $oAccounts = Accounts::find();

        return $this->createResourceCollectionResponse($oAccounts);
    }

    public function getAccountById($id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $user_id = $this->userService->getIdentity();
        $nAccountID = $id;

        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('id' => $nAccountID));
        }
        $oAccount = Account::with(['ProfileImage','User','Department', 'Department.Branch', 'Department.Branch.Company'])->where('id', '=', $nAccountID)->get()->first();

        $aAccount = array('account' => $oAccount, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aAccount, 'data');
    }

    public function findBy()
    {
        $aOperation = ['=', 'like', '>', '<', '>=', '<=', '!='];

        $operation = !empty($this->request->getPost("operation")) ? $this->request->getPost("operation") : '=';
        $key = !empty($this->request->getPost("key")) ? $this->request->getPost("key") : 'email';
        $value = !empty($this->request->getPost("value")) ? $this->request->getPost("value") : '';
        $oAccount = new Account();
        $columns = DB::getSchemaBuilder()->getColumnListing('accounts');

        if (!in_array($key, $columns)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'key not in account']);
        }

        if (!in_array($operation, $aOperation)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'operation not allowed']);
        }

        $sCondition = "$key  $operation '$value' ";
        if ($operation == 'like') {
            $sCondition = "$key  $operation '%$value%' ";
        }

        $oAccounts = Accounts::find($sCondition);
        if ($oAccounts->count() == 0) {
            return $this->createArrayResponse(['accounts' => [], 'response_code' => 1, 'response_message' => "success", 'status' => 'success'], 'data');
        }

        $aAccounts = $oAccounts->toArray();
        foreach($oAccounts as $k=>$oAccount){
            $aAccounts[$k]['user'] = $oAccount->Users->toArray() ;
            $aAccounts[$k]['profile_image'] = $oAccount->ImageProfile;
            $aAccounts[$k]['department'] = $oAccount->Department;
            $oAccountRoles = $oAccount->AccountRole;
            $aAccounts[$k]['roles'] = [];
            foreach( $oAccountRoles as $oAccountRole) {
                $aAccounts[$k]['roles'][] = $oAccountRole->Role;
            }
        }

        $aResponse = array('accounts' => $aAccounts, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');

    }

    public function exist()
    {

        $id = $this->request->getPost('id');
        if (!$id) {
            $response = [
                'errorMessage' => 'Missing id argument',
                'status' => false,
                'id' => $id
            ];
            return $this->createArrayResponse($response, 'data');
        }
        if (!is_numeric($id)) {
            $response = [
                'errorMessage' => 'id must be Numeric',
                'status' => false
            ];
            return $this->createArrayResponse($response, 'data');
        }

        $oAccount = Accounts::findFirst("id = $id");
        $response = [
            'errorMessage' => 'NOT FOUND',
            'status' => false
        ];
        if ($oAccount) {
            $response = ['oAccount' => $oAccount,
                'status' => true
            ];
        }

        return $this->createArrayResponse($response, 'data');
    }

    public function make_default()
    {
        $id = $this->request->getPost('id');

        if (empty($id) || !is_numeric($id)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('id' => $id));
        }

        $user_id = $this->userService->getIdentity();
        $oAccounts = $this->db->query("update accounts set is_default=0 where user_id='$user_id' ");
        $oAccount = Accounts::findFirst("id = '$id'");

        if ($oAccount) {

            $oAccount->is_default = 1;
            if (!$oAccount->save()) {
                exit("NOT FOUND");
            }
            $response = [
                'status' => true
            ];

        } else {
            $response = [
                'status' => false,
                'user_id' => $user_id,
                'id' =>  $id ,
            ];
        }
        return $this->createArrayResponse($response, 'data');
    }

    public function company_details()
    {
        $response = [
            'data_missing' => array()
        ];
        $key = $this->request->getPost('key');
        $value = $this->request->getPost('value');
        $error = false;

        if (empty($key)) {
            $response['data_missing'][] = 'key';
            $error = true;
        }

        if (empty($value)) {
            $response['data_missing'][] = 'value';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $oAccount = Accounts::findFirst("$key = $value");
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        $nCompany_id = $oAccount->company_id;
        $oCompanyDetails = Companies::findFirst("id = '$nCompany_id' ");
        $response = [
            'status' => true,
            'oCompany' => $oCompanyDetails
        ];


        return $this->createArrayResponse($response, 'data');

    }

    public function getDepartmentsByAccountId($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirstById($nAccountId);

        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oAccountDepartments = $oAccount->Department ? $oAccount->Department : [];
        $oDepartments = array('departments' => $oAccountDepartments, 'response_code' => 1, 'response_message' => "success");


        return $this->createArrayResponse($oDepartments, 'data');
    }

    public function getToWhomAddresses()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nCompnayID = $this->request->getPost("company_id");
        if (empty($nCompnayID) || !is_numeric($nCompnayID)) {
            $aAccounts = ['to_whom_addresses'=>[], 'response_code' => 1, 'response_message' => "success"];
            return $this->createArrayResponse($aAccounts, 'data');
        }

        $oAccounts = Accounts::find("company_id='$nCompnayID' and active = 1 ");
        $aAccounts = array('to_whom_addresses' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($oAccounts as $k => $oAccount) {
            $aAccounts['to_whom_addresses'][] = [
                'id' => $oAccount->id,
                'account_name' => $oAccount->account_name,
                'first_name' => $oAccount->Users->first_name,
                'last_name' => $oAccount->Users->last_name,
                'user_id' => $oAccount->user_id,
                'profile_image' => $oAccount->profile_image,
                'email' => $oAccount->Users->email,
                'department_name' => $oAccount->Department ? $oAccount->Department->name : '',
                'department_id' => $oAccount->department_id,
            ];


        }
        return $this->createArrayResponse($aAccounts, 'data');
    }

    public function sent($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirst("id=$nAccountId and user_id=$nUserId");
        if (!$oAccount) {
            $oUsersLink= UserLink::find("user_id=".$nUserId) ;
            $bAccountAccess = false;
            foreach($oUsersLink as $oUserLink){
                $UserLinkId = $oUserLink->user_link_id;
                $oAccount = Accounts::findFirst("id=".$nAccountId . " and user_id=$UserLinkId") ;
                if($oAccount){
                    $bAccountAccess = true;
                    break;
                }
            }
            if(!$bAccountAccess){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
            }

        }

        $aSent = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocuments = $this->getDocumentBySenderAccount($oAccount);

        foreach ($oDocuments as $oDocument) {

            $aSent['document'][] = $oDocument ;
        }
        $aSent['RecordsTotal'] = sizeof($aSent['document']);
        return $this->createArrayResponse($aSent, 'data');
    }

    public function draft($id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirst("id=$nAccountId and user_id=$nUserId");
        if (!$oAccount) {
            $oUsersLink= UserLink::find("user_id=".$nUserId) ;
            $bAccountAccess = false;
            foreach($oUsersLink as $oUserLink){
                $UserLinkId = $oUserLink->user_link_id;
                $oAccount = Accounts::findFirst("id=".$nAccountId . " and user_id=$UserLinkId") ;
                if($oAccount){
                    $bAccountAccess = true;
                    break;
                }
            }
            if(!$bAccountAccess){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
            }

        }

        $aInbox = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocumentsSignerAccounts = DraftDocumentSignerAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));
        if ($oDocumentsSignerAccounts->count() > 0) {
            foreach ($oDocumentsSignerAccounts as $oDocumentsSignerAccount) {
                $nDocumentID = $oDocumentsSignerAccount->document_id;
                $oDocument = $this->getDraftDocumentById($nDocumentID);
                if ($oDocument) {
                    $aInbox['document'][$oDocumentsSignerAccount->document_id] = $oDocument;
                }
            }
        }

        $oDocumentsToAccounts = DraftDocumentToAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        foreach ($oDocumentsToAccounts as $oDocumentsToAccount) {
            $nDocumentID = $oDocumentsToAccount->document_id;
            $oDocument = $this->getDraftDocumentById($nDocumentID);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsToAccount->document_id] = $oDocument;
            }

        }

        $oDocumentsCCAccounts = DraftDocumentCcAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        foreach ($oDocumentsCCAccounts as $oDocumentsCCAccount) {
            $nDocumentID = $oDocumentsCCAccount->document_id;
            $oDocument = $this->getDraftDocumentById($nDocumentID);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsCCAccount->document_id] = $oDocument;

            }

        }

        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($aInbox['document'] as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }

    public function inbox($id,$sFilter)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirst("id=$nAccountId and user_id=$nUserId");
        if (!$oAccount) {
            $oUsersLink= UserLink::find("user_id=".$nUserId) ;
            $bAccountAccess = false;
            foreach($oUsersLink as $oUserLink){
                $UserLinkId = $oUserLink->user_link_id;
                $oAccount = Accounts::findFirst("id=".$nAccountId . " and user_id=$UserLinkId") ;
                if($oAccount){
                    $bAccountAccess = true;
                    break;
                }
            }
            if(!$bAccountAccess){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
            }

        }

        switch ($sFilter) {
            case 'pending' :
                $status = 'inprogress';
                break;
            case 'completed' :
                $status = 'completed';
                break;
            case 'inbox' :
                $status = 'inbox';
                break;
            case 'rejected' :
                $status = 'rejected';
                break;
            default :
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }


        $aInbox = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocumentsSignerAccounts = DocumentSignerAccount::find(array("account_id=$nAccountId and is_visible=1", "order" => "document_id desc"));
        if ($oDocumentsSignerAccounts->count() > 0) {
            foreach ($oDocumentsSignerAccounts as $oDocumentsSignerAccount) {
                $nDocumentID = $oDocumentsSignerAccount->document_id;
                $oDocument = $this->getDocumentById($nDocumentID, $status);
                if ($oDocument) {
                    $aInbox['document'][$oDocumentsSignerAccount->document_id] = $oDocument;
                }
            }
        }

        $oDocumentsToAccounts = DocumentToAccount::find(array("account_id=$nAccountId  and is_visible=1", "order" => "document_id desc"));

        foreach ($oDocumentsToAccounts as $oDocumentsToAccount) {
            $nDocumentID = $oDocumentsToAccount->document_id;
            $oDocument = $this->getDocumentById($nDocumentID, $status);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsToAccount->document_id] = $oDocument;
            }

        }

        $oDocumentsCCAccounts = DocumentCcAccount::find(array("account_id=$nAccountId  and is_visible=1", "order" => "document_id desc"));

        foreach ($oDocumentsCCAccounts as $oDocumentsCCAccount) {
            $nDocumentID = $oDocumentsCCAccount->document_id;
            $oDocument = $this->getDocumentById($nDocumentID, $status);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsCCAccount->document_id] = $oDocument;

            }

        }

        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($aInbox['document'] as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }

    public function pending($id){

            if (!$this->userService->getIdentity()) {
                throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
            }

            $nUserId = $this->userService->getIdentity();
            $nAccountId = $id;
            if (empty($nAccountId) || !is_numeric($nAccountId)) {
                throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
            }

            $oAccount = Accounts::findFirst("id=$nAccountId and user_id=$nUserId");
            if (!$oAccount) {
                $oUsersLink= UserLink::find("user_id=".$nUserId) ;
                $bAccountAccess = false;
                foreach($oUsersLink as $oUserLink){
                    $UserLinkId = $oUserLink->user_link_id;
                    $oAccount = Accounts::findFirst("id=".$nAccountId . " and user_id=$UserLinkId") ;
                    if($oAccount){
                        $bAccountAccess = true;
                        break;
                    }
                }
                if(!$bAccountAccess){
                    throw new Exception(ErrorCodes::DATA_NOT_FOUND);
                }

            }

            $oAccountRoles = AccountRole::find("account_id='$nAccountId'") ;
        $aWorkflowID = [];
        $aWorkFlowInfo = [];
            foreach($oAccountRoles as $oAccountRole){
                $oStateRoles = StateActionPermitsRole::find("role_id=" . $oAccountRole->role_id ) ;
                if($oStateRoles->count()>0){
                    foreach($oStateRoles as $oStateRole) {
                        $oStagesState = $oStateRole->StateAction->State->StageState;
                        foreach( $oStagesState as $oStageState){
                            $aWorkflowID[] = $oStageState->Stage->Workflow->id;
                            $aWorkflowInfo[] = ['stage_id'=> $oStageState->Stage->id,'workflow_id' => $oStageState->Stage->Workflow->id ] ;
                        }
                    }
                }
            }

        $oStatesAccount = StateActionPermitsUser::find("account_id=" . $nAccountId) ;
        if($oStatesAccount->count()>0){
            foreach($oStatesAccount as $oStateAccount) {
                $oStagesState = $oStateAccount->StateAction->State->StageState;
                foreach( $oStagesState as $oStageState){
                    $aWorkflowID[] = $oStageState->Stage->Workflow->id;
                    $aWorkflowInfo[] = ['stage_id'=> $oStageState->Stage->id,'workflow_id' => $oStageState->Stage->Workflow->id ] ;
                }

            }
        }

        $aInbox_new = array('document' => [], 'response_code' => 1, 'response_message' => "success");

        if(sizeof($aWorkflowID)==0){
            return $this->createArrayResponse($aInbox_new, 'data');
        }

        $sWorkflowID = implode($aWorkflowID,',');

        $oDocumentsWorkflow = DocumentWorkflow::find(["workflow_id in ($sWorkflowID) and active = 1 ",'order'=>'document_id desc' ]) ;
        $aInbox = [];
        foreach($oDocumentsWorkflow as $oDocumentWorkflow){
            $bSkip = false;
            foreach($aWorkflowInfo as $aWorkflow){

                if($aWorkflow['stage_id']==$oDocumentWorkflow->current_stage_id){
                    $bSkip = true;
                }
            }
            if($bSkip){
                $nDocumentID = $oDocumentWorkflow->document_id;
                $oDocument = $this->getDocumentById($nDocumentID, 'inprogress');
                if ($oDocument) {
                    $aInbox['document'][$nDocumentID] = $oDocument;
                }
            }
        }


        foreach ($aInbox['document'] as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');

    }

    public function badges($id)
    {
        $aBadges = [];
        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $aBadges['inbox'] = Documents::find("account_id=$nAccountId and status='completed' ")->count();
        $aBadges['sent'] = Documents::find("account_id=$nAccountId")->count();
        $aBadges['draft'] = DraftDocuments::find("account_id=$nAccountId")->count();
        $aBadges['pennding'] = Documents::find("account_id=$nAccountId and status = 'inprogress'")->count();
        $aBadges['rejected'] = Documents::find(array("account_id=$nAccountId and status='rejected'", "order" => "created_at desc"))->count();

        $aBadgesWithTotal = array('badgets' => $aBadges, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aBadgesWithTotal, 'data');
    }

    public function getDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("account_id");

        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountID));
        }

        $oAccount = Accounts::findFirstById($nAccountID);
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountID));
        }

        $nSignID = $oAccount->default_sign_id;
        if (!is_null($nSignID) && !empty($nSignID)) {
            $oSign = Signs::findFirstById($nSignID);
            if (!$oSign) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
            }
            $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aResult, 'data');
        }

        $oSign = Signs::findFirst("user_id=" . $nUserID);
        if (!$oSign) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
        }
        $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    public function setDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("account_id");
        $nSigntID = $this->request->getPost("sign_id");
        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountID));
        }

        $oAccount = Accounts::findFirstById($nAccountID);
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountID));
        }

        $oSign = Signs::findFirstById($nSigntID);
        if (!$oSign) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSigntID));
        }

        $oAccount->default_sign_id = $nSigntID;

        if (!$oAccount->save()) {
            return $oAccount->getMessages();
        }

        $aResult = array('response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    public function sign(){


        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $nDocumentID = $this->request->getPost("document_id");
        $nStateID = $this->request->getPost("state_id") ;
        $nSignID  = $this->request->getPost("sign_id") ;



        $this->db->begin();

        if( empty($nAccountID) || !is_numeric($nAccountID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'1',array('id' => $nAccountID));
        }

        if( empty($nDocumentID) || !is_numeric($nDocumentID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'2',array('document_id' => $nDocumentID));
        }

        if( empty($nSignID) || !is_numeric($nSignID) ){
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and is_default=1') ;
            if(!$oSign){
                $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature"' ) ;
                if(!$oSign) {
                    throw new Exception(ErrorCodes::DATA_MISSING,'3',array('account' =>"account didn't has sign"));
                }
            }

        }else{
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and id=' . $nSignID) ;
            if(!$oSign){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND,'',[]);
            }
        }

        $nSignID = $oSign->id ;
        $oDefaultSign = Signs::findFirst("user_id=" . $nUserID . ' and sign_type="Signature" and id=' . $nSignID) ;

        if(!$oDefaultSign){
            throw new Exception(ErrorCodes::DATA_MISSING,$nUserID.'add sign' . $nSignID,array('sign' => $nSignID));

        }

        $oDocument = Documents::findFirst("id=$nDocumentID") ;
        $oDocumentSignerAccount = DocumentSignerAccount::findFirst("account_id='$nAccountID' and document_id=$nDocumentID ");
        if(!$oDocumentSignerAccount){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND,'',[]);
        }

        $oDocumentSignerAccount->is_sign = 1;
        $oDocumentSignerAccount->sign_at = date("Y-m-d H:i:s");
        $oDocumentSignerAccount->sign_id = $nSignID;

        $sSignCode =   $nDocumentID . '.' . $nAccountID . '.' . $oDocumentSignerAccount->account_id ;
        $sSignCodeWithUrl = $this->config->web->host . '/' . $this->config->web->sign_validation_action . '/' . $sSignCode;
        $qrImage = 'data:image/png;base64,' . $this->qr->getBarcodePNG($sSignCodeWithUrl, "QRCODE");

        $oDefaultSignWithQR = $this->addQRToSign($qrImage,$oDefaultSign->sign_data) ;

        $oDocumentSignData = new DocumentSignData();
        $oDocumentSignData->sign_code = $sSignCode ;
        $oDocumentSignData->sign_image = $oDefaultSignWithQR ;
        $oDocumentSignData->created_at = date("Y-m-d H:i:s") ;
        if(!$oDocumentSignData->save()){
            throw new Exception(ErrorCodes::DATA_FAILED,'',[]);
        }

        $oDocumentSignerAccount->document_sign_data_id = $oDocumentSignData->id;

        $aDocumentSigners = array('oDocumentSignee' => [], 'status' => false);

        if ($oDocumentSignerAccount->save()) {

            $nIsSignCounter = 0;
            # To-Do : check all signer (user,department,role)
            $oAllDocumentSigners = DocumentSignerAccount::find("document_id=$nDocumentID ");
            foreach ($oAllDocumentSigners as $oSigner){
                if ($oSigner->is_sign == 1){
                    $nIsSignCounter++;
                }
            }


            //document sender
            $oDocumentSenderAccount = DocumentSenderAccount::findFirst("document_id=".$oDocument->id) ;
            Notifications::createNotification($oDocumentSenderAccount->account_id ,$nAccountID,$oDocument->id,4);

            //document is completed
            $this->db->commit();
            if ($oAllDocumentSigners->count() == $nIsSignCounter) {
//                if( empty($nStateID) || !is_numeric($nStateID) ){
//                    $oDocumentWorkFlow = DocumentWorkflow::where("document_id",$nDocumentID)->first();
//                    $oStage = Stage::find($oDocumentWorkFlow->wf_current_stage_id)->first() ;
//                    $oStageState = State::where("stage_id",$oStage->id)->first() ;
//                    $nStateID = $oStageState->id;
//                }


                //$oStageState = State::find($nStateID)->first() ;
                //$nNextStage = $oStageState->next_stage_id ;

                $oDocument = Documents::findFirstById($nDocumentID);
                //$oDocumentWorkFlow = DocumentWorkflow::where("document_id",$nDocumentID)->first();
                //$oDocumentWorkFlow->wf_current_stage_id  = $nNextStage ;
//                if(!$oDocumentWorkFlow->save()){
//                    throw new Exception(ErrorCodes::DATA_FAILED,'',array('error' => $oDocumentWorkFlow->getMessage()));
//                }

                $oDocument->status = 'completed';
                $oDocument->save();

                $oDocument = \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                    'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SignerAccount.Account.User', 'SignerAccount.Account.User', 'SignerAccount.DocumentSignData', 'SenderAccount.Account.ProfileImage',
                    'ToUser.User.ProfileImage', 'CcUser.User.ProfileImage', 'SignerUser.User.ProfileImage', 'SenderUser.User.ProfileImage',
                    'ToRole.Role', 'CcRole.Role', 'SignerRole.Role',
                    'ToInvitations.Invitations', 'CcInvitations.Invitations', 'SignerInvitations.Invitations',
//            'WorkFlow.WorkFlow.Stage.WfState.WfStateAction.wfAction.wfFunction',
                    'Layout.LayoutComponent',
                    'QrCode',
                    'Files.File'])->where('id', $nDocumentID)->get()->first();
                $aDocument = $oDocument->toArray();
                $pdfPath = PDF::saveFromDocument($aDocument);
                $aEmails = [] ;
                $oToAccouts = DocumentToAccount::find("document_id=" . $nDocumentID );
                foreach($oToAccouts as $oAccount){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {

                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oCcAccouts = DocumentCcAccount::find("document_id=" . $nDocumentID );

                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {

                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oToInvitaion = DocumentToInvitations::find("document_id=" . $nDocumentID );
                foreach($oToInvitaion as $oInvitaion){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oCcAccouts = DocumentCcInvitations::find("document_id=" . $nDocumentID );
                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Invitations->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oSignerAccouts = DocumentCcAccount::find("document_id=" . $nDocumentID );
                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                //find document creator
                $oDocumentSenderAccount = DocumentSenderAccount::findFirst("document_id=".$oDocument->id) ;
                //create notification when document signed
                Notifications::createNotification($oDocumentSenderAccount->account_id,$nAccountID,$oDocument->id,3);

            }



            $aDocumentSigners['oDocumentSignee'] = $oDocumentSignerAccount;
            $aDocumentSigners['status'] = true;
        }

        $this->db->commit();


       // UserActivity::createActivity('user_signed_document',$nUserID,$nAccountID);

        return $this->createArrayResponse($aDocumentSigners, 'data');

    }

    public function getWaitingFromOthers($accountId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nDocumentSender = DocumentSenderAccount::find(array("account_id=$accountId"));

        $waitingDocuments = array();
        foreach ($nDocumentSender as $documentSender) {
            $docId = $documentSender->document_id;
            $documentSignerAccount = DocumentSignerAccount::findFirst("document_id=$docId and is_sign=0");
            if ($documentSignerAccount) {
                $doc = $this->getDocumentById($docId, 'inprogress');
                if ($doc){
                    $waitingDocuments[] = $doc;
                }
            }
        }

        if ($waitingDocuments)
            $aDocumentsWaitingOthers = array('document' => $waitingDocuments, 'response_code' => 1, 'response_message' => "success");
        else
            $aDocumentsWaitingOthers = array('document' => [], 'response_code' => 1, 'response_message' => "success");

        $aDocumentsWaitingOthers['RecordsTotal'] = sizeof($aDocumentsWaitingOthers['document']);

        return $this->createArrayResponse($aDocumentsWaitingOthers, 'data');
    }

    public function needsMySign($nAccountId)
    {


        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocumentsSignerAccounts = DocumentSignerAccount::find(array("account_id=$nAccountId and is_sign =0", "order" => "document_id desc"));
//        $aInbox = array('document' => $oDocumentsSignerAccounts, 'response_code' => 1, 'response_message' => "success");

        $oDocumentsSignerAccounts = DocumentSignerAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        $aNeedsMySignDocuments = array();
        foreach ($oDocumentsSignerAccounts as $oDocumentsSignerAccount) {
            $nDocumentID = $oDocumentsSignerAccount->document_id;
            $oDocument = $this->getDocumentById($nDocumentID, 'inprogress');
            if ($oDocument) {
                $aNeedsMySignDocuments['document'][$oDocumentsSignerAccount->document_id] = $oDocument;
            }
        }

        if (sizeof($aNeedsMySignDocuments) > 0) {
            $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
            foreach ($aNeedsMySignDocuments['document'] as $aDocument) {
                $aInbox_new['document'][] = $aDocument;
            }
            $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

            return $this->createArrayResponse($aInbox_new, 'data');
        }

        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }

    public function changeProfileImage(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $fProfileImage = $this->request->getPost("profile_image") ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if(!empty($fProfileImage)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('profile_image' => $fProfileImage));
        }



        if(!$this->request->hasFiles()) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'You should to upload file', []);
        }
        foreach ($this->request->getUploadedFiles() as $file) {
            $nFile = File::put($file);

        }

        $oAccount->profile_image = $nFile;

        if(!$oAccount->save()){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oFile = Files::findFirstById($nFile);

        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');


    }


    public function getContacts($nAccountID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContacts = Contacts::find("account_id=" . $nAccountID );
        $aContacts = $oContacts->toArray();
        foreach($oContacts as $k=>$oContact){
            $aContacts[$k]['emails'] = $oContact->Emails;
            $aContacts[$k]['mobiles'] = $oContact->Mobiles;
        }

        $aResponse = array('aContacts' => $aContacts ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getContactsForRecipient($nAccountID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $sFilter = $this->request->get('q');

        $nUserID    = $this->userService->getIdentity();
        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContacts = Contacts::find("display_name like '%" . $sFilter . "%' and account_id='" . $nAccountID . "'" );
        $aContacts = $oContacts->toArray();
        foreach($oContacts as $k=>$oContact){
            $aContacts[$k]['emails'] = $oContact->Emails;
            $aContacts[$k]['mobiles'] = $oContact->Mobiles;
            $aContacts[$k]['recipient_type'] = 'contact';
        }

        $aResponse = array('aContacts' => $aContacts ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function saveContacts($nAccountID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $aContacts = $this->request->getPost('aContacts') ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        foreach($aContacts as $aContact) {
            $oContact = Contacts::findFirst("google_id='" . $aContact['resource_name'] . "'");
            if(!$oContact) {
                $oContact = new Contacts();
            }

            $oContact->account_id = $nAccountID;
            $oContact->first_name = isset($aContact['first_name']) ? $aContact['first_name'] : null ;
            $oContact->last_name = isset($aContact['last_name']) ? $aContact['last_name'] : null ;
            $oContact->display_name =  isset($aContact['display_name']) ? $aContact['display_name'] : null ;
            $oContact->google_id = isset($aContact['resource_name']) ?  $aContact['resource_name'] : null ;
            $oContact->created_at = date('Y-m-d H:i:s') ;
            if(!$oContact->save()){
                return $oContact->getMessages();
            }

            if(isset($aContact['emails']) && sizeof($aContact['emails'])>0){
                foreach($aContact['emails'] as $email){
                    $oEmail = Emails::findFirst("email='" . $email['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oEmail){
                        $oEmail = new Emails();
                    }
                    $oEmail->email = $email['value'];
                    $oEmail->type = $email['type'];
                    $oEmail->contact_id = $oContact->id;
                    $oEmail->created_at = date('Y-m-d H:i:s') ;
                    $oEmail->save();
                }

            }

            if(isset($aContact['mobiles']) && sizeof($aContact['mobiles'])>0){
                foreach($aContact['mobiles'] as $mobile){
                    $oMobile = Mobiles::findFirst("mobile='" . $mobile['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oMobile){
                        $oMobile = new Mobiles();
                    }
                    $oMobile->mobile = $mobile['value'];
                    $oMobile->type = $mobile['type'];
                    $oMobile->contact_id = $oContact->id;
                    $oMobile->created_at = date('Y-m-d H:i:s') ;

                    $oMobile->save();
                }

            }


        }

        $aResponse = [ 'response_code' => 1 ,'response_message' => "success" ];
        return $this->createArrayResponse($aResponse, 'data' );
    }


    public function changeCoverImage(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $fProfileImage = $this->request->getPost("profile_cover") ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }


        if(!$this->request->hasFiles()) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'You should to upload file', []);
        }
        foreach ($this->request->getUploadedFiles() as $file) {
            $nFile = File::put($file);

        }

        $oAccount->profile_cover = $nFile;

        if(!$oAccount->save()){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oFile = Files::findFirstById($nFile);

        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');


    }

    public function deactivate(){

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $this->request->getPost('account_id');
        $sPassword = $this->request->getPost('password');
        $sNote = $this->request->getPost('message');

        if(empty($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountId));
        }

        if(empty($sPassword)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('password' => $sPassword));
        }

        $oUser = Users::findFirstById($nUserId);
        $sEmail = $oUser->email;
        $sUserName = $sEmail;
        $AccountType = \App\Auth\UsernameAccountType::NAME;
        if(empty($sEmail)) {
            $sUserName= $oUser->mobile;
            $AccountType = \App\Auth\UsernameAccountType::MOBILE;
        }



        $session = $this->authManager->loginWithUsernamePassword($AccountType, $sUserName,
            $sPassword);

        $oAccount = Accounts::findFirstById($nAccountId) ;
        if(!$oAccount){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oAccount->active = 0;

        if(!$oAccount->save()){
            $aResponse = array('status' => 'false','error'=>$oAccount->getMessages() ,'response_code' => 0 ,'response_message' => "failed" );
            return $this->createArrayResponse($aResponse, 'data');
        }

        $oAccountDeactivate = new AccountDeactivate();
        $oAccountDeactivate->account_id = $nAccountId;
        $oAccountDeactivate->note = $sNote ;
        $oAccountDeactivate->created_at = date('Y-m-d H:i:s') ;
        $oAccountDeactivate->save();
        $aResponse = array( 'status' => 'true','response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');

    }


    private function getDocumentByID($nDocumentID,$status='inbox'){
        if($status=='inbox'){
            $oDocument =  \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage' , 'SenderAccount.Account.User',
            ])->where('id', $nDocumentID)->get()->first();
            $getIfRead = DocumentUserRead::findFirst("user_id=". $this->userService->getIdentity() . " and document_id=" . $oDocument->id) ;
            $bIsRead = $getIfRead ? true : false ;
            $oDocument['is_read'] = $bIsRead ;
            return $oDocument;
        }

        return \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
            'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage' , 'SenderAccount.Account.User',
        ])->where('id', $nDocumentID)->where('status', $status)->get()->first();
    }


    private function getDraftDocumentByID($nDocumentID){

            $oDocument =  \MyCompose\LaravelModels\DraftDocument\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage' , 'SenderAccount.Account.User',
            ])->where('id', $nDocumentID)->get()->first();

            $bIsRead =true;
            $oDocument['is_read'] = $bIsRead ;
            return $oDocument;
            }


    private function getDocumentBySenderAccount($oAccount){
        $oDocumentSenderAccounts = DocumentSenderAccount::find("account_id=" . $oAccount->id ) ;
        $aDocuments = [] ;
        foreach($oDocumentSenderAccounts as $oDocumentSenderAccount) {
            $aDocuments[] = $this->getDocumentByID($oDocumentSenderAccount->document_id) ;
        }
        return $aDocuments ;

    }

    private function addQRToSign($fQR,$fSign)
    {
        $manager = new \Intervention\Image\ImageManager();
        $img = $manager->make($fSign);
        $type = 'png';
        $image_width = $img->width();
        $image_height = $img->height();
        $img2 = $manager->make($fQR);
        $img->insert($img2, 'bottom-right', 0, 0);
        $img->encode('png');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        $sSignDataWithQR = $base64;

        return $sSignDataWithQR;
    }

}
