<?php

namespace App\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use MyCompose\Model\Contacts\MessagesQueue;
use MyCompose\Model\Document\DocumentCcAccount;
use MyCompose\Model\Document\DocumentNotes;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentSignData;
use MyCompose\Model\Document\DocumentSignerAccount;
use MyCompose\Model\Document\DocumentSignerContact;
use MyCompose\Model\Document\DocumentToAccount;
use MyCompose\Model\Document\DocumentWorkflow;

use MyCompose\Model\Document\Recipients\Contacts;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Profiles\IONumber;
use MyCompose\Model\Profiles\IONumberSeqences;
use MyCompose\Model\Signs;
use MyCompose\Model\Workflow\Action;
use MyCompose\Model\Workflow\DocumentWorkflowActionHistory;
use MyCompose\Model\Workflow\Stage;
use MyCompose\Model\Workflow\StageState;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;

class ActionsController extends CrudResourceController
{

    private $aParams;

    public function getByDocumentID($nDocumentID)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocumentWorkflow = DocumentWorkflow::findFirst(["document_id=$nDocumentID", 'order' => 'created_at desc']);
        $nCurrentStageID = $oDocumentWorkflow->current_stage_id;
        $oStage = Stage::findFirstById($nCurrentStageID);
        $oStageStates = StageState::find("stage_id=" . $nCurrentStageID);
        $aAction = [];
        $aStates = [];
        $oStates = [];
        foreach ($oStageStates as $oStageState) {
            $aStates[] = $oStageState->State->toArray();
            $oStates[] = $oStageState->State;
        }

        $oStateActions = [];
        foreach ($oStates as $k => $oState) {
            $aStates[$k]['StateActions'] = $oState->StateAction->toArray();
            $oStateActions = $oState->StateAction;
            foreach ($oStateActions as $k2 => $oStateAction) {


                $aStates[$k]['StateActions'][$k2]['Action'] = $oStateAction->Action->toArray();
                $aStates[$k]['StateActions'][$k2]['StateActionPermitsUser'] = $oStateAction->StateActionPermitsUser;
                $aStates[$k]['StateActions'][$k2]['StateActionPermitsRole'] = $oStateAction->StateActionPermitsRole;
                $aStates[$k]['StateActions'][$k2]['Action']['function'] = $oStateAction->Action->WorkflowFunction;

            }

        }

        $aResponse = ['aStates' => $aStates, 'oCurrentStage' => $oStage];
        return $this->createArrayResponse($aResponse, 'data');


    }

    public function saveAction()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $action_id = $this->request->getPost('action_id');
        $document_id = $this->request->getPost('document_id');
        $stage_id = $this->request->getPost('stage_id');
        $state_id = $this->request->getPost('state_id');
        $this->state_id = $state_id;
        $this->stage_id = $stage_id;
        $this->document_id = $document_id;

        $this->action_id = $action_id;
        $this->user_id = $this->userService->getIdentity();

        $oAction = Action::findFirstById($action_id);
        if (!$oAction) {
            return 'action_id not found ' . $action_id;
        }
        $oFunction = $oAction->WorkflowFunction;
        $this->setParam('action', $oAction->source_type);
        $this->setParam('action_type', $oAction->action_type);
        $this->setParam('frontend', $oFunction->frontend);
        eval($oFunction->backend);


        $oDocumentAction = new DocumentWorkflowActionHistory();
        $oDocumentAction->document_id = $document_id;
        $oDocumentAction->user_id = $this->user_id;
        $oDocumentAction->action_id = $action_id;
        $oDocumentAction->stage_id = $stage_id;
        $oDocumentAction->action_date = date('Y-m-d H:i:s');
        $oDocumentAction->created_at = date('Y-m-d H:i:s');

        if (!$oDocumentAction->save()) {
            return $oDocumentAction->getMessages();
        }

        $oDocumentWorkFlow = DocumentWorkflow::findFirst(["document_id=" . $this->document_id, 'order' => 'id desc']);
        $nCurrentStage = $oDocumentWorkFlow->current_stage_id;
        $oStage = Stage::findFirstById($nCurrentStage);
        if ($oStage) {
            if ($oStage->notify == 'signers') {
                //  \MyCompose\Model\Document\Recipients\Contacts::addMessageSignerToQueue($this->document_id);
                // \MyCompose\Model\Document\Recipients\Contacts::addMessageSignerToQueue($this->document_id);
            }
        }

        $aParams = $this->getParams();
        return json_encode($aParams);

    }

    private function nextStage()
    {

        $this->db->begin();

        $oDocumentWorkflow = DocumentWorkflow::findFirst(["document_id=" . $this->document_id, 'order' => 'id desc']);
        if (!$oDocumentWorkflow) {
            $oDocumentWorkflow = new DocumentWorkflow();
            $oDocumentWorkflow->document_id = $this->document_id;
            $oDocumentWorkflow->workflow_id = 1;
            $oDocumentWorkflow->current_stage_id = 2;
            $oDocumentWorkflow->active = 1;
            $oDocumentWorkflow->created_at = date('Y-m-d H:i:s');
            $oDocumentWorkflow->save();
        }

        $current_workflow = $oDocumentWorkflow->workflow_id;
        $current_stage = $oDocumentWorkflow->current_stage_id;
        $oDocumentWorkflow->active = 0;
        $oDocumentWorkflow->save();
        $oCurrentStage = StageState::findFirst("stage_id=" . $current_stage . " and state_id=" . $this->state_id);
        if (!$oCurrentStage) {
            return false;
        }

        $oDocumentWorkflow = new DocumentWorkflow();
        $oDocumentWorkflow->current_stage_id = $oCurrentStage->next_stage_id;
        $oDocumentWorkflow->document_id = $this->document_id;
        $oDocumentWorkflow->workflow_id = $current_workflow;
        $oDocumentWorkflow->created_at = date('Y-m-d H:i:s');
        $oDocumentWorkflow->active = 1;

        if (!$oDocumentWorkflow->save()) {
            return false;
        }

        $oStage = Stage::findFirstById($oCurrentStage->next_stage_id);
        $nDocumentID = $this->document_id;
        if ($oStage->action_type == 'In Progress') {

            $oDocument = Documents::findFirstById($nDocumentID);
            $oDocument->status = 'inprogress';
            $oDocument->save();
        } elseif ($oStage->action_type == 'END') {
            $oDocument = Documents::findFirstById($nDocumentID);
            $oDocument->status = 'completed';
            $oDocument->save();
        }
        $this->db->commit();
        return true;
    }

    private function GoToThePreviousStage()
    {

        $oDocumentWorkflow = DocumentWorkflow::findFirst(["document_id=" . $this->document_id, 'order' => 'id desc']);
        $current_workflow = $oDocumentWorkflow->workflow_id;
        $current_stage = $oDocumentWorkflow->current_stage_id;
        $oDocumentWorkflow->active = 0;
        $oDocumentWorkflow->save();
        if (is_null($current_stage)) {
            var_dump($oDocumentWorkflow->toArray());
            exit();
        }

        $oCurrentStage = StageState::findFirst("next_stage_id=" . $current_stage);
        if (!$oCurrentStage) {
            return false;
        }
        $oDocumentWorkflow = new DocumentWorkflow();
        $oDocumentWorkflow->current_stage_id = $oCurrentStage->stage_id;
        $oDocumentWorkflow->document_id = $this->document_id;
        $oDocumentWorkflow->workflow_id = $current_workflow;
        $oDocumentWorkflow->created_at = date('Y-m-d H:i:s');
        $oDocumentWorkflow->active = 1;

        if (!$oDocumentWorkflow->save()) {
            return false;
        }

        return true;
    }

    public function sendDocumentToRecipient()
    {

        $oDocumentContacts = Contacts::find("document_id=" . $this->document_id);
        if ($oDocumentContacts->count() > 0) {
            foreach ($oDocumentContacts as $oDocumentContact) {
                $oDocumentContact->can_read = 1;
                if (!$oDocumentContact->save()) {
                    dd($oDocumentContact->getMessages());
                }
            }
        }

    }

    public function generateOutboxNumber($oDocument)
    {
        if ($oDocument->outbox_number == '') {
            $sOutboxNumber = '#Out of Range';
            $oIONumber = IONumber::findFirst("user_id=" . $oDocument->user_id . " and active=1 ");
            if ($oIONumber) {
                $oIONumberSeqences = IONumberSeqences::findFirst("user_id=" . $oDocument->user_id . " and type='outbox' and year ='" . date('Y') . "'");
                if ($oIONumberSeqences) {
                    $sOutboxNumber = $oIONumber->generateIONumber([
                        'user_id' => $oDocument->user_id,
                        'document_id' => $oDocument->id,
                        'oIONumberSeqences' => $oIONumberSeqences]);
                }
            }

            $oDocument->outbox_number = $sOutboxNumber;
            $oDocument->save();
        }
    }

    public function generateInboxNumberForRecipients($oDocument){
        $oRecipients = Recipients::find("document_id=" . $oDocument->id );
        foreach($oRecipients as $oRecipient) {
            $oRecipient->inbox_number = $this->generateInboxNumber( $oDocument->id , $oRecipient->user_id );
            $oRecipient->save();
        }
    }

    public function generateInboxNumber($nDocumentID, $nUserID )
    {
        $sInboxNumber = '#Out of Range';
        $oIONumber = IONumber::findFirst("user_id=" . $nUserID . " and active=1 ");
        if ($oIONumber) {
            $oIONumberSeqences = IONumberSeqences::findFirst("user_id=" . $nUserID . " and type='inbox' and year ='" . date('Y') . "'");
            if ($oIONumberSeqences) {
                $sInboxNumber = $oIONumber->generateIONumber([
                    'user_id' => $nUserID,
                    'document_id' => $nDocumentID,
                    'oIONumberSeqences' => $oIONumberSeqences]);
            }
        }

        return $sInboxNumber;
    }

    public function saveSign()
    {
        $nDocumentID = $this->document_id;

        $nUserID = $this->user_id;
        $nSignID = $this->request->getPost('sign_id');
        if (empty($nSignID)) {
            return false;
        }
        $oDocumentRecipients = Recipients::findFirst("document_id=$nDocumentID and user_id=$nUserID and type='Signer' ");
        $oDefaultSign = Signs::findFirstById($nSignID);
        if (!$oDefaultSign) {
            return false;
        }

        if ($oDocumentRecipients) {
            $oDocumentRecipients->is_sign = 1;
            $oDocumentRecipients->sign_at = date('Y-m-d H:i:s');
            $oDocumentRecipients->sign_id = $nSignID;
            $sSignCode = $nDocumentID . '.' . $nUserID . '.' . $nSignID;
            $sSignCodeWithUrl = $this->config->web->host . '/' . $this->config->web->sign_validation_action . '/' . $sSignCode;
            $qrImage = 'data:image/png;base64,' . $this->qr->getBarcodePNG($sSignCodeWithUrl, "QRCODE");

            $oDefaultSignWithQR = Signs::addQRToSign($qrImage, $oDefaultSign->sign_data);

            $oDocumentSignData = new DocumentSignData();
            $oDocumentSignData->sign_code = $sSignCode;
            $oDocumentSignData->sign_image = $oDefaultSignWithQR;
            $oDocumentSignData->created_at = date("Y-m-d H:i:s");
            if (!$oDocumentSignData->save()) {
                throw new Exception(ErrorCodes::DATA_FAILED, '', []);
            }
            $oDocumentRecipients->sign_data_id = $oDocumentSignData->id;
            if (!$oDocumentRecipients->save()) {
                throw new Exception(ErrorCodes::DATA_FAILED, '', []);
            }

            $oUserSigner = Recipients::find("document_id = $nDocumentID and type='Signer' ");
            $nAllSigner = $oUserSigner->count();
            $nSignerCount = 0;
            if ($nAllSigner > 0) {
                foreach ($oUserSigner as $oSigner) {
                    if ($oSigner->is_sign == '1') {
                        $nSignerCount++;
                    }
                }
            }
            if ($nAllSigner == $nSignerCount) {
                $oDocument = Documents::findFirstById($nDocumentID);
                $this->generateOutboxNumber($oDocument);
                $this->generateInboxNumberForRecipients($oDocument);

                $this->nextStage();
            }

        }

    }

    public function getByStageID($stageID)
    {

        #to do
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if (empty($nWorkflowID) || !is_numeric($nWorkflowID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('workflow_id' => $nWorkflowID));
        }

        $aWorkflow = array('workflow' => array(), 'response_code' => 1, 'response_message' => "success");


        return $this->createArrayResponse($aWorkflow, 'data');
    }

    public function setParam($key, $value)
    {
        $this->aParams[$key] = $value;
        return true;
    }

    public function getParams()
    {
        return $this->aParams;
    }
}
