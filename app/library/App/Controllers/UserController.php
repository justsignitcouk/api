<?php

namespace App\Controllers;

use App\Auth\Manager;
use App\Components\File\File;
use App\Components\Mail\Mail;
use App\Components\SMS\SMS;
use App\Constants\DefaultValues;
use App\Models\Workflow\DocumentWorkflow;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Database\Capsule\Manager as DB;
use MyCompose\LaravelModels\Contacts\Contact;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Companies;
use MyCompose\Model\Contacts\Avatar;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\MessagesQueue;
use MyCompose\Model\Contacts\Mobiles;
use MyCompose\Model\Contacts\MyContact;
use MyCompose\Model\Contacts\MyContactsGroup;
use MyCompose\Model\Contacts\MyContactsGroupContact;
use MyCompose\Model\Contacts\Names;
use MyCompose\Model\Departments;
use MyCompose\Model\Devices;
use MyCompose\Model\Document\DocumentFile;
use MyCompose\Model\Document\DocumentLayout;
use MyCompose\Model\Document\DocumentReference;
use MyCompose\Model\Document\Forward\ForwardUser;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\DocumentCcInvitations;
use MyCompose\Model\DocumentCcUser;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentsenderUser;
use MyCompose\Model\Document\DocumentsignData;
use MyCompose\Model\Document\DocumentsignerAccount;
use MyCompose\Model\Document\DocumentsignerInvitations;
use MyCompose\Model\Document\DocumentsignerUser;
use MyCompose\Model\DocumentToAccount;
use MyCompose\Model\DocumentToInvitations;
use MyCompose\Model\DocumentToUser;
use MyCompose\Model\DocumentTowhom;
use MyCompose\Model\Document\DocumentUserRead;
use MyCompose\Model\Draft\DraftDocuments;
use MyCompose\Model\Draft\DraftDocumentsenderUser;
use MyCompose\Model\FriendRequest;
use MyCompose\Model\Friends;
use MyCompose\Model\Invitations;
use MyCompose\Model\Profiles\IONumber;
use MyCompose\Model\Profiles\IONumberSeqences;
use MyCompose\Model\Profiles\UserProfileRequirements;
use MyCompose\Model\Signs;
use MyCompose\Model\UserAvatar;
use MyCompose\Model\UserEmailInvitation;
use MyCompose\Model\UserLink;
use MyCompose\Model\Users;
use MyCompose\Model\NeedHelp;
use MyCompose\LaravelModels\Account;
use MyCompose\LaravelModels\Friend;
use MyCompose\LaravelModels\User;
use MyCompose\LaravelModels\Workflow\Stage;
use MyCompose\LaravelModels\Workflow\State;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;

class UserController extends CrudResourceController
{
    private $user;
    private $oContact;

    public function all()
    {

        $oUsers = Users::find();
        return $this->createResourceCollectionResponse($oUsers);
    }

    public function me($id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $session = $this->authManager->getSession();
        $nUserID = $this->userService->getIdentity();

        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';


        $aParams = [
            'sIP' => $sIP,
            'sFCMToken' => $sFCMToken,
            'sBrowser' => $sBrowser,
            'sBrowserVersion' => $sBrowserVersion,
            'sPlatform' => $sPlatform,
            'sPlatformVersion' => $sPlatformVersion,
            'sDevice' => $sDevice,
            'sLanguages' => $sLanguages,

        ];


        $response = \App\Components\Auth\Manager::getLoginInfo($nUserID, $aParams, $session);
        return $this->createArrayResponse($response, 'data');
    }

    public function accounts($id)
    {
        $aResponse = array('accounts' => array(), 'response_code' => 1, 'response_message' => "success");

        $aAccounts = [];
        $aLinkUsers = [];
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if ($id != $this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oUsersLink = UserLink::find("user_id=$id");
        if ($oUsersLink->count() > 0) {
            foreach ($oUsersLink as $k => $oUserLink) {
                $nUserLinkId = $oUserLink->user_link_id;
                $oLinkUsers = User::where("id", $nUserLinkId)->with('ProfileImage', 'Accounts.Department.Branch.Company', 'Accounts.ProfileImage')->get();

                $aLinkUsers = array_merge($aLinkUsers, $oLinkUsers->toArray());
            }
        }

        $oUsers = User::where('id', $id)->with('ProfileImage', 'Accounts.Department.Branch.Company', 'Accounts.ProfileImage')->get();

        $aAllUsers = array_merge($aLinkUsers, $oUsers->toArray());
        return $this->createArrayResponse($aAllUsers, 'data');
    }

    public function authenticate()
    {

        $username = $this->request->getUsername();
        $password = $this->request->getPassword();

        $AccountType = \App\Auth\UsernameAccountType::ID;
        $session = $this->authManager->loginWithUsernamePassword($AccountType, $username,
            $password);

        $transformer = new \App\Transformers\UserTransformer;
        $transformer->setModelClass('MyCompose\Model\Users');

        $nUserId = $session->getIdentity();

        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';

        $aParams = [
            'sIP' => $sIP,
            'sFCMToken' => $sFCMToken,
            'sBrowser' => $sBrowser,
            'sBrowserVersion' => $sBrowserVersion,
            'sPlatform' => $sPlatform,
            'sPlatformVersion' => $sPlatformVersion,
            'sDevice' => $sDevice,
            'sLanguages' => $sLanguages,

        ];


        $response = \App\Components\Auth\Manager::getLoginInfo($nUserId, $aParams, $session);

        return $this->createArrayResponse($response, 'data');
    }

    public function authenticateToLink()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $nUserId = $this->userService->getIdentity();
        $username = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $AccountType = \App\Auth\UsernameAccountType::NAME;
        } else {
            $AccountType = \App\Auth\UsernameAccountType::MOBILE;
        }

        $session = $this->authManager->loginWithUsernamePassword($AccountType, $username,
            $password);

        $nUserLinkId = $session->getIdentity();
        $oUserLink = new UserLink();
        $oUserLink->user_id = $nUserId;
        $oUserLink->user_link_id = $nUserLinkId;
        $oUserLink->created_at = date("Y-m-d H:i:s");

        if (!$oUserLink->save()) {
            return $oUserLink->getMessages();
        }

        $response = [
            'status' => 'true',
            'user_id' => $nUserId,
            'user_link_id' => $nUserLinkId
        ];

        return $this->createArrayResponse($response, 'data');

    }

    public function register()
    {
        $this->db->begin();

        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];

        $display_name = $this->request->getPost('display_name');
        $first_name = $this->request->getPost('first_name');
        $middle_name = $this->request->getPost('middle_name');
        $last_name = $this->request->getPost('last_name');
        $email = $this->request->getPost('email');
        $mobile = $this->request->getPost('mobile');
        $sOrignPassword = $this->request->getPost('password');
        $active = 0;
        $created_date = date("Y-m-d H:i:s");
        $password = $this->security->hash($sOrignPassword);

        if (empty($email) && empty($mobile)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', ['error' => 'email or mobile required']);
        }


        if (empty($first_name) || empty($last_name)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', ['error' => 'First Name and Last Name is required']);
        }

        $oUser = Users::findFirst("email='" . $email . "'");
        if ($oUser) {
            return false;
        }

        $sIP = $this->request->hasPost("ip") ? $this->request->getPost("ip") : 'unknown';
        $sFCMToken = $this->request->hasPost("fcm_token") ? $this->request->getPost("fcm_token") : 'unknown';
        $sBrowser = $this->request->hasPost("browser") ? $this->request->getPost("browser") : 'unknown';
        $sBrowserVersion = $this->request->hasPost("browser_version") ? $this->request->getPost("browser_version") : 'unknown';
        $sPlatform = $this->request->hasPost("platform") ? $this->request->getPost("platform") : 'unknown';
        $sPlatformVersion = $this->request->hasPost("platform_version") ? $this->request->getPost("platform_version") : 'unknown';
        $sDevice = $this->request->hasPost("device") ? $this->request->getPost("device") : 'unknown';
        $sLanguages = $this->request->hasPost("languages") ? $this->request->getPost("languages") : 'unknown';


        $oUser = new Users();
        $oUser->email = $email;
        $oUser->mobile = $mobile;
        $oUser->mobile = $mobile;
        $oUser->created_at = $created_date;
        $oUser->created_by_ip = $sIP;
        $oUser->first_name = $first_name;
        $oUser->middle_name = $middle_name;
        $oUser->last_name = $last_name;
        $oUser->full_name = $first_name . (!empty($middle_name) ? " " . $middle_name : '') . " " . $last_name;
        $sUserName = Users::getUsername($first_name, $last_name);
        $oUser->username = $sUserName;
        $oUser->password = $password;
        $oUser->activated = 0;
        $oUser->verify_token = rand('90000000', '99999999');
        if (!$oUser->save()) {
            return $oUser->getMessages();
        }

        $oRecipients = Recipients::find("identifier='$email' or identifier='$mobile' ") ;
        if($oRecipients->count()>0){
            foreach($oRecipients as $oRecipient){
                $oRecipient->user_id = $oUser->id;
                $oRecipient->save();
            }
        }

        $AccountType = \App\Auth\UsernameAccountType::ID;
        $session = $this->authManager->loginWithUsernamePassword($AccountType, $oUser->id,
            $sOrignPassword);


        $aParams = [
            'sIP' => $sIP,
            'sFCMToken' => $sFCMToken,
            'sBrowser' => $sBrowser,
            'sBrowserVersion' => $sBrowserVersion,
            'sPlatform' => $sPlatform,
            'sPlatformVersion' => $sPlatformVersion,
            'sDevice' => $sDevice,
            'sLanguages' => $sLanguages,

        ];

        $nUserId = $session->getIdentity();
        $response = \App\Components\Auth\Manager::getLoginInfo($nUserId, $aParams, $session);

        $Identifier = !empty($email) ? $email : $mobile;
        $IdentifierType = !empty($email) ? 'email' : 'sms';
        $oMessagesQueue = new MessagesQueue();
        $oMessagesQueue->type = $IdentifierType;
        $oMessagesQueue->contact = $Identifier;
        $oMessagesQueue->template = 'register';
        $oMessagesQueue->recipient_type = 'user';
        $oMessagesQueue->user_id = $oUser->id;
        $oMessagesQueue->save();

        $this->db->commit();
        return $this->createArrayResponse($response, 'data');

    }

    public function resendSMSCode($id)
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }

        $oUser = Users::findFirstById($id);
        if (!$oUser) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }

        if (empty($oUser->mobile)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $rand = $oUser->verify_token;
        $sms = new SMS();
        $sms->to = $oUser->mobile;
        $sms->message = "Welcome to my compose, Your code number is " . $rand;
        $r = $sms->send();
        $response = [
            'status' => 'true',
            'user_id' => $oUser->id,
            'oUser' => $oUser
        ];

        return $this->createArrayResponse($response, 'data');
    }

    public function verifyEmailAndToken()
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];

        $verify_token = $this->request->getPost('verify_token');

        $error = false;


        if (empty($verify_token)) {
            $response['data_missing'][] = 'verify_token';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $user = Users::findFirst(" verify_token='" . $verify_token . "'");
        $aToken = array('oUser' => [], 'status' => false);
        if ($user) {
            $aToken = array('oUser' => [], 'status' => true);
            $aToken['oUser'] = $user;
        }


        return $this->createArrayResponse($aToken, 'data');
    }

    public function verfiyResetCode()
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];

        $resetCode = $this->request->getPost('code');
        $error = false;

        if (empty($resetCode)) {
            $response['data_missing'][] = 'Reset Code';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $user = Users::findFirst("verify_token='" . $resetCode . "'");
        $aToken = array('oUser' => [], 'status' => false);
        if ($user) {
            $aToken = array('oUser' => [], 'status' => true);
            $aToken['oUser'] = $user;
        }


        return $this->createArrayResponse($aToken, 'data');
    }

    public function getVerifyToken()
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];

        $sEmail = $this->request->getPost('email');

        $error = false;
        if (empty($sEmail)) {
            $response['data_missing'][] = 'email';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $sAccountType = 'email';
        if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {

            $sAccountType = 'email';
        } else {
            $sAccountType = 'mobile';
        }

        $oUser = Users::findFirst($sAccountType . "='" . $sEmail . "'");

        if (!$oUser) {
            return $this->createArrayResponse(['oUser' => false, 'status' => false], 'data');
        }

        if ($oUser->activated == 0) {

        }
        $aToken = array('oUser' => [], 'status' => false);
        if ($oUser) {
            $rand = rand(100000, 999999);
            $oUser->verify_token = $rand;
            if (!$oUser->save()) {
                return $oUser->getMessages();
            }

            $aToken = array('oUser' => [], 'status' => true);
            $aToken['oUser'] = $oUser;
        }

        $mail = new Mail();
        $sendmail = $mail->send($sEmail, 'MyCompose Reset Password', 'confirmation', ['sToken' => $oUser->verify_token]);

        return $this->createArrayResponse($aToken, 'data');
    }

    public function activateUser()
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];
        $userID = $this->request->getPost('id');

        $error = false;
        if (empty($userID)) {
            $response['data_missing'][] = 'id';
            $error = true;
        }
        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $oUser = Users::findFirst("id = '" . $userID . "'");
        if ($oUser) {
            $oUser->activated = 1;
            if ($oUser->save())
                $response['status'] = true;
            else
                $response['status'] = false;
        }


        $oUserProfileRequirements = new UserProfileRequirements();
        $oUserProfileRequirements->user_id = $userID;
        $oUserProfileRequirements->profile_requirements_id = '2';
        $oUserProfileRequirements->status = 'complete';
        $oUserProfileRequirements->created_at = date('Y-m-d H:i:s');
        $oUserProfileRequirements->save();

        return $this->createArrayResponse($response, 'data');

    }

    public function resetPassword()
    {
        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }

        $response = [
            'data_missing' => array()
        ];


        $verify_token = $this->request->getPost('verify_token');

        $error = false;
        if (empty($verify_token)) {
            $response['data_missing'][] = 'verify_token';
            $error = true;
            return $this->createArrayResponse($response, 'data');
        }


        $password = $this->request->getPost('password');

        $oUser = Users::findFirst("verify_token=$verify_token");
        if (!$oUser) {
            $response['message'] = 'Missing Data';
            $response['data_missing'][] = 'id';
            $response['data_missing'][] = $verify_token;
            return $this->createArrayResponse($response, 'data');

        }

        $userID = $oUser->id;


        if (empty($password)) {
            $response['data_missing'][] = 'password';
            $error = true;
            return $this->createArrayResponse($response, 'data');
        }


        $password = $this->security->hash($password);

        if ($oUser) {
            $oUser->password = $password;
            if ($oUser->save() && $oUser->activated == 1) {
                $oUser->verify_token = $this->security->hash(rand(1, 50));
                $oUser->save();
                $response['status'] = true;
            } else {
                $response['status'] = false;
                $response['message'] = 'Inactive User! Please activate user account';
            }
        }

        return $this->createArrayResponse($response, 'data');
    }

    public function unique()
    {

        $response = [
            'data_missing' => array()
        ];

        $field_db = $this->request->getPost('field_db');
        $field_value = $this->request->getPost('field_value');

        $error = false;
        if (empty($field_db)) {
            $response['data_missing'][] = 'field_db';
            $error = true;
        }
        if (empty($field_value)) {
            $response['data_missing'][] = 'field_value';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $checkExistUser = Users::findFirst($field_db . "='" . $field_value . "'");
        if ($checkExistUser) {
            $response = [
                'status' => true // false:= not unique
            ];
        }

        return $this->createArrayResponse($response, 'data');
    }

    public function exist()
    {

        if ($this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $response = [
            'data_missing' => array()
        ];

        $field_db = $this->request->getPost('field_db');
        $field_value = $this->request->getPost('field_value');

        $error = false;
        if (empty($field_db)) {
            $response['data_missing'][] = 'field_db';
            $error = true;
        }
        if (empty($field_value)) {
            $response['data_missing'][] = 'field_value';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $checkExistUser = Users::findFirst($field_db . "='" . $field_value . "'");
        if ($checkExistUser) {
            $response = [
                'status' => true // true:= value exist
            ];
        }
        return $this->createArrayResponse($response, 'data');
    }

    public function whitelist()
    {
        return [
            'firstName',
            'lastName',
            'password'
        ];
    }

    public function getDefaultAccount()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserId = $this->userService->getIdentity();

        $oAccount = Accounts::findFirst("user_id=$nUserId and is_default=1");
        if (!$oAccount) {
            $oAccount = [];
        }
        $aAccount = array('account' => $oAccount, 'response_code' => 1, 'response_message' => "success");
        //$aAccount['RecordsTotal'] = $oAccount->count();
        return $this->createArrayResponse($aAccount, 'data');
    }

    public function setCurrentAccount()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $this->request->getPost('account_id');
        $oUser = Users::findFirstById($nUserId);
//        $oUser->current_account = $nAccountId;
//        if (!$oUser->save()) {
//            throw new Exception(ErrorCodes::DATA_FAILED,'',['error'=>$oUser->getMessages()]);
//        }

        $oAccount = Account::where('id', $nAccountId)->with('ProfileImage', 'Department.Branch.Company', 'User')->get()->first();
        $aAccount = array('account' => $oAccount, 'response_code' => 1, 'response_message' => "success");
        //$aAccount['RecordsTotal'] = $oAccount->count();
        return $this->createArrayResponse($aAccount, 'data');
    }

    public function setDefaultAccount()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $this->request->getPost('account_id');
        $oUser = Users::findFirstById($nUserId);
        $oUser->current_account = $nAccountId;
        if (!$oUser->save()) {
            throw new Exception(ErrorCodes::DATA_FAILED, '', ['error' => $oUser->getMessages()]);
        }

        $oAccount = Account::where('id', $nAccountId)->with('ProfileImage', 'Department.Branch.Company', 'User')->get()->first();
        $aAccount = array('account' => $oAccount, 'response_code' => 1, 'response_message' => "success");
        //$aAccount['RecordsTotal'] = $oAccount->count();
        return $this->createArrayResponse($aAccount, 'data');
    }

    public function getSignatures($nUserId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $aSigns = [];
        $oSigns = Signs::find("user_id=$nUserId and sign_type='Signature' and deleted=0 ");
        $oSignInitials = Signs::find("user_id=$nUserId and sign_type='Initials' and deleted=0");
        $aSignsResponse = array('signs' => $oSigns->toArray(), 'initials' => $oSignInitials->toArray(), 'response_code' => 1, 'response_message' => "success");
        $aSignsResponse['RecordsTotal'] = $oSigns->count() + $oSignInitials->count();
        $aSignsResponse['RecordsTotalSign'] = $oSigns->count();
        $aSignsResponse['RecordsTotalInitials'] = $oSignInitials->count();

        return $this->createArrayResponse($aSignsResponse, 'data');
    }

    public function saveSignatures()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->request->getPost('user_id');

        if (empty($nUserId) || !is_numeric($nUserId)) {
            $nUserId = $this->userService->getIdentity();
        }

        $sSignData = $this->request->getPost("sign_data");

        if ($this->request->hasFiles()) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $key = $file->getKey();
                $size = $file->getSize();
                if ($key == 'sign_image') {
                    if ($size > '2097152') {
                        throw new Exception(ErrorCodes::POST_DATA_NOT_PROVIDED, 'Large File', array('sign_image size' => $size));
                    }
                    $path = 'files/' . $file->getName();
                    $file->moveTo($path);
                    $type = pathinfo($path, PATHINFO_EXTENSION);

                    $thumb = new \Imagick();
                    $thumb->readImage($path);
                    $thumb->setCompressionQuality(10);
                    $thumb->resizeImage(500, 196, \Imagick::FILTER_LANCZOS, 1);
                    $thumb->writeImage($path);
                    $thumb->clear();
                    $thumb->destroy();
                    $data = file_get_contents($path);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    unlink($path);
                    $sSignData = $base64;
                    break;
                }
            }

        }


        if (empty($sSignData)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('sign_data' => $sSignData));
        }


        $sSignDataWithWaterMark = $sSignData;//$this->addWaterMark($sSignData);

        $sSignType = 'Signature';
        if ($this->request->hasPost('sign_type')) {
            $sSignType = $this->request->getPost("sign_type");
        }

        $oSign = new Signs();
        $oSign->user_id = $nUserId;
        $oSign->sign_data = $sSignData;
        $oSign->sign_data_with_watermark = $sSignDataWithWaterMark;
        $oSign->sign_type = $sSignType;
        $oSign->created_at = date("Y-m-d H:i:s");
        $oSign->save();
        $aSign = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aSign, 'data');
    }


    public function getDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nPostedUserID = $this->request->getPost("user_id");

        if (empty($nPostedUserID) || !is_numeric($nPostedUserID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('user_id' => $nPostedUserID));
        }

        $oSigns = Signs::findFirst("user_id=$nPostedUserID and is_default=1");

        if (!$oSigns) {
            $oAllSigns = Signs::findFirst("user_id=$nPostedUserID");

            if (!$oAllSigns) {
                $aResult = array('sign' => array(), 'response_code' => 0, 'response_message' => "no_default_signiture");
                return $this->createArrayResponse($aResult, 'data');
            }

            $oSigns = $oAllSigns;
        }

        $nSignID = $oSigns->id;
        if (!is_null($nSignID) && !empty($nSignID)) {
            $oSign = Signs::findFirstById($nSignID);
            if (!$oSign) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
            }
            $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aResult, 'data');
        }

        $oSign = Signs::findFirst("user_id=" . $nUserID);
        if (!$oSign) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
        }
        $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    public function setDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nPostedUserID = $this->request->getPost("user_id");
        $nSigntID = $this->request->getPost("sign_id");
        if (empty($nPostedUserID) || !is_numeric($nPostedUserID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('user_id' => $nPostedUserID));
        }

        $oUser = Users::findFirstById($nPostedUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('user_id' => $nPostedUserID));
        }

        $oSigns = Signs::find("user_id=$nPostedUserID");

        if (!$oSigns) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSigntID));
        }

        foreach ($oSigns as $sign) {
            if ($sign->id == $nSigntID) {
                $sign->is_default = 1;
            } else {
                $sign->is_default = 0;
            }
            if (!$sign->save()) {
                return $sign->getMessages();
            }
        }

        $aResult = array('response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    private function addWaterMark($sSignData)
    {
        $manager = new \Intervention\Image\ImageManager();
        $img = $manager->make($sSignData);
        $type = 'png';
        $image_width = $img->width();
        $image_height = $img->height();
        $img2 = $manager->make('assets/images/logo_small.png')->resize($image_width, $image_height)->opacity(30);
        $img->insert($img2, 'top', 10, 10);
        $img->encode('png');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        $sSignDataWithWaterMark = $base64;

        return $sSignDataWithWaterMark;
    }

    public function deleteSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->request->getPost("user_id");
        if (empty($nUserId) || !is_numeric($nUserId)) {
            $nUserId = $this->userService->getIdentity();
        }


        $nSignID = $this->request->getPost("id");
        # check if the sign default

        $oSign = Signs::findFirst("user_id=$nUserId and id=$nSignID");
        if ($oSign) {
            $oSign->deleted = 1;
            $oSign->deleted_at = date('Y-m-d H:i:s');
            $oSign->save();
            $aSign = array('sign' => '', 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aSign, 'data');
        }

        $aSign = array('sign' => '', 'response_code' => 1, 'response_message' => "not found");
        $aSign = array('sign' => '', 'response_code' => 1, 'response_message' => "not found");
        return $this->createArrayResponse($aSign, 'data');
    }

    public function editable()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $sKey = $this->request->getPost("name");
        $sValue = $this->request->getPost("value");

        switch ($sKey) {
            case 'email' :
                throw new Exception(ErrorCodes::POST_DATA_NOT_PROVIDED);
            case 'mobile' :
                throw new Exception(ErrorCodes::POST_DATA_NOT_PROVIDED);
        }

        $oUser = Users::findFirstById($nUserId);
        if ($oUser) {
            if (property_exists($oUser, $sKey)) {
                $oUser->{$sKey} = $sValue;
                if (!$oUser->save()) {
                    throw new Exception(ErrorCodes::DATA_FAILED, null, ['error' => $oUser->getMessage()]);
                }
                $aResponse = array('response_code' => 1, 'response_message' => "success");
                return $this->createArrayResponse($aResponse, 'data');
            }
        }
    }

    public function check_mail()
    {
        $email = $this->request->getPost('email');
        $check_mail = Users::findFirst("email" . "='" . $email . "'");

        if (!empty($check_mail)) {

            $check_mail_flag = true;

        } else {

            $check_mail_flag = false;

        }

        $mail_response = array('email' => $check_mail_flag, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($mail_response, 'data');

    }

    public function check_phone()
    {

        $phone = $this->request->getPost('phone');
        $check_phone = Users::findFirst("mobile" . "='" . $phone . "'");

        if (!empty($check_phone)) {

            $check_phone_flag = true;

        } else {

            $check_phone_flag = false;

        }


        $phone_response = array('email' => $check_phone_flag, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($phone_response, 'data');


    }

    public function getProfileCompleteness($nUserId)
    {
        $weightedAvgCompleteness = 0;
        $emailWeight = 0.3;
        $mobileWeight = 0.2;
        $profileImageWeight = 0.1;
        $signatureWeight = 0.4;

        $oUser = Users::findFirst("id" . "='" . $nUserId . "'");
        if (!$oUser)
            throw new Exception(ErrorCodes::USER_NOT_FOUND, 'User not exists');

        $email = $oUser->email;
        if (!empty($email)) {
            $weightedAvgCompleteness += (1 * $emailWeight);
        }
        $mobile = $oUser->mobile;
        if (!empty($mobile)) {
            $weightedAvgCompleteness += (1 * $mobileWeight);
        }
        $profileImage = $oUser->profile_image;
        if ($profileImage) {
            $weightedAvgCompleteness += (1 * $profileImageWeight);
        }

        $oSignature = Signs::findFirst("user_id" . "='" . $nUserId . "'");
        if ($oSignature) {
            $weightedAvgCompleteness += (1 * $signatureWeight);
        }
        $totalWeights = $emailWeight + $mobileWeight + $profileImageWeight + $signatureWeight;

        $weightedAvgCompleteness /= $totalWeights;
        $weightedAvgCompleteness *= 100;
        $profile_complete_response = array('userId' => $nUserId, 'email' => $email, 'mobile' => !empty($mobile) ? true : false,
            'profileImage' => !empty($profileImage) ? true : false, 'signature' => $oSignature ? true : false, 'completeness' => $weightedAvgCompleteness, 'response_code' => 1,
            'response_message' => "success");
        return $this->createArrayResponse($profile_complete_response, 'data');
    }

    public function need_help()
    {

        $aFormData = $this->request->getPost('form_data');

        $oNeedHelp = new NeedHelp();

        $oNeedHelp->title = $aFormData['title'];
        $oNeedHelp->name = $aFormData['name'];
        $oNeedHelp->email = $aFormData['email'];
        $oNeedHelp->message = $aFormData['message'];

        if (!$oNeedHelp->save()) {
            $messages = $oNeedHelp->getMessages();

            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];
            return $response;
        }


        $need_help_response = array('status' => true);
        return $this->createArrayResponse($need_help_response, 'data');

    }

    public function badges($id)
    {

        $aBadges = [];
        $nUserId = $this->userService->getIdentity();
        if ($nUserId != $id) {
            throw new Exception(ErrorCodes::AUTH_TOKEN_INVALID, 'Token Invalid', array('user_id' => $id));
        }

        $oRecipients = Recipients::find("user_id=$nUserId and can_read=1");


        $aBadges = ['inbox' => 0, 'sent' => 0, 'pending' => 0, 'rejected' => 0, 'completed' => 0, 'draft' => 0, 'template' => 0];
        $aDocumentID = [];
        foreach ($oRecipients as $oRecipient) {
            $sDocumentStatus = $oRecipient->Document->status;
            $sDocumentID = $oRecipient->Document->id;
            if ($sDocumentStatus == 'completed' || $sDocumentStatus == 'inprogress') {

                $bUserRead = $oRecipient->is_read;
                $bCanRead = $oRecipient->can_read;
                if ( $bUserRead==0 && $bCanRead==1 && $sDocumentStatus == 'completed') {
                    $aBadges['inbox']++;
                }

            }

            if ( $sDocumentStatus == 'inprogress' ) {
                $aBadges['pending']++;
            }

            if ($sDocumentStatus == 'rejected') {
                $aBadges['rejected']++;
            }

            if ($sDocumentStatus == 'completed') {
                $aBadges['completed']++;
            }

        }

        $oDocuments = Documents::find("user_id=" . $nUserId);

        foreach ($oDocuments as $oDocument) {
            if ($oDocument->status == 'template') {
                $aBadges['template']++;
            }

            if ($oDocument->status == 'draft') {
                $aBadges['draft']++;
            }

            if ($oDocument->status != 'template' && $oDocument->status != 'draft') {
                $aBadges['sent']++;
            }


        }


        $aBadgesWithTotal = array('badges' => $aBadges, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aBadgesWithTotal, 'data');
    }

    public function draft($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nPostedUserId = $id;
        if (empty($nPostedUserId) || !is_numeric($nPostedUserId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array('user_id' => $nPostedUserId));
        }

        $oUser = Users::findFirst("id=$nUserId ");
        if (!$oUser) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }


        $aDraft = array('draft' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocuments = DraftDocumentSenderUser::find(array("user_id=$nPostedUserId"));

        foreach ($oDocuments as $oDocument) {
            $oDocument = $this->getDraftDocumentById($oDocument->document_id, 'inbox');
            $aDraft['document'][] = $oDocument;
        }
        $aDraft['RecordsTotal'] = sizeof($aDraft['document']);
        return $this->createArrayResponse($aDraft, 'data');
    }

    public function inbox($id, $sFilter)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $id;


        $oUser = Users::findFirst("id=$nUserId ");

        if (!$oUser) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        switch ($sFilter) {
            case 'pending' :
                $status = ['start', 'inprogress'];
                break;
            case 'completed' :
                $status = ['completed'];
                break;
            case 'inbox' :
                $status = ['completed'];
                break;
            case 'rejected' :
                $status = ['rejected'];
                break;
            case 'draft' :
                $status = ['draft'];
                break;
            case 'sent' :
                $status = ['sent'];
                break;
            default :
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        if (!in_array('sent', $status) && !in_array('draft', $status)) {


            $aInbox = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
            $aDocuments = [];
            $sIdentifier = !empty($oUser->email) ? $oUser->email : $oUser->mobile;
            $oRecipients = Recipients::find(["(user_id=" . $nUserId . " or identifier='" . $sIdentifier . "') and can_read=1 ", 'order' => 'created_at desc']);
            if ($oRecipients->count() > 0) {

                foreach ($oRecipients as $oRecipient) {
                    $bIsRead = false;
                    $nDocumentID = $oRecipient->document_id;
                    $oDocument = Documents::findFirstById($nDocumentID);
                    if (in_array($oDocument->status, $status)) {
                        $aDocuments[$nDocumentID] = $oDocument->toArray();
                        $oSender = Users::findFirstById($oDocument->user_id);
                        $aDocuments[$nDocumentID]['sender'] = $oSender->toArray();
                        $aDocuments[$nDocumentID]['sender']['avatar'] = !empty($oSender->Avatar) ? $oSender->Avatar : '';
                        if (!isset($aDocuments[$nDocumentID]['is_read']) || (isset($aDocuments[$nDocumentID]['is_read']) && $aDocuments[$nDocumentID]['is_read'] == false)) {
                            $bIsRead = $oRecipient->is_read ? true : false;
                            $aDocuments[$nDocumentID]['is_read'] = $bIsRead;
                        }
                        $aDocuments[$nDocumentID]['has_attachments'] = false;
                        $oDocumentFiles = DocumentFile::findFirst("document_id=" . $nDocumentID ) ;
                        if($oDocumentFiles){
                            $aDocuments[$nDocumentID]['has_attachments'] = true;
                        }
                    }
                }


            }
        } else {

            $oDocuments = Documents::find("user_id=" . $nUserId);

            $aDocuments = [];

            foreach ($oDocuments as $oDocument) {
                if (in_array($oDocument->status, $status) || (in_array('sent', $status) && $oDocument->status != 'draft' && $oDocument->status != 'template')) {
                    $nDocumentID = $oDocument->id;
                    $aDocuments[$nDocumentID] = $oDocument->toArray();
                    $oSender = Users::findFirstById($oDocument->user_id);
                    $oRecipients = Recipients::find("document_id=$nDocumentID and type='To' ");
                    $aRecipients = [];
                    $sRecipients = '';
                    if($oRecipients->count()>0){
                        $aRecipients = $oRecipients->toArray();
                        $size = '' ;
                        $and = '' ;
                        if(is_array($aRecipients)){
                            $size = $oRecipients->count()  - 1;
                            if($size!=0){
                                $and =   " (" . $size . ")";
                            }

                        }
                        $sRecipients = $aRecipients[0]['identifier'] . $and;
                    }
                    $aDocuments[$nDocumentID]['sender'] = $oSender->toArray();
                    $aDocuments[$nDocumentID]['recipients'] = $sRecipients;
                    $aDocuments[$nDocumentID]['sender']['avatar'] = !empty($oSender->Avatar) ? $oSender->Avatar : '';

                    $aDocuments[$nDocumentID]['has_attachments'] = false;
                    $oDocumentFiles = DocumentFile::findFirst("document_id=" . $nDocumentID ) ;
                    if($oDocumentFiles){
                        $aDocuments[$nDocumentID]['has_attachments'] = true;
                    }

                }

            }
        }

        $aInbox_new = array('document' => [], 'response_code' => 1, 'response_message' => "success");
        foreach ($aDocuments as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }


    public function sendFriendRequest()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();

        $nFriendId = $this->request->getPost("friend_id");

        if (empty($nUserId) || !is_numeric($nUserId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $nUserId]));
        }

        if (empty($nFriendId) || !is_numeric($nFriendId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['friend_id' => $nFriendId]));
        }

        $oFriendRequest = FriendRequest::findFirst("user_id = $nUserId and requester_id = $nFriendId");

        if ($oFriendRequest) {
            throw new Exception(ErrorCodes::FRIEND_REQUEST_EXISTS, 'Friend request exists', []);
        }

        $oFriendRequest = new FriendRequest();
        $oFriendRequest->user_id = $nUserId;
        $oFriendRequest->requester_id = $nFriendId;
        $oFriendRequest->created_at = date("Y-m-d H:i:s");

        if (!$oFriendRequest->save()) {
            $aFriendRequestResponse = array('response_code' => 0, 'response_message' => 'Failed to send request');
            return $this->createArrayResponse($aFriendRequestResponse, 'data');
        }

        $aFriendRequestResponse = array('friend_request' => $oFriendRequest->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aFriendRequestResponse, 'data');
    }

    public function sendEmailInvitation()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();

        $nEmail = $this->request->getPost("email");
        $nCustomMessage = $this->request->getPost("custom_message");

        if (empty($nUserId) || !is_numeric($nUserId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $nUserId]));
        }

        if (empty($nEmail)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['email' => $nEmail]));
        }

        $oEmailInvitation = new UserEmailInvitation();
        $oEmailInvitation->user_id = $nUserId;
        $oEmailInvitation->email = $nEmail;
        if (!empty($nCustomMessage)) {
            $oEmailInvitation->custom_message = $nCustomMessage;
        }
        $oEmailInvitation->created_at = date("Y-m-d H:i:s");

        if (!$oEmailInvitation->save()) {
            $aFriendRequestResponse = array('response_code' => 0, 'response_message' => 'Failed to send email');
            return $this->createArrayResponse($aFriendRequestResponse, 'data');
        }

        $userObject = Users::findFirst("id = $nUserId");
        $userName = $userObject->user_name;
        $mail = new Mail();
        $messageParams = ['sUsername' => $userName];

        if (!empty($nCustomMessage)) {
            $messageParams['sCustomMessage'] = $nCustomMessage;
        }

        $sendmail = $mail->send($nEmail, 'MyCompose invitation from ' . $userName, 'invitation', $messageParams);

        $aFriendRequestResponse = array('friend_request' => $oEmailInvitation->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aFriendRequestResponse, 'data');
    }

    public function getContacts($userId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $userId = $this->userService->getIdentity();
        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }

        //$aMyContact = MyContact::findByUserId($userId);
        $aContacts = Contacts::findByUserId($userId);
        $aResponse = array('mycontacts' => $aContacts, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getGroups($userId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $userId = $this->userService->getIdentity();
        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }

        $aMyContactGroup = MyContactsGroup::findByUserId($userId);

        $aResponse = array('my_contact_group' => $aMyContactGroup, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getLayouts($userId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $userId = $this->userService->getIdentity();

        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }

        $aLayouts = DocumentLayout::find("user_id=" . $userId . " and deleted=0");


        $aResponse = array('layouts' => $aLayouts, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function saveLayout()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $userId = $this->userService->getIdentity();

        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }

        $header = $this->request->getPost('header');
        $footer = $this->request->getPost('footer');
        $name= $this->request->getPost('layout_name');
        $oDocumentLayout = new DocumentLayout();
        $oDocumentLayout->header = $header;
        $oDocumentLayout->footer = $footer;
        $oDocumentLayout->user_id = $userId;
        $oDocumentLayout->name = $name;
        $oDocumentLayout->created_at = date('Y-m-d H:i:s');
        $oDocumentLayout->save();



        $aResponse = array('layout' => $oDocumentLayout, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function deleteLayout()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $userId = $this->userService->getIdentity();

        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }
        $id = $this->request->getPost('id');
        $oDocumentLayout = DocumentLayout::findFirst("id=$id and user_id=$userId") ;
        if(!$oDocumentLayout){
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }

        $oDocument = Documents::findFirst("layout_id=". $id );
        $hard_delete = false;
        if(!$oDocument){
            $oDocumentLayout->delete();
            $hard_delete = true;
        }
        $oDocumentLayout->deleted  = 1 ;
        $oDocumentLayout->deleted_at = date('Y-m-d H:i:s');
        $oDocumentLayout->save();
        $aResponse = array('layout' => $oDocumentLayout, 'hard_delete' => $hard_delete, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function addGroup()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $userId = $this->userService->getIdentity();
        $name = $this->request->getPost('name');
        $oGroup = MyContactsGroup::findFirst("name='" . $name . "'");
        if (!$oGroup) {
            $oGroup = new MyContactsGroup();
            $oGroup->name = $name;
            $oGroup->user_id = $userId;
            $oGroup->created_at = date('Y-m-d H:i:s');
            $oGroup->save();

        }

        $aResponse = array('my_contact_group' => $oGroup, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function addContact()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $userId = $this->userService->getIdentity();
        $sName = $this->request->getPost('name');
        $sEmail = $this->request->getPost('email');
        $sMobile = $this->request->getPost('mobile');
        $sGroups = $this->request->getPost('groups');
        $nContactID = false;
        if (empty($sName) && empty($sEmail) && empty($sMobile)) {
            throw new Exception(ErrorCodes::DATA_FAILED);
        }
        $oContact = new Contacts();
        $oContact->user_id = $userId;
        $oContact->created_at = date('Y-m-d H:i:s');
        if (!$oContact->save()) {
            dd($oContact->getMessages());
        }
        if (!empty($sName)) {


            $oContact->display_name = $sName;
            $oContact->full_name = $sName;
            $oContact->created_at = date('Y-m-d H:i:s');
            $aName = explode(" ", $sName);
            if (sizeof($aName) == 2) {
                $oContact->first_name = $aName[0];
                $oContact->last_name = $aName[1];
            }
            if (sizeof($aName) == 3) {
                $oContact->first_name = $aName[0];
                $oContact->middle_name = $aName[1];
                $oContact->last_name = $aName[2];
            }
            if (sizeof($aName) > 3) {
                $oContact->first_name = $aName[0];
                $oContact->middle_name = $aName[1];
                $oContact->last_name = last($aName);
            }


            if (!$oContact->save()) {
                echo "11";
                dd($oContact->getMessages());

            }
        }

        if (!empty($sEmail)) {
            $oEmail = new Emails();
            $oEmail->email = $sEmail;
            $oEmail->contact_id = $oContact->id;
            $oEmail->user_id = $userId;
            $oEmail->created_at = date('Y-m-d H:i:s');
            $oEmail->save();
            if (!$oEmail->save()) {
                dd($oEmail->getMessages());

            }
        }

        if (!empty($sMobile)) {
            $oMobile = new Mobiles();
            $oMobile->mobile = $sMobile;
            $oMobile->contact_id = $oContact->id;
            $oMobile->created_at = date('Y-m-d H:i:s');
            $oMobile->user_id = $userId;

            if (!$oMobile->save()) {
                dd($oMobile->getMessages());

            }
        }

        if(!empty($sGroups)){
            $aGroups = explode(',', $sGroups);
            foreach($aGroups as $sGroupName){
                $oGroup = MyContactsGroup::findFirst("name like '$sGroupName' ") ;
                if(!$oGroup){
                    $oGroup = new MyContactsGroup();
                    $oGroup->name = $sGroupName  ;
                    $oGroup->user_id = $userId  ;
                    $oGroup->created_at = date('Y-m-d H:i:s') ;
                    $oGroup->save();
                }

                $oMyContactsGroupContact = new MyContactsGroupContact();
                $oMyContactsGroupContact->contact_id = $oContact->id;
                $oMyContactsGroupContact->group_id = $oGroup->id;
                $oMyContactsGroupContact->created_at = date('Y-m-d H:i:s') ;
                $oMyContactsGroupContact->save();

            }
        }
        $aResponse = array('contact' => $oContact, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }


    public function editContact()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $userId = $this->userService->getIdentity();
        $nContactID = $this->request->getPost('id');
        $sName = $this->request->getPost('name');
        $sEmail = $this->request->getPost('email');
        $sMobile = $this->request->getPost('mobile');
        $sGroups = $this->request->getPost('groups');

        if (empty($sName) && empty($sEmail) && empty($sMobile)) {
            throw new Exception(ErrorCodes::DATA_FAILED);
        }
        $oContact = Contacts::findFirst("id=" . $nContactID);
        if (!$oContact) {
            dd('error');
        }

        if (!empty($sName)) {

            $oContact->display_name = $sName;
            $oContact->full_name = $sName;
            $oContact->created_at = date('Y-m-d H:i:s');
            $aName = explode(" ", $sName);
            if (sizeof($aName) == 2) {
                $oContact->first_name = $aName[0];
                $oContact->last_name = $aName[1];
            }
            if (sizeof($aName) == 3) {
                $oContact->first_name = $aName[0];
                $oContact->middle_name = $aName[1];
                $oContact->last_name = $aName[2];
            }
            if (sizeof($aName) > 3) {
                $oContact->first_name = $aName[0];
                $oContact->middle_name = $aName[1];
                $oContact->last_name = last($aName);
            }


            if (!$oContact->save()) {
                echo "11";
                dd($oContact->getMessages());

            }
        }

        if (!empty($sEmail)) {
            $oEmail = Emails::findFirst("contact_id=" . $nContactID);
            if (!$oEmail) {
                $oEmail = new Emails();
                $oEmail->contact_id = $oContact->id;
                $oEmail->created_at = date('Y-m-d H:i:s');
            }
            $oEmail->email = $sEmail;
            if (!$oEmail->save()) {
                dd($oEmail->getMessages());

            }
        }

        if (!empty($sMobile)) {

            $oMobile = Mobiles::findFirst("contact_id=" . $nContactID);
            if (!$oMobile) {
                $oMobile = new Mobiles();
                $oMobile->contact_id = $oContact->id;
                $oMobile->created_at = date('Y-m-d H:i:s');
            }

            $oMobile->mobile = $sMobile;
            if (!$oMobile->save()) {
                dd($oMobile->getMessages());

            }
        }

        if(!empty($sGroups)){
            $oMyContactsGroupContact = MyContactsGroupContact::find("contact_id=" . $oContact->id );
            if($oMyContactsGroupContact->count()>0){
                $oMyContactsGroupContact->delete();
            }

            $aGroups = explode(',', $sGroups);
            foreach($aGroups as $sGroupName){
                $oGroup = MyContactsGroup::findFirst("name like '$sGroupName' ") ;
                if(!$oGroup){
                    $oGroup = new MyContactsGroup();
                    $oGroup->name = $sGroupName  ;
                    $oGroup->user_id = $userId  ;
                    $oGroup->created_at = date('Y-m-d H:i:s') ;
                    $oGroup->save();
                }

                $oMyContactsGroupContact = new MyContactsGroupContact();
                $oMyContactsGroupContact->contact_id = $oContact->id;
                $oMyContactsGroupContact->group_id = $oGroup->id;
                $oMyContactsGroupContact->created_at = date('Y-m-d H:i:s') ;
                $oMyContactsGroupContact->save();

            }
        }


        $aResponse = array('contact' => $oContact, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getFriendRequests($userId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if (empty($userId) || !is_numeric($userId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['user_id' => $userId]));
        }


        //$oFriends =  Friend::where("requester_id", $userId)->orWhere("requested_id",$userId)->with(['User','Requster'])->get();
        $oFriendRequests = FriendRequest::find("requester_id=" . $userId . " or requested_id=" . $userId);
        $aFriends = $oFriendRequests->toArray();
        foreach ($oFriendRequests as $k => $v) {
            if ($v->requester_id == $userId) {
                $aFriends[$k]['contact'] = $v->Requested;
            } else {
                $aFriends[$k]['contact'] = $v->Requester;
            }

        }
        return $this->createArrayResponse($aFriends, 'data');
    }

    public function acceptFriendRequest($friendId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $userId = $this->userService->getIdentity();

        if (empty($friendId) || !is_numeric($friendId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['requested_id' => $friendId]));
        }

        $oFriendRequest = FriendRequest::findFirst("requester_id = $friendId");

        if (!$oFriendRequest) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'Friend Request not found!', array(['requester_id' => $friendId]));
        }

        $oFriend = new Friends();
        $oFriend->requested_id = $friendId;
        $oFriend->requester_id = $userId;
        $oFriend->created_at = date("Y-m-d H:i:s");;

        if ($oFriendRequest->delete()) {
            if ($oFriend->save()) {
                $aRequest = array('friend_request' => $oFriendRequest->toArray(), 'response_code' => 1, 'response_message' => "success");
                return $this->createArrayResponse($aRequest, 'data');
            }
        }
        $aRequest = array('response_code' => 0, 'response_message' => "failed");
        return $this->createArrayResponse($aRequest, 'data');
    }

    public function declineFriendRequest($friendId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $userId = $this->userService->getIdentity();

        if (empty($friendId) || !is_numeric($friendId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array(['requested_id' => $friendId]));
        }

        $oFriendRequest = FriendRequest::findFirst("requester_id = $friendId");

        if (!$oFriendRequest) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'Friend Request not found!', array(['requester_id' => $friendId]));
        }

        if ($oFriendRequest->delete()) {
            $aRequest = array('friend_request' => $oFriendRequest->toArray(), 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aRequest, 'data');
        }

        $aRequest = array('response_code' => 0, 'response_message' => "failed");
        return $this->createArrayResponse($aRequest, 'data');
    }

    private function getDocumentByID($nDocumentID, $status = 'inbox')
    {

        if ($status == 'inbox') {
            $oDocument = \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                'ToUser.User.ProfileImage', 'CcUser.User.ProfileImage', 'SignerUser.User.ProfileImage', 'SenderUser.User.ProfileImage',
            ])->where('id', $nDocumentID)->get()->first();


            $getIfRead = Recipients::findFirst("user_id=" . $this->userService->getIdentity() . " and document_id=" . $oDocument->id . " and and can_read=1");
            $bIsRead = $getIfRead ? true : false;
            $oDocument['is_read'] = $bIsRead;
            return $oDocument;
        }

        return \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
            'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage', 'SenderAccount.Account.User',
        ])->where('id', $nDocumentID)->where('status', $status)->get()->first();
    }

    private function getDraftDocumentByID($nDocumentID, $status = 'inbox')
    {

        if ($status == 'inbox') {
            $oDocument = \MyCompose\LaravelModels\DraftDocument\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                'ToUser.User.ProfileImage', 'CcUser.User.ProfileImage', 'SignerUser.User.ProfileImage', 'SenderUser.User.ProfileImage',
            ])->where('id', $nDocumentID)->get()->first();

            $getIfRead = Recipients::findFirst("user_id=" . $this->userService->getIdentity() . " and document_id=" . $oDocument->id . " and can_read=1 ");
            $bIsRead = $getIfRead ? true : false;
            $oDocument['is_read'] = $bIsRead;
            return $oDocument;
        }

        return \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
            'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage', 'SenderAccount.Account.User',
        ])->where('id', $nDocumentID)->where('status', $status)->get()->first();
    }

    public function sign()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nPostedUserId = $this->request->getPost("user_id");
        $nDocumentID = $this->request->getPost("document_id");

        $nStateID = $this->request->getPost("state_id");
        $nSignID = $this->request->getPost("sign_id");

        $this->db->begin();

        if (empty($nPostedUserId) || !is_numeric($nPostedUserId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array('user_id' => $nPostedUserId));
        }

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '2', array('document_id' => $nDocumentID));
        }

        if (empty($nSignID) || !is_numeric($nSignID)) {
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and is_default=1');
            if (!$oSign) {
                $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature"');
                if (!$oSign) {
                    throw new Exception(ErrorCodes::DATA_MISSING, '3', array('account' => "account didn't has sign"));
                }
            }

        } else {
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and id=' . $nSignID);
            if (!$oSign) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', []);
            }
        }

        $nSignID = $oSign->id;
        $oDefaultSign = Signs::findFirst("user_id=" . $nUserID . ' and sign_type="Signature" and id=' . $nSignID);

        if (!$oDefaultSign) {
            throw new Exception(ErrorCodes::DATA_MISSING, $nUserID . 'add sign' . $nSignID, array('sign' => $nSignID));

        }
        $oDocument = Documents::findFirst("id=$nDocumentID");
        $oDocumentSignerUser = null;

        $oDocumentSignerUser = DocumentSignerUser::findFirst("user_id='$nPostedUserId' and document_id=$nDocumentID ");
        if (!$oDocumentSignerUser) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', []);
        }

        $oDocumentSignerUser->is_sign = 1;
        $oDocumentSignerUser->sign_at = date("Y-m-d H:i:s");
        $oDocumentSignerUser->sign_id = $nSignID;
        $sSignCode = $nDocumentID . '.' . $nPostedUserId . '.' . $oDocumentSignerUser->user_id;


        $sSignCodeWithUrl = $this->config->web->host . '/' . $this->config->web->sign_validation_action . '/' . $sSignCode;
        $qrImage = 'data:image/png;base64,' . $this->qr->getBarcodePNG($sSignCodeWithUrl, "QRCODE");

        $oDefaultSignWithQR = $this->addQRToSign($qrImage, $oDefaultSign->sign_data);

        $oDocumentSignData = new DocumentSignData();
        $oDocumentSignData->sign_code = $sSignCode;
        $oDocumentSignData->sign_image = $oDefaultSignWithQR;
        $oDocumentSignData->created_at = date("Y-m-d H:i:s");
        if (!$oDocumentSignData->save()) {
            throw new Exception(ErrorCodes::DATA_FAILED, '', []);
        }

        $oDocumentSignerUser->sign_data_id = $oDocumentSignData->id;

        $aDocumentSigners = array('oDocumentSignee' => [], 'status' => false);

        if ($oDocumentSignerUser->save()) {

            $nIsSignCounter = 0;
            # To-Do : check all signer (user,department,role)
            $oAllDocumentSigners = DocumentSignerUser::find("document_id=$nDocumentID ");
            foreach ($oAllDocumentSigners as $oSigner) {
                if ($oSigner->is_sign == 1) {
                    $nIsSignCounter++;
                }
            }

            if ($oAllDocumentSigners->count() == $nIsSignCounter) {
                if (empty($nStateID) || !is_numeric($nStateID)) {
                    $oDocumentWorkFlow = DocumentWorkflow::where("document_id", $nDocumentID)->first();
                    $oStage = Stage::find($oDocumentWorkFlow->wf_current_stage_id)->first();
                    $oStageState = State::where("stage_id", $oStage->id)->first();
                    $nStateID = $oStageState->id;
                }

                $oStageState = State::find($nStateID)->first();
                $nNextStage = $oStageState->next_stage_id;

                $oDocument = Documents::findFirstById($nDocumentID);
                $oDocumentWorkFlow = DocumentWorkflow::where("document_id", $nDocumentID)->first();
                $oDocumentWorkFlow->wf_current_stage_id = $nNextStage;
                if (!$oDocumentWorkFlow->save()) {
                    throw new Exception(ErrorCodes::DATA_FAILED, '', array('error' => $oDocumentWorkFlow->getMessage()));
                }

                $oDocument->status = 'completed';
                $oDocument->save();
            }

            $aDocumentSigners['oDocumentSignee'] = $oDocumentSignerUser;
            $aDocumentSigners['status'] = true;
        }

        $this->db->commit();

        return $this->createArrayResponse($aDocumentSigners, 'data');

    }

    private function addQRToSign($fQR, $fSign)
    {
        $manager = new \Intervention\Image\ImageManager();
        $img = $manager->make($fSign);
        $type = 'png';
        $image_width = $img->width();
        $image_height = $img->height();
        $img2 = $manager->make($fQR);
        $img->insert($img2, 'bottom-right', 0, 0);
        $img->encode('png');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        $sSignDataWithQR = $base64;

        return $sSignDataWithQR;
    }

    public function testSms($phone)
    {
        $sms = new SMS();
        $sms->to = $phone;
        $sms->message = "mycompose";
        $response = $sms->send();
        return $response;
    }

    public function confirm()
    {

        $sIdentifier = $this->request->getPost("sIdentifier");
        $sIdentifierType = $this->request->getPost("type");
        $code = $this->request->getPost("code");
        if (empty($sIdentifier)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', ['error' => 'email or mobile required']);
        }
        if (empty($code)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', ['error' => 'code required']);
        }

        $oUser = Users::findFirst("$sIdentifierType='" . $sIdentifier . "'");
        if (!$oUser) {
            throw new Exception(ErrorCodes::USER_NOT_FOUND, '', ['error' => 'user not exist']);
        }

        $verify_token = $oUser->verify_token;
        if ($verify_token != $code) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'wrong code']);
        }

        $oUser->activated = 1;
        $oUser->save();

        $oUserProfileRequirements = new UserProfileRequirements();
        $oUserProfileRequirements->user_id = $oUser->id;
        $oUserProfileRequirements->profile_requirements_id = 2;
        $oUserProfileRequirements->created_at = date('Y-m-d H:i:s');
        $oUserProfileRequirements->status = 'complete';
        $oUserProfileRequirements->save();

        $aResponse = array('response' => [], 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function setPassword()
    {

        $mobile = $this->request->getPost("mobile");
        $code = $this->request->getPost("code");
        $password = $this->request->getPost("password");
        $hashPassword = $this->security->hash($password);

        $oUser = Users::findFirst("mobile='$mobile' and verify_token='$code' ");
        if (!$oUser) {
            throw new Exception(ErrorCodes::USER_NOT_FOUND, '', ['error' => 'user not exist']);
        }
        $oUser->password = $hashPassword;
        if (!$oUser->save()) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'password required']);
        }
        $aResponse = array('response' => [], 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');


    }

    public function findBy()
    {
        $aOperation = ['=', 'like', '>', '<', '>=', '<=', '!='];

        $operation = !empty($this->request->getPost("operation")) ? $this->request->getPost("operation") : '=';
        $key = !empty($this->request->getPost("key")) ? $this->request->getPost("key") : 'email';
        $value = !empty($this->request->getPost("value")) ? $this->request->getPost("value") : '';
        $oUser = new Users();
        $columns = DB::getSchemaBuilder()->getColumnListing('users');

        if (!in_array($key, $columns)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'key not in user']);
        }

        if (!in_array($operation, $aOperation)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'operation not allowed']);
        }

        $sCondition = "$key  $operation '$value' ";
        if ($operation == 'like') {
            $sCondition = "$key  $operation '%$value%' ";
        }
        $oUser = Users::find($sCondition);
        if ($oUser->count() == 0) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $aUser[] = ['id' => '0', 'email' => $value, 'first_name' => $value, 'last_name' => ''];
                $aResponse = array('response' => $aUser, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
                return $this->createArrayResponse($aResponse, 'data');
            }

            if (strncmp($value, DefaultValues::MOBILE_PHONE_PREFIX, strlen(DefaultValues::MOBILE_PHONE_PREFIX)) === 0
                && strlen($value) >= 10
            ) {
                $aUser[] = ['id' => '0', 'mobile' => $value, 'first_name' => $value, 'last_name' => ''];
                $aResponse = array('response' => $aUser, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
                return $this->createArrayResponse($aResponse, 'data');
            }


            return $this->createArrayResponse(['response' => [], 'response_code' => 1, 'response_message' => "success", 'status' => 'success'], 'data');
            //throw new Exception(ErrorCodes::USER_NOT_FOUND, '', ['error' => 'user not exist']);
        }
        $transformer = new \App\Transformers\UserTransformer;
        $transformer->setModelClass('MyCompose\Model\Users');
        $aUser = $oUser->toArray();
        foreach ($oUser as $k => $v) {
            $aUser[$k] = $this->createItemResponse($v, $transformer);
            $oDefaultAccount = Accounts::findFirst("user_id=" . $aUser[$k]['id'] . " and is_default=1");
            if ($oDefaultAccount) {
                $aUser[$k]['default_account'] = $oDefaultAccount;
                $aUser[$k]['id'] = $oDefaultAccount->id;
            }

        }


        $aResponse = array('response' => $aUser, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');

    }


    public function check_identifier()
    {
        $sIdentifier = $this->request->getPost('identifier');
        $oUser = Users::findFirst("email='" . $sIdentifier . "' or mobile='" . $sIdentifier . "'");
        if (!$oUser) {
            return $this->createArrayResponse(['oUser' => false, 'status' => 'failed'], 'data');
        }
        return $this->createArrayResponse(['oUser' => ['id' => $oUser->id], 'status' => 'success'], 'data');
    }


    private function addNewUser($aParams = [])
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oUser = new Users();
        $oUser->email = isset($aParams['email']) ? $aParams['email'] : null;
        $oUser->mobile = isset($aParams['mobile']) ? $aParams['mobile'] : null;
        $oUser->user_name = isset($aParams['user_name']) ? $aParams['user_name'] : null;
        $oUser->first_name = isset($aParams['first_name']) ? $aParams['first_name'] : null;
        $oUser->last_name = isset($aParams['last_name']) ? $aParams['last_name'] : null;
        $oUser->activated = isset($aParams['activated']) ? $aParams['activated'] : 0;
        $oUser->password = isset($aParams['password']) ? $aParams['password'] : $this->security->hash(rand(100000, 999999));
        $oUser->created_by = isset($aParams['created_by']) ? $aParams['created_by'] : $nUserID;
        $oUser->created_at = date('Y-m-d H:i:s');
        if (!$oUser->save()) {
            return $oUser->getMessages();
        }

        return $oUser;
    }

    private function moveInvitationToAccount($invitation_id, $account_id)
    {

        $oDocumentToInvitations = DocumentToInvitations::find("invitation_id=$invitation_id");
        if ($oDocumentToInvitations->count() > 0) {
            foreach ($oDocumentToInvitations as $oDocumentToInvitation) {
                $oDocumentToAccount = new DocumentToAccount();
                $oDocumentToAccount->account_id = $account_id;
                $oDocumentToAccount->description = $oDocumentToInvitation->description;
                $oDocumentToAccount->save();
            }
        }

        $oDocumentCcInvitations = DocumentCcInvitations::find("invitation_id=$invitation_id");
        if ($oDocumentCcInvitations->count() > 0) {
            foreach ($oDocumentCcInvitations as $oDocumentCcInvitation) {
                $oDocumentCcAccount = new DocumentCcAccount();
                $oDocumentCcAccount->account_id = $account_id;
                $oDocumentCcAccount->description = $oDocumentCcInvitation->description;
                $oDocumentCcAccount->save();
            }
        }

        $oDocumentSignerInvitations = DocumentSignerInvitations::find("invitation_id=$invitation_id");
        if ($oDocumentSignerInvitations->count() > 0) {
            foreach ($oDocumentSignerInvitations as $oDocumentSignerInvitation) {
                $oDocumentSignerAccount = new DocumentSignerAccount();
                $oDocumentSignerAccount->account_id = $account_id;
                $oDocumentSignerAccount->description = $oDocumentSignerInvitation->description;
                $oDocumentSignerAccount->save();
            }
        }
        return true;
    }

    public function getDevices($user_id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if ($user_id != $this->userService->getIdentity()) {
            $oLinkedUser = UserLink::findFirst("user_id=" . $this->userService->getIdentity() . " and user_link_id=" . $user_id);
            if (!$oLinkedUser) {
                throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
            }
        }

        $oDevices = Devices::find(["user_id=" . $user_id, 'order' => "id desc"]);

        $aResponse = array('devices' => $oDevices, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function skipRequirement()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nRequirementID = $this->request->getPost('id');
        $nUserID = $this->userService->getIdentity();
        $oUserProfileRequirements = new UserProfileRequirements();
        $oUserProfileRequirements->user_id = $nUserID;
        $oUserProfileRequirements->profile_requirements_id = $nRequirementID;
        $oUserProfileRequirements->status = 'skip';
        $oUserProfileRequirements->created_at = date('Y-m-d H:i:s');
        if (!$oUserProfileRequirements->save()) {
            dd($oUserProfileRequirements->getMessage());
        }
        $aResponse = array('response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');

    }

    public function updatePrimary()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            return false;
        }
        $nContactID = $oUser->contact_id;
        $oContact = Contacts::findFirstById($nContactID);
        $type = $this->request->getPost("type");
        $value = $this->request->getPost("value");
        $oResponse = false;
        switch ($type) {
            case 'name' :
                $oResponse = $this->updatPrimaryName($oContact->id, $value);
                break;
            case 'email' :
                $oResponse = $this->updatPrimaryEmail($oContact->id, $value);
                break;
            default:
                $oResponse = false;
        }

        $aResponse = array('response_code' => $oResponse, 'response_message' => "failed", 'status' => 'failed');
        if ($oResponse) {
            $aResponse = array('response_code' => $oResponse, 'response_message' => "success", 'status' => 'success');
        }

        return $this->createArrayResponse($aResponse, 'data');
    }

    public function updatPrimaryName($nContactID, $value)
    {
        $oNames = Names::find("contact_id=$nContactID ");
        if ($oNames->count() > 0) {
            foreach ($oNames as $oName) {
                $oName->is_primary = 0;
                if (!$oName->save()) {
                    dd($oName->getMessage());
                }
            }

            $oPrimaryName = Names::findFirst("contact_id=$nContactID and display_name='" . $value . "' ");
            if ($oPrimaryName) {
                $oPrimaryName->is_primary = 1;
                if (!$oPrimaryName->save()) {
                    dd($oPrimaryName->getMessage());
                }
                return true;
            }
        }
        return false;
    }

    public function updatPrimaryEmail($nContactID, $value)
    {
        $oEmails = Emails::find("contact_id=$nContactID ");
        if ($oEmails->count() > 0) {
            foreach ($oEmails as $oEmail) {
                $oEmail->is_primary = 0;
                if (!$oEmail->save()) {
                    dd($oEmail->getMessage());
                }
            }

            $oPrimaryEmail = Emails::findFirst("contact_id=$nContactID and email='" . $value . "' ");
            if ($oPrimaryEmail) {
                $oPrimaryEmail->is_primary = 1;
                if (!$oPrimaryEmail->save()) {
                    dd($oPrimaryEmail->getMessage());
                }
                return true;
            }
        }
        return false;
    }

    public function updatPrimaryMobile($nContactID, $value)
    {
        $oMobiles = Mobiles::find("contact_id=$nContactID ");
        if ($oMobiles->count() > 0) {
            foreach ($oMobiles as $oMobile) {
                $oMobile->is_primary = 0;
                if (!$oMobile->save()) {
                    dd($oMobile->getMessage());
                }
            }

            $oPrimaryMobile = Mobiles::findFirst("contact_id=$nContactID and mobile='" . $value . "' ");
            if ($oPrimaryMobile) {
                $oPrimaryMobile->is_primary = 1;
                if (!$oPrimaryMobile->save()) {
                    dd($oPrimaryMobile->getMessage());
                }
                return true;
            }
        }
        return false;
    }

    public function resendVerify()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oUser = Users::findFirstById($nUserID);
        if (!empty($oUser->email)) {
            $oMessagesQueue = new MessagesQueue();
            $oMessagesQueue->type = 'email';
            $oMessagesQueue->contact = $oUser->email;
            $oMessagesQueue->template = 'register';
            $oMessagesQueue->recipient_type = 'user';
            $oMessagesQueue->user_id = $oUser->id;
            $oMessagesQueue->save();
        }


        if (!empty($oUser->mobile)) {
            $oMessagesQueue = new MessagesQueue();
            $oMessagesQueue->type = 'sms';
            $oMessagesQueue->contact = $oUser->mobile;
            $oMessagesQueue->template = 'register';
            $oMessagesQueue->recipient_type = 'user';
            $oMessagesQueue->user_id = $oUser->id;
            $oMessagesQueue->save();
        }

        return $this->createArrayResponse(['status' => 'true'], 'data');


    }

    public function checkIfHaveSignture()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oSign = Signs::findFirst("user_id=" . $nUserID);
        if (!$oSign) {
            return $this->createArrayResponse(['status' => 'false'], 'data');
        }

        return $this->createArrayResponse(['status' => 'true'], 'data');

    }

    public function getMyContact($nUserID)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $sQ = $this->request->get("q");
        $nUserID = $this->userService->getIdentity();

        if(!empty($sQ)){
            $oContacts = Contacts::find("user_id=" . $nUserID . " and display_name like '%$sQ%' ");
        }
        $oContacts = Contacts::find("user_id=" . $nUserID );
        $aContacts = [];
        $addNew = true;
        foreach ($oContacts as $k => $oContact) {
            $oEmails = $oContact->Emails;
            $oMobiles = $oContact->Mobiles;

            foreach ($oEmails as $oEmail) {
                $aContacts[] = ['identifier' => $oEmail->email, 'name' => $oContact->display_name,'id'=>$oContact->id];
            }
            foreach ($oMobiles as $oMobile) {
                $aContacts[] = ['identifier' => $oMobile->mobile, 'name' => $oContact->display_name,'id'=>$oContact->id];
            }

        }


        return $this->createArrayResponse(['contacts' => $aContacts, 'status' => 'true'], 'data');

    }

    public function saveAvatar()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            dd($oUser->getMessage());
        }

        if ($this->request->hasFiles()) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $nFile = File::put($file);

            }

        }


        if (empty($nFile)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('avatar_data' => $nFile));
        }


        //$sSignDataWithWaterMark = $this->addWaterMark($sSignData);

        $oUser->avatar = $nFile;

        if (!$oUser->save()) {
            dd($oUser->getMessages());
        }

        $oUserAvatar = new UserAvatar();
        $oUserAvatar->user_id = $nUserID;
        $oUserAvatar->file_id = $nFile;
        $oUserAvatar->created_at = date('Y-m-d H:i:s');
        $oUserAvatar->save();

        $oUserProfileRequirements = new UserProfileRequirements();
        $oUserProfileRequirements->user_id = $nUserID;
        $oUserProfileRequirements->profile_requirements_id = 1;
        $oUserProfileRequirements->created_at = date('Y-m-d H:i:s');
        $oUserProfileRequirements->status = 'complete';
        $oUserProfileRequirements->save();

        $aAvatar = array('avatar' => $oUser->Avatar, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aAvatar, 'data');
    }

    public function changePassword()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();


        $currentPassword = $this->request->getPost("current_password");
        $sPassword = $this->request->getPost("password_input");
        $sRepeatPassword = $this->request->getPost("repeat_password_input");

        $AccountType = \App\Auth\UsernameAccountType::ID;
        $session = $this->authManager->loginWithUsernamePassword($AccountType, $nUserID,
            $currentPassword);
        if ($sPassword != $sRepeatPassword) {
            return $this->createArrayResponse(['response_code' => 0, 'response_message' => "failed"], 'data');

        }
        $nUserID = $session->getIdentity();
        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            return false;
        }
        $sHashPassword = $this->security->hash($sPassword);
        $oUser->password = $sHashPassword;
        $oUser->save();

        $aResponse = array('oUser' => $oUser, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function changeUsername()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();


        $sUsername = $this->request->getPost("username");
        if(empty($sUsername)) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $oUser = Users::findFirst("username='" . $sUsername . "'");
        if($oUser){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $oUser = Users::findFirstById($nUserID);
        $oUser->username = $sUsername ;
        $oUser->save();

        $aResponse = array('oUser' => $oUser, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function setReference()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();


        $sReference = $this->request->getPost("reference_input");

        if(empty($sReference)) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $oIONumber = IONumber::findFirst("user_id=$nUserID and template = '". $sReference ."'");
        if($oIONumber){
            $this->db->execute("update " . $oIONumber->getSource() . " set active=0 where user_id=" . $nUserID );
            $oIONumber->active=1;
            $oIONumber->update_at = date('Y-m-d H:i:s');
            $oIONumber->save();
            $aResponse = array('oIONumber' => $oIONumber, 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aResponse, 'data');
        }

        $oIONumber = IONumber::findFirst("user_id=$nUserID");
        if($oIONumber){
            $this->db->execute("update " . $oIONumber->getSource() . " set active=0 where user_id=" . $nUserID );
        }
        $oIONumber = new IONumber();
        $oIONumber->user_id = $nUserID;
        $oIONumber->template = $sReference;
        $oIONumber->active = 1;
        $oIONumber->created_at = date('Y-m-d H:i:s');
        if(!$oIONumber->save()){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if($oIONumber){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }


        $aResponse = array('oIONumber' => $oIONumber, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }


    public function saveMyContacts($nUsreID)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $aContacts = $this->request->getPost('aContacts');

        $oUser = Users::findFirstById($nUserID);

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        foreach ($aContacts as $aContact) {
            $oContact = Contacts::findFirst("google_id='" . $aContact['resource_name'] . "' and user_id=$nUserID");
            if (!$oContact) {

                $nMyContactID = false;
                if (isset($aContact['emails']) && sizeof($aContact['emails']) > 0) {
                    foreach ($aContact['emails'] as $email) {
                        $oEmail = Emails::findFirst("email='" . $email['value'] . "'");
                        if ($oEmail) {
                            $nMyContactID = $oEmail->id;
                            break;
                        }

                    }
                }

                if (!$nMyContactID) {
                    if (isset($aContact['mobiles']) && sizeof($aContact['mobiles']) > 0) {
                        foreach ($aContact['mobiles'] as $mobile) {
                            $oMobile = Mobiles::findFirst("mobile='" . $mobile['value'] . "'");
                            if ($oMobile) {
                                $nMyContactID = $oMobile->id;
                                break;
                            }

                        }

                    }
                }

                if (!$nMyContactID) {
                    $oContact = new Contacts();
                    $oContact->user_id = $nUserID;
                    $oContact->created_at = date('Y-m-d H:i:s');


                    if (isset($aContact['names']) && sizeof($aContact['names']) > 0) {
                        foreach ($aContact['names'] as $name) {
                            $oContact = new Contacts();
                            $oContact->user_id = $nUserID;
                            $oContact->created_at = date('Y-m-d H:i:s');

                            $filter = new \Phalcon\Filter();
                            $sName = $filter->sanitize($name['display_name'], "striptags");

                            $oContact->display_name = !empty($name['display_name']) ? $name['display_name'] : '';
                            $oContact->last_name = !empty($name['last_name']) ? $name['last_name'] : '';
                            $oContact->first_name = !empty($name['first_name']) ? $name['first_name'] : '';
                            $oContact->middle_name = !empty($name['middle_name']) ? $name['middle_name'] : '';
                            if (!empty($name['middle_name'])) {
                                $oContact->full_name = $oContact->first_name . " " . $oContact->middle_name . " " . $oContact->last_name;
                            } else {
                                $oContact->full_name = $oContact->first_name . " " . $oContact->last_name;
                            }

                            $oContact->google_id = isset($aContact['resource_name']) ? $aContact['resource_name'] : null;

                            if (!$oContact->save()) {
                                dd($oContact->getMessages());
                            }

                            if (isset($aContact['emails']) && sizeof($aContact['emails']) > 0) {
                                foreach ($aContact['emails'] as $email) {
                                    $oEmail = Emails::findFirst("email='" . $email['value'] . "' and contact_id=" . $oContact->id);
                                    if (!$oEmail) {
                                        $oEmail = new Emails();
                                    }
                                    $oEmail->email = $email['value'];
                                    $oEmail->type = $email['type'];
                                    $oEmail->contact_id = $oContact->id;
                                    $oEmail->user_id = $nUserID;
                                    $oEmail->created_at = date('Y-m-d H:i:s');
                                    $oEmail->save();
                                }

                            }


                            if (isset($aContact['mobiles']) && sizeof($aContact['mobiles']) > 0) {
                                foreach ($aContact['mobiles'] as $mobile) {
                                    $oMobile = Mobiles::findFirst("mobile='" . $mobile['value'] . "' and contact_id=" . $oContact->id);
                                    if (!$oMobile) {
                                        $oMobile = new Mobiles();
                                    }
                                    $oMobile->mobile = $mobile['value'];
                                    $oMobile->type = $mobile['type'];
                                    $oMobile->contact_id = $oContact->id;
                                    $oMobile->user_id = $nUserID;
                                    $oMobile->created_at = date('Y-m-d H:i:s');

                                    $oMobile->save();
                                }

                            }


                        }


                    }


                }


            }
        }
        $aResponse = ['response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getContact()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $this->request->getPost('id');

        $oUser = Users::findFirstById($nUserID);

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContact = Contacts::findFirst("id=$nContactID and user_id=$nUserID");
        if (!$oContact) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        $aContact = $oContact->toArray();

        $aContact['emails'] = !empty($oContact->Emails) && sizeof($oContact->Emails) > 0 ? $oContact->Emails->toArray()[0] : null;
        $aContact['mobiles'] = !empty($oContact->Mobiles) && sizeof($oContact->Mobiles) > 0 ? $oContact->Mobiles->toArray()[0] : null;


        $aContact['groups'] = [];
        if(!empty($oContact->GroupContact) &&$oContact->GroupContact->count() > 0 ) {
            $oGroupContact = $oContact->GroupContact ;
            $aGroup = [] ;
            foreach($oGroupContact as $k=> $oGroupContactOne ){
                $aGroup[] = $oGroupContactOne->Group->name;
            }

            $aContact['groups'] = implode($aGroup,',');

        }

        $aResponse = ['aContact' => $aContact, 'response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function deleteContact()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $this->request->getPost('id');

        $oUser = Users::findFirstById($nUserID);

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContact = Contacts::findFirst("id=$nContactID and user_id=$nUserID");
        if (!$oContact) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        $oEmails = Emails::find("contact_id=" . $oContact->id);
        if ($oEmails->count() > 0) {
            $oEmails->delete();
        }

        $oMobiles = Mobiles::find("contact_id=" . $oContact->id);
        if ($oMobiles->count() > 0) {
            $oMobiles->delete();
        }

        $oContact->delete();


        $aResponse = ['response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getAvatars()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oUserAvatars = UserAvatar::find("user_id=" . $nUserID);

        $aFiles = [];
        foreach ($oUserAvatars as $k => $oUserAvatar) {
            $aFiles[] = $oUserAvatar->Avatar;
        }

        $aResponse = ['Avatars' => $aFiles, 'response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function setAvatar()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nFileID = $this->request->getPost('file_id');

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            return ['response_message' => "faild"];
        }
        $oUser->avatar = $nFileID;
        $oUser->save();

        $aResponse = ['response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }


    public function getSequences($nUserID)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $oIONumberSequences = IONumberSeqences::find("user_id=" . $nUserID ) ;

        $aResponse = ['oIONumberSequences' => $oIONumberSequences, 'response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }



    public function saveSequences()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $sStartSequence = $this->request->getPost('start_sequence');
        $sEndSequence = $this->request->getPost('end_sequence');
        $sYear = $this->request->getPost('year');
        $nID = $this->request->getPost('id');
        $sType = $this->request->getPost('type');
        if(empty($nID)){
            $oIONumberSequences = new IONumberSeqences() ;
        }else{
            $oIONumberSequences = IONumberSeqences::findFirstById($nID);
        }

        $oIONumberSequences->year = $sYear;
        $oIONumberSequences->start_sequence = $sStartSequence;
        $oIONumberSequences->end_sequence = $sEndSequence;
        $oIONumberSequences->user_id = $nUserID;
        $oIONumberSequences->created_at = date('Y-m-d H:i:s');
        $oIONumberSequences->type = $sType;

        $oIONumberSequences->save();

        $aResponse = ['oIONumberSequences' => $oIONumberSequences, 'response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function deleteSequence()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $nID = $this->request->getPost('id');
        if(empty($nID)){
            $aResponse = ['response_code' => 0, 'response_message' => "success"];
            return $this->createArrayResponse($aResponse, 'data');
        }else{
            $oIONumberSequences = IONumberSeqences::findFirstById($nID);
        }

         if($oIONumberSequences){
             $oIONumberSequences->delete();
         }

        $aResponse = ['response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function setLanguage()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $sLang = $this->request->getPost('lang');
        if(empty($sLang)){
            $aResponse = ['response_code' => 0, 'response_message' => "success"];
            return $this->createArrayResponse($aResponse, 'data');
        }

        $oUser = Users::findFirstById($nUserID);
        if(!$oUser){
            exit('111');
        }
        $oUser->lang_code = $sLang;
        $oUser->save();
        $aResponse = ['response_code' => 1, 'response_message' => "success"];
        return $this->createArrayResponse($aResponse, 'data');
    }

}
