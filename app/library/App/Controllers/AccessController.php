<?php

namespace App\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MyCompose\LaravelModels\Permission;

use MyCompose\Model\Business\Companies;
use MyCompose\Model\FeaturePermission;
use MyCompose\Model\Access\PermissionRole;
use MyCompose\Model\Access\Permissions;
use MyCompose\Model\Plans\Plan;

use MyCompose\Model\Plans\PlanFeature;
use MyCompose\Model\Plans\PlanPermission;
use MyCompose\Model\Plans\PlanRole;
use MyCompose\Model\Users;
use MyCompose\Model\Workflow\Workflow;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class AccessController extends CrudResourceController
{
    public function can(){

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nPlanID = $this->request->getPost('plan_id');
        $sKey = $this->request->getPost('key') ;
        $oPermissions = Permissions::findFirst("key='" . $sKey . "'") ;
        if(!$oPermissions){
            return false;
        }

        $nPermissionID = $oPermissions->id ;


        $checkRole = false ;
        $oPermissionRoles = PermissionRole::find("permission_id=" . $nPermissionID) ;
        if($oPermissionRoles->count() > 0) {
            $checkRole = true ;
        }

        if(!empty($nPlanID)){
            $checkPlanPermission = PlanPermission::findFirst("plan_id=" . $nPlanID . " and permission_id=" . $nPermissionID ) ;
            if($checkPlanPermission) {
                return true;
            }

            if( $checkRole ){
                foreach($oPermissionRoles as $oPermissionRole){
                    $nRoleID = $oPermissionRole->role_id ;
                    $oPlanRole = PlanRole::findFirst("plan_id=" . $nPlanID . " role_id=" . $nRoleID ) ;
                    if($oPlanRole){
                        return true;
                    }
                }

            }


            $getPlanFeatures = PlanFeature::find("plan_id='" . $nPlanID . "'" );
            if($getPlanFeatures->count()>0){
                foreach($getPlanFeatures as $oPlanFeature) {
                    $nFeatureID = $oPlanFeature->feature_id ;
                    $checkAccess = FeaturePermission::findFirst("permission_id=" . $nPermissionID . " and feature_id=" . $nFeatureID );
                    if($checkAccess){
                        return true;
                    }
                }
            }
        }

        return false;
        //$oPermissions = Permission::with([]);
    }
}
