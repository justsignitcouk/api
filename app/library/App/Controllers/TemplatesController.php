<?php

namespace App\Controllers;

use MyCompose\Model\Templates;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class TemplatesController extends CrudResourceController
{


    public function all(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $aTemplates = array('templates' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oTemplates = Templates::find();
        $aTemplates['templates'] = $oTemplates ;
        return $this->createArrayResponse($aTemplates,'data');
    }

    public function getByLayoutID(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nLayoutID = $this->request->getPost("layout_id") ;

        if( empty($nLayoutID) || !is_numeric($nLayoutID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('layout_id' => $nCompnayID));
        }
        $aTemplates = array('templates' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oTemplates = Templates::find("document_layout_id=$nLayoutID");
        $aTemplates['templates'] = $oTemplates ;
        return $this->createArrayResponse($aTemplates,'data');
    }


}
