<?php

namespace App\Controllers;

use MyCompose\LaravelModels\DocumentLayouts;
use MyCompose\LaravelModels\DocumentLayoutComponents;
use MyCompose\Model\Document\DocumentLayout;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;

class DocumentLayoutsController extends CrudResourceController
{


    public function all(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oLayouts = DocumentLayout::find();
        $aLayouts = array('document_layouts' => $oLayouts ,'response_code' => 1 ,'response_message' => "success" );


        return $this->createArrayResponse($aLayouts,'data');
    }

    public function getLayoutByID(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nLayoutID = $this->request->getPost("id") ;

        if( empty($nLayoutID) || !is_numeric($nLayoutID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('id' => $nLayoutID));
        }

        $aLayouts = array('document_layouts' => array() ,'response_code' => 1 ,'response_message' => "success" );

        $header = $this->view->getPartial('layouts/layout' . $nLayoutID . '/header', []);
        $footer = $this->view->getPartial('layouts/layout' . $nLayoutID . '/footer', []);
        $html = $this->view->getPartial('layouts/layout' . $nLayoutID . '/content', []);

        $aLayouts['document_layouts'][] = [ 'layout_components' => ['header'=>$header, 'footer'=>$footer, 'content'=>$html] ] ;
        return $this->createArrayResponse($aLayouts,'data');
    }

    public function getDefaultLayout() {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nLayoutID = 1 ;

        if( empty($nLayoutID) || !is_numeric($nLayoutID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('id' => $nLayoutID));
        }

        $aLayouts = array('document_layouts' => array() ,'response_code' => 1 ,'response_message' => "success" );

        $header = $this->view->getPartial('layouts/layout' . $nLayoutID . '/header', []);
        $footer = $this->view->getPartial('layouts/layout' . $nLayoutID . '/footer', []);
        $html = $this->view->getPartial('layouts/layout' . $nLayoutID . '/content', []);


        $aLayouts['document_layouts'][] = [ 'layout_components' => ['header'=>$header, 'footer'=>$footer, 'content'=>$html] ] ;
        return $this->createArrayResponse($aLayouts,'data');
    }

    public function storeHeader(){

        $header = $this->request->getPost("header") ;
        $name   = $this->request->getPost("name") ;

        $documentLayouts = new DocumentLayouts ;
        $documentLayouts->name = $name;
        $documentLayouts->save();

        $documentLayoutComponents = new DocumentLayoutComponents ;
        $documentLayoutComponents->name     = $name;
        $documentLayoutComponents->content  = $header;
        $documentLayoutComponents->position = 'header';
        $documentLayoutComponents->layout_id= $documentLayouts->id;
        $documentLayoutComponents->save();

    }

    public function storeFooter(){

        $header = $this->request->getPost("header") ;
        $name   = $this->request->getPost("name") ;

        $documentLayouts = new DocumentLayouts ;
        $documentLayouts->name = $name;
        $documentLayouts->save();

        $documentLayoutComponents = new DocumentLayoutComponents ;
        $documentLayoutComponents->name     = $name;
        $documentLayoutComponents->content  = $header;
        $documentLayoutComponents->position = 'footer';
        $documentLayoutComponents->layout_id= $documentLayouts->id;
        $documentLayoutComponents->save();

    }

    public function allHeaders($accountID){

        return DocumentLayoutComponents::select('id','name')->where([['account_id',$accountID],['position','header']])->get();

    }

    public function allFooters($accountID){

        return DocumentLayoutComponents::select('id','name')->where([['account_id',$accountID],['position','footer']])->get();

    }

    public function getHeader($id){

        return DocumentLayoutComponents::where([['id',$id],['position','header']])->get();

    }

    public function getFooter($id){

        return DocumentLayoutComponents::where([['id',$id],['position','footer']])->get();

    }




}
