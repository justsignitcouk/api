<?php

namespace App\Controllers;

use App\Components\File\File;
use App\Components\Mail\Mail;
use App\Components\PDF\PDF;
use MyCompose\LaravelModels\Document\Document;
use MyCompose\LaravelModels\Document\Recipients\Contact;
use MyCompose\Model\AccountDeactivate;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Companies as Companies;
use MyCompose\Model\Contacts\Avatar;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\Mobiles;
use MyCompose\Model\Contacts\MyContact;
use MyCompose\Model\Contacts\Names;
use MyCompose\Model\Document\DocumentCcAccount;
use MyCompose\Model\Document\DocumentCcContact;
use MyCompose\Model\Document\DocumentCcInvitations;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentSenderAccount ;
use MyCompose\Model\Document\DocumentToContact;
use MyCompose\Model\Document\DocumentToInvitations;
use MyCompose\Model\Document\DocumentWorkflow;
use MyCompose\Model\Draft\DraftDocumentCcAccount;
use MyCompose\Model\Draft\DraftDocumentSignerAccount;
use MyCompose\Model\Draft\DraftDocumentToAccount;
use MyCompose\Model\Files;
use MyCompose\Model\Profiles\UserProfileRequirements;
use MyCompose\Model\UserLink;
use MyCompose\Model\Users;
use MyCompose\LaravelModels\Document\DocumentSenderAccount as DSenderA;
use MyCompose\Model\Document\DocumentSignData;
use MyCompose\Model\Document\DocumentSignerAccount;
use MyCompose\LaravelModels\Document\DocumentSignerAccount as DSignerA ;
use MyCompose\Model\Document\DocumentToAccount;
use MyCompose\Model\Document\DocumentUserRead;
use MyCompose\Model\Draft\DraftDocuments;
use MyCompose\Model\Draft\DraftDocumentTowhom;
use MyCompose\Model\Signs;
use MyCompose\LaravelModels\Account;
use MyCompose\LaravelModels\Workflow\Stage;
use MyCompose\LaravelModels\Workflow\State;
use MyCompose\LaravelModels\UserActivity;
use MyCompose\Model\Workflow\StateActionPermitsUser;
use MyCompose\Model\Workflow\StateActionPermitsRole;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
use App\Components\Notifications\Notifications;
use Illuminate\Database\Capsule\Manager as DB;

class ContactController extends CrudResourceController
{


    public function all()
    {

        $oAccounts = Accounts::find();

        return $this->createResourceCollectionResponse($oAccounts);
    }

    public function getMyContacts($nContactID) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $sFilter = $this->request->get('q');

        $oMyContacts = MyContact::find("my_id=" . $nContactID ) ;
        $aMyContacts = $oMyContacts->toArray();

        foreach($oMyContacts as $k=>$oMyContact){
            $oName = Names::findFirst("contact_id=" . $oMyContact->contact_id . " and is_primary=1 " . " and display_name like '%". $sFilter ."%' ") ;
            if(!$oName){
                $oName = Names::findFirst("contact_id=" . $oMyContact->contact_id . " and display_name like '%". $sFilter ."%' "  ) ;
                if(!$oName){
                    unset($aMyContacts[$k]) ;
                    continue;
                }

            }
            $aMyContacts[$k]['display_name'] = $oName->display_name ;
            $aMyContacts[$k]['first_name'] = $oName->first_name;
            $aMyContacts[$k]['last_name'] = $oName->last_name;
            $aMyContacts[$k]['element_type'] = 'contact';
            $aMyContacts[$k]['id'] = $oMyContact->contact_id;

        }

        $aContacts = array('contacts' => $aMyContacts, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aContacts, 'data');
    }

    public function saveMyContacts($nContactID) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $aContacts = $this->request->getPost('aContacts') ;

        $oContact = Contacts::findFirstById($nContactID);

        if(!$oContact){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        foreach($aContacts as $aContact) {
            $oContact = Contacts::findFirst("google_id='" . $aContact['resource_name'] . "'");
            if(!$oContact) {
                $nMyContactID = false;
                if(isset($aContact['emails']) && sizeof($aContact['emails'])>0) {
                    foreach ($aContact['emails'] as $email) {
                        $oEmail = Emails::findFirst("email='" . $email['value'] . "'");
                        if ($oEmail) {
                            $nMyContactID = $oEmail->id;
                            break;
                        }

                    }
                }

                if(!$nMyContactID){
                    if(isset($aContact['mobiles']) && sizeof($aContact['mobiles'])>0){
                        foreach($aContact['mobiles'] as $mobile){
                            $oMobile = Mobiles::findFirst( "mobile='" . $mobile['value'] . "'" ) ;
                            if($oMobile){
                                $nMyContactID = $oMobile->id;
                                break;
                            }

                        }

                    }
                }

                if(!$nMyContactID){
                    $oContact = new Contacts();
                    $oContact->created_at = date('Y-m-d H:i:s') ;
                }

            }


            $oContact->google_id = isset($aContact['resource_name']) ?  $aContact['resource_name'] : null ;

            if(!$oContact->save()){
                return $oContact->getMessages();
            }

            if(isset($aContact['emails']) && sizeof($aContact['emails'])>0){
                foreach($aContact['emails'] as $email){
                    $oEmail = Emails::findFirst("email='" . $email['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oEmail){
                        $oEmail = new Emails();
                    }
                    $oEmail->email = $email['value'];
                    $oEmail->type = $email['type'];
                    $oEmail->contact_id = $oContact->id;
                    $oEmail->user_id = $nUserID;
                    $oEmail->created_at = date('Y-m-d H:i:s') ;
                    $oEmail->save();
                }

            }

            if(isset($aContact['names']) && sizeof($aContact['names'])>0){
                foreach($aContact['names'] as $name){
                    $filter = new \Phalcon\Filter();
                    $sName = $filter->sanitize($name['display_name'], "striptags");
                    $oNames = Names::findFirst("display_name='" . addslashes($sName) . "' and contact_id=" . $oContact->id ) ;
                    if(!$oNames){
                        $oNames = new Names();
                        $oNames->created_at = date('Y-m-d H:i:s') ;
                    }
                    $oNames->display_name = !empty($name['display_name']) ? $name['display_name'] : '';
                    $oNames->last_name = !empty($name['last_name']) ? $name['last_name'] : '';
                    $oNames->first_name = !empty($name['first_name']) ? $name['first_name'] : '';
                    $oNames->middle_name = !empty($name['middle_name']) ? $name['middle_name'] : '';
                    if(!empty($name['middle_name'])){
                        $oNames->full_name = $oNames->first_name . " " . $oNames->middle_name . " " . $oNames->last_name ;
                    }else{
                        $oNames->full_name = $oNames->first_name . " " . $oNames->last_name ;
                    }

                    $oNames->contact_id = $oContact->id;
                    $oNames->updated_at = date('Y-m-d H:i:s') ;
                    $oNames->user_id = $nUserID;
                    if(!$oNames->save()){
                        dd($oNames->getMessages());
                    }
                }

            }

            if(isset($aContact['mobiles']) && sizeof($aContact['mobiles'])>0){
                foreach($aContact['mobiles'] as $mobile){
                    $oMobile = Mobiles::findFirst("mobile='" . $mobile['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oMobile){
                        $oMobile = new Mobiles();
                    }
                    $oMobile->mobile = $mobile['value'];
                    $oMobile->type = $mobile['type'];
                    $oMobile->contact_id = $oContact->id;
                    $oMobile->created_at = date('Y-m-d H:i:s') ;
                    $oMobile->user_id = $nUserID;

                    $oMobile->save();
                }

            }

            $oMyContact = new MyContact();
            $oMyContact->contact_id = $oContact->id;
            $oMyContact->my_id = $nContactID;
            $oMyContact->by = 'all';
            $oMyContact->user_id = $nUserID;
            $oMyContact->created_at = date('Y-m-d H:i:s');
            $oMyContact->save();

        }

        $aResponse = [ 'response_code' => 1 ,'response_message' => "success" ];
        return $this->createArrayResponse($aResponse, 'data' );
    }

    public function getAccountById($id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $user_id = $this->userService->getIdentity();
        $nAccountID = $id;

        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('id' => $nAccountID));
        }
        $oAccount = Account::with(['ProfileImage','User','Department', 'Department.Branch', 'Department.Branch.Company'])->where('id', '=', $nAccountID)->get()->first();

        $aAccount = array('account' => $oAccount, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aAccount, 'data');
    }

    public function findBy()
    {
        $aOperation = ['=', 'like', '>', '<', '>=', '<=', '!='];

        $operation = !empty($this->request->getPost("operation")) ? $this->request->getPost("operation") : '=';
        $key = !empty($this->request->getPost("key")) ? $this->request->getPost("key") : 'email';
        $value = !empty($this->request->getPost("value")) ? $this->request->getPost("value") : '';
        $oAccount = new Account();
        $columns = DB::getSchemaBuilder()->getColumnListing('accounts');

        if (!in_array($key, $columns)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'key not in account']);
        }

        if (!in_array($operation, $aOperation)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, '', ['error' => 'operation not allowed']);
        }

        $sCondition = "$key  $operation '$value' ";
        if ($operation == 'like') {
            $sCondition = "$key  $operation '%$value%' ";
        }

        $oAccounts = Accounts::find($sCondition);
        if ($oAccounts->count() == 0) {
            return $this->createArrayResponse(['accounts' => [], 'response_code' => 1, 'response_message' => "success", 'status' => 'success'], 'data');
        }

        $aAccounts = $oAccounts->toArray();
        foreach($oAccounts as $k=>$oAccount){
            $aAccounts[$k]['user'] = $oAccount->Users->toArray() ;
            $aAccounts[$k]['profile_image'] = $oAccount->ImageProfile;
            $aAccounts[$k]['department'] = $oAccount->Department;
            $oAccountRoles = $oAccount->AccountRole;
            $aAccounts[$k]['roles'] = [];
            foreach( $oAccountRoles as $oAccountRole) {
                $aAccounts[$k]['roles'][] = $oAccountRole->Role;
            }
        }

        $aResponse = array('accounts' => $aAccounts, 'response_code' => 1, 'response_message' => "success", 'status' => 'success');
        return $this->createArrayResponse($aResponse, 'data');

    }

    public function exist()
    {

        $id = $this->request->getPost('id');
        if (!$id) {
            $response = [
                'errorMessage' => 'Missing id argument',
                'status' => false,
                'id' => $id
            ];
            return $this->createArrayResponse($response, 'data');
        }
        if (!is_numeric($id)) {
            $response = [
                'errorMessage' => 'id must be Numeric',
                'status' => false
            ];
            return $this->createArrayResponse($response, 'data');
        }

        $oAccount = Accounts::findFirst("id = $id");
        $response = [
            'errorMessage' => 'NOT FOUND',
            'status' => false
        ];
        if ($oAccount) {
            $response = ['oAccount' => $oAccount,
                'status' => true
            ];
        }

        return $this->createArrayResponse($response, 'data');
    }

    public function make_default()
    {
        $id = $this->request->getPost('id');

        if (empty($id) || !is_numeric($id)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('id' => $id));
        }

        $user_id = $this->userService->getIdentity();
        $oAccounts = $this->db->query("update accounts set is_default=0 where user_id='$user_id' ");
        $oAccount = Accounts::findFirst("id = '$id'");

        if ($oAccount) {

            $oAccount->is_default = 1;
            if (!$oAccount->save()) {
                exit("NOT FOUND");
            }
            $response = [
                'status' => true
            ];

        } else {
            $response = [
                'status' => false,
                'user_id' => $user_id,
                'id' =>  $id ,
            ];
        }
        return $this->createArrayResponse($response, 'data');
    }

    public function company_details()
    {
        $response = [
            'data_missing' => array()
        ];
        $key = $this->request->getPost('key');
        $value = $this->request->getPost('value');
        $error = false;

        if (empty($key)) {
            $response['data_missing'][] = 'key';
            $error = true;
        }

        if (empty($value)) {
            $response['data_missing'][] = 'value';
            $error = true;
        }

        $response['status'] = false;
        if ($error) {
            return $this->createArrayResponse($response, 'data');
        }

        $oAccount = Accounts::findFirst("$key = $value");
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        $nCompany_id = $oAccount->company_id;
        $oCompanyDetails = Companies::findFirst("id = '$nCompany_id' ");
        $response = [
            'status' => true,
            'oCompany' => $oCompanyDetails
        ];


        return $this->createArrayResponse($response, 'data');

    }

    public function getDepartmentsByAccountId($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirstById($nAccountId);

        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oAccountDepartments = $oAccount->Department ? $oAccount->Department : [];
        $oDepartments = array('departments' => $oAccountDepartments, 'response_code' => 1, 'response_message' => "success");


        return $this->createArrayResponse($oDepartments, 'data');
    }

    public function getToWhomAddresses()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nCompnayID = $this->request->getPost("company_id");
        if (empty($nCompnayID) || !is_numeric($nCompnayID)) {
            $aAccounts = ['to_whom_addresses'=>[], 'response_code' => 1, 'response_message' => "success"];
            return $this->createArrayResponse($aAccounts, 'data');
        }

        $oAccounts = Accounts::find("company_id='$nCompnayID' and active = 1 ");
        $aAccounts = array('to_whom_addresses' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($oAccounts as $k => $oAccount) {
            $aAccounts['to_whom_addresses'][] = [
                'id' => $oAccount->id,
                'account_name' => $oAccount->account_name,
                'first_name' => $oAccount->Users->first_name,
                'last_name' => $oAccount->Users->last_name,
                'user_id' => $oAccount->user_id,
                'profile_image' => $oAccount->profile_image,
                'email' => $oAccount->Users->email,
                'department_name' => $oAccount->Department ? $oAccount->Department->name : '',
                'department_id' => $oAccount->department_id,
            ];


        }
        return $this->createArrayResponse($aAccounts, 'data');
    }

    public function sent($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $id;
        if (empty($nContactID) || !is_numeric($nContactID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('contact_id' => $nContactID));
        }

        $oContact = Contacts::findFirst("id=$nContactID");

        $aSent = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocuments = $this->getDocumentBySender($oContact);

        foreach ($oDocuments as $oDocument) {

            $aSent['document'][] = $oDocument ;
        }
        $aSent['RecordsTotal'] = sizeof($aSent['document']);
        return $this->createArrayResponse($aSent, 'data');
    }

    public function draft($id)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirst("id=$nAccountId and user_id=$nUserId");
        if (!$oAccount) {
            $oUsersLink= UserLink::find("user_id=".$nUserId) ;
            $bAccountAccess = false;
            foreach($oUsersLink as $oUserLink){
                $UserLinkId = $oUserLink->user_link_id;
                $oAccount = Accounts::findFirst("id=".$nAccountId . " and user_id=$UserLinkId") ;
                if($oAccount){
                    $bAccountAccess = true;
                    break;
                }
            }
            if(!$bAccountAccess){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
            }

        }

        $aInbox = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oDocumentsSignerAccounts = DraftDocumentSignerAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));
        if ($oDocumentsSignerAccounts->count() > 0) {
            foreach ($oDocumentsSignerAccounts as $oDocumentsSignerAccount) {
                $nDocumentID = $oDocumentsSignerAccount->document_id;
                $oDocument = $this->getDraftDocumentById($nDocumentID);
                if ($oDocument) {
                    $aInbox['document'][$oDocumentsSignerAccount->document_id] = $oDocument;
                }
            }
        }

        $oDocumentsToAccounts = DraftDocumentToAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        foreach ($oDocumentsToAccounts as $oDocumentsToAccount) {
            $nDocumentID = $oDocumentsToAccount->document_id;
            $oDocument = $this->getDraftDocumentById($nDocumentID);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsToAccount->document_id] = $oDocument;
            }

        }

        $oDocumentsCCAccounts = DraftDocumentCcAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        foreach ($oDocumentsCCAccounts as $oDocumentsCCAccount) {
            $nDocumentID = $oDocumentsCCAccount->document_id;
            $oDocument = $this->getDraftDocumentById($nDocumentID);

            if ($oDocument) {
                $aInbox['document'][$oDocumentsCCAccount->document_id] = $oDocument;

            }

        }

        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($aInbox['document'] as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }

    public function inbox($id,$sFilter)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $id;
        if (empty($nContactID) || !is_numeric($nContactID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('contact_id' => $nContactID ));
        }

        $oContact = Contacts::findFirst("id=$nContactID");


        switch ($sFilter) {
            case 'pending' :
                $status = ['start','inprogress'];
                break;
            case 'completed' :
                $status = ['completed'];
                break;
            case 'inbox' :
                $status = 'inbox';
                break;
            case 'rejected' :
                $status = ['rejected'];
                break;
            default :
                throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }


        $aInbox = array('document' => array(), 'response_code' => 1, 'response_message' => "success");

        $oContacts = \MyCompose\Model\Document\Recipients\Contacts::find(array("contact_id=$nContactID and (can_read=1 or type='Signer') ", "order" => "document_id desc"));
        if ($oContacts->count() > 0) {
            foreach ($oContacts as $oContact) {
                $nDocumentID = $oContact->document_id;
                $oDocument = $this->getDocumentById($nDocumentID, $status);
                if ($oDocument) {
                    $aInbox['document'][$oContact->document_id] = $oDocument;
                }
            }
        }


        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        foreach ($aInbox['document'] as $aDocument) {
            $aInbox_new['document'][] = $aDocument;
        }

        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }


    public function badges($id)
    {
        $aBadges = [];
        $nUserId = $this->userService->getIdentity();
        $nAccountId = $id;
        if (empty($nAccountId) || !is_numeric($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountId));
        }

        $aBadges['inbox'] = Documents::find("account_id=$nAccountId and status='completed' ")->count();
        $aBadges['sent'] = Documents::find("account_id=$nAccountId")->count();
        $aBadges['draft'] = DraftDocuments::find("account_id=$nAccountId")->count();
        $aBadges['pennding'] = Documents::find("account_id=$nAccountId and status = 'inprogress'")->count();
        $aBadges['rejected'] = Documents::find(array("account_id=$nAccountId and status='rejected'", "order" => "created_at desc"))->count();

        $aBadgesWithTotal = array('badgets' => $aBadges, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aBadgesWithTotal, 'data');
    }

    public function getDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("account_id");

        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountID));
        }

        $oAccount = Accounts::findFirstById($nAccountID);
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountID));
        }

        $nSignID = $oAccount->default_sign_id;
        if (!is_null($nSignID) && !empty($nSignID)) {
            $oSign = Signs::findFirstById($nSignID);
            if (!$oSign) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
            }
            $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aResult, 'data');
        }

        $oSign = Signs::findFirst("user_id=" . $nUserID);
        if (!$oSign) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSignID));
        }
        $aResult = array('sign' => $oSign->toArray(), 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    public function setDefaultSignature()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("account_id");
        $nSigntID = $this->request->getPost("sign_id");
        if (empty($nAccountID) || !is_numeric($nAccountID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('account_id' => $nAccountID));
        }

        $oAccount = Accounts::findFirstById($nAccountID);
        if (!$oAccount) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountID));
        }

        $oSign = Signs::findFirstById($nSigntID);
        if (!$oSign) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('sign_id' => $nSigntID));
        }

        $oAccount->default_sign_id = $nSigntID;

        if (!$oAccount->save()) {
            return $oAccount->getMessages();
        }

        $aResult = array('response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResult, 'data');

    }

    public function sign(){


        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $nDocumentID = $this->request->getPost("document_id");
        $nStateID = $this->request->getPost("state_id") ;
        $nSignID  = $this->request->getPost("sign_id") ;



        $this->db->begin();

        if( empty($nAccountID) || !is_numeric($nAccountID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'1',array('id' => $nAccountID));
        }

        if( empty($nDocumentID) || !is_numeric($nDocumentID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'2',array('document_id' => $nDocumentID));
        }

        if( empty($nSignID) || !is_numeric($nSignID) ){
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and is_default=1') ;
            if(!$oSign){
                $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature"' ) ;
                if(!$oSign) {
                    throw new Exception(ErrorCodes::DATA_MISSING,'3',array('account' =>"account didn't has sign"));
                }
            }

        }else{
            $oSign = Signs::findFirst('user_id=' . $nUserID . ' and sign_type="Signature" and id=' . $nSignID) ;
            if(!$oSign){
                throw new Exception(ErrorCodes::DATA_NOT_FOUND,'',[]);
            }
        }

        $nSignID = $oSign->id ;
        $oDefaultSign = Signs::findFirst("user_id=" . $nUserID . ' and sign_type="Signature" and id=' . $nSignID) ;

        if(!$oDefaultSign){
            throw new Exception(ErrorCodes::DATA_MISSING,$nUserID.'add sign' . $nSignID,array('sign' => $nSignID));

        }

        $oDocument = Documents::findFirst("id=$nDocumentID") ;
        $oDocumentSignerAccount = DocumentSignerAccount::findFirst("account_id='$nAccountID' and document_id=$nDocumentID ");
        if(!$oDocumentSignerAccount){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND,'',[]);
        }

        $oDocumentSignerAccount->is_sign = 1;
        $oDocumentSignerAccount->sign_at = date("Y-m-d H:i:s");
        $oDocumentSignerAccount->sign_id = $nSignID;

        $sSignCode =   $nDocumentID . '.' . $nAccountID . '.' . $oDocumentSignerAccount->account_id ;
        $sSignCodeWithUrl = $this->config->web->host . '/' . $this->config->web->sign_validation_action . '/' . $sSignCode;
        $qrImage = 'data:image/png;base64,' . $this->qr->getBarcodePNG($sSignCodeWithUrl, "QRCODE");

        $oDefaultSignWithQR = $this->addQRToSign($qrImage,$oDefaultSign->sign_data) ;

        $oDocumentSignData = new DocumentSignData();
        $oDocumentSignData->sign_code = $sSignCode ;
        $oDocumentSignData->sign_image = $oDefaultSignWithQR ;
        $oDocumentSignData->created_at = date("Y-m-d H:i:s") ;
        if(!$oDocumentSignData->save()){
            throw new Exception(ErrorCodes::DATA_FAILED,'',[]);
        }

        $oDocumentSignerAccount->document_sign_data_id = $oDocumentSignData->id;

        $aDocumentSigners = array('oDocumentSignee' => [], 'status' => false);

        if ($oDocumentSignerAccount->save()) {

            $nIsSignCounter = 0;
            # To-Do : check all signer (user,department,role)
            $oAllDocumentSigners = DocumentSignerAccount::find("document_id=$nDocumentID ");
            foreach ($oAllDocumentSigners as $oSigner){
                if ($oSigner->is_sign == 1){
                    $nIsSignCounter++;
                }
            }


            //document sender
            $oDocumentSenderAccount = DocumentSenderAccount::findFirst("document_id=".$oDocument->id) ;
            Notifications::createNotification($oDocumentSenderAccount->account_id ,$nAccountID,$oDocument->id,4);

            //document is completed
            $this->db->commit();
            if ($oAllDocumentSigners->count() == $nIsSignCounter) {
//                if( empty($nStateID) || !is_numeric($nStateID) ){
//                    $oDocumentWorkFlow = DocumentWorkflow::where("document_id",$nDocumentID)->first();
//                    $oStage = Stage::find($oDocumentWorkFlow->wf_current_stage_id)->first() ;
//                    $oStageState = State::where("stage_id",$oStage->id)->first() ;
//                    $nStateID = $oStageState->id;
//                }


                //$oStageState = State::find($nStateID)->first() ;
                //$nNextStage = $oStageState->next_stage_id ;

                $oDocument = Documents::findFirstById($nDocumentID);
                //$oDocumentWorkFlow = DocumentWorkflow::where("document_id",$nDocumentID)->first();
                //$oDocumentWorkFlow->wf_current_stage_id  = $nNextStage ;
//                if(!$oDocumentWorkFlow->save()){
//                    throw new Exception(ErrorCodes::DATA_FAILED,'',array('error' => $oDocumentWorkFlow->getMessage()));
//                }

                $oDocument->status = 'completed';
                $oDocument->save();

                $oDocument = \MyCompose\LaravelModels\Document\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                    'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SignerAccount.Account.User', 'SignerAccount.Account.User', 'SignerAccount.DocumentSignData', 'SenderAccount.Account.ProfileImage',
                    'ToUser.User.ProfileImage', 'CcUser.User.ProfileImage', 'SignerUser.User.ProfileImage', 'SenderUser.User.ProfileImage',
                    'ToRole.Role', 'CcRole.Role', 'SignerRole.Role',
                    'ToInvitations.Invitations', 'CcInvitations.Invitations', 'SignerInvitations.Invitations',
//            'WorkFlow.WorkFlow.Stage.WfState.WfStateAction.wfAction.wfFunction',
                    'Layout.LayoutComponent',
                    'QrCode',
                    'Files.File'])->where('id', $nDocumentID)->get()->first();
                $aDocument = $oDocument->toArray();
                $pdfPath = PDF::saveFromDocument($aDocument);
                $aEmails = [] ;
                $oToAccouts = DocumentToAccount::find("document_id=" . $nDocumentID );
                foreach($oToAccouts as $oAccount){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {

                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oCcAccouts = DocumentCcAccount::find("document_id=" . $nDocumentID );

                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {

                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oToInvitaion = DocumentToInvitations::find("document_id=" . $nDocumentID );
                foreach($oToInvitaion as $oInvitaion){
                    $sEmail = $oAccount->Accounts->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oCcAccouts = DocumentCcInvitations::find("document_id=" . $nDocumentID );
                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Invitations->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                $oSignerAccouts = DocumentCcAccount::find("document_id=" . $nDocumentID );
                foreach($oCcAccouts as $oAccount){
                    $sEmail = $oAccount->Users->email;
                    if(in_array($sEmail,$aEmails)){
                        continue;
                    }
                    $aEmails[] = $sEmail;
                    if (filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
                        $mail = new Mail();

                        $sendmail = $mail->send($sEmail, 'MyCompose New Document', 'new_document', [ 'document_id' => $nDocumentID, 'email' => $sEmail]);
                    }

                }

                //find document creator
                $oDocumentSenderAccount = DocumentSenderAccount::findFirst("document_id=".$oDocument->id) ;
                //create notification when document signed
                Notifications::createNotification($oDocumentSenderAccount->account_id,$nAccountID,$oDocument->id,3);

            }



            $aDocumentSigners['oDocumentSignee'] = $oDocumentSignerAccount;
            $aDocumentSigners['status'] = true;
        }

        $this->db->commit();


       // UserActivity::createActivity('user_signed_document',$nUserID,$nAccountID);

        return $this->createArrayResponse($aDocumentSigners, 'data');

    }

    public function getWaitingFromOthers($accountId)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nDocumentSender = DocumentSenderAccount::find(array("account_id=$accountId"));

        $waitingDocuments = array();
        foreach ($nDocumentSender as $documentSender) {
            $docId = $documentSender->document_id;
            $documentSignerAccount = DocumentSignerAccount::findFirst("document_id=$docId and is_sign=0");
            if ($documentSignerAccount) {
                $doc = $this->getDocumentById($docId, 'inprogress');
                if ($doc){
                    $waitingDocuments[] = $doc;
                }
            }
        }

        if ($waitingDocuments)
            $aDocumentsWaitingOthers = array('document' => $waitingDocuments, 'response_code' => 1, 'response_message' => "success");
        else
            $aDocumentsWaitingOthers = array('document' => [], 'response_code' => 1, 'response_message' => "success");

        $aDocumentsWaitingOthers['RecordsTotal'] = sizeof($aDocumentsWaitingOthers['document']);

        return $this->createArrayResponse($aDocumentsWaitingOthers, 'data');
    }

    public function needsMySign($nAccountId)
    {


        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocumentsSignerAccounts = DocumentSignerAccount::find(array("account_id=$nAccountId and is_sign =0", "order" => "document_id desc"));
//        $aInbox = array('document' => $oDocumentsSignerAccounts, 'response_code' => 1, 'response_message' => "success");

        $oDocumentsSignerAccounts = DocumentSignerAccount::find(array("account_id=$nAccountId", "order" => "document_id desc"));

        $aNeedsMySignDocuments = array();
        foreach ($oDocumentsSignerAccounts as $oDocumentsSignerAccount) {
            $nDocumentID = $oDocumentsSignerAccount->document_id;
            $oDocument = $this->getDocumentById($nDocumentID, 'inprogress');
            if ($oDocument) {
                $aNeedsMySignDocuments['document'][$oDocumentsSignerAccount->document_id] = $oDocument;
            }
        }

        if (sizeof($aNeedsMySignDocuments) > 0) {
            $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
            foreach ($aNeedsMySignDocuments['document'] as $aDocument) {
                $aInbox_new['document'][] = $aDocument;
            }
            $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

            return $this->createArrayResponse($aInbox_new, 'data');
        }

        $aInbox_new = array('document' => array(), 'response_code' => 1, 'response_message' => "success");
        $aInbox_new['RecordsTotal'] = sizeof($aInbox_new['document']);

        return $this->createArrayResponse($aInbox_new, 'data');
    }

    public function changeProfileImage(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $fProfileImage = $this->request->getPost("profile_image") ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if(!empty($fProfileImage)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('profile_image' => $fProfileImage));
        }



        if(!$this->request->hasFiles()) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'You should to upload file', []);
        }
        foreach ($this->request->getUploadedFiles() as $file) {
            $nFile = File::put($file);

        }

        $oAccount->profile_image = $nFile;

        if(!$oAccount->save()){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oFile = Files::findFirstById($nFile);

        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');


    }



    public function saveAvatar()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $this->request->get('contact_id');

        if ($this->request->hasFiles()) {
            foreach ($this->request->getUploadedFiles() as $file) {
                     $nFile = File::put($file);

            }

        }


        if (empty($nFile)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('avatar_data' => $nFile));
        }


        //$sSignDataWithWaterMark = $this->addWaterMark($sSignData);


        $oAvatar = new Avatar();
        $oAvatar->file_id = $nFile;
        $oAvatar->contact_id = $nContactID;
        $oAvatar->created_at = date('Y-m-d H:i:s');
        $oAvatar->is_primary = 1;
        if(!$oAvatar->save()){
            dd($oAvatar->getMessages());
        }
        $aAvatar = $oAvatar->toArray();
        $aAvatar['file'] = $oAvatar->File;
        $oUserProfileRequirements= new UserProfileRequirements();
        $oUserProfileRequirements->user_id = $nUserID ;
        $oUserProfileRequirements->profile_requirements_id = 1 ;
        $oUserProfileRequirements->created_at = date('Y-m-d H:i:s') ;
        $oUserProfileRequirements->status = 'complete' ;
        $oUserProfileRequirements->save();

        $aAvatar= array('avatar' => $aAvatar, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aAvatar, 'data');
    }


    public function deleteAvatar()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nContactID = $this->request->getPost('contact_id');
        $nFileID = $this->request->getPost('contact_id');
        if (empty($nFile)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('avatar_data' => $nFile));
        }

        $oAvatar = Avatar("file_id=" . $nFileID. " and contact_id=" . $nContactID );
        $oAvatar->deleted_at = date('Y-m-d H:i:s');
        $oAvatar->is_primary = 0;
        $oAvatar->deleted = 1;
        if(!$oAvatar->save()){
            dd($oAvatar->getMessages());
        }
        $oUserProfileRequirements=UserProfileRequirements("user_id=" . $nUserID . ' and profile_requirements_id=1');
        $oUserProfileRequirements->delete();

        $aAvatar= array( 'response_code' => 1, 'response_message' => "success" );
        return $this->createArrayResponse($aAvatar, 'data');
    }


    public function getContactsForRecipient($nAccountID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $sFilter = $this->request->get('q');

        $nUserID    = $this->userService->getIdentity();
        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContacts = Contacts::find("display_name like '%" . $sFilter . "%' and account_id='" . $nAccountID . "'" );
        $aContacts = $oContacts->toArray();
        foreach($oContacts as $k=>$oContact){
            $aContacts[$k]['emails'] = $oContact->Emails;
            $aContacts[$k]['mobiles'] = $oContact->Mobiles;
            $aContacts[$k]['recipient_type'] = 'contact';
        }

        $aResponse = array('aContacts' => $aContacts ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getContacts($nContactID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $sFilter = $this->request->get('q');
        $oContact = Contacts::findFirstById($nContactID);

        if(!$oContact){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oContacts = MyContact::find("my_id=" . $nContactID );
        $aContacts = $oContacts->toArray();
        foreach($oContacts as $k=>$oContact){
            if(!empty($sFilter)){
                foreach($oContact->Contact->Names as $name){
                    if(!strpos($name->display_name,$sFilter)){
                        continue;
                    }
                }
            }
            $aContacts[$k]['emails'] = $oContact->Contact->Emails;
            $aContacts[$k]['mobiles'] = $oContact->Contact->Mobiles;
            $aContacts[$k]['info'] = $oContact->Contact;
        }

        $aResponse = array('aContacts' => $aContacts ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');
    }



    public function saveContacts($nAccountID){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $aContacts = $this->request->getPost('aContacts') ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        foreach($aContacts as $aContact) {
            $oContact = Contacts::findFirst("google_id='" . $aContact['resource_name'] . "'");
            if(!$oContact) {
                $oContact = new Contacts();
            }

            $oContact->created_at = date('Y-m-d H:i:s') ;
            if(!$oContact->save()){
                return $oContact->getMessages();
            }

            if(isset($aContact['emails']) && sizeof($aContact['emails'])>0){
                foreach($aContact['emails'] as $email){
                    $oEmail = Emails::findFirst("email='" . $email['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oEmail){
                        $oEmail = new Emails();
                    }
                    $oEmail->email = $email['value'];
                    $oEmail->type = $email['type'];
                    $oEmail->contact_id = $oContact->id;
                    $oEmail->created_at = date('Y-m-d H:i:s') ;
                    $oEmail->save();
                }

            }

            if(isset($aContact['mobiles']) && sizeof($aContact['mobiles'])>0){
                foreach($aContact['mobiles'] as $mobile){
                    $oMobile = Mobiles::findFirst("mobile='" . $mobile['value'] . "' and contact_id=" . $oContact->id ) ;
                    if(!$oMobile){
                        $oMobile = new Mobiles();
                    }
                    $oMobile->mobile = $mobile['value'];
                    $oMobile->type = $mobile['type'];
                    $oMobile->contact_id = $oContact->id;
                    $oMobile->created_at = date('Y-m-d H:i:s') ;

                    $oMobile->save();
                }

            }


        }

        $aResponse = [ 'response_code' => 1 ,'response_message' => "success" ];
        return $this->createArrayResponse($aResponse, 'data' );
    }

    public function changeCoverImage(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID    = $this->userService->getIdentity();
        $nAccountID = $this->request->getPost("id");
        $fProfileImage = $this->request->getPost("profile_cover") ;

        $oAccount = Accounts::findFirstById($nAccountID);

        if(!$oAccount){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }


        if(!$this->request->hasFiles()) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, 'You should to upload file', []);
        }
        foreach ($this->request->getUploadedFiles() as $file) {
            $nFile = File::put($file);

        }

        $oAccount->profile_cover = $nFile;

        if(!$oAccount->save()){
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oFile = Files::findFirstById($nFile);

        $aFile = array('file_id' => $nFile,'oFile'=>$oFile ,'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aFile, 'data');


    }

    public function deactivate(){

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $nAccountId = $this->request->getPost('account_id');
        $sPassword = $this->request->getPost('password');
        $sNote = $this->request->getPost('message');

        if(empty($nAccountId)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('account_id' => $nAccountId));
        }

        if(empty($sPassword)) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('password' => $sPassword));
        }

        $oUser = Users::findFirstById($nUserId);
        $sEmail = $oUser->email;
        $sUserName = $sEmail;
        $AccountType = \App\Auth\UsernameAccountType::NAME;
        if(empty($sEmail)) {
            $sUserName= $oUser->mobile;
            $AccountType = \App\Auth\UsernameAccountType::MOBILE;
        }



        $session = $this->authManager->loginWithUsernamePassword($AccountType, $sUserName,
            $sPassword);

        $oAccount = Accounts::findFirstById($nAccountId) ;
        if(!$oAccount){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oAccount->active = 0;

        if(!$oAccount->save()){
            $aResponse = array('status' => 'false','error'=>$oAccount->getMessages() ,'response_code' => 0 ,'response_message' => "failed" );
            return $this->createArrayResponse($aResponse, 'data');
        }

        $oAccountDeactivate = new AccountDeactivate();
        $oAccountDeactivate->account_id = $nAccountId;
        $oAccountDeactivate->note = $sNote ;
        $oAccountDeactivate->created_at = date('Y-m-d H:i:s') ;
        $oAccountDeactivate->save();
        $aResponse = array( 'status' => 'true','response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse, 'data');

    }

    private function getDocumentByID($nDocumentID,$aIn='inbox'){


        if($aIn=='inbox'){

            $oDocument =  \MyCompose\LaravelModels\Document\Document::with(['DocumentContact.Contact.oPrimaryNames','DocumentContact.Contact.Avatar',
                'DocumentSenderContact.oPrimaryNames','DocumentSenderContact.Avatar.File',
            ])->where('id', $nDocumentID)->whereNotIn('status',['template','draft'])->get()->first();
            if($oDocument){
                $getIfRead = DocumentUserRead::findFirst("user_id=". $this->userService->getIdentity() . " and document_id=" . $nDocumentID) ;
                $bIsRead = $getIfRead ? true : false ;
                $oDocument['is_read'] = $bIsRead ;
            }

            return $oDocument;
        }

        return \MyCompose\LaravelModels\Document\Document::with(['DocumentContact.Contact.oPrimaryNames','DocumentContact.Contact.Avatar',
            'DocumentSenderContact.oPrimaryNames','DocumentSenderContact.Avatar.File',
        ])->where('id', $nDocumentID)->whereIn('status', $aIn )->get()->first();
    }

    private function getDraftDocumentByID($nDocumentID){

            $oDocument =  \MyCompose\LaravelModels\DraftDocument\Document::with(['ToDepartment.Department', 'CcDepartment.Department', 'SignerDepartment.Department', 'SenderDepartment.Department',
                'ToAccount.Account.ProfileImage', 'CcAccount.Account.ProfileImage', 'SignerAccount.Account.ProfileImage', 'SenderAccount.Account.ProfileImage' , 'SenderAccount.Account.User',
            ])->where('id', $nDocumentID)->get()->first();

            $bIsRead =true;
            $oDocument['is_read'] = $bIsRead ;
            return $oDocument;
            }

    private function getDocumentBySender($oContact){

        $oDocuments = Documents::find("contact_id=" . $oContact->id ) ;
        $aDocuments = [] ;
        foreach($oDocuments as $oDocument) {
            $aDoc = Document::getDocumentByID($oDocument->id);
            if($aDoc){
                $aDocuments[] = $aDoc;
            }

        }

        return $aDocuments ;

    }

    private function addQRToSign($fQR,$fSign)
    {
        $manager = new \Intervention\Image\ImageManager();
        $img = $manager->make($fSign);
        $type = 'png';
        $image_width = $img->width();
        $image_height = $img->height();
        $img2 = $manager->make($fQR);
        $img->insert($img2, 'bottom-right', 0, 0);
        $img->encode('png');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        $sSignDataWithQR = $base64;

        return $sSignDataWithQR;
    }

}
