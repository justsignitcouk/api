<?php

namespace App\Controllers;

use App\Constants\DefaultValues;
use App\Models\Workflow\DocumentWorkflow;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MyCompose\Model\Document\Documents;
use MyCompose\LaravelModels\Workflow\Action;
use MyCompose\LaravelModels\Workflow\Stage;
use MyCompose\LaravelModels\Workflow\State;
use MyCompose\LaravelModels\Workflow\Workflow;
use MyCompose\Model\WorkflowDepartment;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class WorkFlowsController extends CrudResourceController
{


    public function all(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $aWorkflow = array('workflows' => array() ,'response_code' => 1 ,'response_message' => "success" );

        $oWorkflow = Workflow::where(['active' => 1])->get();
        $aWorkflow['workflows'] = $oWorkflow ;
        return $this->createArrayResponse($aWorkflow,'data');
    }

    public function getWorkflowByID($nWorkflowID){

        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        if( empty($nWorkflowID) || !is_numeric($nWorkflowID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('workflow_id' => $nWorkflowID));
        }

        $aWorkflow = array('workflow' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oWorkflow = Workflow::where(['id'=>$nWorkflowID])->with(['Stage.StageState.State.StateAction.Action.WorkflowFunction','Stage.StageState.State.StateAction'])->get();
        $aWorkflow ['workflow'] = $oWorkflow->first();

        return $this->createArrayResponse($aWorkflow,'data');
    }

    public function getByAccount(){

        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nAccountID = $this->request->getPost("account_id") ;

        $aWorkflow = array('workflow' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oWorkflow = Workflow::with(['Stage.StageState.State.StateAction.Action.Function','Stage.StageState.State.StateAction'])->get();
        $aWorkflow ['workflow'] = $oWorkflow;


        return $this->createArrayResponse($aWorkflow,'data');


    }

    public function getByCompany(){

        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nCompanyID = $this->request->getPost("company_id") ;
        $term = $this->request->get('term');
        $aWorkflow = array('workflow' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oWorkflow = Workflow::where('company_id',$nCompanyID)->where('name','like','%'.$term.'%')->with(['Stage'=>function ($stages)
        {
            $stages->orderBy('sequence');
        },'Stage.StageState.State.StateAction.Action','Stage.StageState.State.StateAction'])->get();
        $aWorkflow ['workflow'] = $oWorkflow;


        return $this->createArrayResponse($aWorkflow,'data');


    }

    public function getStageDetails(){

        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $id = $this->request->get('id');
        $aStage = array('stage' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oStage = Stage::where('id',$id)->with(['StageState.State.StateAction.Action',
            'StageState.State.StateAction.StateActionPermitsUser.Account',
            'StageState.State.StateAction.StateActionPermitsRole.Role'
        ])
            ->orderBy('sequence')
            ->get()->first();
        $aStage ['stage'] = $oStage;


        return $this->createArrayResponse($aStage,'data');


    }

    public function saveWfStateAction(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nStateID = $this->request->getPost("state_id") ;
        $nActionID = $this->request->getPost("action_id") ;
        $nDocumentID = $this->request->getPost("document_id") ;
        $oState = State::where("id",'=',$nStateID)->get()->first();
        $oAction = Action::find($nActionID);

        if($oAction->action_type == 'to_next_stage') {
            $nNextStage = $oState['next_stage_id'] ;
            try{
                $oDocumentWorkflow = DocumentWorkflow::where('document_id',$nDocumentID)->firstOrFail();
            }catch (ModelNotFoundException $e){
                $oDocumentWorkflow = new DocumentWorkflow() ;
                $oDocumentWorkflow->document_id = $nDocumentID ;
                $oDocumentWorkflow->workflow_id = 1 ;
            }
            $oDocumentWorkflow->wf_current_stage_id = $nNextStage;

            if(!$oDocumentWorkflow->save()){
                return ['response_code' => 0 ,'response_message' => "failed" ,'message' => $oDocumentWorkflow->getMessage() ] ;
            }

            $oDocument = Documents::findFirstById($nDocumentID) ;
            if($oAction->name == 'Approve') {
                $oDocument->status = 'completed' ;
            }elseif($oAction->name == 'reject') {
                $oDocument->status = 'rejected' ;
            }

            $oDocument->save();

            return ['response_code' => 1 ,'response_message' => "success" ] ;
        }


    }

    public function getDefault(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nUserID = $this->userService->getIdentity();

        $nWorkFlowID = DefaultValues::WORKFLOW_ID;

        if( empty($nWorkFlowID) || !is_numeric($nWorkFlowID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('workflow_id' => 'check the default value'));
        }

        $oWorkflow = Workflow::where(['id'=>$nWorkFlowID])->with([
            'Stage.State.StateAction.Action.WorkflowFunction',
            'Stage.State.StateAction.Roles',
            'Stage.State.StateAction.Users',
        ])->get();
        $aWorkflow = $oWorkflow->first();

        $aResponse = array('workflow' => $aWorkflow ,'response_code' => 1 ,'response_message' => "success" );

        return $this->createArrayResponse($aResponse,'data');
    }


}
