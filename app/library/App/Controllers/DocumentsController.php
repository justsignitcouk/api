<?php

namespace App\Controllers;


use App\Auth\Access;
use App\Components\File\File;
use App\Components\Mail\Mail;
use App\Components\PDF\PDF;
use App\Components\SMS\SMS;
use App\Constants\DefaultValues;
use App\Constants\ErrorCodes;
use App\Middleware\AccessMiddleware;
use MyCompose\LaravelModels\Contacts\Contact;
use MyCompose\LaravelModels\Document\Document;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\ContactsRoles;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\Mobiles;
use MyCompose\Model\Document\DocumentCategories;
use MyCompose\Model\Document\DocumentCcRole;
use MyCompose\Model\Document\DocumentCcUser;
use MyCompose\Model\Document\DocumentFile;
use MyCompose\Model\Document\DocumentHistory;
use MyCompose\Model\Document\DocumentImage;
use MyCompose\Model\Document\DocumentLayout;
use MyCompose\Model\Document\DocumentLock;
use MyCompose\Model\Document\DocumentNotes;
use MyCompose\Model\Document\DocumentReference;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentUserRead;
use MyCompose\Model\Document\DocumentWorkflow;
use MyCompose\Model\Document\Forward\DocumentForward;
use MyCompose\Model\Document\Forward\ForwardContacts;
use MyCompose\Model\Document\Forward\ForwardUser;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Files;
use MyCompose\Model\Invitations;
use MyCompose\Model\Profiles\IONumber;
use MyCompose\Model\QrCodes;
use MyCompose\Model\Users;
use MyCompose\LaravelModels\UserActivity;
use MyCompose\LaravelModels\Workflow\Stage;
use MyCompose\Model\Workflow\Action;
use MyCompose\Model\Workflow\DocumentWorkflowActionHistory;
use MyCompose\Model\Workflow\StageState;
use MyCompose\Model\Workflow\Workflow;
use PhalconApi\Exception;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class DocumentsController extends CrudResourceController
{


    /************************ compose *********************************/

    public function compose()
    {
        $this->db->begin();
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }


        $nUserID = $this->userService->getIdentity();

        $sOutboxNumber = '';


        $sTitle = $this->request->getPost('title');
        $sContent = $this->request->getPost('content');

        $nLayoutID = $this->request->getPost('layout_id');
        $nCategoryID = $this->request->getPost('category_id');
        $nWorkFlowID = $this->request->getPost('workflow_id');

        $sStatus = $this->request->getPost('status');

        $aToUsers = $this->request->getPost('to_users');
        $aSignerUsers = $this->request->getPost('signers_users');
        $aCCUsers = $this->request->getPost('cc_users');

        $aDocumentsReference = $this->request->getPost('documents_reference');
        $sDocumentImage = $this->request->getPost('document_image');

        $aFiles = $this->request->getPost('files');

        $dCreatedDate = date("Y-m-d H:i:s");
        $dDocumentDate = $this->request->getPost('document_date');
        $sDocumentNumber = $this->request->getPost('document_number');
        $nDocumentId = $this->request->getPost('document_id');
        $emails = [];


        $bSendToSigner = true;
        if($this->request->hasPost('bSendToSigner')) {
            $bSendToSigner = true;
        }


        $error = false;

        if (empty($nLayoutID)) {
            $nLayoutID = 1;
        }


        $oDocumentLayout = DocumentLayout::findFirstById($nLayoutID);

        if (!$oDocumentLayout) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('layout_id' => $nLayoutID));
        }

        if (!empty($nCategoryID)) {
            $oDocumentCategory = DocumentCategories::findFirstById($nCategoryID);
            if (!$oDocumentCategory) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('category_id' => $nCategoryID));
            }
        }

        if (empty($nCategoryID)) {
            $nCategoryID = DefaultValues::CATEGORY_ID;
        }

        $oCategory = DocumentCategories::findFirstById($nCategoryID);
        $nWorkFlowID = false;
        if ($oCategory) {
            $nWorkFlowID = $oCategory->default_workflow_id;
        }

        if (!empty($nWorkFlowID)) {
            $oWorkFlow = Workflow::findFirstById($nWorkFlowID);
            if (!$oWorkFlow) {
                throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('workflow_id' => $nWorkFlowID));
            }
        }

        if (empty($nWorkFlowID)) {
            if (empty($nDocumentId)) {
                $nWorkFlowID = 1;
            } else {
                $nWorkFlowID = '';
            }
        }

        if ( $sStatus == 'inprogress') {
            if (
            empty($aToUsers)
            ) {
                $response['data_missing'][] = 'You must fill on of contacts,department';
                $error = true;
            }

        if (empty($sTitle) ) {
            $response['data_missing'][] = 'title';
            $error = true;
        }

        if (empty($sContent)) {
            $response['data_missing'][] = 'content';
            $error = true;
        }

        }

        if ($sStatus == 'inprogress') {
            $dDocumentDate = date('Y-m-d');
        }

        if ($error) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', $response['data_missing']);
        }

        $oUser = Users::findFirst("id=$nUserID ");


        if (empty($nDocumentId)) {
            $oDocument = new Documents();

        } else {
            $oDocument = Documents::findFirstById($nDocumentId);

            if ($oDocument->status == 'template') {
                $oDocument = new Documents();
            }

            $oRecipients = Recipients::find("document_id=" .$nDocumentId ) ;
            if($oRecipients->count()>0){
                $oRecipients->delete();
            }

            $oDocumentFile = DocumentFile::find("document_id=" . $nDocumentId );
            if($oDocumentFile->count()>0) {
                $oDocumentFile->delete();
            }

//            $oDocumentHistory = new DocumentHistory();
//            $oDocumentHistory->title = $oDocument->title;
//            $oDocumentHistory->content = $oDocument->content;
//            $oDocumentHistory->user_id = $nUserID;
//            $oDocumentHistory->document_id = $oDocument->id;
//            $oDocumentHistory->created_at = date('Y-m-d H:i:s');
//            $oDocumentHistory->save();
        }


        $oDocument->user_id = $nUserID;

        $oDocument->title = $sTitle;

        $oDocument->content = $sContent;
        $oDocument->text_content = strip_tags($sContent);

        $oDocument->category_id = $nCategoryID;
        $oDocument->layout_id = $nLayoutID;

        $oDocument->created_at = $dCreatedDate;

        $oDocument->privacy = 'private';
        $oDocument->document_date = $dDocumentDate;

        if(!empty($sStatus)){
            $oDocument->status = $sStatus;
        }


        if (!$oDocument->save()) {
            $messages = $oDocument->getMessages();
            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];
            return $this->createArrayResponse($response, 'data');
        }
        $nDocumentID = $oDocument->id;
        $oDocument->outbox_number = '';


        $oDocument->save();

        if ($oDocument->status == 'inprogress') {



            $oQrCode = new QrCodes();
            $oQrCode->generate(['data' => $oDocument->outbox_number]);
            $oQrCode->created_at = date('Y-m-d H:i:s');
            $oQrCode->created_by = $nUserID;
            if (!$oQrCode->save()) {
                return $oQrCode->getMessages();
            }

            $oDocument->qr_code_id = $oQrCode->id;
            $oDocument->save();
        }

        $inbox_count = 1;
        // $aToUsers , $aSignerUsers , $aCCUsers
        if (!empty($aToUsers) && is_array($aToUsers)) {
            foreach ($aToUsers as $key => $aToUser) {
                $sIdentifier = $key;
                $sIdentifier = str_replace("___", "@", $sIdentifier);
                $sIdentifier = str_replace("---", ".", $sIdentifier);
                $sIdentifier_type = 'mobile';
                if (filter_var($sIdentifier, FILTER_VALIDATE_EMAIL)) {

                    $sIdentifier_type = 'email';
                    $oUser = Users::findFirst("email='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }

                    # at send document buton, save new contacts in connections
                    $oEmail = Emails::findFirst("email='" . $sIdentifier . "' and user_id=" . $nUserID ) ;
                    if(!$oEmail){
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at  = $dCreatedDate;
                        $oContact->full_name  = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id ;
                        $oEmail  = new Emails();
                        $oEmail->email = $sIdentifier ;
                        $oEmail->contact_id = $nContactID;
                        $oEmail->user_id = $nUserID;
                        $oEmail->save();
                    }

                } else {
                    $oUser = Users::findFirst("mobile='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }
                    # at send document buton, save new contacts in connections
                    $oMobile = Mobiles::findFirst("mobile='" . $sIdentifier . "' and user_id=" . $nUserID);
                    if (!$oMobile) {
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at = $dCreatedDate;
                        $oContact->full_name = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id;
                        $oMobile = new Mobiles();
                        $oMobile->mobile = $sIdentifier;
                        $oMobile->contact_id = $nContactID;
                        $oMobile->user_id = $nUserID;
                        $oMobile->save();
                    }
                }
                $oRecipients = Recipients::findFirst("document_id=" . $oDocument->id . " and identifier='" . $sIdentifier . "' and type='To' ");
                if (!$oRecipients) {
                    $oRecipients = new Recipients();
                    $oRecipients->created_at = $dCreatedDate;
                    $oRecipients->document_id = $oDocument->id;
                    $oRecipients->identifier_type = $sIdentifier_type;
                    $oRecipients->type = 'To';
                }
                $oRecipients->user_id = $nRecipientsUserID;
                $oRecipients->identifier = $sIdentifier;
                $oRecipients->identifier_type = $sIdentifier_type;
                $oRecipients->display_name = $aToUser;
                $oRecipients->updated_at = $dCreatedDate;
                $oRecipients->inbox_number = $oDocument->outbox_number . "-" . $inbox_count++ ;
                if (!$oRecipients->save()) {
                    dd($oRecipients->getMessages());
                }


            }
        }


        if (!empty($aSignerUsers) && is_array($aSignerUsers)) {
            foreach ($aSignerUsers as $key => $aSignerUser) {
                $sIdentifier = $key;
                $sIdentifier = str_replace("___", "@", $sIdentifier);
                $sIdentifier = str_replace("---", ".", $sIdentifier);
                $sIdentifier_type = 'mobile';
                if (filter_var($sIdentifier, FILTER_VALIDATE_EMAIL)) {

                    $sIdentifier_type = 'email';

                    $oUser = Users::findFirst("email='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }

                    # at send document buton, save new contacts in connections
                    $oEmail = Emails::findFirst("email='" . $sIdentifier . "' and user_id=" . $nUserID ) ;
                    if(!$oEmail){
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at  = $dCreatedDate;
                        $oContact->full_name  = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id ;
                        $oEmail  = new Emails();
                        $oEmail->email = $sIdentifier ;
                        $oEmail->contact_id = $nContactID;
                        $oEmail->user_id = $nUserID;
                        $oEmail->save();
                    }
                } else {
                    $oUser = Users::findFirst("mobile='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }
                    # at send document buton, save new contacts in connections
                    $oMobile = Mobiles::findFirst("mobile='" . $sIdentifier . "' and user_id=" . $nUserID);
                    if (!$oMobile) {
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at = $dCreatedDate;
                        $oContact->full_name = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id;
                        $oMobile = new Mobiles();
                        $oMobile->mobile = $sIdentifier;
                        $oMobile->contact_id = $nContactID;
                        $oMobile->user_id = $nUserID;

                        $oMobile->save();
                    }

                }
                $oRecipients = Recipients::findFirst("document_id=" . $oDocument->id . " and identifier='" . $sIdentifier . "' and type='Signer' ");
                if (!$oRecipients) {
                    $oRecipients = new Recipients();
                    $oRecipients->created_at = $dCreatedDate;
                    $oRecipients->document_id = $oDocument->id;
                    $oRecipients->identifier_type = $sIdentifier_type;
                    $oRecipients->type = 'Signer';
                    $oRecipients->can_read = 0;
                    if($bSendToSigner){
                        $oRecipients->can_read = 1;
                    }

                }
                $oRecipients->user_id = $nRecipientsUserID;
                $oRecipients->identifier = $sIdentifier;
                $oRecipients->identifier_type = $sIdentifier_type;
                $oRecipients->display_name = $aSignerUser;
                $oRecipients->updated_at = $dCreatedDate;
                $oRecipients->inbox_number = $oDocument->outbox_number . "-" . $inbox_count++ ;
                if (!$oRecipients->save()) {
                    dd($oRecipients->getMessages());
                }
            }
        }

        if (!empty($aCCUsers) && is_array($aCCUsers)) {
            foreach ($aCCUsers as $key => $aCCUser) {
                $sIdentifier = $key;
                $sIdentifier = str_replace("___", "@", $sIdentifier);
                $sIdentifier = str_replace("---", ".", $sIdentifier);
                $sIdentifier_type = 'mobile';
                if (filter_var($sIdentifier, FILTER_VALIDATE_EMAIL)) {

                    $sIdentifier_type = 'email';

                    $oUser = Users::findFirst("email='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }

                    # at send document buton, save new contacts in connections
                    $oEmail = Emails::findFirst("email='" . $sIdentifier . "' and user_id=" . $nUserID ) ;
                    if(!$oEmail){
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at  = $dCreatedDate;
                        $oContact->full_name  = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id ;
                        $oEmail  = new Emails();
                        $oEmail->email = $sIdentifier ;
                        $oEmail->contact_id = $nContactID;
                        $oEmail->user_id = $nUserID;
                        $oEmail->save();
                    }
                } else {
                    $oUser = Users::findFirst("mobile='" . $sIdentifier . "'");
                    $nRecipientsUserID = null;
                    if (!$oUser) {
                        //invite
                    } else {
                        $nRecipientsUserID = $oUser->id;
                    }

                    # at send document buton, save new contacts in connections
                    $oMobile = Mobiles::findFirst("mobile='" . $sIdentifier . "' and user_id=" . $nUserID);
                    if (!$oMobile) {
                        $oContact = new Contacts();
                        $oContact->user_id = $nUserID;
                        $oContact->created_at = $dCreatedDate;
                        $oContact->full_name = $aToUser;
                        $oContact->save();
                        $nContactID = $oContact->id;
                        $oMobile = new Mobiles();
                        $oMobile->mobile = $sIdentifier;
                        $oMobile->contact_id = $nContactID;
                        $oMobile->user_id = $nUserID;
                        $oMobile->save();
                    }
                }
                $oRecipients = Recipients::findFirst("document_id=" . $oDocument->id . " and identifier='" . $sIdentifier . "' and type='CC' ");
                if (!$oRecipients) {
                    $oRecipients = new Recipients();
                    $oRecipients->created_at = $dCreatedDate;
                    $oRecipients->document_id = $oDocument->id;
                    $oRecipients->identifier_type = $sIdentifier_type;
                    $oRecipients->type = 'CC';
                }

                $oRecipients->user_id = $nRecipientsUserID;
                $oRecipients->identifier = $sIdentifier;
                $oRecipients->display_name = $aCCUser;
                $oRecipients->updated_at = $dCreatedDate;
                $oRecipients->inbox_number = $oDocument->outbox_number . "-" . $inbox_count++ ;
                if (!$oRecipients->save()) {
                    dd($oRecipients->getMessages());
                }
            }
        }


        #end save sign

        #assign document on workflow

        if($oDocument->status=='inprogress') {
            $oDocumentWorkFlow = DocumentWorkflow::findFirst("document_id=" . $oDocument->id . " and active=1");
            //$oWorkFlow = Workflow::findFirstById($oDocumentWorkFlow->workflow_id) ;
            $oDocumentWorkFlow->active = 0;
            $oDocumentWorkFlow->updatead_at = $dCreatedDate;
            $oDocumentWorkFlow->save();
            $oStageState = StageState::findFirst("stage_id=" . $oDocumentWorkFlow->current_stage_id);
            $oDocumentWorkFlow_new = new DocumentWorkFlow();
            $oDocumentWorkFlow_new->document_id = $oDocument->id;
            $oDocumentWorkFlow_new->workflow_id = $oDocumentWorkFlow->workflow_id;
            $oDocumentWorkFlow_new->current_stage_id = $oStageState->next_stage_id;
            $oDocumentWorkFlow_new->active = 1;
            $oDocumentWorkFlow_new->created_at = $dCreatedDate;
            $oDocumentWorkFlow_new->save();



        }
        #assign document on workflow

        if (!empty($aDocumentsReference)) {
            foreach ($aDocumentsReference as $aDocumentReferenceID) {
                $oDocumentReference = new DocumentReference();
                $oDocumentReference->document_id = $aDocumentReferenceID;
                $oDocumentReference->document_reference_id = $oDocument->id;
                $oDocumentReference->created_at = date('Y-m-d H:i:s');
                if (!$oDocumentReference->save()) {
                    var_dump($oDocumentReference->getMessages());
                    exit();
                }
            }
        }

        #save attachments

        if (!empty($aFiles) && is_array($aFiles)) {
            foreach ($aFiles as $FileKey => $nFile) {
                $oDocumentFile = new DocumentFile();
                $oDocumentFile->file_id = $nFile;
                $oDocumentFile->document_id = $oDocument->id;
                $oDocumentFile->created_at = date('Y-m-d H:i:s');
                $oDocumentFile->save();
            }
        };
        $oDocument->save();

        $response = [
            'status' => 'true',
            'document_id' => $oDocument->id,
            'document_status' => $sStatus,

        ];

        $this->db->commit();

        return $this->createArrayResponse($response, 'data');

    }

    public function insertNewDraft()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }

        $nTemplateId = $this->request->getPost('template_id');
        if(empty($nTemplateId)) {


        $nUserID = $this->userService->getIdentity();
        $oDocument = new Document();
        $oDocument->status = 'draft';
        $oDocument->user_id = $nUserID;
        $oDocument->created_at = date('Y-m-d H:i:s');
        $oDocument->layout_id = 1;
        $oDocument->title = '';
        $oDocument->content = '';
        if (!$oDocument->save()) {
            dd($oDocument->getMessages());
        }
        }else{
            $oTemplate = Documents::findFirstById($nTemplateId) ;

            $nUserID = $this->userService->getIdentity();
            $oDocument = new Document();
            $oDocument->status = 'draft';
            $oDocument->user_id = $nUserID;
            $oDocument->created_at = date('Y-m-d H:i:s');
            $oDocument->layout_id = $oTemplate->layout_id;
            $oDocument->title = $oTemplate->title;
            $oDocument->content = $oTemplate->content;
            if (!$oDocument->save()) {
                dd($oDocument->getMessages());
            }

            $oRecipients = Recipients::find("document_id=" . $nTemplateId ) ;
            foreach($oRecipients as $oRecipient){
                $oR = new Recipients();
                $oR->document_id =$oDocument->id ;
                $oR->user_id =$oRecipient->user_id ;
                $oR->identifier =$oRecipient->identifier ;
                $oR->identifier_type =$oRecipient->identifier_type ;
                $oR->type =$oRecipient->type ;
                $oR->display_name =$oRecipient->display_name ;
                $oR->created_at = date('Y-m-d H:i:s');
                if(!$oR->save()){
                    dd($oR->getMessages());
                }
            }

            $oDocumentFiles = DocumentFile::find("document_id=" . $nTemplateId) ;
            foreach($oDocumentFiles as $oDocumentFile){
                $oDF = new DocumentFile();
                $oDF->file_id = $oDocumentFile->file_id;
                $oDF->created_at = date('Y-m-d H:i:s');
                $oDF->document_id = $oDocument->id ;
                $oDF->save();
            }

        }

        $oDocumentWorkFlow = new DocumentWorkflow();
        $oDocumentWorkFlow->workflow_id = DefaultValues::WORKFLOW_ID;
        $oDocumentWorkFlow->document_id = $oDocument->id ;
        $oDocumentWorkFlow->active = 1 ;
        $oDocumentWorkFlow->created_at = date('Y-m-d H:i:s');
        $oWorkFlow = Workflow::findFirstById(DefaultValues::WORKFLOW_ID);
        $oDocumentWorkFlow->current_stage_id  = $oWorkFlow->startStageID();

        $oDocumentWorkFlow->save();

        return $this->createArrayResponse(['draft_id' => $oDocument->id], 'data');
    }

    public function forwardDocument()
    {
        $this->db->begin();

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }


        $nUserID = $this->userService->getIdentity();


        if (empty($nUserID) || !is_numeric($nUserID)) {

            throw new Exception(ErrorCodes::DATA_MISSING, 'Data Missing!', array('contact_id' => $nContactID));

        }

        $sOutboxNumber = '';

        $nParentDocumentID = $this->request->getPost('document_id');
        $nForwardID = $this->request->getPost('forward_id');
        $aUserForward = $this->request->getPost('user_forward');
        $aNoteContent = $this->request->getPost('note_content');
        $aNotePosition = $this->request->getPost('note_content_position');
        $sStatus = 'forwarded';

        $dCreatedDate = date("Y-m-d H:i:s");

        $oUser = Users::findFirst("id=$nUserID ");


        if (empty($nParentDocumentID)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', []);
        }

        $oParentDocument = Documents::findFirstById($nParentDocumentID);

        if (!$oParentDocument) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', []);
        }

        $oDocumentForward = new DocumentForward();

        $oDocumentForward->document_id = $oParentDocument->id;
        $oDocumentForward->note = $aNoteContent;
        $oDocumentForward->note_position = $aNotePosition;
        $oDocumentForward->created_at = $dCreatedDate;
        $oDocumentForward->user_id = $nUserID;
        $oDocumentForward->parent_forward_id = $nForwardID;

        if (!$oDocumentForward->save()) {
            $messages = $oDocumentForward->getMessages();
            $errorMsg = '';
            foreach ($messages as $message) {
                $errorMsg .= "{$message} <br>";
            }
            $response = ['messages' => $errorMsg];
            return $this->createArrayResponse($response, 'data');
        }


        if (!empty($aUserForward) && is_array($aUserForward)) {
            foreach ($aUserForward as $nToUserID) {
                $sIdent = str_replace("___", "@", $nToUserID);
                $sIdent = str_replace("---", ".", $sIdent);
                $oForwardUser = new Recipients();
                $oUser = Users::findFirst("email='" . $sIdent . "' or mobile='" . $sIdent . "'");

                if ($oUser) {
                    $nid = $oUser->id;
                    $oForwardUser->user_id = $nid;
                }

                $oForwardUser->identifier = $sIdent;
                $oForwardUser->type = 'Forward';
                if (filter_var($sIdent, FILTER_VALIDATE_EMAIL)) {
                    $oForwardUser->identifier_type = 'email';
                } else {
                    $oForwardUser->identifier = 'mobile';
                }

                $oForwardUser->document_id = $oParentDocument->id;
                $oForwardUser->forward_id = $oDocumentForward->id;
                $oForwardUser->created_at = $dCreatedDate;
                if (!$oForwardUser->save()) {
                    dd($oForwardUser->getMessages());
                }
            }
        }

        $response = [
            'status' => 'true',
            'document_id' => $nParentDocumentID,
            'forward_id' => $oDocumentForward->id
        ];

        $this->db->commit();

        return $this->createArrayResponse($response, 'data');

    }


    private function addDocumentContracts($oContracts, $nDocumentID, $type)
    {
        foreach ($oContracts as $nRContactID => $sRContactDescription) {

            $checkContact = Contacts::findFirst("id=" . $nRContactID);
            if (!$checkContact) {
                continue;
            }
            $oDocumentRContact = new \MyCompose\Model\Document\Recipients\Contacts();
            $oDocumentRContact->contact_id = $nRContactID;
            $oDocumentRContact->display_name = $sRContactDescription;
            $oDocumentRContact->type = $type;
            if ($type == 'Signer') {
                $oDocumentRContact->can_read = 1;
            }
            $oDocumentRContact->document_id = $nDocumentID;
            $oDocumentRContact->created_at = date('Y-m-d H:i:s');

            if (!$oDocumentRContact->save()) {
                var_dump($oDocumentRContact->getMessages());
                return $oDocumentRContact->getMessages();
            }
        }
        return true;
    }

    private function checkIfToInvite($nToUserID, $nAccountID)
    {
        $nToUserID = trim($nToUserID);
        if (filter_var($nToUserID, FILTER_VALIDATE_EMAIL)) {

            $oUser = Users::findFirst("email='$nToUserID'");

            if ($oUser) {
                throw new Exception(ErrorCodes::DATA_FAILED, 'error', ['error' => 'user exist']);
            }

            $mail = new Mail();
            $sendmail = $mail->send($nToUserID, 'MyCompose invitation', 'myComposeInvitation', []);

            $checkInvite = Invitations::findFirst("email='$nToUserID' and account_id='$nAccountID' ");
            if ($checkInvite) {
                return $checkInvite->id;
            }

            $oInvitaion = new Invitations();
            $oInvitaion->email = $nToUserID;
            $oInvitaion->user_id = $this->userService->getIdentity();
            $oInvitaion->account_id = $nAccountID;
            $oInvitaion->created_at = date('Y-m-d H:i:s');
            if (!$oInvitaion->save()) {
                var_dump($oInvitaion->getMessages());
                exit();
            }

            return $oInvitaion->id;

        } else if (strncmp($nToUserID, DefaultValues::MOBILE_PHONE_PREFIX, strlen(DefaultValues::MOBILE_PHONE_PREFIX)) === 0
            && strlen("$nToUserID") >= 10
        ) {

            $oUser = Users::findFirst("mobile='$nToUserID'");

            if ($oUser) {
                throw new Exception(ErrorCodes::DATA_FAILED, 'error', ['error' => 'user exist']);
            }

            $sms = new SMS();
            $sms->to = $nToUserID;
            $sms->from = 'MyCompose';
            $sms->message = "Welcome to my compose, Someone sent to you invitation,To show this document, you can click bellow and go to MyCompose to read it:" . $this->config->web->host;
            $r = $sms->send();

            $checkInvite = Invitations::findFirst("mobile='$nToUserID' and account_id='$nAccountID' ");
            if ($checkInvite) {
                return $checkInvite->id;
            }

            $oInvitaion = new Invitations();
            $oInvitaion->mobile = $nToUserID;
            $oInvitaion->user_id = $this->userService->getIdentity();
            $oInvitaion->created_at = date('Y-m-d H:i:s');
            $oInvitaion->save();


            return $oInvitaion->id;

        }

        throw new Exception(ErrorCodes::USER_NOT_FOUND, 'User/s not found', array('user_id' => $nToUserID));


    }

    function incrementalHash($len = 5)
    {
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';

        $now = explode(' ', microtime())[1];
        while ($now >= $base) {
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }
        return substr($result, -5);
    }

    /************************ compose *********************************/

    public function getDocumentForwarded($nForwardedID)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $oDocumentForwarded = DocumentForward::findFirstById($nForwardedID);
        if (!$oDocumentForwarded) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $aResponse = array('oDocumentForward' => $oDocumentForwarded, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function getDocumentByID($nDocumentID, $nUserID = null)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        if (!$this->checkIfUserHasAccess($nDocumentID, $oUser)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', array('document_id' => $nDocumentID));
        }

        //save Read :
        $oRead = Recipients::findFirst("user_id=" . $nUserID . " and document_id=" . $nDocumentID . " and can_read=1 ");
        if ($oRead) {
            if ($oRead->is_read == 0) {
                $oRead->is_read = 1;
                $oRead->read_at = date('Y-m-d H:i:s');
                $oRead->save();
            }
        }

        $oDocumentRecipients = Recipients::find("document_id=" . $nDocumentID . " and user_id=$nUserID and can_read=1");

        if ($oDocumentRecipients->count() > 0) {
            foreach ($oDocumentRecipients as $oDocumentRecipient) {
                if (empty($oDocumentRecipient->read_at)) {
                    $oDocumentRecipient->read_at = date('Y-m-d H:i:s');
                    if (!$oDocumentRecipient->save()) {

                    }
                }

            }
        }


        // workflow , attachment , user profile_image  , account_profile_image,
        $oDocument = \MyCompose\LaravelModels\Document\Document::with([
            'Recipients.User', 'Recipients.Sign',
            'Layout',
            'WorkFlow.WorkFlow.Stage.StageState.State.StateAction.Action',
            'QrCode',
            'DocumentNotes.User',
            'Files.File'])->where('id', $nDocumentID)->get()->first();
        $oActions =[];
        if(!empty($oDocument->WorkFlow)){
        $oActions = Action::getActionsByUserId([    'document_id' => $oDocument->id,
                                                    'workflow_id' => $oDocument->WorkFlow->workflow_id ,
                                                    'stage_id' => $oDocument->WorkFlow->current_stage_id,
                                                    'user_id' =>$nUserID
        ]);
        }
        $aDocument = array('document' => $oDocument,'oActions'=>$oActions, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aDocument, 'data');
    }

    public function checkAccess($nDocumentID)
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $oUser = Users::findFirstById($nUserID);

        if (!$this->checkIfUserHasAccess($nDocumentID, $oUser)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', array('document_id' => $nDocumentID));
        }

        $aDocument = array('access' => true, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aDocument, 'data');
    }

    public function setStatus($nDocumentID)
    {
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $sStatus = $this->request->getPost("status");
        if (!$this->checkIfStatusValid($sStatus)) {
            throw new Exception(ErrorCodes::POST_DATA_INVALID, 'Status invalid', array('status' => DefaultValues::DOCUMENT_STATUS));
        }

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        if (empty($sStatus) || !is_string($sStatus)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('status' => $sStatus));
        }
        $oDocument = Documents::findFirstById($nDocumentID);
        $oDocument->status = $sStatus;
        $aDocument = array('oDocument' => [], 'status' => false);
        if ($oDocument->save()) {
            $aDocument = array('oDocument' => $oDocument, 'status' => true);
        }

        if ($sStatus == 'reject') {

            UserActivity::createActivity('user_rejected_document', $this->userService->getIdentity(), null);
        }

        return $this->createArrayResponse($aDocument, 'data');
    }

    public function getOutboxNumber($aParams = [])
    {

        return date("dmyHis") . '-' .
            $aParams['company_id'] . '-' .
            $aParams['department_id'] . '-' .
            $aParams['contact_id'] . '-' .
            $aParams['document_id'];
    }

    public function getLayout()
    {
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nDocumentID = $this->request->getPost("document_id");

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND, '', array('document_id' => $nDocumentID));
        }
    }

    public function pdf($id)
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nDocumentID = $id;
        $nUserID = $this->userService->getIdentity();

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        if (!$this->checkIfUserHasAccess($nDocumentID, $oUser)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', array('document_id' => $nDocumentID));
        }

        //save Read :
        $oRead = Recipients::findFirst("user_id=" . $nUserID . " and document_id=" . $nDocumentID . " and can_read=1 ");
        if ($oRead) {
            if ($oRead->is_read == 0) {
                $oRead->is_read = 1;
                $oRead->read_at = date('Y-m-d H:i:s');
                $oRead->save();
            }
        }

        $oDocumentRecipients = Recipients::find("document_id=" . $nDocumentID . " and user_id=$nUserID and can_read=1");

        if ($oDocumentRecipients->count() > 0) {
            foreach ($oDocumentRecipients as $oDocumentRecipient) {
                if (empty($oDocumentRecipient->read_at)) {
                    $oDocumentRecipient->read_at = date('Y-m-d H:i:s');
                    if (!$oDocumentRecipient->save()) {

                    }
                }

            }
        }


        // workflow , attachment , user profile_image  , account_profile_image,
        $oDocument = \MyCompose\LaravelModels\Document\Document::with([
            'Recipients.User', 'Recipients.Sign',
            'Layout',
            'WorkFlow.WorkFlow.Stage.StageState.State.StateAction.Action',
            'QrCode',
            'DocumentNotes.User',
            'Files.File'])->where('id', $nDocumentID)->get()->first();
        $aDocument = $oDocument->toArray();
        return PDF::generateFromDocument($aDocument);

    }

    public function lock($id)
    {

        $nUserID = $this->userService->getIdentity();
        $nDocumentID = $id;
        $nAccountID = $this->request->getPost('account_id');

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        //check if this account have access

        if (!$this->checkIfUserHasAccess($nDocumentID, $oUser)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, '', array('document_id' => $nDocumentID));
        }

        $oDocumentLock = new DocumentLock();
        $oDocumentLock->document_id = $id;
        $oDocumentLock->account_id = $nAccountID;
        $oDocumentLock->created_at = date('Y-m-d H:i:s');
        $oDocumentLock->active = 1;
        $oDocumentLock->save();


    }


    public function getNotes($id)
    {

        $nUserID = $this->userService->getIdentity();
        $nDocumentID = $id;

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }


        $oDocumentNotes = DocumentNotes::find("document_id=" . $nDocumentID);
        $aDocumentNotes = $oDocumentNotes->toArray();
        foreach ($oDocumentNotes as $k => $v) {
            $aDocumentNotes[$k]['contact'] = !empty($v->Contact) ? $v->Contact->toArray() : [];
            $aDocumentNotes[$k]['contact']['user'] = !empty($v->Contact) ? $v->Contact->User : '';
            $aDocumentNotes[$k]['contact']['name'] = !empty($v->Contact) ? $v->Contact->oPrimaryName : '';
        }
        $aResponse = array('notes' => $aDocumentNotes, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');


    }


    public function getForwardNote()
    {

        $nUserID = $this->userService->getIdentity();
        $nForwardID = $this->request->getPost('forward_id');

        if (empty($nForwardID) || !is_numeric($nForwardID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('forward' => $nForwardID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oForward = DocumentForward::findFirstById($nForwardID);
        if (!$oForward) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        $aNotes = [];
        if(!empty($oForward->parent_forward_id)){
            $aNotes = $this->getAllParentForward($oForward->parent_forward_id);

        }

        $aForward = $oForward->toArray();
        $aForward['parents'] = $aNotes;
        $aForward['user'] = !empty($oForward->User) ? $oForward->User->toArray() : [];
        $aForward['user']['name'] = !empty($oForward->User) ? $oForward->User->full_name : '';

        $aResponse = array('notes' => $aForward, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');


    }


    public function savePosition()
    {

        $nUserID = $this->userService->getIdentity();
        $nNoteID = $this->request->getPost('note_id');
        $sPositions = $this->request->getPost('positions');

        if (empty($nNoteID) || !is_numeric($nNoteID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('note_id' => $nNoteID));
        }

        $oUser = Users::findFirstById($nUserID);
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }


        $oDocumentNotes = DocumentNotes::findFirstById($nNoteID);

        if (!$oDocumentNotes) {
            return $this->createArrayResponse(['response_code' => 0, 'response_message' => "faild"], 'data');
        }
        $oDocumentNotes->positions = $sPositions;
        $oDocumentNotes->save();
        $aResponse = array('notes' => $oDocumentNotes, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');


    }

    public function deleteDraft(){

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::ACCESS_DENIED);
        }
        $aResponse = [];
        $nUserID = $this->userService->getIdentity();
        if($this->request->hasPost('ids')){

            $aDocumentIDs = $this->request->getPost('ids');
            $array=json_decode($aDocumentIDs);

            foreach($array as $nDocumentID) {

                $aResponse = $this->deleteDraftById($nDocumentID);
            }

        }elseif($this->request->hasPost('id')){
            $nDocumentID = $this->request->getPost('id');
            $aResponse = $this->deleteDraftById($nDocumentID);
        }elseif($this->request->hasPost('all')) {
            $oDrafts = Documents::find("user_id=$nUserID and status='draft' ") ;
            foreach($oDrafts as $oDraft){
                $aResponse = $this->deleteDraftById($oDraft->id);
            }

        }



        return $this->createArrayResponse($aResponse, 'data');

    }

#private function
    private function deleteDraftById($nDocumentID){

        $oDocument = Documents::findFirstById($nDocumentID);
        if(!$oDocument){
            throw new Exception(ErrorCodes::ACCESS_DENIED);

        }

        if($oDocument->status !='draft'){
            $aResponse = array( 'response_code' => 1, 'response_message' => "success");
            return $this->createArrayResponse($aResponse, 'data');
        }

        $oDocumentR = DocumentReference::find("document_id=" . $nDocumentID . " or document_reference_id=" . $nDocumentID);
        if($oDocumentR->count()>0){
            $oDocumentR->delete();
        }

        $oDocumentUserRead = DocumentUserRead::find("document_id=" . $nDocumentID);
        if($oDocumentUserRead->count()>0){
            $oDocumentUserRead->delete();
        }

        $oRecipients = Recipients::find("document_id=" .$nDocumentID ) ;
        if($oRecipients->count()>0){
            $oRecipients->delete();
        }

        $oDocumentFile = DocumentFile::find("document_id=" . $nDocumentID );
        if($oDocumentFile->count()>0) {
            $oDocumentFile->delete();
        }

        $oDocumentWorkflow = DocumentWorkflowActionHistory::find("document_id=$nDocumentID");
        if($oDocumentWorkflow->count()>0){
            $oDocumentWorkflow->delete();
        }

        $oDocumentWorkflow = DocumentWorkflow::find("document_id=$nDocumentID");
        if($oDocumentWorkflow->count()>0){
            $oDocumentWorkflow->delete();
        }

        $oDocumentLock= DocumentLock::find("document_id=$nDocumentID");
        if($oDocumentLock->count()>0){
            $oDocumentLock->delete();
        }
        if(!$oDocument->delete()){
            return $oDocument->getMessages();
        }
        return ['response_code' => 1, 'response_message' => "success"];

    }

    private function getAllParentForward($parent_forward_id){
        $aNotes = [];
        $oForward = DocumentForward::findFirstById($parent_forward_id);
        if($oForward) {
            $aForward = $oForward->toArray();
            $aForward['user'] = !empty($oForward->User) ? $oForward->User->toArray() : [];
            $aForward['user']['name'] = !empty($oForward->User) ? $oForward->User->full_name : '';
            if(!empty($oForward->parent_forward_id)){
                $aNotes[] = $aForward;
                $aParents = $this->getAllParentForward($oForward->parent_forward_id) ;
                foreach($aParents as $aParent){
                    $aNotes[] = $aParent;
                }
            }else{
                $aNotes[] = $aForward;
            }
        }
        return $aNotes;
    }


    private function checkIfUserHasAccess($nDocumentID, $oUser)
    {
        $nUserID = $oUser->id;
        $oDocument = Documents::findFirst("id=$nDocumentID and user_id=" . $oUser->id);
        if ($oDocument) {
            return true;
        }
        $sIdentified = !empty($oUser->email) ? $oUser->email : $oUser->mobile;

        $oRecipients = Recipients::findFirst("identifier='" . $sIdentified . "' and document_id=" . $nDocumentID . " and can_read=1");
        if ($oRecipients) {
            return true;
        }

        return $this->checkIfUserHaveAccess($nDocumentID, $oUser);

    }

    private function checkIfUserHaveAccess($nDocumentID, $oUser)
    {
        $oContactRecipients = Recipients::findFirst("user_id=" . $oUser->id . " and document_id=$nDocumentID and can_read=1");
        if ($oContactRecipients) {
            return true;
        }

        return $this->checkIfRolesHaveAccess($nDocumentID, $oUser);
    }

    private function checkIfRolesHaveAccess($nDocumentID, $oContactRecipients)
    {
        return false;
    }

    private function checkIfStatusValid($status)
    {
        $statusArray = DefaultValues::DOCUMENT_STATUS;
        foreach ($statusArray as $stat) {
            if ($status == $stat)
                return true;
        }
        return false;
    }

    public function details()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nDocumentID = $this->request->getPost('document_id');

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDocument = Documents::findFirstById($nDocumentID);
        $oDocumentWorkflow = DocumentWorkflow::findFirst("document_id=" . $nDocumentID . " and active=1") ;

        $oDocumentForwarded = DocumentForward::find("document_id=" . $nDocumentID);

        $oDocumentRecipients = Recipients::find("document_id=" . $nDocumentID);

        $aDetails = ['oDocumentForwarded' => $oDocumentForwarded, 'document' => $oDocument,'oDocumentWorkflow' => $oDocumentWorkflow ,'recipients' => $oDocumentRecipients];
        $aResponse = array('aDetails' => $aDetails, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');

    }

    public function attachments()
    {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();
        $nDocumentID = $this->request->getPost('id');

        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }

        $oUser = Users::findFirstById($nUserID);

        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oFiles = $this->db->query("select * from files f join document_file df on f.id = df.file_id where document_id=" . $nDocumentID. " order by f.id desc");
        $aFiles = $oFiles->fetchAll();
        $aResponse = array('aFiles' => $aFiles, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');

    }


    public function import()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserId = $this->userService->getIdentity();
        $sFile = $this->request->getPost("file_data");
        $sPathType = $this->request->get("path_type");

        if ($this->request->hasFiles()) {
            foreach ($this->request->getUploadedFiles() as $file) {

                $aFile = File::putWithHtml($file, $sPathType, $nUserId);

            }

        }
        $oFile = Files::findFirstById($aFile['file_id']);

        $aFile = array('file_id' => $aFile['file_id'], 'document_id' => $aFile['document_id'], 'oFile' => $oFile, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aFile, 'data');
    }

    public function importFromPdf()
    {
        $pdf = new \Gufy\PdfToHtml\Pdf('files/new.pdf');
        $html = $pdf->html();
        return $html;

    }


}
