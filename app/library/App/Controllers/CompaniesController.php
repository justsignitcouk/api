<?php

namespace App\Controllers;

use MyCompose\LaravelModels\Enterprise\Branch;
use MyCompose\LaravelModels\Enterprise\Company;
use MyCompose\Model\Access\Roles;
use MyCompose\Model\Business\Branches;
use MyCompose\Model\Business\Companies;
use MyCompose\Model\Business\Employees;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
use PhalconRest\Middleware\AuthenticationMiddleware;
class CompaniesController extends CrudResourceController
{
    public function initialize()
    {


    }

    public function getByCreator($nUserID) {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oCompanies = Companies::find("user_id=" . $nUserID ) ;
        $aResponse = array('companies' => $oCompanies, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');

    }

    public function getCompanyBySlug($sSlug) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oCompany= Company::with(['Branches.Departments','MainBranch.Country','Logo'])->where('slug',$sSlug)->get()->first() ;
        $aResponse = array('company' => $oCompany, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }

    public function getBranchByID($sSlug,$nBranchID) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oBranch = Branch::with(['Departments'])->where('id',$nBranchID)->get()->first() ;
        $aResponse = array('branch' => $oBranch, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }


    public function getEmployeesByCompanySlug($sSlug) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $oCompany = Companies::findFirst("slug='" . $sSlug . "'") ;
        if(!$oCompany) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oEmployees = Employees::find("company_id=" . $oCompany->id) ;
        $aResponse = array('employees' => $oEmployees, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }

    public function getRolesByCompanySlug($sSlug) {

        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $oCompany = Companies::findFirst("slug='" . $sSlug . "'") ;
        if(!$oCompany) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oRoles = Roles::find("company_id=" . $oCompany->id) ;
        $aResponse = array('roles' => $oRoles, 'response_code' => 1 ,'response_message' => "success" );
        return $this->createArrayResponse($aResponse,'data');
    }

    public function all(){

        $oCompanies = Companies::find();

        return $this->createResourceCollectionResponse($oCompanies);
    }

    public function save() {
        $this->db->begin();
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $sSlug = $this->request->getPost('slug') ;
        $sLicenseNumber = $this->request->getPost('license_number') ;
        $sCompanyName = $this->request->getPost('company_name') ;

        $nCompanyID = $this->request->getPost('id') ;
        $sBranchName = $this->request->getPost('branch_name') ;
        $sBranchEmail = $this->request->getPost('branch_email') ;
        $sBranchType = $this->request->getPost('branch_type') ;
        $nCountryID = $this->request->getPost('country_id') ;
        $sPhone = $this->request->getPost('phone') ;
        $sFax = $this->request->getPost('fax') ;
        $sZipCode = $this->request->getPost('zip_code') ;
        $sCity = $this->request->getPost('city') ;
        $sAddress = $this->request->getPost('address') ;
        $sMessage = $this->request->getPost('message') ;

        if(!empty($nCompanyID)) {
            $oCompany = Companies::findFirstById($nCompanyID) ;
            if(!$oCompany) {
                $aResponse = array('company' => [], 'response_code' => 0, 'response_message' => "The company not exist");
                return $this->createArrayResponse($aResponse, 'data');
            }
            $oCompany->updated_at = date('Y-m-d H:i:s') ;

        }
        if(empty($nCompanyID)) {

            $oCompany = Companies::findFirst("slug='" . $sSlug . "'") ;
            if($oCompany){
                $aResponse = array('company' => [], 'response_code' => 0, 'response_message' => "The company exist, please enter a new slug");
                return $this->createArrayResponse($aResponse, 'data');
            }
            $oCompany = new Companies();
            $oCompany->created_at = date('Y-m-d H:i:s') ;
            $oCompany->slug = $sSlug ;

        }
        $oCompany->name = $sCompanyName ;

        $oCompany->license_no = $sLicenseNumber ;
        //$oCompany->logo = '' ;
        $oCompany->user_id = $nUserID;


        if(!$oCompany->save()){
            var_dump( $oCompany->getMessages());
            exit();
            $aResponse = array('company' => [], 'response_code' => 0, 'response_message' =>   $oCompany->getMessages());
            return $this->createArrayResponse($aResponse, 'data');
        }

        if(empty($oCompany->MainBracnch)){
            $oBranch = new Branches();
            $oBranch->created_at = date('Y-m-d H:i:s');
        }else{
            $oBranch = Branches::findFirst("company_id=" . $oCompany->id . " and is_main=1 ") ;
        }

        $oBranch->name = $sBranchName;
        $oBranch->email = $sBranchEmail;
        $oBranch->branch_type_id = $sBranchType;
        $oBranch->address = $sAddress;
        $oBranch->pobox = $sZipCode;
        $oBranch->zip_code = $sZipCode;
        $oBranch->phone = $sPhone;
        $oBranch->fax = $sFax;
        $oBranch->country_id = $nCountryID;
        $oBranch->city = $sCity;
        $oBranch->license_no = $sLicenseNumber;
        $oBranch->company_id = $oCompany->id;
        $oBranch->is_main = 1;
        $oBranch->updated_at = date('Y-m-d H:i:s');
        if(!$oBranch->save()){
            $aResponse = array('company' => [], 'response_code' => 0, 'response_message' => $oBranch->getMessage());
            return $this->createArrayResponse($aResponse, 'data');
        }


        $this->db->commit();
        $aResponse = array('company' => ['company' => $oCompany,'branch'=>$oBranch] ,'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');



    }


    public function saveBranch() {
        $this->db->begin();
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $sLicenseNumber = $this->request->getPost('license_number') ;
        $sSlug =  $this->request->getPost('slug') ;
        $nBranchID = $this->request->getPost('id') ;
        $sBranchName = $this->request->getPost('branch_name') ;
        $sBranchEmail = $this->request->getPost('branch_email') ;
        $sBranchType = $this->request->getPost('branch_type') ;
        $nCountryID = $this->request->getPost('country_id') ;
        $sPhone = $this->request->getPost('phone') ;
        $sFax = $this->request->getPost('fax') ;
        $sZipCode = $this->request->getPost('zip_code') ;
        $sCity = $this->request->getPost('city') ;
        $sAddress = $this->request->getPost('address') ;
        $sMessage = $this->request->getPost('message') ;


        $oCompany = Companies::findFirst("slug='" . $sSlug ."' ") ;
        if(!$oCompany){
            $aResponse = array('branch' => [], 'response_code' => 0, 'response_message' => "The company not exist");
            return $this->createArrayResponse($aResponse, 'data');
        }

        if(!empty($nBranchID)) {
            $oBranch = Branches::findFirstById($nBranchID) ;
            if(!$nBranchID) {
                $aResponse = array('branch' => [], 'response_code' => 0, 'response_message' => "The branch not exist");
                return $this->createArrayResponse($aResponse, 'data');
            }
            $oBranch->updated_at = date('Y-m-d H:i:s') ;

        }else{
            $oBranch = new Branches();
            $oBranch->created_at = date('Y-m-d H:i:s') ;
        }


        $oBranch->name = $sBranchName;
        $oBranch->email = $sBranchEmail;
        $oBranch->branch_type_id = $sBranchType;
        $oBranch->address = $sAddress;
        $oBranch->pobox = $sZipCode;
        $oBranch->zip_code = $sZipCode;
        $oBranch->phone = $sPhone;
        $oBranch->fax = $sFax;
        $oBranch->country_id = $nCountryID;
        $oBranch->city = $sCity;
        $oBranch->license_no = $sLicenseNumber;
        $oBranch->company_id = $oCompany->id;
        $oBranch->is_main = 1;
        $oBranch->updated_at = date('Y-m-d H:i:s');
        if(!$oBranch->save()){
            $aResponse = array('company' => [], 'response_code' => 0, 'response_message' => $oBranch->getMessage());
            return $this->createArrayResponse($aResponse, 'data');
        }

        $this->db->commit();
        $aResponse = array('company' => ['company' => $oCompany,'branch'=>$oBranch] ,'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }

    public function exist(){

        $value = $this->request->getPost('value');
        if(!$value){
            $response = [
                'errorMessage' => 'Missing value argument',
                'status'   => false,
                'value'  => $value
            ] ;
            return $this->createArrayResponse($response, 'data');
        }

        $key = 'slug' ;
        if($this->request->hasPost('key')){
            $key = $this->request->getPost('key') ;

        }
        $oCompany = Companies::findFirst("$key = '$value'");
        $response = [
            'errorMessage' => 'NOT FOUND',
            'status'   => false
                    ] ;
        if($oCompany){
            $response = ['oCompany' => $oCompany,
                         'status'   => true
                        ] ;
        }

        return $this->createArrayResponse($response, 'data');
    }

    public function getCompanyDepartments(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nCompanyId = $this->request->get("company_id") ;
        if( empty($nCompanyId) || !is_numeric($nCompanyId) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('company_id' => $nCompanyId));
        }
        $oBranches = Branches::find("company_id=$nCompanyId") ;
        $oDepartments = array();
        $aDepartments = array('company_departments' => array() ,'response_code' => 1 ,'response_message' => "success" );
        if($oBranches->count()>0){
            foreach($oBranches as $k=>$oBranch){
                $nBranchId = $oBranch->id ;
                if(is_object($oBranch->Departments) && !is_null($oBranch->Departments)){
                    foreach($oBranch->Departments as $oDepartment){
                        $aDepartments['company_departments'][] = $oDepartment;
                    }

                }
            }
        }

        return $this->createArrayResponse($aDepartments,'data');

    }

}
