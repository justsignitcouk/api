<?php


namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use MyCompose\LaravelModels\UserDocumentActivity;

class UserActivitiesController extends CrudResourceController
{

    public function all(){

        $account_id = $this->request->get('account_id');
        $lang  = $this->request->get('lang');
        $limit = $this->request->get('limit');

        $activities =  UserDocumentActivity::where('account_id',$account_id)->with('Document')->limit($limit)->get();

        $messages = [] ;
        foreach ($activities as $activity){

            switch ($activity->type) {

                case 1 :
                    $message['message']     = sprintf($this->lang->setLanguage($lang)->_('you sent document %s'),$activity->document->title) ;
                    $message['type']        = $activity->type;
                    $message['document_id'] = $activity->document_id;
                    $message['created_at']  = $activity->created_at;

                    array_push($messages,$message);
                    break;
                case 2 :
                    $message['message'] = sprintf($this->lang->setLanguage($lang)->_('you signed document %s'),$activity->document->title) ;
                    $message['type']    = $activity->type;
                    $message['document_id'] = $activity->document_id;
                    $message['created_at']  = $activity->created_at;

                    array_push($messages,$message);
                    break;
            }

        }

        return $messages ;

    }





}