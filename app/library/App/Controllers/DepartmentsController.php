<?php

namespace App\Controllers;

use MyCompose\Model\Departments;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class DepartmentsController extends CrudResourceController
{


    public function all(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $oDepartments = Departments::find();
        return $this->createResourceCollectionResponse($oDepartments);
    }

    public function getDepartmentsByAccountId(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nAccountId = $this->request->get("account_id") ;
        if( empty($nAccountId) || !is_numeric($nAccountId) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('account_id' => $nAccountId));
        }

        $oAccount = Accounts::findFirstById($nAccountId);
        if(!$oAccount){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oAccountDepartments = $oAccount->AccountDepartment;
        $oDepartments = array();
        foreach($oAccountDepartments as $oAccountDepartment)
        {
            if(!is_null($oAccountDepartment->Departments)){
                $oDepartments[] = $oAccountDepartment->Departments;
            }
        }

        return $this->createArrayResponse($oDepartments);
    }

    public function getDepartmentsByUserId($nAccountId){

    }

}
