<?php

namespace App\Controllers;

use MyCompose\LaravelModels\Document\Document;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\DocumentCategories;
use MyCompose\Model\Document\DocumentLayout;
use MyCompose\Model\Document\DocumentUserRead;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\TemplateDocuments;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;

class TemplateDocumentsController extends CrudResourceController
{

    public function getByUserID()
    {
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->request->getPost("user_id");

        if (empty($nUserID) || !is_numeric($nUserID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('user_id' => $nUserID));
        }



        $oDocument = \MyCompose\LaravelModels\Document\Document::with([
            'Recipients.User', 'Recipients.Sign',
            'Layout',
            'WorkFlow.WorkFlow.Stage.StageState.State.StateAction.Action',
            'QrCode',
            'Files.File'])->where('status', 'template')->where('user_id',$nUserID )->get();


        $aResponse = array('templates' => $oDocument, 'response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aResponse, 'data');
    }


    public function deleteTemplateDocuments()
    {

        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $nDocumentID = $this->request->getPost("id");

        $nUserID = $this->userService->getIdentity();


        if (empty($nDocumentID) || !is_numeric($nDocumentID)) {
            throw new Exception(ErrorCodes::DATA_MISSING, '', array('document_id' => $nDocumentID));
        }


        $oDocument = Documents::findFirstById($nDocumentID);
        if (!$oDocument) {
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }
        if($oDocument->status !='template'){
            throw new Exception(ErrorCodes::DATA_NOT_FOUND);
        }

        $oRecipients = Recipients::find("document_id=" . $nDocumentID ) ;
        $oRecipients->delete();

        $oDocumentUserRead = DocumentUserRead::find("document_id=" . $nDocumentID );
        $oDocumentUserRead->delete();

        $oDocument->delete();

        $aDocument = array('response_code' => 1, 'response_message' => "success");
        return $this->createArrayResponse($aDocument, 'data');
    }
}
