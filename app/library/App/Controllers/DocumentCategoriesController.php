<?php

namespace App\Controllers;

use MyCompose\Model\Document\DocumentCategories;
use MyCompose\Model\Document\DocumentCategoryDepartment;
use MyCompose\Model\LayoutCategories;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class DocumentCategoriesController extends CrudResourceController
{


    public function all(){
        if (!$this->userService->getIdentity()) {

            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $aDocumentCategories = array('document_categories' => array() ,'response_code' => 1 ,'response_message' => "success" );
        $oDocumentCategories = DocumentCategories::find();
        $aDocumentCategories['document_categories'] = $oDocumentCategories ;
        return $this->createArrayResponse($aDocumentCategories,'data');
    }

    public function getByDocumentDepartment(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nDepartmentID = $this->request->getPost('department_id');

        if( empty($nDepartmentID) || !is_numeric($nDepartmentID) ){
            throw new Exception(ErrorCodes::DATA_MISSING,'',array('department_id' => $nDepartmentID));
        }

        $oDocumentCategoryDepartment = DocumentCategoryDepartment::find("department_id=$nDepartmentID") ;

        $aDocumentCategories = array('document_categories' => [] ,'response_code' => 1 ,'response_message' => "success" );
        if($oDocumentCategoryDepartment->count()>0){
            foreach($oDocumentCategoryDepartment as $obj ){
                $oDocumentCategories = $obj->DocumentCategories ;

                $aDocumentCategoryLayout = array();
                if(is_object($oDocumentCategories) && !is_null($oDocumentCategories) ){
                    $nCategoryID = $oDocumentCategories->id ;

                    $oLayoutCategories = LayoutCategories::find("category_id=$nCategoryID") ;
                    if($oLayoutCategories->count()>0){
                        foreach($oLayoutCategories as $obj3){
                            $aDocumentCategoryLayout[] = $obj3->DocumentLayout;
                        }
                    }

                }
                $aDocumentCategory = $oDocumentCategories->toArray() ;
                $aDocumentCategory['document_layouts'] = $aDocumentCategoryLayout ;
                $aDocumentCategories['document_categories'][] = $aDocumentCategory ;

            }

        }
        return $this->createArrayResponse($aDocumentCategories,'data');

    }


}
