<?php

namespace App\Controllers;

use MyCompose\Model\Business\Companies;
use MyCompose\Model\Plans\Feature;
use MyCompose\Model\Users;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Constants\ErrorCodes;
use PhalconApi\Exception;
class QuotaController extends CrudResourceController
{

    public function all(){

    }

    public function get(){
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $nUserID = $this->userService->getIdentity();

        $oUser = Users::findFirstById($nUserID) ;
        $oPlans = $oUser->Plans;
        $aPlans = $oPlans->toArray();
        foreach($oPlans as $k=>$oPlan){
            $aFeatures  = $oPlan->Features->toArray();
            foreach($aFeatures as $k2=>$aFeature) {
                $aFeatures[$k2]['usage'] = null ;
                if($aFeature['key'] == 'number_of_companies') {
                    $oCompanies = Companies::find("user_id=" . $nUserID ) ;
                    $aFeatures[$k2]['usage'] = $oCompanies->count();
                }
            }

                $aPlans[$k]['Features'] = $aFeatures ;
        }
        $aResponse = ['aPlans' => $aPlans, 'response_code' => 1 ,'response_message' => "success" ];
        return $this->createArrayResponse($aResponse,'data');
    }
}
