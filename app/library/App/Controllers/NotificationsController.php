<?php


namespace App\Controllers;

use MyCompose\Model\Document\DocumentNotification;
use PhalconRest\Mvc\Controllers\CollectionController;
use MyCompose\LaravelModels\Notifications ;
use MyCompose\LaravelModels\Document\DocumentCcAccount;
use MyCompose\LaravelModels\Document\DocumentToAccount;
use MyCompose\LaravelModels\Document\DocumentSignerAccount;
use App\Components\Notifications\Notifications as NotificationService;
use MyCompose\LaravelModels\Document\Document;
use MyCompose\LaravelModels\UserDocumentActivity;


class NotificationsController  extends CollectionController
{


    public function getUserNotification()
    {
        if (!$this->userService->getIdentity()) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }
        $lang = $this->request->get('lang');
        $nUserID = $this->userService->getIdentity();


        $oNotification = DocumentNotification::find(["user_id=$nUserID",'order'=>'id desc']) ;
        //$user_notifications = Notifications::where('receiver_id',$user_id)->with('Account')->with('Document')->with('Signer')->get();

        $messages = [] ;
//        foreach ($user_notifications as $notification) {
//
//            switch ($notification->type) {
//
//                case 1 :
//                    $message['message'] = sprintf($this->lang->setLanguage($lang)->_('%s sent you the document %s'),$notification->account->account_name,$notification->document->title) ;
//                    $message['notification_id'] = $notification->id;
//                    $message['document_id']     = $notification->document_id;
//                    $message['document_title']  = $notification->document->title;
//                    $message['sender_id']       = $notification->sender_id;
//                    $message['type']            = $notification->type;
//                    $message['read_status']     = $notification->read;
//                    array_push($messages,$message);
//                    break;
//                case 2 :
//                    $message['message'] = sprintf($this->lang->setLanguage($lang)->_('%s make CC for you for document %s'),$notification->account->account_name,$notification->document->title) ;
//                    $message['notification_id'] = $notification->id;
//                    $message['document_id']     = $notification->document_id;
//                    $message['document_title']  = $notification->document->title;
//                    $message['sender_id']       = $notification->sender_id;
//                    $message['type']            = $notification->type;
//                    $message['read_status']     = $notification->read;
//                    array_push($messages,$message);
//                    break;
//                case 3 :
//                    $message['message'] = sprintf($this->lang->setLanguage($lang)->_('document %s is completed'), $notification->document->title) ;
//                    $message['notification_id'] = $notification->id;
//                    $message['document_id']     = $notification->document_id;
//                    $message['document_title']  = $notification->document->title;
//                    $message['sender_id']       = $notification->sender_id;
//                    $message['type']            = $notification->type;
//                    $message['read_status']     = $notification->read;
//                    array_push($messages,$message);
//                    break;
//                case 4 :
//                    $message['message'] = sprintf($this->lang->setLanguage($lang)->_('%s signed by %s'),$notification->document->title,$notification->signer->account_name) ;
//                    $message['notification_id'] = $notification->id;
//                    $message['document_id']     = $notification->document_id;
//                    $message['document_title']  = $notification->document->title;
//                    $message['sender_id']       = $notification->sender_id;
//                    $message['type']            = $notification->type;
//                    $message['read_status']     = $notification->read;
//                    array_push($messages,$message);
//                    break;
//            }
//
//        }

        $anewNotification = array('notifications' => $oNotification , 'response_code' => 1, 'response_message' => "success");

        $anewNotification['RecordsTotal'] = sizeof($anewNotification['notifications']);

        return $this->createArrayResponse($anewNotification, 'data');

    }



    public function create_user_notifications()
    {

        $documentsToAccount = DocumentToAccount::with('Sender')->get();
        $documentsCCAccount = DocumentCcAccount::with('Sender')->get();
        $documentSignerAccount = DocumentSignerAccount::with('Sender')->get();
        $documents =  Document::where('status','completed')->with('Sender')->get();


        //send document notification
        foreach ($documentsToAccount as $toAccount){

             $checkNotification = Notifications::where('sender_id',$toAccount->sender->account_id)->where('document_id',$toAccount->document_id)->where('type',1)->get();

             if(count($checkNotification) == 0){
                 NotificationService::createNotification($toAccount->account_id,$toAccount->sender->account_id,$toAccount->document_id, 1);
                 UserDocumentActivity::createDocumentActivity(1 , $toAccount->sender->account_id ,$toAccount->document_id);
             }

        }

        //send CC notification
        foreach ($documentsCCAccount as $CCAccount){

            $checkNotification = Notifications::where('sender_id',$CCAccount->sender->account_id)->where('document_id',$CCAccount->document_id)->where('type',2)->get();

            if(count($checkNotification) == 0){
                NotificationService::createNotification($CCAccount->account_id,$CCAccount->sender->account_id,$CCAccount->document_id, 2);

            }

        }

        //send completed notification
        foreach ($documents as $document){

            $checkNotification = Notifications::where('sender_id',$document->account_id)->where('document_id',$document->id)->where('type',3)->get();

            if(count($checkNotification) == 0){
                NotificationService::createNotification($document->account_id,$document->account_id,$document->id, 3);

            }

        }


        //send sign notification
        foreach ($documentSignerAccount as $SignerAccount){

            $checkNotification = Notifications::where('sender_id',$SignerAccount->sender->account_id)->where('document_id',$SignerAccount->document_id)->where('type',4)->get();

            if(count($checkNotification) == 0){

                NotificationService::createNotification($SignerAccount->account_id,$SignerAccount->sender->account_id,$SignerAccount->document_id, 4);
                UserDocumentActivity::createDocumentActivity(2 , $SignerAccount->sender->account_id , $SignerAccount->document_id);


            }

        }

    }

    public function change_read_status(){

        $notification_id =  $this->request->getPost("notification_id");
        $document_id =  $this->request->getPost("document_id");

        $Not = DocumentNotification::findFirst("notification_id=" . $notification_id . " and document_id=" . $document_id);

        $Not->read = 1 ;

        $Not->save();

        return $this->createArrayResponse($Not->read , 'data');

    }

}