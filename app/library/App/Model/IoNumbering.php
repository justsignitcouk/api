<?php
namespace MyCompose\Model;
class IoNumbering extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $prefix;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $counter;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $postfix;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $sperator;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $year;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $departmint_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'IoVariables', 'io_numbering_id', ['alias' => 'IoVariables']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'io_numbering';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return IoNumbering[]|IoNumbering
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return IoNumbering
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
