<?php
namespace MyCompose\Model;

class Sms extends \Phalcon\Mvc\Model
{

    public function initialize()
    {

    }

    public function getSource()
    {
        return 'sms';
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
