<?php
namespace MyCompose\Model;

class QrCodes extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return 'qr_codes';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function generate($aParams){
        $data = $aParams['data'] ;
        $di = \Phalcon\DI::getDefault();
        $sUrl = $di->get('config')->web->host . '/' . $sUrl = $di->get('config')->web->sign_validation_action . '/' ;

        $sData = 'data:image/png;base64,' . $di->get('qr')->getBarcodePNG( $sUrl . $data, "QRCODE");
        $this->data = $sData ;
        return true;
    }

}
