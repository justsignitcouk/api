<?php
namespace MyCompose\Model;
class Logs extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $object_id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $table_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $action;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $old_value;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $new_value;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'logs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Logs[]|Logs
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Logs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
