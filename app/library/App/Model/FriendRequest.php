<?php

namespace MyCompose\Model;

class FriendRequest extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $ID;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $USER_ID;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $REQUESTER_ID;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $CREATED_AT;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('requester_id', 'MyCompose\Model\FriendRequest', 'user_id', ['alias' => 'UserRequests']);
        $this->hasMany('user_id', 'MyCompose\Model\FriendRequest', 'requester_id', ['alias' => 'FriendRequests']);
        $this->belongsTo('requester_id', 'MyCompose\Model\Users', 'id', ['alias' => 'Requester']);
        $this->belongsTo('requested_id', 'MyCompose\Model\Users', 'id', ['alias' => 'Requested']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'friend_requests';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS[]|MCUSERS
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
