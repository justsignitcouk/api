<?php
namespace MyCompose\Model;
class Files extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $path;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $hash_name;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $hash_path;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $is_private;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $account_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $size;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'Branches', 'logo', ['alias' => 'Branches']);
        $this->hasMany('id', 'Companies', 'logo', ['alias' => 'Companies']);
        $this->hasMany('id', 'Departments', 'logo', ['alias' => 'Departments']);
        $this->hasMany('id', 'DocumentFile', 'file_id', ['alias' => 'DocumentFile']);
        $this->hasMany('id', 'Signs', 'file_id', ['alias' => 'Signs']);
        $this->hasMany('id', 'Stamps', 'file_id', ['alias' => 'Stamps']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'files';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files[]|Files
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
