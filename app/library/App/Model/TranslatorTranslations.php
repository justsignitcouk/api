<?php
namespace MyCompose\Model;
class TranslatorTranslations extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $locale;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $namespace;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $group;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $item;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $text;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $unstable;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $locked;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('locale', '\TranslatorLanguages', 'locale', ['alias' => 'TranslatorLanguages']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'translator_translations';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranslatorTranslations[]|TranslatorTranslations
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranslatorTranslations
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
