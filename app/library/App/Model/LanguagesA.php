<?php
namespace MyCompose\Model;
class LanguagesA extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $app_name;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $flag;

    /**
     *
     * @var string
     * @Column(type="string", length=3, nullable=false)
     */
    public $abbr;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $script;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $native;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=false)
     */
    public $active;

    /**
     *
     * @var integer
     * @Column(type="integer", length=3, nullable=false)
     */
    public $default;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $deleted_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'languages_a';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LanguagesA[]|LanguagesA
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LanguagesA
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
