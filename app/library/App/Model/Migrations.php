<?php

namespace MyCompose\Model;

class Migrations extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=20, nullable=false)
     */
    public $version;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $migration_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $start_time;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $end_time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'migrations';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Migrations[]|Migrations
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Migrations
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'version' => 'version',
            'migration_name' => 'migration_name',
            'start_time' => 'start_time',
            'end_time' => 'end_time'
        ];
    }

}
