<?php
namespace MyCompose\Model;
class CompileLogs extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return 'logs';
    }

    public function initialize()
    {
        $this->setSchema("mc_logs");

    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Logs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
