<?php
/**
 * Created by PhpStorm.
 * User: odai
 * Date: 27/09/17
 * Time: 11:03 ص
 */

namespace MyCompose\Model;


class NeedHelp extends \Phalcon\Mvc\Model
{


    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;


    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $email;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $message;



}