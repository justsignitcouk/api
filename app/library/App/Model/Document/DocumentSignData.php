<?php

namespace MyCompose\Model\Document;

class DocumentSignData extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $sign_code;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $sign_image;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('id', 'MyCompose\Model\Document\DocumentSignerAccount', 'document_id', ['alias' => 'DocumentSignerAccount']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_sign_data';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentSignData[]|DocumentSignData
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentSignData
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
