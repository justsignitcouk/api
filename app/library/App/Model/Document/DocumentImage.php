<?php
namespace MyCompose\Model\Document;

class DocumentImage extends \Phalcon\Mvc\Model
{

    public function initialize()
    {

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_image';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction[]|DocumentAction
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
