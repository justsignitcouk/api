<?php
namespace MyCompose\Model\Document;

class DocumentLayout extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Document\Documents', 'layout_id', ['alias' => 'Documents']);
        $this->hasMany('id', 'MyCompose\Model\Document\LayoutCategories', 'layout_id', ['alias' => 'LayoutCategories']);
        $this->hasMany('id', 'MyCompose\Model\Document\LayoutComponents', 'layout_id', ['alias' => 'LayoutComponents']);
        $this->hasMany('id', 'MyCompose\Model\Document\Templates', 'design_id', ['alias' => 'Templates']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_layout';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentLayout[]|DocumentLayout
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentLayout
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
