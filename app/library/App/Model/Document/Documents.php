<?php
namespace MyCompose\Model\Document;

class Documents extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $content;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $category_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $privacy;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $expiry_date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $account_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $parent_document_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layout_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $outbox_number;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Document\DocumentAction', 'document_id', ['alias' => 'DocumentAction']);
        $this->hasMany('id', 'MyCompose\Model\Document\Recipients\Recipients', 'document_id', ['alias' => 'Recipients']);

        $this->belongsTo('qr_code_id', 'MyCompose\Model\QrCodes', 'id', ['alias' => 'QRCode']);


            $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'Sender']);

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'documents';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Documents[]|Documents
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Documents
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
