<?php
namespace MyCompose\Model\Document\Forward;

class DocumentForward extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Document']);
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);

    }

    public function getSource()
    {
        return 'document_forward';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction[]|DocumentAction
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
