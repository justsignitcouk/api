<?php
namespace MyCompose\Model\Document\Forward;

class ForwardUser extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('contact_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('forward_id', 'MyCompose\Model\Document\DocumentForward', 'id', ['alias' => 'DocumentForward']);
    }

    public function getSource()
    {
        return 'forward_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction[]|DocumentAction
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentAction
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
