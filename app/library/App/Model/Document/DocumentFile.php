<?php
namespace MyCompose\Model\Document;

class DocumentFile extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $document_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $file_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $valid;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Documents']);
        $this->belongsTo('file_id', 'MyCompose\Model\Files', 'id', ['alias' => 'File']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_file';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentFile[]|DocumentFile
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentFile
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
