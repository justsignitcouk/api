<?php

namespace MyCompose\Model\Document;

class DocumentSenderDepartment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $document_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $department_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Documents']);
        $this->belongsTo('department_id', 'MyCompose\Model\\Departments', 'id', ['alias' => 'Departments']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_sender_department';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentSenderDepartment[]|DocumentSenderDepartment
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentSenderDepartment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
