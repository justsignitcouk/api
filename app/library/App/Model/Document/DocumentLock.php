<?php
namespace MyCompose\Model\Document;

class DocumentLock extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Document']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_lock';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Documents[]|Documents
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Documents
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
