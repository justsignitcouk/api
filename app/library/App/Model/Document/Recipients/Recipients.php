<?php

namespace MyCompose\Model\Document\Recipients;

use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\MessagesQueue;
use MyCompose\Model\Contacts\Mobiles;

class Recipients extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Document']);
        $this->belongsTo('user_id', 'MyCompose\Model\\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('sign_data_id', 'MyCompose\Model\Document\DocumentSignData', 'id', ['alias' => 'Sign']);
    }

    public function getSource()
    {
        return 'document_recipient';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
