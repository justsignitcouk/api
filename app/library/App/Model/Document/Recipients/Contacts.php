<?php

namespace MyCompose\Model\Document\Recipients;

use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\MessagesQueue;
use MyCompose\Model\Contacts\Mobiles;

class Contacts extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('document_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Documents']);
        $this->belongsTo('contact_id', 'MyCompose\Model\\Contacts\\Contacts', 'id', ['alias' => 'Contact']);
        $this->belongsTo('sign_data_id', 'MyCompose\Model\Document\DocumentSignData', 'id', ['alias' => 'Sign']);
    }

    public function getSource()
    {
        return 'document_contact_recipient';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function addMessageSignerToQueue($nDocumentID)
    {
        $oDocumentSigners = parent::find("document_id=" . $nDocumentID . " and type='Signer' ");
        foreach ($oDocumentSigners as $oDocumentSigner) {
            $oEmails = $oDocumentSigner->Contact->Emails;
            $oMobiles = $oDocumentSigner->Contact->Mobiles;
            $oPrimaryEmail = Emails::findFirst(["contact_id=" . $oDocumentSigner->contact_id, 'order' => 'is_primary desc']);
            $oPrimaryMobile = Mobiles::findFirst(["contact_id=" . $oDocumentSigner->contact_id, 'order' => 'is_primary desc']);
            if ($oPrimaryEmail) {
                    $sEmail = $oPrimaryEmail->email;
                    $oMessagesQueue = new MessagesQueue();
                    $oMessagesQueue->type = 'email';
                    $oMessagesQueue->contact = $oPrimaryEmail->email;
                    $oMessagesQueue->template = 'sign_email';
                    $oMessagesQueue->recipient_type = 'contact';
                    $oMessagesQueue->created_at = date('Y-m-d H:i:s');
                    if (!$oMessagesQueue->save()) {
                        var_dump($oMessagesQueue->getMessages());
                        exit();
                    }
                continue;
            }

            if ($oPrimaryMobile) {
                    $sMobile = $oPrimaryMobile->mobile;
                    $oMessagesQueue = new MessagesQueue();
                    $oMessagesQueue->type = 'sms';
                    $oMessagesQueue->contact = $oPrimaryMobile->mobile;
                    $oMessagesQueue->template = 'sign_mobile';
                    $oMessagesQueue->recipient_type = 'contact';
                    $oMessagesQueue->created_at = date('Y-m-d H:i:s');
                    if (!$oMessagesQueue->save()) {
                        var_dump($oMessagesQueue->getMessages());
                        exit();
                    }
            }
        }
    }
}
