<?php
namespace MyCompose\Model\Document;

class DocumentCategories extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $parent_category_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Document\DocumentCategories', 'parent_category_id', ['alias' => 'DocumentCategories']);
        $this->hasMany('id', 'MyCompose\Model\Document\DocumentCategoryDepartment', 'category_id', ['alias' => 'DocumentCategoryDepartment']);
        $this->hasMany('id', 'MyCompose\Model\Document\Documents', 'category_id', ['alias' => 'Documents']);
        $this->hasMany('id', 'MyCompose\Model\Document\LayoutCategories', 'category_id', ['alias' => 'LayoutCategories']);
        $this->belongsTo('parent_category_id', 'MyCompose\Model\Document\DocumentCategories', 'id', ['alias' => 'DocumentCategories']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_categories';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentCategories[]|DocumentCategories
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentCategories
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
