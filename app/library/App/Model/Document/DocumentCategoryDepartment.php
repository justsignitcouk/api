<?php
namespace MyCompose\Model\Document;

class DocumentCategoryDepartment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $category_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $department_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $active;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('category_id', 'MyCompose\Model\Document\DocumentCategories', 'id', ['alias' => 'DocumentCategories']);
        $this->belongsTo('department_id', 'MyCompose\Model\Document\Documents', 'id', ['alias' => 'Documents']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'document_category_department';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentCategoryDepartment[]|DocumentCategoryDepartment
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DocumentCategoryDepartment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
