<?php
namespace MyCompose\Model;

class UserLink extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_link';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actions[]|Actions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
