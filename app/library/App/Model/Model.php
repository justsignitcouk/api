<?php
namespace MyCompose\Model;

class Model extends \Phalcon\Mvc\Model
{




    protected function mergeArray($key, array $in, array $add)
    {
        return array_merge($in, $add);
    }

    protected function addChild($key, array $in, array $add)
    {
        $in[$key] = $add;
        return $in;
    }

    protected function addChildren($key, array $in, $objects, $arrayKey = null)
    {
        $in[$key] = array();
        foreach ($objects as $object) {
            if ($arrayKey) {
                $in[$key][$object->$arrayKey] = $object->toArray();
            } else {
                $in[$key][] = $object->toArray();
            }
        }
        return $in;
    }

    public function toJson(array $params = [])
    {
        $modelObject = $this->toArray();
        foreach (static::$toJsonRelations as $related => $relation) {
            $modelRelated = $this->getRelated(\Phalcon\Text::camelize($related));
            if ($modelRelated) {
                $modelObject = $this->$relation($related, $modelObject, $modelRelated->toArray());
            }
        }
        $refClass = new \ReflectionClass($this);
        return [
            strtolower($refClass->getShortName()) => $modelObject
        ];
    }

}