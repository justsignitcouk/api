<?php
namespace MyCompose\Model;
class Sessions extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Column(type="string", length=255, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $ip_address;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $user_agent;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $payload;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $last_activity;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sessions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sessions[]|Sessions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sessions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
