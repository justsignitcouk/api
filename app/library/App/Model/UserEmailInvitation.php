<?php

namespace MyCompose\Model;

class UserEmailInvitation extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $ID;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $USER_ID;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=false)
     */
    public $EMAIL;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $CUSTOM_MESSAGE;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $CREATED_AT;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    /*    $this->belongsTo('requester_id', 'MyCompose\Model\User', 'id', ['alias' => 'RequesterUser']);
        $this->belongsTo('user_id', 'MyCompose\Model\User', 'id', ['alias' => 'RequestedUser']);
        $this->hasMany('requester_id', 'MyCompose\Model\FriendRequest', 'user_id', ['alias' => 'UserRequests']);
        $this->hasMany('user_id', 'MyCompose\Model\FriendRequest', 'requester_id', ['alias' => 'FriendRequests']);*/

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_invitation_email';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS[]|MCUSERS
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
