<?php
namespace MyCompose\Model;
class TranslatorLanguages extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=false)
     */
    public $locale;

    /**
     *
     * @var string
     * @Column(type="string", length=60, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=10, nullable=true)
     */
    public $direction;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $deleted_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('locale', 'TranslatorTranslations', 'locale', ['alias' => 'TranslatorTranslations']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'translator_languages';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranslatorLanguages[]|TranslatorLanguages
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TranslatorLanguages
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
