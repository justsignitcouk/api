<?php

namespace MyCompose\Model\Business;

class Companies extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $ID;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $NAME;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $CREATED_AT;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $UPDATED_AT;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $LICENSE_NO;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc_full");
        $this->hasMany('id', 'MyCompose\Model\Business\Branches', 'company_id', ['alias' => 'Bracnches']);
        $this->belongsTo('id', 'MyCompose\Model\Business\Branches', 'company_id', ['alias' => 'MainBracnch','params' => [
            'conditions' => "is_main = '1' "
        ] ]);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'companies';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Companies[]|Companies
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Companies
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
//    public function columnMap()
//    {
//        return [
//            'id' => 'id',
//            'name' => 'name',
//            'slug' => 'slug',
//            'created_at' => 'created_at',
//            'updated_at' => 'updated_at',
//            'license_no' => 'license_no'
//        ];
//    }

}
