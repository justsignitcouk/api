<?php
namespace MyCompose\Model\Business;

class Departments extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $parent_department_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $branch_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $logo;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\DepartmentPermission', 'department_id', ['alias' => 'DepartmentPermission']);
        $this->hasMany('id', 'MyCompose\Model\DepartmentRole', 'department_id', ['alias' => 'DepartmentRole']);
        $this->hasMany('id', 'MyCompose\Model\WorkflowDepartment', 'department_id', ['alias' => 'WorkflowDepartment']);
        $this->belongsTo('branch_id', 'MyCompose\Model\Branches', 'id', ['alias' => 'Branches']);
        $this->belongsTo('logo', '\Files', 'id', ['alias' => 'Files']);

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'departments';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Departments[]|Departments
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Departments
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
