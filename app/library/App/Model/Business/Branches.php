<?php
namespace MyCompose\Model\Business;
use Phalcon\Validation;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;

class Branches extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $address;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $pobox;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $phone;

    /**
     *
     * @var string
     * @Column(type="string", length=20, nullable=true)
     */
    public $fax;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $email;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $is_main;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $license_no;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $branch_type_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $country_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $company_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=10, nullable=true)
     */
    public $logo;

    /**
     * Validations and business logic
     *
     * @return boolean
     */


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Departments', 'branch_id', ['alias' => 'Departments']);
        $this->belongsTo('company_id', 'MyCompose\Model\Companies', 'id', ['alias' => 'Companies']);
        $this->belongsTo('branch_type_id', '\BranchType', 'id', ['alias' => 'BranchType']);
        $this->belongsTo('country_id', '\Countries', 'id', ['alias' => 'Countries']);
        $this->belongsTo('logo', '\Files', 'id', ['alias' => 'Files']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'branches';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Branches[]|Branches
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Branches
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
