<?php
namespace MyCompose\Model\Access;
class PermissionUser extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");

    }

    public function getSource()
    {
        return 'permission_user';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
