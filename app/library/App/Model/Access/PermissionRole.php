<?php
namespace MyCompose\Model\Access;
class PermissionRole extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('role_id', 'MyCompose\Model\Access\Roles', 'id', ['alias' => 'Role']);
        $this->belongsTo('permission_id', 'MyCompose\Model\Access\Permissions', 'id', ['alias' => 'Permission']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'permission_role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Permissions[]|Permissions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Permissions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
