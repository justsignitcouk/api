<?php
namespace MyCompose\Model;

class Signs extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $file_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('file_id', 'MyCompose\Model\Files', 'id', ['alias' => 'Files']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'signs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Signs[]|Signs
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Signs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function addQRToSign($fQR,$fSign)
    {
        $manager = new \Intervention\Image\ImageManager();
        $img = $manager->make($fSign);
        $type = 'png';
        $image_width = $img->width();
        $image_height = $img->height();
        $img2 = $manager->make($fQR);
        $img->insert($img2, 'bottom-right', 0, 0);
        $img->encode('png');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
        $sSignDataWithQR = $base64;
        return $sSignDataWithQR;
    }

}
