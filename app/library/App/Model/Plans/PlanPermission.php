<?php
namespace MyCompose\Model\Plans;
class PlanPermission extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
//        $this->hasMany('id', 'AccountPermission', 'permission_id', ['alias' => 'AccountPermission']);
//        $this->hasMany('id', 'DepartmentPermission', 'permission_id', ['alias' => 'DepartmentPermission']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'prices_plan_permission';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Permissions[]|Permissions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Permissions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
