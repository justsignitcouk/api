<?php

namespace MyCompose\Model\Plans;

class Feature extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return 'prices_features';
    }

    public function initialize() {

        $this->hasMany('id', \MyCompose\Model\PlanFeature::class, 'feature_id', [
            'alias' => 'PlanFeature',
        ]);

    }
}
