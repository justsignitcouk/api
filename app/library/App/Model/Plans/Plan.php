<?php

namespace MyCompose\Model\Plans;

class Plan extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return 'prices_plans';
    }

    public function initialize() {

        $this->hasMany('id', \MyCompose\Model\Plans\PlanFeature::class, 'plan_id', [
            'alias' => 'PlanFeature',
        ]);

        $this->hasMany('id', \MyCompose\Model\Plans\PlanRole::class, 'plan_id', [
            'alias' => 'PlanRole',
        ]);

        $this->hasManyToMany(
            "id",
            "MyCompose\Model\Plans\PlanFeature",
            "plan_id",
            "feature_id",
            "MyCompose\Model\Plans\Feature",
            "id",
            [
                'params'   => [
                    'conditions' => "enable = '1'",
                    "columns" => "id,key,title,default_value,custom_icon_class,value"
                ],
                'alias' => 'Features']
        );
    }
}
