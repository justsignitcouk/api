<?php

namespace MyCompose\Model\Plans;

class PlanFeature extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return 'prices_plan_features';
    }

    public function initialize() {

        $this->belongsTo('plan_id', \MyCompose\Model\Plans\Plan::class, 'id', [
            'alias' => 'Plan',
        ]);

        $this->belongsTo('feature_id', \MyCompose\Model\Plans\Feature::class, 'id', [
            'alias' => 'Feature',
        ]);
    }
}
