<?php
namespace MyCompose\Model;
class LayoutCategories extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $category_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layout_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('category_id', 'MyCompose\Model\Document\DocumentCategories', 'id', ['alias' => 'DocumentCategories']);
        $this->belongsTo('layout_id', 'MyCompose\Model\DocumentLayout', 'id', ['alias' => 'DocumentLayout']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'layout_categories';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LayoutCategories[]|LayoutCategories
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LayoutCategories
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
