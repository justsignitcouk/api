<?php
namespace MyCompose\Model\Profiles;
use Phalcon\Di;

class ProfileRequirements extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Profiles\UserProfileRequirements', 'id', ['alias' => 'UserProfileRequirements']);
    }

    public function getSource()
    {
        return 'profile_requirements';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getProfileRequirementsByUserID($nUserID){
        $di = DI::getDefault();
        $db = $di->get('db');
        $aProfileRequirements = $db->fetchAll("select * from " . self::getSource() . " as pr 
         where pr.id not in (select upr.profile_requirements_id from " . UserProfileRequirements::getSource() ." as upr where user_id=" . $nUserID . ") and status=1 "
        ) ;

        return $aProfileRequirements;
    }

}
