<?php
namespace MyCompose\Model\Profiles;
use MyCompose\Model\Document\Documents;
use Phalcon\Di;

class IONumberSeqences extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");

    }

    public function getSource()
    {
        return 'io_numbering_sequences';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }




}
