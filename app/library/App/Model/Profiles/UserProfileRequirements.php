<?php
namespace MyCompose\Model\Profiles;

class UserProfileRequirements extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('profile_requirements_id', 'MyCompose\Model\Profiles\ProfileRequirements', 'id', ['alias' => 'ProfileRequirement']);
    }
    
    public function getSource()
    {
        return 'user_profile_requirements';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
