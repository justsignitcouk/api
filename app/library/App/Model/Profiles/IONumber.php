<?php
namespace MyCompose\Model\Profiles;
use MyCompose\Model\Document\Documents;
use Phalcon\Di;

class IONumber extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        //$this->setSchema("mc");

    }

    public function getSource()
    {
        return 'io_numbering_template';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function generateIONumber($aParams){
        $sTemplate = $this->template;
        $aTemplaet = explode(',',$sTemplate);
        $sOutboxNumber = '';
        foreach($aTemplaet as $sTemplate){
            if($sTemplate=='Year'){
                $sOutboxNumber .= date('Y');
            }elseif($sTemplate=='Month'){
                $sOutboxNumber .= date('m');
            }elseif($sTemplate=='Day'){
                $sOutboxNumber .= date('d');
            }elseif($sTemplate=='Date'){
                $sOutboxNumber .= date('Ymd');
            }elseif($sTemplate=='DocumentID'){
                $sOutboxNumber .= $aParams['document_id'];
            }elseif($sTemplate=='UserID'){
                $sOutboxNumber .= $aParams['user_id'];
            }elseif($sTemplate=='Sequence'){
                $oDocumentsForCount = Documents::find("user_id=" . $aParams['user_id'] . " and created_at like '" . date('Y') . "%' ");
                $nSequence_number = $aParams['oIONumberSeqences']->start_sequence + $oDocumentsForCount->count();



                if( $aParams['oIONumberSeqences']->end_sequence < $nSequence_number  ){
                    return '#Out of Range';
                }
                $sOutboxNumber .= $nSequence_number;
            }else{
                $sOutboxNumber .= $sTemplate;
            }
        }
        return $sOutboxNumber;
    }


}
