<?php

namespace MyCompose\Model;

use Illuminate\Support\Str;
use MyCompose\LaravelModels\Account;
use MyCompose\LaravelModels\User;

class Users extends \MyCompose\Model\Model
{

    public static $toJsonRelations = [
        'Contact' => 'addChild', // can be mergeArray | addChild | addChildren depending on the relation ( 1<->1 | N->1 | 1->N)
    ];


    public function initialize()
    {
        //$this->setSchema("mc_full");

          $this->belongsTo('avatar', 'MyCompose\Model\Files', 'id', ['alias' => 'Avatar']);
        $this->hasManyToMany(
            "id",
            "MyCompose\Model\Plans\PlanUser",
            "user_id",
            "plan_id",
            "MyCompose\Model\Plans\Plan",
            "id",
            [
                'params'   => [
                    'conditions' => "is_active = '1'"
                ],
                'alias' => 'Plans']
        );

        $this->hasManyToMany(
            "id",
            "MyCompose\Model\Access\RoleUser",
            "user_id",
            "role_id",
            "MyCompose\Model\Access\Roles",
            "id",
            [
                'params'   => [
                    'conditions' => ""
                ],
                'alias' => 'Roles']
        );

    }

    public function getSource()
    {
        return 'users';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS[]|MCUSERS
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getUsername($sFirstName, $sLastName) {
        $username = Str::slug($sFirstName . "-" . $sLastName);
        $userRows  = User::whereRaw("username REGEXP '^{$username}(-[0-9]*)?$'")->get();
        $countUser = count($userRows) + 1;

        return ($countUser > 1) ? "{$username}-{$countUser}" : $username;
    }


    public static function CreateUserByEmail($sEmail,$sName=null,$nCreatedBy=null){

        $oUser = new Users();
        $oUser->email = $sEmail;
       if(strpos($sName," ") !== false) {
            $aExplodeName = explode(' ',$sName) ;
            $first_name = $aExplodeName[0] ;
            $last_name = $aExplodeName[1] ;
           $oUser->first_name = $first_name ;
           $oUser->last_name = $last_name ;
       }else{
           $oUser->first_name = $sName ;
       }
        $oUser->activated = 0 ;
        $oUser->created_by = $nCreatedBy ;
        $oUser->created_at = date('Y-m-d H:i:s') ;
        if( !$oUser->save() ){
            return $oUser->getMessages();
        }

        $nUserID = $oUser->id ;
        $oAccount = new Accounts();
        $oAccount->user_id = $nUserID ;
        $oAccount->account_name = $sName ;
        $oAccount->active = 0 ;
        $oAccount->verify = 0 ;
        $oAccount->company_id = 1 ;
        $oAccount->is_default = 1 ;
        $oAccount->created_at = date('Y-m-d H:i:s') ;
        if( !$oAccount->save() ){
            return $oAccount->getMessages();
        }
        return [
            'oUser'=>$oUser->toArray(),
            'oAccount'=>$oAccount->toArray(),
            'id'=>$nUserID,
            'account_id'=>$oAccount->id ] ;

    }

    public function mainAccount(){
        $oAccount = Accounts::findFirst("user_id=" . $this->id . " and is_default=1") ;
        return $oAccount;
    }

    public function getByContactId($nContactID){
        if(empty($nContactID) || !is_numeric($nContactID)){
            return false;
        }

        $oContact = self::findFirst("contact_id=" . $nContactID);

        if(!$oContact){
            return false;
        }

        return $oContact;
    }


}
