<?php
namespace MyCompose\Model\Workflow;

class Stage extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Workflow\StageState', 'stage_id', ['alias' => 'StageState']);
        $this->belongsTo('workflow_id', 'MyCompose\Model\Workflow\Workflow', 'id', ['alias' => 'Workflow']);


    }


    public function getSource()
    {
        return 'wf_stage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
