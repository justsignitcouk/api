<?php
namespace MyCompose\Model\Workflow;

class Workflow extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    public $active;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $updated_by;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\DocumentWorkflow', 'workflow_id', ['alias' => 'DocumentWorkflow']);
        $this->hasMany('id', 'MyCompose\Model\WfStage', 'workflow_id', ['alias' => 'Stage']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'wf_workflow';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function startStageID(){
        $nWorkFlowID = $this->id ;
        $oStage = Stage::findFirst("workflow_id=" . $nWorkFlowID . " and action_type='START' ");
        if(!$oStage) {
            return false;
        }
        return $oStage->id;
    }

}
