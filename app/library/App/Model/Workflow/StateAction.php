<?php

namespace MyCompose\Model\Workflow;

use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Users;

class StateAction extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('action_id', 'MyCompose\Model\Workflow\Action', 'id', ['alias' => 'Action']);
        $this->belongsTo('state_id', 'MyCompose\Model\Workflow\State', 'id', ['alias' => 'State']);
        $this->hasMany('id', 'MyCompose\Model\Workflow\StateActionPermitsUser', 'state_action_id', ['alias' => 'StateActionPermitsUser']);
        $this->hasMany('id', 'MyCompose\Model\Workflow\StateActionPermitsRole', 'state_action_id', ['alias' => 'StateActionPermitsRole']);

    }


    public function getSource()
    {
        return 'wf_state_action';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getIfHasAccess($aParams)
    {
        $nStateActionID = $this->id;
        $nUserId = $aParams['user_id'] ;
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db');

        $type = $this->Action->action_type;
        if($type=='sign'){
                    $oRecipients = Recipients::findFirst("user_id=" . $nUserId . " 
                    and document_id=" . $aParams['document_id'] . " 
                    and type='Signer' 
                    and can_read=1 ");
                    if($oRecipients){
                        return true;
                    }


            return false;
        }
        $check = $db->query("SELECT 
                                          ru.user_id,
                                          pr.role_id 
                                        FROM
                                          `wf_state_action_permits_role` pr 
                                          JOIN `role_user` ru 
                                            ON pr.`role_id` = ru.`role_id` 
                                        WHERE ru.`user_id` = $nUserId 
                                          AND state_action_id = $nStateActionID ")->numRows();

        if ($check > 0) {
            return true;
        }

        $check = $db->query("SELECT 
                                          pu.user_id
                                           FROM
                                          `wf_state_action_permits_user` pu 
                                        WHERE pu.`user_id` = $nUserId")->numRows();


        if ($check > 0) {
            return true;
        }

        return false;
    }


}
