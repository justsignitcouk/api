<?php
namespace MyCompose\Model\Workflow;

class StateActionPermitsUser extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('state_action_id', 'MyCompose\Model\Workflow\StateAction', 'id', ['alias' => 'StateAction']);

    }


    public function getSource()
    {
        return 'wf_state_action_permits_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
