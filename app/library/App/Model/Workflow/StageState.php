<?php
namespace MyCompose\Model\Workflow;
use MyCompose\Model\Model;
class StageState extends Model
{

    public static $toJsonRelations = [
        'State' => 'addChild',
    ];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('state_id', 'MyCompose\Model\Workflow\State', 'id', ['alias' => 'State']);
        $this->belongsTo('stage_id', 'MyCompose\Model\Workflow\Stage', 'id', ['alias' => 'Stage']);

    }


    public function getSource()
    {
        return 'wf_stage_state';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
