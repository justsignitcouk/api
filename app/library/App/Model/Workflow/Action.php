<?php
namespace MyCompose\Model\Workflow;

class Action extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('function_id', 'MyCompose\Model\Workflow\WorkflowFunction', 'id', ['alias' => 'WorkflowFunction']);

    }


    public function getSource()
    {
        return 'wf_action';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getActionsByUserId($aParams){

        $aActions = [] ;
        $oWorkFlow = Workflow::findFirstById($aParams['workflow_id']);
        $oStage = Stage::findFirstById( $aParams['stage_id'] );
        $oStageStates = $oStage->StageState ;
        if(empty($oStageStates)){
            return [] ;
        }

        foreach( $oStageStates as $oStageState ) {
            $nStateID = $oStageState->state_id;
            $oStateActions = StateAction::find("state_id=" . $nStateID ) ;
            foreach($oStateActions as $oStateAction) {
                $checkAccess = $oStateAction->getIfHasAccess($aParams) ;

                if(!$checkAccess){
                    continue;
                }
                $oAction = Action::findFirstById($oStateAction->action_id);
                if($oAction){
                    $aAction= $oAction->toArray();
                    $aAction['stage_id'] = $aParams['stage_id'];
                    $aAction['state_id'] = $nStateID;
                    $aActions[] = $aAction;
                }

            }
        }

        return $aActions;

    }

}
