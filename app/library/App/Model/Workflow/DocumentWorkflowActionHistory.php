<?php
namespace MyCompose\Model\Workflow;

class DocumentWorkflowActionHistory extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        //$this->belongsTo('function_id', 'MyCompose\Model\Workflow\WorkflowFunction', 'id', ['alias' => 'WorkflowFunction']);

    }


    public function getSource()
    {
        return 'document_workflow_action_history';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
