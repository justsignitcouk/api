<?php

namespace MyCompose\Model;

class Friends extends \Phalcon\Mvc\Model
{


    public function initialize()
    {
        //$this->setSchema("mc_full");
//        $this->hasMany('id', 'MyCompose\Model\MCREMEMBERTOKENS', 'user_id', ['alias' => 'MCREMEMBERTOKENS']);
//        $this->hasMany('id', 'MyCompose\Model\MCSUCCESSLOGINS', 'user_id', ['alias' => 'MCSUCCESSLOGINS']);
        $this->belongsTo('requester_id', 'MyCompose\Model\Users', 'id', ['alias' => 'Requester']);
        $this->belongsTo('requested_id', 'MyCompose\Model\Users', 'id', ['alias' => 'Requested']);

    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'friends';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS[]|MCUSERS
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MCUSERS
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
