<?php
namespace MyCompose\Model\Contacts;

use Phalcon\Di;

class MyContactsGroupContact extends \MyCompose\Model\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('group_id', 'MyCompose\Model\Contacts\MyContactsGroup', 'id', ['alias' => 'Group']);
        $this->belongsTo('contact_id', 'MyCompose\Model\Contacts\MyContact', 'id', ['alias' => 'Contact']);

    }


    public function getSource()
    {
        return 'contact_group_contacts';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


}
