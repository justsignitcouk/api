<?php
namespace MyCompose\Model\Contacts;

class MessagesQueue extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
//        $this->hasMany('id', 'MyCompose\Model\Contacts\Emails', 'contact_id', ['alias' => 'Emails']);
//        $this->hasMany('id', 'MyCompose\Model\Contacts\Mobiles', 'contact_id', ['alias' => 'Mobiles']);

    }


    public function getSource()
    {
        return 'messages_queue';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
