<?php
namespace MyCompose\Model\Contacts;

use Illuminate\Support\Str;
use MyCompose\LaravelModels\Contacts\Contact;
use MyCompose\Model\Files;
use MyCompose\Model\Users;

class ContactsRoles extends \MyCompose\Model\Model
{

    private $errors = [] ;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('contact_id', 'MyCompose\Model\Contacts\Contacts', 'id', ['alias' => 'Contact']);
        $this->belongsTo('role_id', 'MyCompose\Model\RolesDocumentToRole', 'id', ['alias' => 'Role']);

    }


    public function getSource()
    {
        return 'contacts_roles';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }



}
