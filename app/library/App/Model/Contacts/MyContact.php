<?php
namespace MyCompose\Model\Contacts;

use MyCompose\Model\Users;
use Phalcon\Di;

class MyContact extends \MyCompose\Model\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);
        $this->belongsTo('my_id', 'MyCompose\Model\Contacts\Contacts', 'id', ['alias' => 'MyContact']);
        $this->belongsTo('contact_id', 'MyCompose\Model\Contacts\Contacts', 'id', ['alias' => 'Contact']);

    }


    public function getSource()
    {
        return 'my_contact';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function findByUserId($nUserId){

        $di = DI::getDefault();
        $db = $di->get('db');

        $oMyContacts = Contacts::find("user_id=" . $nUserId) ;
        $aMyContacts=$oMyContacts->toArray();
        if($oMyContacts->count() > 0 ){
            foreach($oMyContacts as $k=>$oContact){
                $aContact = $oContact->toArray();
                $oName = $oContact->oPrimaryName ;
                if(!$oName){
                    continue;
                }
                $aMyContacts[$k]['oPrimaryName']  = $oName->toArray();
                $oEmail = $oContact->oPrimaryEmail ;
                $aMyContacts[$k]['register'] = false;
                if($oEmail){
                    $aMyContacts[$k]['oPrimaryEmail']  = $oEmail->toArray();
                    $oUser = Users::findFirst("email='" . $oEmail->email . "'") ;
                    if($oUser){
                        $aMyContacts[$k]['register'] = true;
                    }
                }

                $oMobile = $oContact->oPrimaryMobile ;
                if($oMobile){
                    $aMyContacts[$k]['oPrimaryMobile']  = $oMobile->toArray();
                    $oUser = Users::findFirst("mobile='" . $oMobile->mobile . "'") ;

                    if($oUser){
                        $aMyContacts[$k]['register'] = true;
                    }
                }



            }


        }



        return $aMyContacts;



    }

}
