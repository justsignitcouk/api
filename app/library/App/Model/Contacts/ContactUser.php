<?php
namespace MyCompose\Model\Contacts;

class ContactUser extends \Phalcon\Mvc\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('id', 'MyCompose\Model\Contacts\Contact', 'contact_id', ['alias' => 'Contact']);
        $this->belongsTo('id', 'MyCompose\Model\Users', 'user_id', ['alias' => 'User']);

    }


    public function getSource()
    {
        return 'contact_user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
