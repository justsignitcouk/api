<?php
namespace MyCompose\Model\Contacts;

class UserContacts extends \MyCompose\Model\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('contact_id', 'MyCompose\Model\Contacts\Contacts', 'id', ['alias' => 'Contact']);
        $this->belongsTo('user_id', 'MyCompose\Model\Users', 'id', ['alias' => 'User']);

    }


    public function getSource()
    {
        return 'user_contacts';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
