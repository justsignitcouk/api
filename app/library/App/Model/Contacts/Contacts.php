<?php
namespace MyCompose\Model\Contacts;

use Illuminate\Support\Str;
use MyCompose\LaravelModels\Contacts\Contact;
use MyCompose\Model\Files;
use MyCompose\Model\Roles;
use MyCompose\Model\Users;
use Phalcon\Di;

class Contacts extends \MyCompose\Model\Model
{

    private $errors = [] ;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->hasMany('id', 'MyCompose\Model\Contacts\Emails', 'contact_id', ['alias' => 'Emails']);
        $this->hasMany('id', 'MyCompose\Model\Contacts\Names', 'contact_id', ['alias' => 'Names']);
        $this->hasMany('id', 'MyCompose\Model\Contacts\Mobiles', 'contact_id', ['alias' => 'Mobiles']);
        $this->belongsTo('id', 'MyCompose\Model\Contacts\Avatar', 'contact_id', ['alias' => 'Avatar']);
        $this->hasMany('id', 'MyCompose\Model\Contacts\UserContacts', 'contact_id', ['alias' => 'UserContacts']);
        $this->belongsTo("id","MyCompose\Model\Users","contact_id", ['alias' => 'User'] );

        $this->belongsTo('id', 'MyCompose\Model\Contacts\Emails', 'contact_id', ['order' => 'is_primary desc','alias' => 'oPrimaryEmail']);
        $this->belongsTo('id', 'MyCompose\Model\Contacts\Mobiles', 'contact_id', ['order' => 'is_primary desc','alias' => 'oPrimaryMobile']);
        $this->hasMany('id', 'MyCompose\Model\Contacts\MyContactsGroupContact', 'contact_id', ['alias' => 'GroupContact']);

    }


    public function getSource()
    {
        return 'contacts';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function createContact($aParams){

        $oContact = new Contacts();
        $oContact->register_by = $aParams['register_by'];

        if(!empty($aParams['user']['gender'])){
            $oContact->gender = $aParams['user']['gender'];
        }

        $oContact->created_at = date('Y-m-d H:i:s');
        if(!$oContact->save()){
            self::$errors[] = $oContact->getMessags();
        }
        $nContactID = $oContact->id ;

        $oUser = new Users() ;
        if(isset($aParams['user']['password'])){
            $oUser->password = $aParams['user']['password'] ;
        }

        $oUser->created_at = date('Y-m-d H:i:s') ;
        $oUser->activated = isset($aParams['user']['activated']) ? $aParams['user']['activated'] : '0';
        $oUser->contact_id = $nContactID ;
        if(!$oUser->save()){
            self::$errors[] = $oUser->getMessags();
        }

        $oUserContacts = new UserContacts();
        $oUserContacts->user_id = $oUser->id ;
        $oUserContacts->contact_id = $nContactID ;
        $oUserContacts->is_primary = 1 ;
        $oUserContacts->is_default = 1 ;
        $oUserContacts->created_at = date('Y-m-d H:i:s') ;
        $oUserContacts->updated_at = date('Y-m-d H:i:s') ;
        $oUserContacts->add_by = $oUser->id ;
        if(!$oUserContacts->save()){
            self::$errors[] = $oUserContacts->getMessags();
        }
        $oUser->contact_id = $oContact->id ;
        $oUser->save();

        if($aParams['register_by']=='facebook' || $aParams['register_by']=='google' || $aParams['register_by']=='twitter') {
            $oSocial = new Social() ;
            $oSocial->social_type = $aParams['register_by'];
            $oSocial->social_id =  $aParams['user']['id'] ;
            $oSocial->created_at =  date('Y-m-d H:i:s') ;
            $oSocial->contact_id =  $nContactID ;
            $oSocial->is_primary =  1 ;
            $oSocial->link = !empty($aParams['user']['link']) ? $aParams['user']['link'] : null;
            if(!$oSocial->save()){
                self::$errors[] = $oSocial->getMessags();
            }
        }

        if(!empty($aParams['mobile'])){
            $oMobile = new Mobiles();
            $oMobile->mobile = $aParams['mobile'];
            $oMobile->contact_id = $nContactID;
            $oMobile->created_at = date('Y-m-d H:i:s');
            $oMobile->is_primary = 1 ;
            $oMobile->verify_code = rand(100000,999999) ;
            if(!$oMobile->save()){
                self::$errors[] = $oMobile->getMessags();
            }
        }

        if(!empty($aParams['email'])){
            $oEmail = new Emails();
            $oEmail->email = $aParams['email'];
            $oEmail->contact_id = $nContactID;
            $oEmail->created_at = date('Y-m-d H:i:s');
            $oEmail->is_primary = 1 ;
            $oEmail->verify_code = rand(100000,999999) ;
            if(!$oEmail->save()){
                self::$errors[] = $oEmail->getMessags();
            }
        }

        if( !empty($aParams['name'] ) ){
            if(is_array($aParams['name'])){
                $first_name = isset($aParams['name']['first_name']) ? $aParams['name']['first_name'] : '' ;
                $middle_name = isset($aParams['name']['middle_name']) ? $aParams['name']['middle_name'] : '' ;
                $last_name = isset($aParams['name']['last_name']) ? $aParams['name']['last_name'] : '' ;

            }else{
                $aName = explode(" ", $aParams['name']);
                if(sizeof($aName)==2){
                    $first_name = $aName[0] ;
                    $middle_name = '' ;
                    $last_name = $aName[1] ;

                }
                if(sizeof($aName)==3){
                    $first_name = $aName[0] ;
                    $middle_name = $aName[1] ;
                    $last_name = $aName[2] ;

                }
            }

            $oName = new Names();

            $oName->full_name = is_array($aParams['name']) ? $first_name . " " . (!empty($middle_name) ? $middle_name . " " : "" ) . $last_name : $aParams['name'];
            $oName->display_name = !empty($aParams['nickname']) ? $aParams['nickname'] : $oName->full_name ;

            $oName->first_name = $first_name;
                $oName->middle_name = $middle_name;
                $oName->last_name = $last_name;


            $oName->contact_id = $nContactID;
            $oName->is_primary = 1;
            $oName->created_at = date('Y-m-d H:i:s');
            if(!$oName->save()){
                self::$errors[] = $oName->getMessags();
            }

            $unique_name = self::getUsername($oName->first_name ,$oName->last_name);
            $oContact->unique_name = $unique_name;
            $oContact->save();
        }

        if(!empty($aParams['avatar'])){
            $oFile = new Files();
            $oFile->url = $aParams['avatar'];
            $oFile->name = 'facebook';
            $oFile->created_at = date('Y-m-d H:i:s');
            if(!$oFile->save()){
                self::$errors[] = $oFile->getMessags();
            }
            $oAvatar = new Avatar();
            $oAvatar->file_id = $oFile->id;
            $oAvatar->contact_id = $nContactID;
            $oAvatar->created_at = date('Y-m-d H:i:s');
            $oAvatar->is_primary = 1 ;
            if(!$oAvatar->save()){
                self::$errors[] = $oAvatar->getMessags();
            }
        }

        $oContactsRoles = new ContactsRoles();
        $oContactsRoles->contact_id = $nContactID;
        $oContactsRoles->role_id = 3;
        $oContactsRoles->created_at = date('Y-m-d H:i:s');
        $oContactsRoles->active = 1;
        $oContactsRoles->save();

        return $oContact;
    }

    public static function updateContact($nContactID,$aParams){

        $oContact = Contacts::findFirstById($nContactID);
        if(!$oContact){
            return false;
        }

        if(!empty($aParams['user']['gender'])){
            $oContact->gender = $aParams['user']['gender'];
        }

        $oContact->updated_at = date('Y-m-d H:i:s');
        if(!$oContact->save()){
            self::$errors[] = $oContact->getMessags();
        }

        $oUserContacts = UserContacts::findFirst("contact_id=" . $nContactID) ;
        if(!$oUserContacts){
            $oUser = new Users() ;
            $oUser->created_at = date('Y-m-d H:i:s') ;
            $oUser->activated = 1 ;
            if(!$oUser->save()){
                self::$errors[] = $oUser->getMessags();
            }

            $oUserContacts = new UserContacts();
            $oUserContacts->user_id = $oUser->id ;
            $oUserContacts->contact_id = $nContactID ;
            $oUserContacts->is_primary = 1 ;
            $oUserContacts->is_default = 1 ;
            $oUserContacts->created_at = date('Y-m-d H:i:s') ;
            $oUserContacts->updated_at = date('Y-m-d H:i:s') ;
            $oUserContacts->add_by = $oUser->id ;
            if(!$oUserContacts->save()){
                self::$errors[] = $oUserContacts->getMessags();
            }
            $oUser->contact_id = $oContact->id ;
            $oUser->save();
        }




        if($aParams['register_by']=='facebook' || $aParams['register_by']=='google' || $aParams['register_by']=='twitter') {
            $oSocial = Social::findFirst("social_type='" . $aParams['register_by'] . "' and contact_id=" . $nContactID  );
            if(!$oSocial) {
                $oSocial = new Social() ;
                $oSocial->created_at =  date('Y-m-d H:i:s') ;
                $oSocial->is_primary =  1 ;
            }
                $oSocial->social_type = $aParams['register_by'];
                $oSocial->social_id =  $aParams['user']['id'] ;
                $oSocial->updated_at =  date('Y-m-d H:i:s') ;
                $oSocial->contact_id =  $nContactID ;

                $oSocial->link = !empty($aParams['user']['link']) ? $aParams['user']['link'] : null;
                if(!$oSocial->save()){
                    self::$errors[] = $oSocial->getMessags();
                }

        }

        if(!empty($aParams['email'])){
            $oEmail = Emails::findFirst("email='" . $aParams['email'] . "' and contact_id=" . $nContactID) ;
            if(!$oEmail){
                $oEmail = new Emails();
                $oEmail->created_at = date('Y-m-d H:i:s');
                $oEmail->is_primary = 1 ;
            }
            $oEmail->email = $aParams['email'];
            $oEmail->contact_id = $nContactID;
            $oEmail->updated_at = date('Y-m-d H:i:s');
            if(!$oEmail->save()){
                self::$errors[] = $oEmail->getMessags();
            }
        }

        if( !empty($aParams['name'] ) ){
            $aName = explode(" ", $aParams['name']);

            $oName = Names::findFirst("full_name='" . $aParams['name'] . "' and contact_id=" . $nContactID);
            if(!$oName){
                $oName = new Names();
                $oName->is_primary = 1;
                $oName->created_at = date('Y-m-d H:i:s');
            }

            $oName->display_name = !empty($aParams['nickname']) ? $aParams['nickname'] : $aParams['name'];
            $oName->full_name = $aParams['name'];
            if(sizeof($aName)==2){
                $oName->first_name = $aName[0];
                $oName->last_name = $aName[1];
            }
            if(sizeof($aName)==3){
                $oName->first_name = $aName[0];
                $oName->middle_name = $aName[1];
                $oName->last_name = $aName[2];
            }

            $oName->contact_id = $nContactID;

            $oName->updated_at = date('Y-m-d H:i:s');
            if(!$oName->save()){
                self::$errors[] = $oName->getMessags();
            }
            if(empty($oContact->unique_name )){
                $unique_name = self::getUsername($oName->first_name ,$oName->last_name);
                $oContact->unique_name = $unique_name;
                $oContact->save();
            }

        }

        if(!empty($aParams['avatar'])){
            $oFile = Files::findFirst("url='" . $aParams['avatar'] . "'") ;
            if(!$oFile){
                $oFile = new Files();
                $oFile->created_at = date('Y-m-d H:i:s');
            }

            $oFile->url = $aParams['avatar'];
            $oFile->name = $aParams['register_by'];
            $oFile->updated_at = date('Y-m-d H:i:s');
            if(!$oFile->save()){
                self::$errors[] = $oFile->getMessags();
            }
            $oAvatar = Avatar::findFirst("contact_id=" . $nContactID . " and file_id=" . $oFile->id) ;
            if(!$oAvatar){
                $oAvatar = new Avatar();
                $oAvatar->created_at = date('Y-m-d H:i:s');
                $oAvatar->is_primary = 1 ;
            }
            $oAvatar->file_id = $oFile->id;
            $oAvatar->contact_id = $nContactID;
            $oAvatar->updated_at = date('Y-m-d H:i:s');

            if(!$oAvatar->save()){
                self::$errors[] = $oAvatar->getMessags();
            }
        }
        return $oContact;
    }

    public function getErrorMessages(){
        return $this->errors;
    }


    public static function getUsername($sFirstName, $sLastName) {
        $username = Str::slug($sFirstName . "-" . $sLastName);
        $userRows  = Contact::whereRaw("unique_name REGEXP '^{$username}(-[0-9]*)?$'")->get();
        $countUser = count($userRows) + 1;

        return ($countUser > 1) ? "{$username}-{$countUser}" : $username;
    }


    public static function findByUserId($nUserId){

        $di = DI::getDefault();
        $db = $di->get('db');

        $oMyContacts = $db->fetchAll("select  c.id, c.display_name, ce.email , cm.mobile, group_concat(cg.name) as groupname
from contacts c
LEFT  OUTER JOIN  contacts_emails ce on c.id = ce.contact_id
LEFT   OUTER JOIN contacts_mobiles cm on c.id = cm.contact_id
LEFT OUTER JOIN contact_group_contacts cgc on c.id = cgc.contact_id
LEFT OUTER JOIN contact_group cg on cg.id = cgc.group_id
where c.user_id=$nUserId
group by c.id, c.display_name, ce.email , cm.mobile");

        return($oMyContacts);

        /*

        $oMyContacts = Contacts::find("user_id=" . $nUserId);
        $aMyContacts= $oMyContacts->toArray();
        if($oMyContacts->count() > 0 ){
            foreach($oMyContacts as $k=>$oContact){
                $aContact = $oContact->toArray();
                $aMyContacts[$k]['oPrimaryName']  =$oContact->full_name;
                $aMyContacts[$k]['name']  =$oContact->full_name;
                $oEmail = $oContact->oPrimaryEmail ;
                $aMyContacts[$k]['register'] = false;
                if($oEmail){
                    $aMyContacts[$k]['oPrimaryEmail']  = $oEmail->toArray();
                    $oUser = Users::findFirst("email='" . $oEmail->email . "'") ;
                    if($oUser){
                        $aMyContacts[$k]['register'] = true;
                    }
                }

                $oMobile = $oContact->oPrimaryMobile ;
                if($oMobile){
                    $aMyContacts[$k]['oPrimaryMobile']  = $oMobile->toArray();
                    $oUser = Users::findFirst("mobile='" . $oMobile->mobile . "'") ;

                    if($oUser){
                        $aMyContacts[$k]['register'] = true;
                    }
                }



            }


        }



        return $aMyContacts;

*/

    }

}
