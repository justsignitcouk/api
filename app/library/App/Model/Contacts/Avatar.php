<?php
namespace MyCompose\Model\Contacts;

class Avatar extends \MyCompose\Model\Model
{


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("mc");
        $this->belongsTo('contact_id', 'MyCompose\Model\Contacts\Contacts', 'id', ['alias' => 'Contact']);
        $this->belongsTo('file_id', 'MyCompose\Model\Files', 'id', ['alias' => 'File']);

    }


    public function getSource()
    {
        return 'contacts_avatar';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow[]|WfWorkflow
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WfWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
