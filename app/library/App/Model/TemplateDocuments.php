<?php
namespace MyCompose\Model;
/**
 * @property  user_id
 */
class TemplateDocuments extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $content;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $category_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $privacy;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $expiry_date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $account_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $parent_document_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layout_id;

    /**
     *
     * @var string
     * @Column(type="string", length=100, nullable=true)
     */
    public $outbox_number;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {

        $this->hasMany('id', 'MyCompose\Model\Draft\DraftDocumentsignee', 'document_id', ['alias' => 'DraftDocumentSignee']);
        $this->hasMany('id', 'MyCompose\Model\DraftDocumentTowhom', 'document_id', ['alias' => 'DraftDocumentTowhom']);
     }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'templates_documents';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return DraftDocuments[]|DraftDocuments
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return DraftDocuments
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
