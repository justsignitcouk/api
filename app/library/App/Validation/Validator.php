<?php
use Phalcon\Validation as ValidationCore;
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\Regex,
    Phalcon\Validation\Validator\InclusionIn,
    Phalcon\Validation\Validator\ExclusionIn,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Email;
class Validation extends ValidationCore{
    public function initialize()
    {

    }

    public function ValidationEmail($sEmail)
    {

        $validation = $this;
        $validation->add('email', new Email(array(
            'message' => 'The email is not valid'
        )));
        $messages = $validation->validate($sEmail);
        $expectedMessages = Phalcon\Validation\Message\Group::__set_state(array(
            '_messages' => array(
                0 => Phalcon\Validation\Message::__set_state(array(
                    '_type' => 'Email',
                    '_message' => 'The email is not valid',
                    '_field' => 'email',
                ))
            )
        ));
        $this->assertEquals($expectedMessages, $messages);
        $_POST = array('email' => 'x=1');
        $messages = $validation->validate($_POST);
        $this->assertEquals($expectedMessages, $messages);
        $_POST = array('email' => 'x.x@hotmail.com');
        $messages = $validation->validate($_POST);
        $this->assertEquals(count($messages), 0);
    }

}