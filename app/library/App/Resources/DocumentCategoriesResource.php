<?php

namespace App\Resources;

use App\Controllers\DocumentCategoriesController;
use MyCompose\Model\Document\DocumentCategories;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\DocumentCategoriesTransformer;
use App\Constants\AclRoles;

class DocumentCategoriesResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('DocumentCategories')
            ->model(DocumentCategories::class)
            ->expectsJsonData()
            ->transformer(DocumentCategoriesTransformer::class)
            ->itemKey('departments')
            ->collectionKey('document_categories')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(DocumentCategoriesController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/department', 'getByDocumentDepartment')->name('getByDocumentDepartment'))
            ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}