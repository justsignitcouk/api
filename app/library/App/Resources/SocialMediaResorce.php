<?php

namespace App\Resources;

use App\Controllers\SocialMediaController;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use MyCompose\Model\Users;
use App\Transformers\UserTransformer;
use App\Controllers\UserController;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use Phalcon\Di;


class SocialMediaResorce extends ApiResource {

    public function initialize()
    {



        $this
            ->name('social_media')
            ->expectsJsonData()
            ->transformer(UserTransformer::class)
            ->itemKey('social_media')
            ->collectionKey('social_media')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(SocialMediaController::class)


            ->endpoint(ApiEndpoint::post('/check_mail', 'check_mail')
                ->name("check_mail")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/regist_by_facebook', 'regist_by_facebook')
                ->name("regist_by_facebook")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/log_in_by_facebook', 'log_in_by_facebook')
                ->name("log_in_by_facebook")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/regist_by_google', 'regist_by_google')
                ->name("regist_by_google")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/log_in_by_google', 'log_in_by_google')
                ->name("log_in_by_google")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/regist_by_linkedin', 'regist_by_linkedin')
                ->name("regist_by_linkedin")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/log_in_by_linkedin', 'log_in_by_linkedin')
                ->name("log_in_by_linkedin")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))


        ;




    }
}