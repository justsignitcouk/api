<?php

namespace App\Resources;

use App\Controllers\AccessController;
use App\Controllers\AccountController;
use App\Controllers\DepartmentsController;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class AccessResource extends ApiResource {

    public function initialize()
    {
        $this
            ->name('access')
            ->expectsJsonData()
            ->itemKey('access')
            ->collectionKey('access')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(AccessController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/', 'can')->name("can"))
        ;

    }
}