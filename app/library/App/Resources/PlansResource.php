<?php

namespace App\Resources;

use App\Controllers\PlansController;

use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;

class PlansResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('plans')
            ->expectsJsonData()
            ->itemKey('plans')
            ->collectionKey('plans')
            ->handler(PlansController::class)
            ->endpoint(ApiEndpoint::all()->allow(AclRoles::UNAUTHORIZED))
//            ->endpoint(ApiEndpoint::post('/', 'getWorkflowByID'))
            ->endpoint(ApiEndpoint::get('/{id}', 'getPlanById')->description('')->name('getPlanById'))
            ->endpoint(ApiEndpoint::get('/getFeatturesByUserId/{id}', 'getFeatturesByUserId')->description('')->name('getFeatturesByUserId'))

        ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}