<?php

namespace App\Resources;

use App\Controllers\WorkFlowsController;

use MyCompose\LaravelModels\Workflow\Workflow;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\WorkFlowTransformer;
use App\Constants\AclRoles;

class WorkFlowResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Workflows')
            ->expectsJsonData()
            ->transformer(WorkFlowTransformer::class)
            ->itemKey('workflows')
            ->collectionKey('workflows')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(WorkFlowsController::class)
            ->endpoint(ApiEndpoint::all())
//            ->endpoint(ApiEndpoint::post('/', 'getWorkflowByID'))
            ->endpoint(ApiEndpoint::get('/{id}', 'getWorkflowByID')->description('')->name('getDocument')->allow(AclRoles::USER))
            ->endpoint(ApiEndpoint::post('/saveWfStateAction', 'saveWfStateAction'))
            ->endpoint(ApiEndpoint::post('/getByAccount', 'getByAccount'))
            ->endpoint(ApiEndpoint::post('/getByCompany', 'getByCompany'))
            ->endpoint(ApiEndpoint::post('/getStageDetails', 'getStageDetails'))
            ->endpoint(ApiEndpoint::get('/get_default', 'getDefault'))

        ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}