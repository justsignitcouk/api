<?php

namespace App\Resources;

use App\Controllers\TemplateDocumentsController;
use MyCompose\Model\TemplateDocuments;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\DocumentsTransformer;
use App\Constants\AclRoles;

class TemplateDocumentsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('template_documents')
            ->model(TemplateDocuments::class)
            ->expectsJsonData()
            ->transformer(DocumentsTransformer::class)
            ->itemKey('template_documents')
            ->collectionKey('template_documents')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(TemplateDocumentsController::class)
            ->endpoint(ApiEndpoint::post('/', 'getByUserID'))
            ->endpoint(ApiEndpoint::post('/delete', 'deleteTemplateDocuments'));
    }
}
