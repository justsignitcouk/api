<?php

namespace App\Resources;

use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use MyCompose\Model\Companies as Companies;
use App\Transformers\CompaniesTransformer;
use App\Controllers\CompaniesController;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class CompaniesResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Companies')
            ->model(Companies::class)
            ->expectsJsonData()
            ->transformer(CompaniesTransformer::class)
            ->itemKey('companies')
            ->collectionKey('companies')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(CompaniesController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::get('/{slug}', 'getCompanyBySlug'))
            ->endpoint(ApiEndpoint::get('/{slug}/{branch_id}', 'getBranchByID'))
            ->endpoint(ApiEndpoint::get('/{slug}/employees', 'getEmployeesByCompanySlug'))
            ->endpoint(ApiEndpoint::get('/{slug}/roles', 'getRolesByCompanySlug'))
            ->endpoint(ApiEndpoint::get('/getByCreator/{account_id}', 'getByCreator'))
            ->endpoint(ApiEndpoint::post('/', 'save'))
            ->endpoint(ApiEndpoint::post('/saveBranch', 'saveBranch'))
            ->endpoint(ApiEndpoint::post('/exist', 'exist'))
            ->endpoint(ApiEndpoint::post('/company_departments', 'getCompanyDepartments'))
        ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}