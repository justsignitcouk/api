<?php

namespace App\Resources;

use App\Controllers\LogsController;
use MyCompose\Model\CompileLogs;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class LogsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Logs')
            ->model(CompileLogs::class)
            ->expectsJsonData()
            ->itemKey('logs')
            ->collectionKey('logs')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(LogsController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/save', 'save')->name("save"))
        ;



    }
}