<?php

namespace App\Resources;


use App\Controllers\QuotaController;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;

class QuotaResource extends ApiResource {

    public function initialize()
    {
        $this
            ->name('quota')
            ->expectsJsonData()
            ->itemKey('quota')
            ->collectionKey('quota')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(QuotaController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/', 'get')->name("get"))
        ;

    }
}