<?php

namespace App\Resources;

use App\Controllers\FilesController;
use App\Controllers\LogsController;
use MyCompose\Model\CompileLogs;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class FilesResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Files')
            ->model(Files::class)
            ->expectsJsonData()
            ->itemKey('files')
            ->collectionKey('files')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(FilesController::class)
            ->endpoint(ApiEndpoint::post('/upload', 'upload')->name("upload"))
            ->endpoint(ApiEndpoint::post('/uploadFromExternal', 'uploadFromExternal')->name("uploadFromExternal"))
        ;



    }
}