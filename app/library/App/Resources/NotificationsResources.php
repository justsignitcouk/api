<?php

namespace App\Resources;

use App\Controllers\NotificationsController;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use MyCompose\Model\Users;
use App\Transformers\UserTransformer;
use App\Controllers\UserController;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use Phalcon\Di;


class NotificationsResources extends ApiResource {

    public function initialize()
    {



        $this
            ->name('notifications')
            ->expectsJsonData()
            ->transformer(UserTransformer::class)
            ->itemKey('notifications')
            ->collectionKey('notifications')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(NotificationsController::class)

            ->endpoint(ApiEndpoint::get('/', 'getUserNotification')
                ->name("get_user_notifications")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::get('/create_user_notifications', 'create_user_notifications')
                ->name("create_user_notifications")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/change_read_status', 'change_read_status')
                ->name("change_read_status")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

            ->endpoint(ApiEndpoint::post('/get_user_mobile_notifications', 'get_user_mobile_notifications')
                ->name("get_user_mobile_notifications")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

        ;

    }
}