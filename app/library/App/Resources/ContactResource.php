<?php

namespace App\Resources;


use App\Controllers\ContactController;
use MyCompose\Model\Contacts\Contacts;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class ContactResource extends ApiResource {

    public function initialize()
    {
        $this
            ->name('contact')
            ->model(Contacts::class)
            ->expectsJsonData()
            ->itemKey('contact')
            ->collectionKey('contact')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(ContactController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::get('/{id}', 'getAccountById')->name("getAccountById"))
            ->endpoint(ApiEndpoint::post('/exist', 'exist'))
            ->endpoint(ApiEndpoint::post('/make_default', 'make_default')->name('make_default'))
            ->endpoint(ApiEndpoint::post('/company_details', 'company_details'))
            ->endpoint(ApiEndpoint::post('/to_whom_addresses', 'getToWhomAddresses')->name('to_whome_addresses'))
            ->endpoint(ApiEndpoint::get('/{id}/{sFilter}', 'inbox'))
            ->endpoint(ApiEndpoint::get('/{id}/sent', 'sent')->name('sent'))
            ->endpoint(ApiEndpoint::get('/{id}/draft', 'draft')->name('draft'))
            ->endpoint(ApiEndpoint::get('/{id}/badges', 'badges'))
            ->endpoint(ApiEndpoint::get('/{id}/departments', 'getDepartmentsByAccountId')->name("accountsdepartment"))
            ->endpoint(ApiEndpoint::post('/getDefaultSignature', 'getDefaultSignature'))
            ->endpoint(ApiEndpoint::post('/setDefaultSignature', 'setDefaultSignature'))
            ->endpoint(ApiEndpoint::post('/sign', 'sign'))
            ->endpoint(ApiEndpoint::get('/{id}/waitingFromOthers', 'getWaitingFromOthers'))
            ->endpoint(ApiEndpoint::get('/{id}/needsMySign', 'needsMySign'))
            ->endpoint(ApiEndpoint::get('/{id}/contacts', 'getContacts'))
            ->endpoint(ApiEndpoint::post('/getContact', 'getContact'))
            ->endpoint(ApiEndpoint::get('/{id}/contacts_for_recipient', 'getContactsForRecipient'))
            ->endpoint(ApiEndpoint::post('/{id}/contacts', 'saveContacts'))
            ->endpoint(ApiEndpoint::post('/setProfileImage', 'changeProfileImage'))
            ->endpoint(ApiEndpoint::post('/setCoverImage', 'changeCoverImage'))
            ->endpoint(ApiEndpoint::post('/deactivate', 'deactivate'))
            ->endpoint(ApiEndpoint::post('/avatar', 'saveAvatar')
                ->name("save_avatar")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED)
                ->description("save contact Avatar"))
            ->endpoint(ApiEndpoint::post('/delete_avatar', 'deleteAvatar')
                ->name("delete_avatar")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED)
                ->description("delete contact Avatar"))
            ->endpoint(ApiEndpoint::post('/findBy', 'findBy')
                ->name("setPassword")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED)
                ->description("findBy"))
            ->endpoint(ApiEndpoint::get('/{id}/mycontact', 'getMyContacts')->name("getMyContacts"))
            ->endpoint(ApiEndpoint::post('/{id}/mycontact', 'saveMyContacts')->name("saveMyContacts"))

        ;

    }
}