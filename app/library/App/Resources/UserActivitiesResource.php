<?php


namespace App\Resources;

use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\UserTransformer;
use App\Controllers\UserActivitiesController;
use App\Constants\AclRoles;
use Phalcon\Di;

class UserActivitiesResource extends ApiResource
{


    public function initialize()
    {


        $this
            ->name('user_activities')
            ->expectsJsonData()
            ->transformer(UserTransformer::class)
            ->itemKey('user_activities')
            ->collectionKey('user_activities')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(UserActivitiesController::class)


            ->endpoint(ApiEndpoint::get('/all', 'all')
                ->name("all")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))




        ;

    }

}