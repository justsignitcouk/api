<?php

namespace App\Resources;

use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Controllers\HelpDeskController;
use MyCompose\Model\Users;



class HelpDiskResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('help_disk')
            ->expectsJsonData()
            ->transformer(UserTransformer::class)
            ->itemKey('help_disk')
            ->collectionKey('help_disk')
            ->handler(HelpDeskController::class)
            ->endpoint(ApiEndpoint::post('/login_share', 'login_share')
            ->name("login_share"))
            ->endpoint(ApiEndpoint::get('/client_login', 'client_login')
                ->name("client_login"))


        ;

    }
}
