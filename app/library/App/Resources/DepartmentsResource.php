<?php

namespace App\Resources;

use App\Controllers\DepartmentsController;
use MyCompose\Model\Departments;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\DepartmentsTransformer;
use App\Constants\AclRoles;

class DepartmentsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Departments')
            ->model(Departments::class)
            ->expectsJsonData()
            ->transformer(DepartmentsTransformer::class)
            ->itemKey('departments')
            ->collectionKey('departments')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(DepartmentsController::class)
            ->endpoint(ApiEndpoint::all())
            ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}