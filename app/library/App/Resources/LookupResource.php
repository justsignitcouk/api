<?php


namespace App\Resources;

use App\Controllers\LookupController;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;
use Phalcon\Di;

class LookupResource extends ApiResource
{
    public function initialize()
    {


        $this
            ->name('lookup')
            ->expectsJsonData()
            ->itemKey('lookup')
            ->collectionKey('lookup')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(LookupController::class)
            ->endpoint(ApiEndpoint::get('/{table}', 'get')
                ->name("lookup")
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED))

        ;

    }

}