<?php

namespace App\Resources;

use App\Controllers\TemplatesController;
use MyCompose\Model\Templates;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\TemplatesTransformer;
use App\Constants\AclRoles;

class TemplatesResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Templates')
            ->model(Templates::class)
            ->expectsJsonData()
            ->transformer(TemplatesTransformer::class)
            ->itemKey('templates')
            ->collectionKey('templates')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(TemplatesController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/ByLayoutID', 'getByLayoutID'))
            ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}