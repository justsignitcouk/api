<?php

namespace App\Resources;

use App\Controllers\ActionsController;
use App\Controllers\WorkFlowsController;

use MyCompose\Model\Actions;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\ActionsTransformer;
use App\Constants\AclRoles;

class ActionsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('actions')
            ->expectsJsonData()
            ->model(Actions::class)
            ->transformer(ActionsTransformer::class)
            ->itemKey('actions')
            ->collectionKey('actions')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(ActionsController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::get('/{nDocumentID}', 'getByDocumentID')->description('')->name('getByDocumentID')->allow(AclRoles::USER))
            ->endpoint(ApiEndpoint::post('/', 'saveAction')->description('')->name('saveAction')->allow(AclRoles::USER))

        ;



//            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
//                ->allow(AclRoles::UNAUTHORIZED)
//                ->deny(AclRoles::AUTHORIZED)
//                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
//                ->exampleResponse([
//                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
//                    'expires' => 1451139067
//                ])
//            );
    }
}