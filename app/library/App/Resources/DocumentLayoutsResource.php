<?php

namespace App\Resources;

use App\Controllers\DocumentLayoutsController;
use MyCompose\LaravelModels\DocumentLayouts;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Constants\AclRoles;

class DocumentLayoutsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('DocumentLayouts')
            ->model(DocumentLayouts::class)
            ->expectsJsonData()
            ->itemKey('document_layouts')
            ->collectionKey('document_layouts')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(DocumentLayoutsController::class)
            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::post('/', 'getLayoutByID'))
            ->endpoint(ApiEndpoint::get('/default', 'getDefaultLayout'))
            ->endpoint(ApiEndpoint::post('/store_header', 'storeHeader'))
            ->endpoint(ApiEndpoint::post('/store_footer', 'storeFooter'))
            ->endpoint(ApiEndpoint::get('/all_headers/{id}', 'allHeaders'))
            ->endpoint(ApiEndpoint::get('/all_footers/{id}', 'allFooters'))
            ->endpoint(ApiEndpoint::get('/get_header/{id}', 'getHeader'))
            ->endpoint(ApiEndpoint::get('/get_footer/{id}', 'getFooter'))
        ;


    }
}