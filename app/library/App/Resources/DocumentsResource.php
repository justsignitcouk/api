<?php

namespace App\Resources;

use App\Controllers\DocumentsController;
use MyCompose\Model\Document\Documents;
use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Transformers\DocumentsTransformer;
use App\Constants\AclRoles;

class DocumentsResource extends ApiResource {

    public function initialize()
    {

        $this
            ->name('Document')
            ->model(Documents::class)
            ->expectsJsonData()
            ->transformer(DocumentsTransformer::class)
            ->itemKey('document')
            ->collectionKey('document')
            ->deny(AclRoles::UNAUTHORIZED)
            ->handler(DocumentsController::class)
            ->endpoint(ApiEndpoint::get('/{id}', 'getDocumentByID')
                ->name("save")
                ->description('Required')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::get('/{id}/forwarded', 'getDocumentForwarded')
                ->name("save")
                ->description('Required')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/', 'compose')
                ->name("save")
                ->description('Required')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/forward', 'forwardDocument')
                ->name("forward")
                ->description('Required')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::get('/{id}/pdf', 'pdf')
                ->name("pdf")
                ->description('pdf')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/{id}/status', 'setStatus')
                ->name("save")
                ->description('Required')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/{id}/lock', 'lock')
                ->name("lock")
                ->description('lock')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::get('/{id}/notes', 'getNotes')
                ->name("notes")
                ->description('notes')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/notes/savePosition', 'savePosition')
                ->name("savePosition")
                ->description('savePosition')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::get('/access/{id}', 'checkAccess')
                ->name("checkAccess")
                ->description('checkAccess')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/forward_note', 'getForwardNote')
                ->name("getForwardNote")
                ->description('getForwardNote')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/draft', 'insertNewDraft')
                ->name("insertNewDraft")
                ->description('insertNewDraft')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/draft/delete', 'deleteDraft')
                ->name("deleteDraft")
                ->description('deleteDraft')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/details', 'details')
                ->name("details")
                ->description('details')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/attachments', 'attachments')
                ->name("attachments")
                ->description('attachments')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/import', 'import')
                ->name("import")
                ->description('import')
                ->exampleResponse([]))
            ->endpoint(ApiEndpoint::post('/importFromPdf', 'importFromPdf')
                ->name("import")
                ->description('importFromPdf')
                ->exampleResponse([]))
        ;
    }
}
