<?php

namespace App\Components\Auth;

use MyCompose\LaravelModels\Contacts\Contact;
use MyCompose\Model\Access\PermissionUser;
use MyCompose\Model\Access\RoleUser;
use MyCompose\Model\Devices;
use MyCompose\Model\Plans\PlanPermission;
use MyCompose\Model\Plans\PlanUser;
use MyCompose\Model\Profiles\IONumber;
use MyCompose\Model\Profiles\ProfileRequirements;
use MyCompose\Model\Profiles\UserProfileRequirements;
use MyCompose\Model\Users;
use \PhalconApi\Auth\Manager as ManagerCore;
use PhalconApi\Auth\Session;
use PhalconApi\Constants\ErrorCodes;
use PhalconApi\Exception;

class Manager extends ManagerCore
{


    public function loginWithUsernamePassword($accountTypeName, $username, $password)
    {

        return $this->login($accountTypeName, [

            self::LOGIN_DATA_USERNAME => $username,
            self::LOGIN_DATA_PASSWORD => $password
        ]);
    }



    public function login($accountTypeName, array $data)
    {

        if (!$account = $this->getAccountType($accountTypeName)) {

            throw new Exception(ErrorCodes::AUTH_INVALID_ACCOUNT_TYPE);
        }
        $account->AccountType=$accountTypeName;

        $identity = $account->login($data);

        if (!$identity) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $startTime = time();

        $session = new Session($accountTypeName, $identity, $startTime, $startTime + $this->sessionDuration);
        $token = $this->tokenParser->getToken($session);
        $session->setToken($token);

        $this->session = $session;

        return $this->session;
    }



    public function login_social($accountTypeName, array $data)
    {

        $user_id  = $data['user_id'] ;

        $account = new \App\Auth\UsernameAccountType();
        $account->AccountType = $accountTypeName;
        $identity = $account->login_social($user_id);
        if (!$identity) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED);
        }

        $startTime = time();

        $session = new Session($accountTypeName, $identity, $startTime, $startTime + $this->sessionDuration);
        $token = $this->tokenParser->getToken($session);
        $session->setToken($token);

        $this->session = $session;

        return $this->session;

    }


    public function loginWithSocialMedia($accountTypeName, $data)
    {
        return $this->login_social($accountTypeName, $data);
    }

    public static function getLoginInfo($nUserId,$aParams,$session){

        $oUser = Users::findFirst("id=" . $nUserId) ;
        if (!$oUser) {
            throw new Exception(ErrorCodes::AUTH_LOGIN_FAILED, 'User Not Found');
        }

        $aUser = $oUser->toArray();

        $aUser['avatar'] = [];
        $aUser['avatar']['full_url'] = (!empty($oUser->Avatar) ? $oUser->Avatar->url . (!empty( $oUser->Avatar->path) ?   '/files/' . $oUser->Avatar->path . '/' . $oUser->Avatar->hash_name : ''  ) : null) ;

        $aProfileRequirements = ProfileRequirements::getProfileRequirementsByUserID($oUser->id);

        $oPermissions  = PermissionUser::find("user_id=" . $nUserId)  ;

        $oRoles  = RoleUser::find("user_id=" . $nUserId)  ;
        $aRoles = $oRoles->toArray();
//        foreach($oRoles as $k => $oRole){
//            $oPermissionRole = $oRole->PermissionRole ;
//            $aPermissionsRole = [];
//            if($oPermissionRole->count()>0){
//                foreach($oPermissionRole as $oPR) {
//                    $aPermissionsRole[] = $oPR->Permission;
//                }
//            }
//            $aRoles[$k]['aPermissions'] = $aPermissionsRole;
//        }

        $oPlansUser = PlanUser::find("user_id=" . $nUserId . " and is_active=1") ;
        $aPlans = [] ;
        $aPlanRole = [];
        foreach($oPlansUser as $oPlanUser ){
            $oPlan = $oPlanUser->Plan;
            $oPlanRoles = $oPlan->PlanRole;
            foreach($oPlanRoles as $oPlanRole){
                $aPlanRole[] = $oPlanRole->Role;
            }
            $aRoles = array_merge($aRoles,$aPlanRole) ;
            if($oPlanUser->Plan){
                $aPlans[] = array_merge($oPlanUser->Plan->toArray(),['aRole' => $aPlanRole ]);
            }


        }
        $aUser['aProfileRequirements'] = $aProfileRequirements;
        $aUser['aPermissions'] = $oPermissions->toArray();
        $aUser['aRoles'] = $aRoles;
        $aUser['aPlans'] = $aPlans ;
        $aUser['sIONumber'] = '' ;
        $oIONumber = IONumber::findFirst("user_id=$nUserId and active=1");
        if($oIONumber){
            $aUser['sIONumber'] = $oIONumber->template ;
        }



        $oDevices = Devices::findFirst("token ='" . $session->getToken() . "'");
        if (!$oDevices) {
            $oDevices = new Devices();
            $oDevices->user_id = $session->getIdentity();
            $oDevices->ip = $aParams['sIP'];
            $oDevices->token = $session->getToken();
            $oDevices->expires = $session->getExpirationTime();
            $oDevices->fcm_token = $aParams['sFCMToken'];
            $oDevices->browser = $aParams['sBrowser'];
            $oDevices->browser_version = $aParams['sBrowserVersion'];
            $oDevices->platform = $aParams['sPlatform'];
            $oDevices->platform_version = $aParams['sPlatformVersion'];
            $oDevices->device = $aParams['sDevice'];
            $oDevices->languages = $aParams['sLanguages'];
            $oDevices->created_at = date('Y-m-d H:i:s');
        }

        $oDevices->last_login = date('Y-m-d H:i:s');
        $oDevices->ip = $aParams['sIP'];

        if (!$oDevices->save()) {
            return $oDevices->getMessages();
        }

        $response = [
            'token' => $session->getToken(),
            'expires' => $session->getExpirationTime(),
            'aUser' => $aUser
        ];

        return $response;
    }

}
