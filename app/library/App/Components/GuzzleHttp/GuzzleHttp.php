<?php

namespace App\Components\GuzzleHttp;

use GuzzleHttp\Client;

class GuzzleHttp extends Client
{
        public static $_instance;
    protected $oClient;
    protected $sApiUrl;
    protected $sToken;
    protected $sUrl;
    protected $sMethod = 'GET';
    private $aMethodAllowed = ['GET','POST','PUT','PATCH','DELETE'] ;
    private $aHeader = [] ;
    private $aBody = [] ;
    private $aQuery = [] ;
    private $aMultiPart = [] ;
    private $bMultiPart = false ;
    private $bWithToken = false ;
    private $oRequest;


    public static function getInstance()
    {
        if ( ! ( self::$_instance instanceof self) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct(){
        $di = \Phalcon\DI::getDefault();
        $this->oClient = new Client();
        $this->sApiUrl = $di->get('config')->CDN_URL;
        $this->sToken = "";
    }

    public function setBaseUrl($sUrl){
        $this->sApiUrl = $sUrl;
        return $this;
    }

    public function setUrl($sUrl){
        $sUrl = $this->sApiUrl . '/' . $sUrl ;
        if (filter_var($sUrl, FILTER_VALIDATE_URL) === FALSE) {
            die('h');
            return false;
        }
        $this->sUrl = $sUrl;

        return $this;
    }

    public function setMethod($sMethod){

        if(!in_array(strtoupper($sMethod),$this->aMethodAllowed)){
            throw new \Exception('THE METHOD NOT ALLOWED');
        }

        $this->sMethod = $sMethod ;
        return $this;
    }

    public function request1(){
        try{

        }catch (\Exception $exception){
            throw $exception;
        }
        $this->oRequest = $this->oClient->request($this->sMethod ,$this->sUrl, ['verify'=>false,
            'headers'         => $this->addIfWithToken(),
            ($this->bMultiPart ? 'multipart' : 'form_params')         => $this->aBody,
            'query' => $this->aQuery
        ]);

        return $this;
    }

    public function setHeader($aParams=[]){

        $this->aHeader = $aParams ;

        return $this;
    }

    public function appendHeader($aParams=[]){
        foreach($aParams as $k=>$value){
            $this->aHeader[$k] = $value ;
        }
        return $this;
    }

    public function setBody($aParams=[]){

        $this->aBody = $aParams ;

        return $this;
    }

    public function setQuery($aParams=[]){

        $this->aQuery = $aParams ;

        return $this;
    }

    public function setMultiPart($bStatus = true){

        $this->bMultiPart = $bStatus ;

        return $this;
    }

    public function appendBody($aParams=[]){
        foreach($aParams as $k=>$value){
            $this->aBody[$k] = $value ;
        }

        return $this;
    }

    public function withToken(){
        $this->bWithToken = true;
        return $this;
    }

    private function addIfWithToken(){
        if($this->bWithToken){
            return array_merge($this->aHeader,array('Authorization'=> 'Bearer ' . $this->sToken));
        }
        return $this->aHeader ;
    }

    public function getContent(){
        return $this->oRequest->getbody()->getContents();
    }
}