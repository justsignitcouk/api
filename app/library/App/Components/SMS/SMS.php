<?php

namespace App\Components\SMS;
use Phalcon\Mvc\User\Component;
use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;
use Phalcon\Mvc\View;
class SMS extends Component
{
    // Base URLS for three methods
    public $base_url_SendSMS = 'http://api.rmlconnect.net:/bulksms/bulksms';
    public $base_url_SendSMSUnicode = 'http://api.rmlconnect.net/bulksms/bulksms';
    public $base_url_QueryBalance = 'http://api.rmlconnect.net/bulksms/bulksms';

    // Public Variables that are used as parameters in API calls
    public $username = 'knowmdo';
    public $password = 'G3uuGYbN';
    public $apikey = '';
    public $message = '';   // LET THIS REMAIN BLANK
    public $from = 'MyCompose';      // LET THIS REMAIN BLANK
    public $to = 'MyCompose';        // LET THIS REMAIN BLANK


    // SEND SMS FUNCTION FOR UNICODE TEXT
    function send()
    {

        $fieldcnt    = 6;
        $fieldstring = "username=". $this->username ."&password=". $this->password ."&type=0&dlr=1&message=". $this->message ."&destination=". $this->to ."&source=". $this->from ;

        $sms = new \MyCompose\Model\Sms();
        $sms->to = $this->to;
        $sms->from = $this->from ;
        $sms->message = $this->message;
        $sms->created_at = date('Y-m-d H:i:s');
        $sms->save();

        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->base_url_SendSMS);
        curl_setopt($ch, CURLOPT_POST, $fieldcnt);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldstring);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
        $sms->response = $res;
        $sms->save();
        return $res;
    }

    // FUNCTION TO QUERY YOUR ACCOUNT BALANCE
    function QueryBalance()
    {
        $fieldcnt    = 3;
        $fieldstring = "Userid=$this->username&pwd=$this->password&APIKEY=$this->apikey";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->base_url_QueryBalance);
        curl_setopt($ch, CURLOPT_POST, $fieldcnt);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldstring);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

}