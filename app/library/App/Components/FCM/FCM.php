<?php

namespace App\Components\FCM;

use MyCompose\Model\Devices;

use MyCompose\LaravelModels\Notifications;
use MyCompose\Model\Users;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;

class FCM
{

    private $server_key = 'AAAAJQqYThY:APA91bFix-yStClqSr6Kxf9gWePFEip9kIcFps54QDEaMiIIoUtxHT09VCA597Bd-5QT9vtyveU_VmhSwMeKSlVMXSsqJVUQcmOUVJ3bZ9faIsDSLT8h2I8mQ60HMRyoBMRvmk067J8R';
    private $client;

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setApiKey($this->server_key);
        $this->client->injectGuzzleHttpClient(new \GuzzleHttp\Client());
    }

//    function sendNotificationNewDocument($body, $account,$aParams=[])
//    {
//        if(!isset($aParams['type'])){
//            $aParams['type'] = 0 ;
//        }
//
//        $notificationsCount = Notifications::where('receiver_id',$account)->get()->count();
//        $aParams['count'] = $notificationsCount ;
//
//        $message = new Message();
//        $message->setPriority('high');
//
//        $oAccount = Accounts::findFirstById($account);
//
//        if (!$oAccount) {
//            return false;
//        }
//
//        $userId = $oAccount->user_id;
//
//        $oDevice = Devices::findFirst("user_id='$userId'");
//        if (!$oDevice) {
//            return false;
//        }
//
//        if(is_null($oDevice->fcm_token)){
//            return false;
//        }
//
//        $message->addRecipient(new Device($oDevice->fcm_token));
//
//        $message
//            ->setNotification(new Notification("", $body))
//            ->setData($aParams);
//
//        $response = $this->client->send($message);
//        return $response;
//    }

    public function Notify($oContact,$body,$aParams=[]){
        $oUser = Users::findFirst("contact_id=" . $oContact->id);
        $nUserID = $oUser->id;

        $oDevice = Devices::findFirst("user_id='$nUserID'");
        if (!$oDevice) {
            return false;
        }

        if(is_null($oDevice->fcm_token)){
            return false;
        }

        $message = new Message();
        $message->setPriority('high');

        $message->addRecipient(new Device($oDevice->fcm_token));

        $message
            ->setNotification(new Notification("", $body))
            ->setData($aParams);

        $response = $this->client->send($message);
    }

}