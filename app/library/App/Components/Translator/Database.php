<?php
namespace MyCompose\Components\Translator;

use Phalcon\Translate\Adapter\Database as DatabaseCore;
use Phalcon\Db;
//use MyCompose\Components\I18N_Arabic\Arabic;
class Database extends DatabaseCore
{
    public function __construct(array $options)
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }

        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }

        if (!isset($options['language'])) {
            throw new Exception("Parameter 'language' is required");
        }

        if (isset($options['useIcuMessageFormatter'])) {
            if (!class_exists('\MessageFormatter')) {
                throw new Exception('"MessageFormatter" class is required');
            }

            $this->useIcuMessageFormatter = (boolean) $options['useIcuMessageFormatter'];
        }

        $this->stmtSelect = sprintf(
            'SELECT text FROM %s WHERE locale = :language AND item = :key_name',
            $options['table']
        );

        $this->stmtExists = sprintf(
            'SELECT COUNT(*) AS `count` FROM %s WHERE locale = :language AND item = :key_name',
            $options['table']
        );

        $this->options = $options;
    }

    public function setLanguage($lang){
        $this->options['language'] = $lang;
        return $this;
    }

    public function query($translateKey, $placeholders = null)
    {
        $options     = $this->options;
        $translation = $options['db']->fetchOne(
            $this->stmtSelect,
            Db::FETCH_ASSOC,
            ['language' => $options['language'], 'key_name' => $translateKey]
        );
        $value       = empty($translation['text']) ? $translateKey : $translation['text'];

        if (is_array($placeholders) && !empty($placeholders)) {
            if (true === $this->useIcuMessageFormatter) {
                $value = \MessageFormatter::formatMessage($options['language'], $value, $placeholders);
            } else {
                foreach ($placeholders as $placeHolderKey => $placeHolderValue) {
                    $value = str_replace('%' . $placeHolderKey . '%', $placeHolderValue, $value);
                }
            }
        }

        return $value;
    }


    public function _($translateKey, $placeholders = null)
    {
        return $this->query($translateKey, $placeholders);
    }
}

?>