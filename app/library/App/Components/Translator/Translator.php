<?php
namespace MyCompose\Components\Translator;
use Phalcon\Mvc\User\Component;
use MyCompose\Components\Translator\Database;


class Translator extends Component{

public $lang = 'en'  ;

    public function getLanguage(){
        return $this->lang ;
    }
    public function setLanguage($lang){
        $this->lang = $lang;
        return $this;
    }

    public function _getTranslation()
    {
        return new Database([
            'db'       => $this->di->get('db'), // Here we're getting the database from DI
            'table'    => 'translator_translations', // The table that is storing the translations
            'language' => $this->lang // Now we're getting the best language for the user
        ]);
    }

}