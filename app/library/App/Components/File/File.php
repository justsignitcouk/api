<?php

namespace App\Components\File;

use App\Components\GuzzleHttp\GuzzleHttp;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Files;

class File extends GuzzleHttp {

    public static function put($file,$path='common'){

        $key = $file->getKey() ;
        $size = $file->getSize ();

        $oServices = Parent::getInstance();

        try{
            $path_internal = 'files/' . $file->getName() ;
            $file->moveTo($path_internal);
            $type = pathinfo($path_internal, PATHINFO_EXTENSION);
            $aFile = ['name'     => 'file',
                'contents' => fopen( $path_internal,'r'),
                'filename' => $file->getName() ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("upload")
                ->setBody(['file' =>$aFile])
                ->setQuery(['path_type' =>$path ])
                ->setMultiPart(true)
                ->request1()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
            ] ;

            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        $sGenerateName = $response_array['result'] ;
        $oFile = new Files();
        $oFile->hash_name = $sGenerateName;
        $oFile->name = $file->getName();
        $oFile->size = $size;
        $oFile->path = $path;
        $oFile->is_private = false;
        $di = \Phalcon\DI::getDefault();
        $oFile->url = $di->get('config')->CDN_URL;
        $oFile->account_id = $di->get('userService')->getIdentity() ;
        if(!$oFile->save()){
            return false ;
        }
        //unlink($path_internal);
        return $oFile->id;
    }


    public static function putWithHtml($file,$path='common',$nUserId){


        $key = $file->getKey() ;
        $size = $file->getSize ();

        $oServices = Parent::getInstance();

        try{
            $path_internal = 'files/' . $file->getName() ;
            $file->moveTo($path_internal);
            $type = pathinfo($path_internal, PATHINFO_EXTENSION);
            $aFile = ['name'     => 'file',
                'contents' => fopen( $path_internal,'r'),
                'filename' => $file->getName() ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("upload")
                ->setBody(['file' =>$aFile])
                ->setQuery(['path_type' =>$path ])
                ->setMultiPart(true)
                ->request1()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
            ] ;

            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        $sGenerateName = $response_array['result'] ;
        $oFile = new Files();
        $oFile->hash_name = $sGenerateName;
        $oFile->name = $file->getName();
        $oFile->size = $size;
        $oFile->path = $path;
        $oFile->is_private = false;
        $di = \Phalcon\DI::getDefault();
        $oFile->url = $di->get('config')->CDN_URL;
        $oFile->account_id = $di->get('userService')->getIdentity() ;
        if(!$oFile->save()){
            return false ;
        }

        if($type == 'docx'){
            $phpWord = \PhpOffice\PhpWord\IOFactory::load($path_internal);
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
            $objWriter->save('files/helloWorld.html');
            $html = file_get_contents('files/helloWorld.html') ;
        }
        if($type == 'pdf'){
            $pdf = new \Gufy\PdfToHtml\Pdf($path_internal,['ignoreImages' => true]);
            ob_start();
            $html = $pdf->html();
            $html = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);
            ob_end_clean();
        }

        $oDocument = new Documents();
        $oDocument->content = $html ;
        $oDocument->user_id = $nUserId ;
        $oDocument->title = $file->getName() ;
        $oDocument->status = 'draft' ;
        $oDocument->created_at = date('Y-m-d H:i:s') ;
        $oDocument->save();

        unlink($path_internal);
        //unlink('files/helloWorld.html');
        return ['file_id' => $oFile->id,'document_id' =>$oDocument->id ];
    }



    public static function putFromUrl($Url,$path='common',$sName='temp.png'){

        $path_internal = 'files/' . $sName ;
        $file   = file($Url);
        $result = file_put_contents($path_internal, $file);


        $oServices = Parent::getInstance();

        try{


            $type = pathinfo($path_internal, PATHINFO_EXTENSION);
            $aFile = ['name'     => 'file',
                'contents' => fopen( $path_internal,'r'),
                'filename' => $sName ] ;

            $oClient =  $oServices
                ->setMethod('post')
                ->setUrl("upload")
                ->setBody(['file' =>$aFile])
                ->setQuery(['path_type' =>$path ])
                ->setMultiPart(true)
                ->request1()
                ->getContent() ;
        }catch (\Exception $exception){
            dd($exception->getMessage());
            $response_array = [
                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
            ] ;

            return $response_array['data'] ;
        }
        $response_array = json_decode($oClient,true);

        $sGenerateName = $response_array['result'] ;
        $oFile = new Files();
        $oFile->hash_name = $sGenerateName;
        $oFile->name = $sName;
        $oFile->size = filesize($path_internal);
        $oFile->path = $path;
        $oFile->is_private = false;
        $di = \Phalcon\DI::getDefault();
        $oFile->url = $di->get('config')->CDN_URL;
        $oFile->account_id = $di->get('userService')->getIdentity() ;
        if(!$oFile->save()){
            return false ;
        }

        unlink($path_internal);
        return $oFile->id;
    }


    public static function putFromBase64($base64,$path){


        $content= base64_decode($base64);
        $file = fopen('files/ssssss.jpg', 'w+');
        fwrite($file, $content);
        fclose($file);
        return true;


//        $path_internal = 'files/' . $sName ;
//        $file   = file($Url);
//        $result = file_put_contents($path_internal, $file);
//
//
//        $oServices = Parent::getInstance();
//
//        try{
//
//
//            $type = pathinfo($path_internal, PATHINFO_EXTENSION);
//            $aFile = ['name'     => 'file',
//                'contents' => fopen( $path_internal,'r'),
//                'filename' => $sName ] ;
//
//            $oClient =  $oServices
//                ->setMethod('post')
//                ->setUrl("upload")
//                ->setBody(['file' =>$aFile])
//                ->setQuery(['path_type' =>$path ])
//                ->setMultiPart(true)
//                ->request1()
//                ->getContent() ;
//        }catch (\Exception $exception){
//            dd($exception->getMessage());
//            $response_array = [
//                'data' => ['RecordsTotal'=>'0','pending'=>[], 'error'=> $exception->getMessage()],
//            ] ;
//
//            return $response_array['data'] ;
//        }
//        $response_array = json_decode($oClient,true);
//
//        $sGenerateName = $response_array['result'] ;
//        $oFile = new Files();
//        $oFile->hash_name = $sGenerateName;
//        $oFile->name = $sName;
//        $oFile->size = filesize($path_internal);
//        $oFile->path = $path;
//        $oFile->is_private = false;
//        $di = \Phalcon\DI::getDefault();
//        $oFile->url = $di->get('config')->CDN_URL;
//        $oFile->account_id = $di->get('userService')->getIdentity() ;
//        if(!$oFile->save()){
//            return false ;
//        }
//
//        unlink($path_internal);
//        return $oFile->id;

    }


}