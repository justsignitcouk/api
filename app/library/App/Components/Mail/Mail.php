<?php

namespace App\Components\Mail;
use App\Components\PDF\PDF;
use Phalcon\Mvc\User\Component;
use Swift_Message as Message;
use Swift_SmtpTransport as Smtp;
use Phalcon\Mvc\View;
class Mail extends Component
{
    protected $transport;

    /**
     * Send a raw e-mail via AmazonSES
     *
     * @param string $raw
     * @return bool
     */

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     * @return string
     */
    public function getTemplate($name, $params)
    {
        $parameters = array_merge([
            'publicUrl' => $this->config->APP_URL
        ], $params);
        return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
            $view->setRenderLevel(View::LEVEL_LAYOUT);
        });
        return $view->getContent();
    }
    /**
     * Sends e-mails via AmazonSES based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     * @return bool|int
     * @throws Exception
     */
    public function send($to, $subject, $name, $params)
    {
        // Settings
        $mailSettings = $this->config->mail;

        $template = $this->getTemplate($name, $params);

        // Create the message
        $message = Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom([
                $mailSettings->fromEmail => $mailSettings->fromName
            ])
            ->setBody($template, 'text/html');

        if(isset($params['document_id'])) {

        $message->attach(
            \Swift_Attachment::fromPath('files/document.pdf')->setFilename('document.pdf')
        );
        }

        if (isset($mailSettings) && isset($mailSettings->smtp)) {
            if (!$this->transport) {
                $this->transport = Smtp::newInstance(
                    $mailSettings->smtp->server,
                    $mailSettings->smtp->port,
                    $mailSettings->smtp->security
                )
                    ->setUsername($mailSettings->smtp->username)
                    ->setPassword($mailSettings->smtp->password);
            }
            // Create the Mailer using your created Transport
            $mailer = \Swift_Mailer::newInstance($this->transport);
            return $mailer->send($message);
        } else {
            return $this->amazonSESSend($message->toString());
        }
    }
}