<?php

namespace App\Components\PDF;
use MyCompose\Model\Document\DocumentLayout;

class PDF
{

    public static function generateFromDocument($aDocument)
    {
        $di = \Phalcon\Di::getDefault();
        $nLayoutID = $aDocument['layout']['id'] ;
        $oLayout = DocumentLayout::findFirstById($nLayoutID);

        $header = $oLayout->header;
        $footer = $oLayout->footer;

        $html = $di->get('view')->getPartial('layouts/layout' . $nLayoutID . '/content', ['aDocument'=>$aDocument,'to_string'=> $di->get('lang')->setLanguage('ar')->_('to') ]);

        $pdf  = $di->get('pdf');
        //$pdf->SetDirectionality('rtl');
        $pdf->useDefaultCSS2 = true;
        $pdf->setAutoTopMargin = true;
        $pdf->setAutoBottomMargin = 'stretch';
        $pdf->showImageErrors = true;
        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);

        $pdf->WriteHTML($html);
        return $pdf->Output();

    }

    public static function saveFromDocument($aDocument)
    {
        $di = \Phalcon\Di::getDefault();
        $nLayoutID = $aDocument['layout']['id'] ;
        $header = $di->get('view')->getPartial('layouts/layout' . $nLayoutID . '/header', ['aDocument'=>$aDocument]);
        $footer = $di->get('view')->getPartial('layouts/layout' . $nLayoutID . '/footer', []);
        $html = $di->get('view')->getPartial('layouts/layout' . $nLayoutID . '/content', ['aDocument'=>$aDocument]);

        $pdf  = $di->get('pdf');
        $pdf->SetDirectionality('rtl');
        $pdf->useDefaultCSS2 = true;
        $pdf->setAutoTopMargin = true;

        $pdf->SetHTMLHeader($header);
        $pdf->SetHTMLFooter($footer);

        $pdf->WriteHTML($html);
        return $pdf->Output('files/document.pdf','F');

    }

}