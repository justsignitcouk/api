<?php

namespace App\Components\Notifications;

use App\Components\FCM\FCM;
use MyCompose\LaravelModels\Notifications as Noti;
use MyCompose\Model\Document\DocumentNotification;


class Notifications
{

    public  static function createNotification($receiver_id,$sender_id,$document_id,$notification_type)
    {


        $notification = new Noti ;

        $notification->receiver_id       = $receiver_id ;
        $notification->sender_id         = $sender_id ;
        $notification->document_id       = $document_id ;
        $notification->type              = $notification_type ;
        $notification->read              = false ;
        if($notification_type == 4) {
            $notification->signer_id = $sender_id ;
        }


        $notification->save();


    }

    public static function notifyDocument($oDocument, $oUser,$nType ,$aParams = [])
    {
        $nNotificationID = $nType;

        $oNotification = new DocumentNotification();
        $oNotification->document_id = $oDocument->id;
        $oNotification->notification_id = $nNotificationID;
        $oNotification->created_at = date('Y-m-d H:i:s');
        $oNotification->updated_at = date('Y-m-d H:i:s');
        $oNotification->updated_at = date('Y-m-d H:i:s');
        $oNotification->sender_id = $oDocument->user_id;
        $oNotification->user_id = $oUser->id;
        $oNotification->content = isset($aParams['sContent']) ? $aParams['sContent'] : '';
        if (!$oNotification->save()) {
            echo "222222222222";
            var_dump($oNotification->getMessages());
            exit();
            return false;
        }
//        $fcm = new FCM();
//        $fcm->Notify($oUser, $oDocument->title, $aParams = []);
        return true;
    }




}