<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as Console;
use Phalcon\Loader;




define("ROOT_DIR", __DIR__ . '/../../../..');
define("APP_DIR", ROOT_DIR . '/app');
define("VENDOR_DIR", ROOT_DIR . '/vendor');
define("CONFIG_DIR", APP_DIR . '/configs');
define('APPLICATION_ENV', getenv('APPLICATION_ENV') ?: 'development');

require VENDOR_DIR . '/autoload.php';

// Using the CLI factory default services container
$di = new CliDI();

/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader = new Loader();

$loader->registerNamespaces([
    'App' => APP_DIR . '/library/App',
    'MyCompose' => APP_DIR . '/library/App',
]);


$loader->registerDirs(
    [
        APP_DIR . '/library/App/Tasks/',
    ]
);
$loader->register();
// Config
$configPath = CONFIG_DIR . '/default.php';

if (!is_readable($configPath)) {
    throw new Exception('Unable to read config from ' . $configPath);
}

$config = new Phalcon\Config(include_once $configPath);

$envConfigPath = CONFIG_DIR . '/server.' . APPLICATION_ENV . '.php';
$override = new Phalcon\Config(include_once $envConfigPath);

$config = $config->merge($override);


if (!is_readable($envConfigPath)) {
    throw new Exception('Unable to read config from ' . $envConfigPath);
}

// Create a console application
$app = new Console();


$di->setShared(\App\Constants\Services::CONFIG, $config);

$di->setShared('eventsManager', function () use ($di, $config) {

    return new Phalcon\Events\Manager;
});

$di->set(\App\Constants\Services::DB, function () use ($config, $di) {

    $config = $config->get('database')->toArray();
    $adapter = $config['adapter'];
    unset($config['adapter']);
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    $connection = new $class($config);

    // Assign the eventsManager to the db adapter instance
    $connection->setEventsManager($di->get(\App\Constants\Services::EVENTS_MANAGER));

    return $connection;
});

$di->set('modelsManager', function () use ($di) {
    $modelsManager = new Phalcon\Mvc\Model\Manager;
    return $modelsManager->setEventsManager($di->get('eventsManager'));
});

$di->setShared(\App\Constants\Services::MODELS_MANAGER, function () use ($di) {

    $modelsManager = new Phalcon\Mvc\Model\Manager;
    return $modelsManager->setEventsManager($di->get(\App\Constants\Services::EVENTS_MANAGER));
});

$di->set(\App\Constants\Services::VIEW, function () use ($config) {

    $view = new \Phalcon\Mvc\View;
    $view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

    $view->setViewsDir($config->get('application')->viewsDir);

    return $view;
});


/**
 * @description PhalconRest - \League\Fractal\Manager
 */
$di->setShared(\App\Constants\Services::FRACTAL_MANAGER, function () {

    $fractal = new League\Fractal\Manager;
    $fractal->setSerializer(new CustomSerializer);

    return $fractal;
});



$app->setDI($di);


 /**
 * Process the console arguments
 */
$arguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k === 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

try {
    // Handle incoming arguments
    $app->handle($arguments);

} catch (\Phalcon\Exception $e) {
    // Do Phalcon related stuff here
    // ..
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    exit(1);
} catch (\Throwable $throwable) {

    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
} catch (\Exception $exception) {

    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    exit(1);
}