<?php

namespace App\Bootstrap;

use App\BootstrapInterface;
use App\Collections\ExportCollection;
use App\Resources\AccessResource;
use App\Resources\AccountResource;
use App\Resources\ActionsResource;
use App\Resources\ContactResource;
use App\Resources\DocumentLayoutsResource;
use App\Resources\DocumentSigneeResource;
use App\Resources\DraftDocumentsResource;
use App\Resources\FilesResource;
use App\Resources\LogsResource;
use App\Resources\LookupResource;
use App\Resources\PlansResource;
use App\Resources\QuotaResource;
use App\Resources\SocialMediaResorce;
use App\Resources\TemplateDocumentsResource;
use App\Resources\UserResource;
use App\Resources\CompaniesResource;
use App\Resources\DepartmentsResource;
use App\Resources\DocumentCategoriesResource;
use App\Resources\WorkFlowResource;
use App\Resources\DocumentsResource;
use App\Resources\TemplatesResource;
use App\Resources\NotificationsResources;
use App\Resources\UserActivitiesResource;
use App\Resources\HelpDiskResource;
use MyCompose\Model\Actions;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconRest\Api;

class CollectionBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        $api
            ->collection(new ExportCollection('/export'))
            ->resource(new UserResource('/user'))
            ->resource(new ContactResource('/contact'))
            ->resource(new CompaniesResource('/companies'))
            ->resource(new DepartmentsResource('/departments'))
            ->resource(new DocumentCategoriesResource('/document_categories'))
            ->resource(new WorkFlowResource('/workflow'))
            ->resource(new ActionsResource('/actions'))
            ->resource(new DocumentsResource('/document'))
            ->resource(new TemplatesResource('/templates'))
            ->resource(new TemplateDocumentsResource('/templateDocuments'))
            ->resource(new DocumentLayoutsResource('/document_layouts'))
            ->resource(new LogsResource('/logs'))
            ->resource(new FilesResource('/files'))
            ->resource(new SocialMediaResorce('/social_media'))
            ->resource(new NotificationsResources('/notifications'))
            ->resource(new UserActivitiesResource('/user_activities'))
            ->resource(new HelpDiskResource('/help_disk'))
            ->resource(new PlansResource('/plans'))
            ->resource(new LookupResource('/lookup'))
            ->resource(new AccessResource('/access'))
            ->resource(new QuotaResource('/quota'))

        ;
    }
}
