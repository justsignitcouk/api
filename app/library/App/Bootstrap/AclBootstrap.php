<?php

namespace App\Bootstrap;

use App\BootstrapInterface;
use App\Constants\Services;
use MyCompose\Model\Access\Roles;
use Phalcon\Acl;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconRest\Api;
use App\Constants\AclRoles;

class AclBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        /** @var \PhalconApi\Acl\MountingEnabledAdapterInterface $acl */
        $acl = $di->get(Services::ACL);

        $oRoles = Roles::find();
        $unauthorizedRole = new Acl\Role(AclRoles::UNAUTHORIZED);
        $authorizedRole = new Acl\Role(AclRoles::AUTHORIZED);
        $acl->addRole($unauthorizedRole);
        $acl->addRole($authorizedRole);
        if($oRoles->count()>0){
            foreach($oRoles as $oRole){
                $acl->addRole(new Acl\Role($oRole->display_name), $authorizedRole);
            }
        }

        $acl->addRole(new Acl\Role(AclRoles::ADMINISTRATOR), $authorizedRole);
        $acl->addRole(new Acl\Role(AclRoles::MANAGER), $authorizedRole);
        $acl->addRole(new Acl\Role(AclRoles::USER), $authorizedRole);


        $acl->mountMany($api->getCollections());
    }
}