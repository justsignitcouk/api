<?php

namespace App\Bootstrap;

use App\BootstrapInterface;
use App\Constants\Services;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconRest\Api;


use PhalconRest\Export\Documentation;
use PhalconRest\Export\Postman\ApiCollection;
use PhalconRest\Mvc\Controllers\CollectionController;
use PhalconRest\Transformers\DocumentationTransformer;
use PhalconRest\Transformers\Postman\ApiCollectionTransformer;
class RouteBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        $api->get('/', function() use ($api) {

            /** @var \Phalcon\Mvc\View\Simple $view */
            $view = $api->di->get(Services::SimpleView);

            return $view->render('general/index');
        });

        $api->get('/proxy.html', function() use ($api, $config) {

            /** @var \Phalcon\Mvc\View\Simple $view */
            $view = $api->di->get(Services::VIEW);

            $view->setVar('client', $config->APP_URL);
            return $view->render('general/proxy');
        });

        $api->get('/documentation.html', function() use ($api, $config) {

            /** @var \Phalcon\Mvc\View\Simple $view */
            $view = $api->di->get(Services::VIEW);


            $view->title =   $config->application->title ;
            $view->description =  $config->application->description ;
            $view->documentationPath =   $config->API_URL . '/export/documentation.json' ;

            return $view->render('general','documentation' );

        });

        $api->get('/docs',function() use ($api, $config) {

            /** @var \Phalcon\Mvc\View\Simple $view */
            $view = $api->di->get(Services::VIEW);
            $config = $this->di->get(Services::CONFIG);
            $documentation = new Documentation($config->application->title, $config->API_URL);
            $documentation->addManyCollections($this->application->getCollections());
            $documentation->addManyRoutes($this->application->getRouter()->getRoutes());
            return $view->render( 'Docs/docs', [
                'documentation' => $documentation,
                'collections' => $this->application->getCollections(),
            ]);

        }
        );

    }
}