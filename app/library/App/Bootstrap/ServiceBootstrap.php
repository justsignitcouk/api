<?php

namespace App\Bootstrap;

use MyCompose\Components\Translator\Translator;
use Phalcon\Config;
use PhalconRest\Api;
use Phalcon\DiInterface;
use App\BootstrapInterface;
use App\Constants\Services;
use App\Auth\UsernameAccountType;
use App\Fractal\CustomSerializer;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View as View;
use Phalcon\Mvc\View\Simple as SimpleView;
use App\User\Service as UserService;
use App\Auth\Manager as AuthManager;
use Phalcon\Events\Manager as EventsManager;
use League\Fractal\Manager as FractalManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use PhalconApi\Auth\TokenParsers\JWTTokenParser;
use App\Components\Mail;

use Milon\Barcode\DNS2D;
class ServiceBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        /**
         * @description Config - \Phalcon\Config
         */
        $di->setShared(Services::CONFIG, $config);

        /**
         * @description Phalcon - \Phalcon\Db\Adapter\Pdo\Mysql
         */
        $di->set(Services::DB, function () use ($config, $di) {

            $config = $config->get('database')->toArray();
            $adapter = $config['adapter'];
            unset($config['adapter']);
            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            $connection = new $class($config);

            // Assign the eventsManager to the db adapter instance
            $connection->setEventsManager($di->get(Services::EVENTS_MANAGER));

            return $connection;
        });

        $di->set('lang', function () {
            $Translator = new Translator() ;
            return $Translator->_getTranslation();
        });

        $di->set(Services::QR, function () use ($config, $di) {
            $oQR = new DNS2D();
            $oQR->setStorPath(__DIR__."/cache/");
            return $oQR;
        });

        $di->set(Services::PDF, function () use ($config, $di) {
            $mpdfConfig = array(
                'mode' => 'utf-8',
                'format' => 'A4',    // format - A4, for example, default ''
                'default_font_size' => 0,     // font size - default 0
                'default_font' => 'sans-serif',    // default font family
                'margin_left' => 0,    	// 15 margin_left
                'margin_right' => 0,    	// 15 margin right
                // 'mgt' => $headerTopMargin,     // 16 margin top
                // 'mgb' => $footerTopMargin,    	// margin bottom
                'margin_header' => 0,     // 9 margin header
                'margin_footer' => 0,     // 9 margin footer
                'orientation' => 'P'  	// L - landscape, P - portrait
            );
            $mpdf = new \Mpdf\Mpdf($mpdfConfig);

            //'utf-8','letter',14,'',0,0 ,8,8,0,0
            $mpdf->autoArabic = false;
            $mpdf->autoScriptToLang = false;
            $mpdf->autoLangToFont = false;

            return $mpdf;
        });

        $di->set(Services::WORD, function () use ($config, $di) {
            $PhpWord = new \PhpOffice\PhpWord\PhpWord();

            return $PhpWord;
        });

        /**
         * @description Phalcon - \Phalcon\Mvc\Url
         */
        $di->set(Services::URL, function () use ($config) {

            $url = new UrlResolver;
            $url->setBaseUri($config->get('application')->baseUri);
            return $url;
        });

        /**
         * @description Phalcon - \Phalcon\Mvc\View\Simple
         */
        $di->set(Services::VIEW, function () use ($config) {

            $view = new View;
            $view->setRenderLevel(View::LEVEL_ACTION_VIEW);

            $view->setViewsDir($config->get('application')->viewsDir);

            return $view;
        });

        $di->set(Services::SimpleView, function () use ($config) {

            $view = new SimpleView;

            $view->setViewsDir($config->get('application')->viewsDir);

            return $view;
        });

        $di->set('mail', function () {
            return new Mail();
        });


        /**
         * @description Phalcon - EventsManager
         */
        $di->setShared(Services::EVENTS_MANAGER, function () use ($di, $config) {

            return new EventsManager;
        });

        /**
         * @description Phalcon - TokenParsers
         */
        $di->setShared(Services::TOKEN_PARSER, function () use ($di, $config) {

            return new JWTTokenParser($config->get('authentication')->secret, JWTTokenParser::ALGORITHM_HS256);
        });

        /**
         * @description Phalcon - AuthManager
         */
        $di->setShared(Services::AUTH_MANAGER, function () use ($di, $config) {

            $authManager = new AuthManager($config->get('authentication')->expirationTime);
            $authManager->registerAccountType(UsernameAccountType::NAME, new UsernameAccountType);
            $authManager->registerAccountType(UsernameAccountType::MOBILE, new UsernameAccountType);
            $authManager->registerAccountType(UsernameAccountType::ID, new UsernameAccountType);
            return $authManager;
        });

        /**
         * @description Phalcon - \Phalcon\Mvc\Model\Manager
         */
        $di->setShared(Services::MODELS_MANAGER, function () use ($di) {

            $modelsManager = new ModelsManager;
            return $modelsManager->setEventsManager($di->get(Services::EVENTS_MANAGER));
        });

        /**
         * @description PhalconRest - \League\Fractal\Manager
         */
        $di->setShared(Services::FRACTAL_MANAGER, function () {

            $fractal = new FractalManager;
            $fractal->setSerializer(new CustomSerializer);

            return $fractal;
        });

        /**
         * @description PhalconRest - \PhalconRest\User\Service
         */
        $di->setShared(Services::USER_SERVICE, new UserService);




    }
}
