<?php

namespace MyCompose\Tasks;


use MyCompose\Model\AccountRole;

use MyCompose\Model\Contacts\MessagesQueue;

use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Users;


class InviteRecipientsTask extends \Phalcon\Cli\Task
{

    public function sendAction()
    {

        $oRecipients = Recipients::find("user_id is null and invited=0");
        if ($oRecipients->count() > 0) {
            $title = 'We Need You on My Compose';
            $aRecipients = [] ;
            foreach ($oRecipients as $oRecipient) {
                $sIdentifier = $oRecipient->identifier ;
                if(in_array($sIdentifier,$aRecipients)){
                    continue;
                }
                $type = 'sms';
                if (filter_var($sIdentifier, FILTER_VALIDATE_EMAIL)) {
                    $type = 'email';
                }elseif($sIdentifier[0]=='+') {
                    $mobile = $sIdentifier ;
                    $mobile[0] = "" ;
                    if(is_numeric($mobile)){
                        $type = 'sms';
                    }
                }elseif(is_numeric($sIdentifier)){
                    $type = 'sms';
                }else{
                    continue;
                }


                $aRecipients[] = $sIdentifier ;
                $oUser = Users::findFirst("email='" . $sIdentifier . "' or mobile='" . $sIdentifier . "'");
                if ($oUser) {
                    continue;
                }

                 $oMessageQueue = new MessagesQueue();
                 $oMessageQueue->type = $type ;
                 $oMessageQueue->created_at = date('Y-m-d H:i:s') ;
                 $oMessageQueue->notify = 0 ;
                 $oMessageQueue->contact = $sIdentifier ;
                 $oMessageQueue->template = 'invitation' ;
                 $oMessageQueue->recipient_type = 'recipient' ;
                 $oMessageQueue->user_id = null ;
                 $oMessageQueue->document_id = $oRecipient->document_id ;
                 $oMessageQueue->save();
                $oRecipientsInvited = Recipients::find("user_id is null and identifier='" . $sIdentifier . "'");
                foreach($oRecipientsInvited as $oRecipientInvited) {
                    $oRecipientInvited->invited = 1 ;
                    $oRecipientInvited->save();

                }

            }

        }

    }

}