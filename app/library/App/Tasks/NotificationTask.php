<?php

namespace MyCompose\Tasks;

use App\Components\FCM\FCM;
use App\Components\Notifications\Notifications;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Contacts\ContactsRoles;
use MyCompose\Model\Document\DocumentNotification;
use MyCompose\Model\Document\DocumentWorkflow;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\Recipients\Contacts;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Workflow\Stage;
use MyCompose\Model\Workflow\Workflow;

class NotificationTask extends \Phalcon\Cli\Task
{

    public function mainAction()
    {
        //$this->cron->runInBackground();
    }

    public function notifyDocumentAction()
    {
        $oDocuments = Documents::find("notify=0");

        foreach ($oDocuments as $oDocument) {
            $oDocumentWorkflow = DocumentWorkflow::findFirst(["document_id=" . $oDocument->id, 'order' => 'id desc']);

            if (!$oDocumentWorkflow) {
                continue;
            }

            if($oDocumentWorkflow->notify =='1'){
                continue;
            }

            $nWorkFlowID = $oDocumentWorkflow->workflow_id;
            $oWorkflow = Workflow::findFirstById($nWorkFlowID);
            $nCurrentStageID = $oDocumentWorkflow->current_stage_id;

            if (is_null($nCurrentStageID)) {
                if (!$oWorkflow) {
                    continue;
                }
                $nCurrentStageID = $oWorkflow->startStageID();
            }

            $oStage = Stage::findFirstById($nCurrentStageID);
            if (!$oStage) {
                echo $nCurrentStageID;
                exit();
            }

            if ($oStage->action_type == 'END') {
                $this->notifyCompleteDocument($oDocument);
                $oDocument->notify = 1;
                $oDocument->save();
                continue;
            }

            $oStageStates = $oStage->StageState;

            foreach ($oStageStates as $oStageState) {

                $oState = $oStageState->State;
                if ($oState) {
                    $oStateAction = $oState->StateAction;
                    if (!empty($oStateAction)) {
                        $this->notifyStateAction($oDocument, $oStateAction);
                    }
                }

            }

            $oDocumentWorkflow->notify = 1 ;
            $oDocumentWorkflow->save();
        }
    }



    private function notifyCompleteDocument($oDocument)
    {
        $sFormat = 'The document %s that refrence %s is complete';
        $sContent = sprintf($sFormat, $oDocument->title, $oDocument->id);

        $oRecipients = $oDocument->Recipients;
        if (!empty($oRecipients)) {
            foreach ($oRecipients as $oRecipient) {
                if($oRecipient->notify == 0){
                    if(!empty( $oRecipient->User)){
                        Notifications::notifyDocument($oDocument, $oRecipient->User, 1 ,['sContent' => $sContent]);
                    }

                $oRecipient->notify = 1 ;
                $oRecipient->save();
                }
            }
        }





        return true;
    }

    private function notifyStateAction($oDocument, $oStateActions)
    {
        foreach ($oStateActions as $oStateAction) {
            if($oStateAction->Action->WorkflowFunction->name=='Sign'){
                $oRecipients = Recipients::find("document_id=" . $oDocument->id  . " and type='Signer' ") ;
                if($oRecipients->count()>0){
                    $sFormat = 'The document %s that reference %s need action %s';
                    $sContent = sprintf($sFormat, $oDocument->title, $oDocument->id, $oStateAction->Action->name);
                    foreach($oRecipients as $oRecipient) {
                        $oRecipient->can_read = 1 ;
                        $oRecipient->notify = 1 ;
                        if(!$oRecipient->save()){
                            echo "111111111111111";
                            return $oRecipient->getMessages();
                        }
                        if(!empty($oRecipient->User )){
                            Notifications::notifyDocument($oDocument, $oRecipient->User , 2,['sContent' => $sContent]);
                        }

                    }
                }
            }

            $oStateActionPermitsUser = isset($oStateAction->StateActionPermitsUser) ? $oStateAction->StateActionPermitsUser : [];
            $oStateActionPermitsRole = isset($oStateAction->StateActionPermitsRole) ? $oStateAction->StateActionPermitsRole : [];
            $oAction = $oStateAction->Action;
            $sFormat = 'The document %s that reference %s need action %s';
            $sContent = sprintf($sFormat, $oDocument->title, $oDocument->id, $oAction->name);

            if ($oStateActionPermitsUser) {
                foreach ($oStateActionPermitsUser as $oSu) {
                    $oUser = $oSu->User;
                    if(!empty($oRecipient->User )) {
                        Notifications::notifyDocument($oDocument, $oUser, 2, ['sContent' => $sContent]);
                    }

                }
            }

//            if ($oStateActionPermitsRole) {
//                $sFormat = 'The document %s that reference %s need action %s By Role';
//                $sContent = sprintf($sFormat, $oDocument->title, $oDocument->id, $oAction->name);
//                foreach ($oStateActionPermitsRole as $oSRole) {
//                    $oRole = $oSRole->Role;
//                    //Notifications::notifyDocument($oDocument, $oAccount, ['type' => 'required_action', 'sContent' => $sContent]);
//                    $nRoleID = $oRole->id ;
//                    $oUserRoles = ContactsRoles::find("role_id=" . $nRoleID ) ;
//                    if($oContactsRoles->count()==0){
//                        continue;
//                    }
//                    foreach($oContactsRoles as $oContactsRole){
//                        $oContact = $oContactsRole->Contact;
//                        Notifications::notifyDocument($oDocument, $oContact, 3,['sContent' => $sContent]);
//
//                    }
//                }
//            }

        }
    }




}