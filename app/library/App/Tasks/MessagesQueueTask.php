<?php

namespace MyCompose\Tasks;

use App\Components\FCM\FCM;
use App\Components\Mail\Mail;
use App\Components\Notifications\Notifications;
use App\Components\SMS\SMS;
use MyCompose\Model\AccountRole;
use MyCompose\Model\Contacts\Contacts;
use MyCompose\Model\Contacts\ContactUser;
use MyCompose\Model\Contacts\Emails;
use MyCompose\Model\Contacts\MessagesQueue;
use MyCompose\Model\Document\DocumentNotification;
use MyCompose\Model\Document\DocumentWorkflow;
use MyCompose\Model\Document\Documents;
use MyCompose\Model\Document\Recipients\Recipients;
use MyCompose\Model\Users;
use MyCompose\Model\Workflow\Stage;
use MyCompose\Model\Workflow\Workflow;

class MessagesQueueTask extends \Phalcon\Cli\Task
{

    public function sendAction()
    {
        $oMessagesQueues = MessagesQueue::find("notify=0");
        if ($oMessagesQueues->count() > 0) {
            foreach ($oMessagesQueues as $oMessageQueue) {
                $title = 'MyCompose';
                $type = $oMessageQueue->type;
                $contact = $oMessageQueue->contact;
                $template = $oMessageQueue->template;
                $recipient_type = $oMessageQueue->recipient_type;
                $user_id = $oMessageQueue->user_id;
                $document_id = $oMessageQueue->document_id;

                if ($type == 'sms') {


                    if ($template == 'register') {
                        $oUser = Users::findFirstById($user_id);
                        if (!$oUser) {
                            continue;
                        }
                        $oUser->verify_token = rand(100000, 999999);
                        $oUser->save();
                        $oSms = new SMS();
                        $mobile = str_replace(" ", '', $oUser->mobile);
                        $oSms->to = $mobile;
                        $oSms->message = "Welcome to My Compose, Your verification number is " . $oUser->verify_token;
                        $oSms->send();
                    }


                }

                if ($type == 'email') {

                    $mail = new Mail();
                    $aParams = [];
                    if ($template == 'register') {
                        $sEmail = $contact;
                        $oUser = Users::findFirstById($user_id);
                        if (!$oUser) {
                            $oUser = Users::findFirst("email='" . $sEmail . "'");
                            if (!$oUser) {
                                exit('111111 ' . $contact);
                            }
                        }

                        $title = 'MyCompose verification code';


                        $oUser->verify_token = $this->security->hash(rand(100000, 999999));
                        $oUser->save();
                        $aParams['sToken'] = $oUser->verify_token;

                    }

                    if ($template == 'sign_email') {
                        $sEmail = $contact;
                        $oUser = Users::findFirst("email='" . $sEmail . "'");
                        if (!$oUser) {
                            exit('111111 ' . $contact);
                        }

                        $oDocumentRecipients = Recipients::findFirst("user_id=" . $oUser->id . " and type='Signer' ");
                        if (!$oDocumentRecipients) {
                            continue;
                        }
                        $aParams['nDocumentID'] = $oDocumentRecipients->document_id;
                        $title = 'MyCompose : Someone request you to sign a document';
                    }

                    if ($template == 'invitation') {

                        $aParams['nDocumentID'] = $document_id;
                        $title = 'You receive new document on the My Compose system';
                        $oDocument = Documents::findFirst("id=$document_id");
                        if (!$oDocument) {
                            continue;
                        }
                        $sender_id = $oDocument->user_id;

                        $aParams['sSenderFullName'] = $oDocument->Sender->full_name;
                    }


                    $sendmail = $mail->send($contact, $title, $template, $aParams);
                    $sendmail = $mail->send('moad.odat@gmail.com', 'for test : ' . $title, $template, $aParams);
                }


            }
            $oMessageQueue->notify = 1;
            if (!$oMessageQueue->save()) {
                var_dump($oMessageQueue->getMessages());
                exit();
            }
        }
    }


}