<?php namespace MyCompose\LaravelModels\Prices;

use Illuminate\Database\Eloquent\Model;

class PlanFeature extends Model
{

    protected $table = 'prices_plan_features';

    public function Plan(){
        return $this->belongsTo('MyCompose\LaravelModels\Prices\Plan','plan_id','id');
    }

    public function Feature(){
        return $this->belongsTo('MyCompose\LaravelModels\Prices\Feature','feature_id','id');
    }

}