<?php namespace MyCompose\LaravelModels\Prices;

use Illuminate\Database\Eloquent\Model;

class AccountRegistrationPlan extends Model
{

    protected $table = 'account_registration_plan';

    public function Plan(){
        return $this->belongsTo('MyCompose\LaravelModels\Prices\Plan','plan_id','id');
    }

}