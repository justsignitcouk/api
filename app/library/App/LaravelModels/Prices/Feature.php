<?php namespace MyCompose\LaravelModels\Prices;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{

    protected $table = 'prices_features';

    public function PlanFeature(){
        return $this->hasMany('MyCompose\LaravelModels\Prices\PlanFeature','id','feature_id');
    }

}