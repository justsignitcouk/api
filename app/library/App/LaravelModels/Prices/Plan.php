<?php namespace MyCompose\LaravelModels\Prices;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    protected $table = 'prices_plans';

    public function PlanFeature(){
        return $this->hasMany('MyCompose\LaravelModels\Prices\PlanFeature','plan_id','id');
    }

}