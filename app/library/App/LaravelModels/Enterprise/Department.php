<?php
namespace MyCompose\LaravelModels\Enterprise;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $table = 'departments';

    //public $timestamps = true;

    protected $guarded = [];

    public function Branch(){
        return $this->belongsTo("\MyCompose\LaravelModels\Enterprise\Branch", 'branch_id', 'id') ;
    }


}
