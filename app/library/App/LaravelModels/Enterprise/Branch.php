<?php
namespace MyCompose\LaravelModels\Enterprise;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //
    protected $table = 'branches';

    //public $timestamps = true;

    protected $guarded = [];

    public function Departments(){
        return $this->hasMany('MyCompose\LaravelModels\Enterprise\Department', 'branch_id', 'id')->orderBy('id');
    }

    public function Company(){
        return $this->belongsTo("\MyCompose\LaravelModels\Enterprise\Company", 'company_id', 'id') ;
    }

    public function Country(){
        return $this->belongsTo('MyCompose\LaravelModels\Common\Country','country_id','id');

    }


}
