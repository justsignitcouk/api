<?php
namespace MyCompose\LaravelModels\Enterprise;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'companies';

    //public $timestamps = true;

    protected $guarded = [];

    public function MainBranch(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Branch', 'id', 'company_id')->orderBy('is_main','desc');
    }

    public function Logo(){
        return $this->belongsTo('MyCompose\LaravelModels\File', 'logo', 'id')->orderBy('id','desc');
    }

    public function Branches(){
        return $this->hasMany('MyCompose\LaravelModels\Enterprise\Branch', 'company_id', 'id')->orderBy('id');
    }


}
