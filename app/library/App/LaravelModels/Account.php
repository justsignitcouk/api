<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    protected $table = 'accounts';

    public function Department(){
        return $this->belongsTo('MyCompose\LaravelModels\Department','department_id','id');
    }


    public function ProfileImage(){
        return $this->belongsTo('MyCompose\LaravelModels\File','profile_image','id');
    }

    public function User(){
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }

    public function Role(){
        return $this->belongsToMany('MyCompose\LaravelModels\Role','account_role','account_id','role_id' );
    }

    public function AccountRegistrationPlan(){
        return $this->belongsTo('MyCompose\LaravelModels\Prices\AccountRegistrationPlan','id','account_id' )->where('end_date', '>' , date('Y-m-d H:i:s'));
    }
    
}