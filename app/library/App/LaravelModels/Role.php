<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $table = 'roles';


    public function Permissions(){
        return $this->belongsToMany('MyCompose\LaravelModels\Permission','permission_role','role_id','permission_id' );
    }

}