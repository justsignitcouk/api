<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class DocumentLayoutComponents extends Model
{
    protected $table = 'document_layout_components';
}