<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{

    protected $table = 'contacts_emails';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','id','contact_id' );
    }

}