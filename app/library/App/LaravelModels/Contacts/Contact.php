<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'contacts';

    public function Emails(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\Emails','contact_id','id' );
    }

    public function Mobiles(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\Mobiles','contact_id','id' );
    }

    public function Names(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\Names','contact_id','id' );
    }

    public function oPrimaryNames(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Names','id','contact_id' )->where("is_primary","1");
    }

    public function Avatar(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Avatar','id','contact_id' )->where('is_primary','1');
    }

    public function ContactBranches(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\ContactBranch','contact_id','id' );
    }

    public function ContactDepartments(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\ContactDepartment','contact_id','id' );
    }

    public function ContactPermissions(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\ContactPermission','contact_id','id' );
    }

    public function ContactRoles(){
        return $this->hasMany('MyCompose\LaravelModels\Contacts\ContactRole','contact_id','id' );
    }
    
}