<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class ContactDepartment extends Model
{

    protected $table = 'contact_department';

    public function Branch(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Branch','branch_id','id' );
    }

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id' );
    }

    public function Company(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Company','company_id','id' );
    }

    public function Department(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Department','department_id','id' );
    }
    
}