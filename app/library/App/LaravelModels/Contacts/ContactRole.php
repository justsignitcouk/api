<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class ContactRole extends Model
{

    protected $table = 'contacts_roles';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id' );
    }

    public function Role(){
        return $this->belongsTo('MyCompose\LaravelModels\Role','role_id','id' );
    }

}