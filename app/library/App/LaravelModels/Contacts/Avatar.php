<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{

    protected $table = 'contacts_avatar';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','id','contact_id' );
    }

    public function File(){
        return $this->belongsTo('MyCompose\LaravelModels\File','file_id','id' );
    }

}