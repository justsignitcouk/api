<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class ContactPermission extends Model
{

    protected $table = 'contacts_permissions';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id' );
    }

    public function Permission(){
        return $this->belongsTo('MyCompose\LaravelModels\Permission','permission_id','id' );
    }

}