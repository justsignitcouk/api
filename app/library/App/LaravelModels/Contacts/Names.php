<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class Names extends Model
{

    protected $table = 'contacts_names';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','id','contact_id' );
    }

}