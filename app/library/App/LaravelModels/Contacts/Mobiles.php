<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class Mobiles extends Model
{
    protected $table = 'contacts_mobiles';

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contacts','id','contact_id' );
    }

}