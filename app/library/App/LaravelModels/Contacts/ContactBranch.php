<?php namespace MyCompose\LaravelModels\Contacts;

use Illuminate\Database\Eloquent\Model;

class ContactBranch extends Model
{

    protected $table = 'contact_branch';

    public function Branch(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Branch','branch_id','id' );
    }

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id' );
    }

    public function Company(){
        return $this->belongsTo('MyCompose\LaravelModels\Enterprise\Company','company_id','id' );
    }
    
}