<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Invitations extends Model
{

    protected $table = 'invitations';

    /*Define relations*/
    public function User(){
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }
}