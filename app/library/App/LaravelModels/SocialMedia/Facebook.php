<?php

namespace MyCompose\LaravelModels\SocialMedia;

use Illuminate\Database\Eloquent\Model;

class Facebook extends Model
{

    protected $table = 'social_facebook';
    public $timestamps = false;

    public function User(){
        return  $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }



}