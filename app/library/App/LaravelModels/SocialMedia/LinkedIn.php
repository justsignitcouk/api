<?php

namespace MyCompose\LaravelModels\SocialMedia;

use Illuminate\Database\Eloquent\Model;

class LinkedIn extends Model
{

    protected $table = 'social_linkedin';
    public $timestamps = false;

}