<?php

namespace MyCompose\LaravelModels\SocialMedia;

use Illuminate\Database\Eloquent\Model;

class Google extends Model
{

    protected $table = 'social_google';
    public $timestamps = false;

    public function User(){
        return  $this->belongsTo('MyCompose\LaravelModels\User','id','user_id');
    }

}