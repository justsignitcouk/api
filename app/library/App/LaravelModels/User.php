<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = 'users';
    public $timestamps = false;


    /*Define relations*/
    public function Avatar(){
        return $this->belongsTo('MyCompose\LaravelModels\File','avatar','id');
    }

    public function FaceBook(){
        return  $this->belongsTo('MyCompose\LaravelModels\SocialMedia\Facebook','user_id','id');
    }

    public function Google(){
        return  $this->belongsTo('MyCompose\LaravelModels\SocialMedia\Google','user_id','id');
    }

    public function Accounts(){
        return  $this->hasMany('MyCompose\LaravelModels\Account','user_id','id');
    }






}