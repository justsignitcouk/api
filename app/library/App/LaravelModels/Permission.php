<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $table = 'permissions';

}