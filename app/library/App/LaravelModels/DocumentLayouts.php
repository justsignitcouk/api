<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class DocumentLayouts extends Model
{
    protected $table = 'document_layout';

    public function LayoutComponents(){
        return $this->hasMany('MyCompose\LaravelModels\DocumentLayoutComponents','layout_id','id');
    }

}