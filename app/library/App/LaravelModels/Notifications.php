<?php

namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;


class Notifications extends Model
{

    protected $table = 'notifications';


    public function Account(){
        return $this->belongsTo('MyCompose\LaravelModels\Account','sender_id','id');
    }

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','document_id','id');
    }

    public function Signer(){
        return $this->belongsTo('MyCompose\LaravelModels\Account','signer_id','id');
    }




}