<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $table = 'departments';

    public function Branch(){
        return $this->belongsTo('MyCompose\LaravelModels\Branch','branch_id','id');
    }

}