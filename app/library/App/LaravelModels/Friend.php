<?php namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{

    protected $table = 'friends';


    /*Define relations*/
    public function User(){
        return $this->belongsTo('MyCompose\LaravelModels\User','requested_id','id');
    }

    public function Requster(){
        return $this->belongsTo('MyCompose\LaravelModels\User','requester_id','id');
    }

}