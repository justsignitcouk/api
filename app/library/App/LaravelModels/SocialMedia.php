<?php

namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{

    protected $table = 'social_media';

    /*Define relations*/
    public function User(){
        return  $this->belongsTo('MyCompose\LaravelModels\User');
    }



}