<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentSenderAccount extends Model
{

    protected $table = 'document_sender_account';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Account(){
        return $this->belongsTo('MyCompose\LaravelModels\Account','account_id','id');
    }


}