<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentSenderDepartment extends Model
{

    protected $table = 'document_sender_department';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Department(){
        return $this->belongsTo('MyCompose\LaravelModels\Department','department_id','id');
    }

}