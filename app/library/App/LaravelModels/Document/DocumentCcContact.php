<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentCcContact extends Model
{

    protected $table = 'document_cc_contact';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id');
    }

    public function SignData(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id');
    }
    
}