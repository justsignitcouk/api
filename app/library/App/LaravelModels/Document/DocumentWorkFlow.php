<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentWorkFlow extends Model
{

    protected $table = 'wf_document_workflow';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function WorkFlow()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\Workflow','workflow_id','id');
    }

}