<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentSignerDepartment extends Model
{

    protected $table = 'document_signer_department';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Department(){
        return $this->belongsTo('MyCompose\LaravelModels\Department','department_id','id');
    }

}