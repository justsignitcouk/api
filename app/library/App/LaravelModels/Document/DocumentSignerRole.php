<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentSignerRole extends Model
{

    protected $table = 'document_signer_role';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Role(){
        return $this->belongsTo('MyCompose\LaravelModels\Role','role_id','id');
    }

}