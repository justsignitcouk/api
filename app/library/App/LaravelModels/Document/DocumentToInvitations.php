<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentToInvitations extends Model
{

    protected $table = 'document_to_invitations';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Invitations(){
        return $this->belongsTo('MyCompose\LaravelModels\Invitations','invitation_id','id');
    }

}