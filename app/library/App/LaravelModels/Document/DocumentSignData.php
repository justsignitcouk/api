<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentSignData extends Model
{

    protected $table = 'document_sign_data';

}