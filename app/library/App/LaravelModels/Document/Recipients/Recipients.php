<?php
namespace MyCompose\LaravelModels\Document\Recipients;

use Illuminate\Database\Eloquent\Model;

class Recipients extends Model
{

    protected $table = 'document_recipient';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','document_id','id');
    }

    public function User(){
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }

    public function Sign(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\DocumentSignData','sign_data_id','id');
    }

}