<?php
namespace MyCompose\LaravelModels\Document\Recipients;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'document_contact_recipient';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','document_id','id');
    }

    public function Contact(){
        return $this->belongsTo('MyCompose\LaravelModels\Contacts\Contact','contact_id','id');
    }

    public function Sign(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\DocumentSignData','sign_data_id','id');
    }

}