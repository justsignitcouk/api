<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentFiles extends Model
{

    protected $table = 'document_file';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }
    public function File(){
        return $this->belongsTo('MyCompose\LaravelModels\File','file_id','id');
    }


}