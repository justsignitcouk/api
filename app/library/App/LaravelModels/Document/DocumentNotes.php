<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentNotes extends Model
{

    protected $table = 'document_notes';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function User(){
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }


}