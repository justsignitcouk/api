<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class DocumentCcAccount extends Model
{

    protected $table = 'document_cc_account';

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','id','document_id');
    }

    public function Account(){
        return $this->belongsTo('MyCompose\LaravelModels\Account','account_id','id');
    }

    public function Sender(){
        return $this->hasOne('MyCompose\LaravelModels\Document\DocumentSenderAccount','document_id','document_id');
    }

}