<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;
use MyCompose\Model\Document\DocumentUserRead;

class Document extends Model
{

    protected $table = 'documents';



    public function Recipients(){
        return $this->hasMany('MyCompose\LaravelModels\Document\Recipients\Recipients','document_id','id');
    }

    public function DocumentSenderUser(){
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }

    #Workflow
    public function WorkFlow(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\DocumentWorkFlow','id','document_id')->where('active','1');
    }

    #Files
    public function Files(){
        return $this->hasMany('MyCompose\LaravelModels\Document\DocumentFiles','document_id','id');
    }

    #layout
    public function Layout(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Layout','layout_id','id');
    }

    #QrCode
    public function QrCode(){
        return $this->belongsTo('MyCompose\LaravelModels\QrCode','qr_code_id','id');
    }

    public function DocumentNotes(){
        return $this->hasMany('MyCompose\LaravelModels\Document\DocumentNotes','document_id','id');
    }

    public static function getDocumentByID($nDocumentID,$aIn='inbox')
    {
        if(!is_array($aIn)){
            if($aIn=='inbox'){
                $aIn = ['start','inprogress','complete','expired'];
            }

        }

        $di = \Phalcon\Di::getDefault();
        $userService = $di->get('userService');
        $oDocument = \MyCompose\LaravelModels\Document\Document::with(['Recipients.User','Recipients.User.Avatar',
            'DocumentSenderUser.Avatar',
        ])->where('id', $nDocumentID)->whereIn('status', $aIn )->get()->first();
        if($oDocument){
            $getIfRead = DocumentUserRead::findFirst("user_id=" . $userService->getIdentity() . " and document_id=" . $oDocument->id);
            $bIsRead = $getIfRead ? true : false;
            $oDocument['is_read'] = $bIsRead;
        }

        return $oDocument;
    }
}