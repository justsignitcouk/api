<?php
namespace MyCompose\LaravelModels\Document;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{

    protected $table = 'document_layout';

    public function LayoutComponent()
    {
        return $this->hasMany('MyCompose\LaravelModels\Document\LayoutComponent','layout_id','id');
    }

}