<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class StateActionPermitsUser extends Model
{
    protected $table = 'wf_state_action_permits_user';

    public $timestamps = true;

    public function Contact()
    {
        return $this->belongsTo('MyCompose\LaravelModels\User','user_id','id');
    }

}
