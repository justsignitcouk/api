<?php namespace MyCompose\LaravelModels\Workflow;


use Illuminate\Database\Eloquent\Model;


class Workflow extends Model
{
    //
    protected $table = 'wf_workflow';

    //public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'active',
        'created_by',
        'updated_by'
    ];

    protected $guarded = [];

    public function Stage(){
        return $this->hasMany('MyCompose\LaravelModels\Workflow\Stage', 'workflow_id', 'id')->orderBy('sequence');
    }


}
