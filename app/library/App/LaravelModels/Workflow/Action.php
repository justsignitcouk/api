<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = 'wf_action';

    public $timestamps = true;

    private static $actionTypes = [
        'SYSTEM'     => 'SYSTEM',
        'CUSTOM'    => 'CUSTOM',
    ];

    public function WorkflowFunction(){
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\WorkflowFunction', 'function_id', 'id');
    }

    public static function getActionTypes() {
        return self::$actionTypes;
    }

    public static function getSystemAction(){
        return self::where(['company_id' => 1, 'type' => 'SYSTEM'])->get();
    }

}
