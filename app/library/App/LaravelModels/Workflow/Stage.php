<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;
use MyCompose\LaravelModels\Workflow\State;

class Stage extends Model
{
    protected $table = 'wf_stage';

    public $timestamps = true;

    private static $actionTypes = [
        'START'     => 'bg-info',
        'REVIEW'    => 'bg-primary',
        'APPROVE'   => 'bg-purple',
        'END'       => 'bg-success',
        'REJECT'    => 'bg-danger'
    ];

    protected $fillable = [
        'name',
        'action_type',
        'description',
        'due_date_in_hours',
        'previous',
        'active',
        'created_by',
        'updated_by'
    ];

    protected $guarded = [];

    public function Workflow()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\Workflow');
    }

    public function StageState()
    {
        return $this->hasMany('MyCompose\LaravelModels\Workflow\StageState', 'stage_id', 'id');
    }

    public function State(){
        return $this->belongsToMany('MyCompose\LaravelModels\Workflow\State', 'wf_stage_state', 'stage_id', 'state_id');
    }

    public static function getActionTypes() {
        return self::$actionTypes;
    }

}
