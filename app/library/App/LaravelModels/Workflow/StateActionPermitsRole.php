<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class StateActionPermitsRole extends Model
{
    protected $table = 'wf_state_action_permits_role';

    public $timestamps = true;

    public function Role()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Role','role_id','id');
    }


}
