<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class WorkflowFunction extends Model
{
    protected $table = 'wf_function';
    public $timestamps = true;

}
