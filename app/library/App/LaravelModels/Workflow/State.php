<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'wf_state';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'active'
    ];

    public function StateAction(){
        return $this->hasMany('MyCompose\LaravelModels\Workflow\StateAction', 'state_id', 'id');
    }

}
