<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class StageState extends Model
{
    protected $table = 'wf_stage_state';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'next_stage_id'
    ];


    public function Stage()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\Stage');
    }

    public function State()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\State','state_id','id');
    }
}
