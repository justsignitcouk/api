<?php

namespace MyCompose\LaravelModels\Workflow;

use Illuminate\Database\Eloquent\Model;

class StateAction extends Model
{
    protected $table = 'wf_state_action';

    public $timestamps = true;

    public function State()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\State');
    }

    public function Action()
    {
        return $this->belongsTo('MyCompose\LaravelModels\Workflow\Action','action_id','id');
    }

    public function StateActionPermitsRole(){
        return $this->hasMany('MyCompose\LaravelModels\Workflow\StateActionPermitsRole', 'state_action_id', 'id');
    }

    public function Roles(){
        return $this->belongsToMany('MyCompose\LaravelModels\Role', 'wf_state_action_permits_role', 'state_action_id', 'role_id');
    }

    public function StateActionPermitsUser(){
        return $this->hasMany('MyCompose\LaravelModels\Workflow\StateActionPermitsUser', 'state_action_id', 'id');
    }

    public function Users(){
        return $this->belongsToMany('MyCompose\LaravelModels\User', 'wf_state_action_permits_user', 'state_action_id', 'user_id');
    }


}
