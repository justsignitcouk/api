<?php

namespace MyCompose\LaravelModels;

use Illuminate\Database\Eloquent\Model;

class UserDocumentActivity extends Model
{

    protected $table = 'user_document_activities';
    protected $fillable = ['user_id','account_id','type'];

    public function Document(){
        return $this->belongsTo('MyCompose\LaravelModels\Document\Document','document_id','id');
    }

    public static function createDocumentActivity($activity_type,$account_id,$document_id)
    {

        $activity = new UserDocumentActivity ;

        $activity->type        = $activity_type ;
        $activity->account_id  = $account_id ;
        $activity->document_id = $document_id ;

        $activity->save();

    }


}