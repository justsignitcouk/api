<?php

namespace App\Constants;

class DefaultValues
{
    const CATEGORY_ID = 1;
    const WORKFLOW_ID = 1;
    const MOBILE_PHONE_PREFIX = '+';
    const DOCUMENT_STATUS = ['archived','completed','deleted','expired','inprogress','rejected','start'];
}