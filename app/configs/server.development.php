<?php

Dotenv::load(__DIR__ . '/../../');

return [

    'debug' => true,
    'components' => ['mpdf' => __DIR__. '/../../vendor/mpdf/mpdf/mpdf.php'],
    'API_URL' => getenv('API_URL'),
    'APP_URL' => getenv('APP_URL'),
    'CDN_URL' => getenv('CDN_URL'),
    'database' => [
        // Change to your own configuration
        'adapter' => 'Mysql',
        'host' => getenv('DBHOST'),
        'username' => getenv('DBUSER'),
        'password' => getenv('DBPASS'),
        'dbname' => getenv('DB'),
        'charset'   =>'utf8'
    ],
    'cors' => [
        'allowedOrigins' => ['*']
    ],
    'mail' => [
        'fromName' => getenv('APP_NAME'),
        'fromEmail' => 'knowmx.dev@gmail.com',
        'smtp' => [
            'server' => getenv('MAIL_HOST'),
            'port' =>  getenv('MAIL_PORT'),
            'security' => 'ssl',
            'encryption' => 'ssl',
            'username' => getenv('MAIL_USERNAME'),
            'password' => getenv('MAIL_PASSWORD')
        ]
    ],
];
