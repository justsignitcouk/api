<?php

Dotenv::load(__DIR__ . '/../../');

return [

    'application' => [
        'title' => getenv('APP_NAME'),
        'description' => getenv('APP_DESCRIPTION'),
        'baseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
    ],
    'web' => [
        'host' => getenv('APP_URL'),
        'sign_validation_action' => 'documents/validation',
    ],
    'authentication' => [
        'secret' => getenv('APP_KEY'),
        'expirationTime' => 86400 * 7, // One week till token expires
    ]
];
